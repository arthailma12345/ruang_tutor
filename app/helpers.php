<?php

use GuzzleHttp\Client;
// use Session;

function loadfile()
{
    $file = new Client(['verify' => public_path('ssl/cacert.pem')]);
    return $file;
}

function url_api_helpdesk($param = '')
{
    $url = env('API_HELPDESK', 'http://localhost/helpdesk_api/public') . "/$param";
    return $url;
}

function encrypt_($id)
{
    $data = base64_encode(base64_encode(base64_encode(base64_encode(base64_encode(base64_encode($id))))));

    return $data;
}

function decrypt_($id)
{
    $data = base64_decode(base64_decode(base64_decode(base64_decode(base64_decode(base64_decode($id))))));

    return $data;
}

function validateSessionToken($api_key = null)
{
    if ($api_key != null || $api_key != '') {
        $data2 = DB::table('users')->where('remember_token', $api_key)->first();

        if (isset($data) || isset($data2)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function headerToken()
{
    $data_session = Session::get('user_app');
    $query = DB::table('users')
        ->where('users.id', $data_session['users_id'])
        ->get()
        ->first();
    // dd($query->remember_token);
    $token = $query->remember_token;
    $myHeader = array(
        "api-token" => $token,
    );
    // return $data_session;
    return $myHeader;
}

function tgl_indo_full($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
}

function tgl_indo_bulan_tahun($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
}

function next_value($current)
{
    $letter = $current[0];
    $number = (int) substr($current, 1);

    if ($number == 999) {
        $letter++;
        $number = 1;
    } else {
        $number++;
    }

    return $letter . str_pad($number, 3, '0', STR_PAD_LEFT);
}

function next_value_nuber_2_digit($current)
{
    $letter = $current[0];
    $number = (int) substr($current, 1);

    if ($number == 99) {
        $letter++;
        $number = 1;
    } else {
        $number++;
    }

    return $letter . str_pad($number, 2, '0', STR_PAD_LEFT);
}

function next_value_nuber_5_digit($current)
{
    $letter = $current[0];
    $number = (int) substr($current, 1);

    if ($number == 99999) {
        $letter++;
        $number = 1;
    } else {
        $number++;
    }

    return $letter . str_pad($number, 5, '0', STR_PAD_LEFT);
}

function next_value_nuber_6_digit($current)
{
    $letter = $current[0];
    $number = (int) substr($current, 1);

    if ($number == 999999) {
        $letter++;
        $number = 1;
    } else {
        $number++;
    }

    return $letter . str_pad($number, 6, '0', STR_PAD_LEFT);
}

function next_value_tiket($current)
{
    $letter = $current;
    $number = substr($current, 16);

    // $tb_issues_cek_tiket = DB::table('issues')
    //     ->select(DB::raw("issues.*"))
    //     ->where(DB::raw('substr(code, 1, 15)'), '=', $number)
    //     ->get();

    // dd($number);
    // if (count($tb_issues_cek_tiket) > 0) {
    if ($number == 99999) {
        $letter++;
        $number = 1;
    } else {
        $number++;
    }

    // dd($number);
    return substr($letter, 0, 16) . str_pad($number, 5, '0', STR_PAD_LEFT);
    // } else {
    //     return $letter;
    // }
}

function status_issues_id_ke_text($status_id)
{
    if ($status_id == '1') {
        $status = '<h3><span class="badge rounded-pill bg-warning">Open</span></h3>';
    } else if ($status_id == '2') {
        $status = '<h3><span class="badge rounded-pill bg-info">Progress</span></h3>';
    } else if ($status_id == '3') {
        $status = '<h3><span class="badge rounded-pill bg-success">Done</span></h3>';
    } else if ($status_id == '4') {
        $status = '<h3><span class="badge rounded-pill bg-primary">Closed</span></h3>';
    } else if ($status_id == '5') {
        $status = '<h3><span class="badge rounded-pill bg-secondary">Close</span></h3>';
    } else {
        $status = "";
    }

    return $status;
}

function get_status_terakhir_per_issues_id($tiket_issues)
{
    $tb_issues_status = DB::table('issues_status')
        ->select(DB::raw("issues_status.*"))
        ->where(DB::raw('issues_status.tiket_issues'), '=', $tiket_issues)
        ->orderBy('issues_status.created_at', 'DESC')
        ->limit(1)
        ->get()
        ->first();

    if ($tb_issues_status == null) {
        $status = 0;
    } else {
        $status = $tb_issues_status->status;
    }

    return $status;
}

function preprocessing_get_string($string)
{
    // $string = str_replace('<div class="ql-editor" data-gramm="false" contenteditable="true"><p>', "", $string);
    // $string = str_replace('</div><div class="ql-clipboard"', "batasteksbro", $string);
    // // $string = str_replace('<img', "batasteksbro", $string);
    // // $string = str_replace('"></p>', "batasteksbro", $string);
    // $string = str_replace('</p><p>', "batasteksbro", $string);
    // $string = str_replace('</p>', "", $string);
    // // $string = str_replace('<p>', "", $string);
    // $string = str_replace('">', "batasteksbro", $string);


    // $stringex = explode("batasteksbro", $string);
    // $stringarr = [];
    // foreach ($stringex as $text) {
    //     if (!preg_match('/base64/i', $text) && !preg_match('/ql-/i', $text) && !preg_match('/contenteditable/i', $text) && !preg_match('/a>/i', $text)) {
    //         $stringarr[] = $text;
    //     }
    // }
    // $finalstring = "";
    // foreach ($stringarr as $word) {
    //     $finalstring .= $word . "<br>";
    // }
    // // dd($finalstring);
    // $finalstring = str_replace(');', ');">', $finalstring);
    // return $finalstring;

    $string = str_replace('<div class="ql-editor" data-gramm="false" contenteditable="true" data-placeholder="Isikan soal text Kamu disini">', "", $string);
    $string = str_replace('<div class="ql-editor" data-gramm="false" contenteditable="true" data-placeholder="Isikan soal reading Kamu disini">', "", $string);
    
    $string = str_replace('</div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden" style="margin-top: -4179px;"><a class="ql-preview" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>', "", $string);
        return $string;
}

function integer_excel_to_date_ymd($tanggal)
{
    // dd($tanggal);
    $UNIX_DATE = ($tanggal - 25569) * 86400;
    $EXCEL_DATE = 25569 + $UNIX_DATE / 86400;
    $UNIX_DATE = ($EXCEL_DATE - 25569) * 86400;
    return gmdate('Y-m-d', $UNIX_DATE);
}