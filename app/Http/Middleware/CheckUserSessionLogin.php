<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use DB;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\homeSuratPerjanjianController;

class CheckUserSessionLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //https://laracasts.com/discuss/channels/laravel/middleware-to-check-if-session-exists

        // dd($request->session());

        if (!$request->session()->exists('user_app')) {
            // user value cannot be found in session

            // $validateSessionToken = validateSessionToken(Session::get('user_app')['token']);
            // dd($validateSessionToken);

            // if ($validateSessionToken == false) {
            // dd('coba');
            Session::flush();
            return redirect('/');
            // } else {
            // return $next($request);
            // }
        } else {
            $validateSessionToken = validateSessionToken(Session::get('user_app')['token']);

            if ($validateSessionToken) {

                $get_session_users_id = Session::get('user_app')['users_id'];
                $get_users = db::table('users')
                    ->select(DB::raw('users.*'))
                    ->where('users.id', '=', $get_session_users_id)
                    ->get()
                    ->first();

                // dd($get_users->status_tanda_tangan);

                if (Session::get('user_app')['role'] == 'R003') { //R003 = role pegawai

                    if ($get_users->status_tanda_tangan == 2 && ($get_users->tanda_tangan != NULL || $get_users->tanda_tangan != "")) {
                        return $next($request);
                    } else {
                        // dd($request);
                        // dd(redirect('home_surat_perjanjian/index'));
                        // return redirect('/');
                        // return redirect('home_surat_perjanjian/index');
                        // return Redirect::to('home_surat_perjanjian/index');
                        // $url = $request->url();
                        // dd($url);
                        // echo header($url);
                        // echo "<script> window.location.href = " . $url . " </script>";
                        // die;
                        // return $next($request);
                        // dd(Redirect::to('home_surat_perjanjian/index'));
                        return Redirect::to('home_surat_perjanjian/index');
                        // return redirect('/');
                        // return back();
                        // return redirect('http://localhost/ruang_tutor/home_surat_perjanjian/index');
                        // return redirect()->route('home_surat_perjanjian.index');
                        // return redirect()->action([homeSuratPerjanjianController::class, 'index']);
                    }
                } else {
                    return $next($request);
                }
            } else {
                // dd('coba');
                Session::flush();
                return redirect('/');
            }
        }

        // return $next($request);
    }
}
