<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;

class teofltestControllerPeserta extends Controller
{
    // menampilkan view paket teofl test
    public function index()
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            return view('pages.teofl_test.index_peserta');
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    //menampilkan database paket dan dikirim ke view
    public function getDataPaket(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];
        $get_session_users_id = $get_session['users_id'];

        if (validateSessionToken($get_session_token)) {

            $tb_paket = DB::table('tb_roll_akses_teofl_test')
                ->select(DB::raw("
                tb_roll_akses_teofl_test.id_username,
                tb_roll_akses_teofl_test.id_paket,
                tb_paket_teofl_test.id_paket,
                tb_paket_teofl_test.nama_paket"))
                ->join('tb_paket_teofl_test', 'tb_paket_teofl_test.id_paket', '=', 'tb_roll_akses_teofl_test.id_paket')
                ->where('tb_roll_akses_teofl_test.id_username', '=',  $get_session_users_id)
                ->get();

            $datas = [];

            if (count($tb_paket) > 0) {

                foreach ($tb_paket as $value) {
                    $datas[] = array(
                        'id_paket' => $value->id_paket,
                        'nama' => $value->nama_paket
                    );
                }
            } else {
                $datas = array();
            }

            $data = $datas;

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    // menampilkan isi materi paket dan dikirim ke view
    public function materiIndex(Request $request, $id_paket)
    {
        // dd($id_paket);

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $tb_materi = DB::table('tb_paket_teofl_test')
                ->select(DB::raw("
            tb_paket_teofl_test.id_paket,
            tb_paket_teofl_test.nama_paket,
            tb_paket_materi_teofl_test.id_materi,
            tb_paket_materi_teofl_test.id_paket,
            tb_paket_materi_teofl_test.judul_materi,
            tb_paket_materi_teofl_test.video_materi"))
                ->join('tb_paket_materi_teofl_test', 'tb_paket_materi_teofl_test.id_paket', '=', 'tb_paket_teofl_test.id_paket')
                ->where('tb_paket_teofl_test.id_paket', '=',  $id_paket)
                ->get();

            $data['id_paket'] = $id_paket;
            $data['tb_materi'] = $tb_materi;

            return view('pages.teofl_test.lihat_materi_peserta', $data);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    // mengambil database dan di kirim ke view
    public function getDataMateri(Request $request, $id_paket)
    {
        // dd($id_paket);

        // dd($request->all());

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $search_materi = is_null($request->search_materi) ? '' : $request->search_materi;


        if (validateSessionToken($get_session_token)) {

            $tb_materi = DB::table('tb_paket_teofl_test')
                ->select(DB::raw("
            tb_paket_teofl_test.id_paket,
            tb_paket_teofl_test.nama_paket,
            tb_paket_materi_teofl_test.id_materi,
            tb_paket_materi_teofl_test.id_paket,
            tb_paket_materi_teofl_test.judul_materi,
            tb_paket_materi_teofl_test.video_materi"))
                ->join('tb_paket_materi_teofl_test', 'tb_paket_materi_teofl_test.id_paket', '=', 'tb_paket_teofl_test.id_paket')

                ->orWhere(function ($query) use (
                    $id_paket,
                    $search_materi
                ) {
                    $query
                        ->where('tb_paket_teofl_test.id_paket', '=',  $id_paket)
                        ->where('tb_paket_materi_teofl_test.judul_materi', 'ilike', '%' . $search_materi . '%');
                })
                ->get();

            $data_uncal = [];

            foreach ($tb_materi as $urlvideo) {

                // convert get id
                $urlDB =  $urlvideo->video_materi;
                // $explodeyt = explode('/', $urlDB);
                // $dataurl = $explodeyt[4];

                // convert replace embed
                $datareplace =  $urlvideo->video_materi;
                // $convertedURLEmbed = str_replace("embed/", "watch?v=", $datareplace);

                $data_uncal[] = array(
                    "judul_materi" => $urlvideo->judul_materi,
                    "video_materi" => $urlvideo->video_materi,
                    "convertedURLEmbed" => $datareplace,
                    "dataurl" => $urlDB,
                );
            }

            $data['data_uncal'] = $data_uncal;
            $data['id_paket'] = $id_paket;

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }
}
