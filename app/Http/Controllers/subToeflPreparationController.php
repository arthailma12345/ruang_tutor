<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;
use File;
use Illuminate\Support\Facades\Storage;

class subToeflPreparationController extends Controller
{
    public function index($toefl_preparation_id)
    {

        $data['m_sub_toefl_preparation'] = DB::table('m_sub_toefl_preparation')
            ->select(DB::raw("m_sub_toefl_preparation.*"))
            ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $toefl_preparation_id)
            ->get();

        $data['m_toefl_preparation'] = DB::table('m_toefl_preparation')
            ->select(DB::raw("m_toefl_preparation.*"))
            ->where('m_toefl_preparation.id', '=', $toefl_preparation_id)
            ->get()
            ->first();

        // dd($data);


        $data['toefl_preparation_id'] = $toefl_preparation_id;

        // dd($data);

        return view('pages.sub_toefl_preparation.index', $data);
    }

    public function getDataSubToeflPreparation(Request $request, $toefl_preparation_id)
    {

        $limit = is_null($request["length"]) ? 25 : $request["length"];
        $offset = is_null($request["start"]) ? 0 : $request["start"];
        $dirs = array("asc", "desc");
        $draw = $request["draw"];
        $searchs = $request["search.value"];
        $resultData = array();
        $data_arr    = [
            'limit' => $limit,
            'offset' => $offset,
            'searchs' => $searchs,
            'dirs' => $dirs,
        ];

        $tb_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
            ->select(DB::raw("m_sub_toefl_preparation.*, 
            m_pembahasan_materi.url_vdeo_pembahasan_materi as m_pembahasan_materi_url_vdeo_pembahasan_materi,
            m_latihan_materi.url_vdeo_latihan_materi as m_latihan_materi_url_vdeo_latihan_materi, 
            tb_reading_materi.text_reading as tb_reading_materi_text_reading"))
            ->leftjoin('m_pembahasan_materi', 'm_pembahasan_materi.m_sub_toefl_preparation_id', '=', 'm_sub_toefl_preparation.id')
            ->leftjoin('m_latihan_materi', 'm_latihan_materi.m_sub_toefl_preparation_id', '=', 'm_sub_toefl_preparation.id')
            ->leftjoin('tb_reading_materi', 'tb_reading_materi.m_sub_toefl_preparation_id', '=', 'm_sub_toefl_preparation.id')
            ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $toefl_preparation_id)
            ->orderBy('m_sub_toefl_preparation.no_urut', 'ASC');

        $total_data = $tb_m_sub_toefl_preparation->count();

        $m_sub_toefl_preparation = $tb_m_sub_toefl_preparation
            ->limit($limit)
            ->offset($offset)
            ->get();

        $datas = [];

        $no = $offset + 1;

        if (count($m_sub_toefl_preparation) > 0) {

            foreach ($m_sub_toefl_preparation as $value) {
                // $valuedb = $value->url_vdeo_pembahasan_materi;
                // dd($value);

                $button_atas = "<button id='urut_atas' data-id_toeflpreparation_urut_atas='" . $value->id . "'  class='btn btn-info'><i class='dripicons-arrow-thin-up'></i></button>";
                $button_bawah = "<button id='urut_bawah' data-id_toeflpreparation_urut_bawah='" . $value->id . "' class='btn btn-warning'><i class='dripicons-arrow-thin-down'></i></button>";

                // dd($m_sub_toefl_preparation->first() == $value);
                // if ($loop->first){
                //     dd();
                // }
                $button_gabungan = '';
                if ($m_sub_toefl_preparation->first() == $value) {
                    $button_gabungan = $button_bawah;
                } else if ($m_sub_toefl_preparation->last() == $value) {
                    $button_gabungan = $button_atas;
                } else {
                    $button_gabungan = $button_atas . $button_bawah;
                }

                $status_deskrispi = '';

                if ($value->deskripsi == null) {
                    $status_deskrispi = 'belum ada';
                } else {
                    $status_deskrispi = 'sudah ada';
                }

                $datas[] = array(

                    'no' => "<div class='row'>" . $button_gabungan . "</div>",
                    'deskripsi' => $status_deskrispi,
                    'nama_sub_toefl_preparation' => $value->nama_sub_toefl_preparation,
                    'aksi' =>
                    '<button type="button" name="edit" id="edit" 
                    class="btn btn-warning waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormEditSubToeflPreparation"
                    data-nama_sub_toefl_preparation="' . $value->nama_sub_toefl_preparation . '" 
                    data-sub_toefl_preparation_id="' . $value->id . '"><i class="dripicons-pencil"></i> Edit </button> &nbsp;' .

                        '<button type="button" name="delete" id="delete" data-sub_toefl_preparation_id="' . $value->id . '" class="btn btn-danger btn-xs" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button> &nbsp;' .

                        '<button type="button" name="tambah_deskripsi_sub_toefl_preparation" id="tambah_deskripsi_sub_toefl_preparation" data-sub_toefl_preparation_id="' . $value->id . '" class="btn btn-secondary btn-xs" href=' . '' . '><i class="dripicons-clipboard"></i> Tambah Deskripsi </button> &nbsp;' .

                        "<button type='button' name='latihan' id='latihan' class='btn btn-info waves-effect waves-light' data-bs-toggle='modal' data-bs-target='#modalFormLatihan'
                        data-m_sub_toefl_preparation_id_latihan='" . $value->id . "' 
                        data-m_pembahasan_materi_url_vdeo_pembahasan_materi='" . $value->m_pembahasan_materi_url_vdeo_pembahasan_materi . "' 
                        data-m_latihan_materi_url_vdeo_latihan_materi='" . $value->m_latihan_materi_url_vdeo_latihan_materi . "'
                        <i class='dripicons-blog'></i> Latihan </button> &nbsp;" .

                        "<button type='button' name='upload_dokumen' id='upload_dokumen' class='btn btn-success waves-effect waves-light' data-bs-toggle='modal' data-bs-target='#modalFormUploadDokumen'
                        data-m_sub_toefl_preparation_id_latihan='" . $value->id . "' 
                        data-m_pembahasan_materi_url_vdeo_pembahasan_materi='" . $value->m_pembahasan_materi_url_vdeo_pembahasan_materi . "' 
                        data-m_upload_dokumen_materi_url_vdeo_upload_dokumen_materi='" . $value->m_latihan_materi_url_vdeo_latihan_materi . "'
                        <i class='dripicons-blog'></i> Upload Dokumen </button>"
                );
            }
        } else {
            $datas = array();
        }

        // dd($datas);
        $recordsTotal = is_null($total_data) ? 0 : $total_data;
        $recordsFiltered = is_null($total_data) ? 0 : $total_data;
        $data = $datas;

        return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
    }

    public function tambah(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $m_toefl_preparation_id = $request->m_toefl_preparation_id;
        $nama_sub_toefl_preparation = $request->nama_sub_toefl_preparation;

        if (validateSessionToken($get_session_token)) {

            $tb_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                ->select(DB::raw("m_sub_toefl_preparation.*"))
                ->get();

            $tb_m_sub_toefl_preparation_count = count($tb_m_sub_toefl_preparation);

            // dd($tb_m_sub_toefl_preparation_per_m_toefl_preparation_terakhir_plus_satu);

            if ($tb_m_sub_toefl_preparation_count == 0) {
                $tambah_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                    ->insert([
                        'id' => 'S000001',
                        'm_toefl_preparation_id' => $m_toefl_preparation_id,
                        'nama_sub_toefl_preparation' => $nama_sub_toefl_preparation,
                        'no_urut' => 1,
                        'created_at' => Carbon::now()
                    ]);
                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            } else {

                $tb_m_sub_toefl_preparation_perm_toefl_preparation = DB::table('m_sub_toefl_preparation')
                    ->select(DB::raw("m_sub_toefl_preparation.*"))
                    ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $m_toefl_preparation_id)
                    ->limit(1)
                    ->orderBy('m_sub_toefl_preparation.no_urut', "DESC")
                    ->get();

                if (count($tb_m_sub_toefl_preparation_perm_toefl_preparation) == 0) {
                    $tb_m_sub_toefl_preparation_per_m_toefl_preparation_terakhir_plus_satu = 1;
                } else {
                    $tb_m_sub_toefl_preparation_per_m_toefl_preparation_terakhir_plus_satu = $tb_m_sub_toefl_preparation_perm_toefl_preparation->first()->no_urut + 1;
                }



                $tb_m_sub_toefl_preparation_get_id_terakhir = DB::table('m_sub_toefl_preparation')
                    ->select(DB::raw("m_sub_toefl_preparation.*"))
                    ->orderby('m_sub_toefl_preparation.id', 'DESC')
                    // ->limit(1)
                    ->get()
                    ->first();

                $tambah_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                    ->insert([
                        'id' => next_value_nuber_6_digit($tb_m_sub_toefl_preparation_get_id_terakhir->id),
                        'm_toefl_preparation_id' => $m_toefl_preparation_id,
                        'nama_sub_toefl_preparation' => $nama_sub_toefl_preparation,
                        'no_urut' => $tb_m_sub_toefl_preparation_per_m_toefl_preparation_terakhir_plus_satu,
                        'created_at' => Carbon::now()
                    ]);
                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            }
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        };


        // dd($response_server);
        // dd();

    }

    public function update(Request $request)
    {

        // $token = headerToken()['api-token'];
        // // $data['data'] = $request->all();
        // // $data['token'] = $token;
        // // dd($request->all());
        // $users_id = Session::get('user_app')['users_id'];
        // $request->request->add(['token' => $token, 'token' => $token, 'users_id' => $users_id]);
        // $data = $request->all();

        // // dd('coba');
        // // dd($request->all());

        // $client = loadfile();

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $m_sub_toefl_preparation_id_edit = $request->m_sub_toefl_preparation_id_edit;
        $nama_sub_toefl_preparation_edit = $request->nama_sub_toefl_preparation_edit;
        // dd($request->all());

        if (validateSessionToken($get_session_token)) {
            $update_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                ->where('m_sub_toefl_preparation.id', '=', $m_sub_toefl_preparation_id_edit)
                ->update([
                    'nama_sub_toefl_preparation' => $nama_sub_toefl_preparation_edit
                ]);
            return response()->json(['success' => 'Data Berhasil Diupdate', 'kode' => 201]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        };



        // dd($response_server);
        // dd();

    }

    public function delete(Request $request, $sub_toefl_preparation_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            // dd($sub_toefl_preparation_id);

            $select_m_toefl_preparation_id = DB::table('m_sub_toefl_preparation')
                ->select(DB::raw("m_sub_toefl_preparation.m_toefl_preparation_id, m_sub_toefl_preparation.no_urut"))
                ->where('m_sub_toefl_preparation.id', '=', $sub_toefl_preparation_id)
                ->get();

            $no_urut = $select_m_toefl_preparation_id->first()->no_urut;

            $select_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                ->select(DB::raw("m_sub_toefl_preparation.*"))
                ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $select_m_toefl_preparation_id->first()->m_toefl_preparation_id)
                ->where('m_sub_toefl_preparation.no_urut', '>', $no_urut)
                ->get();

            $m_toefl_preparation_id = $select_m_toefl_preparation_id->first()->m_toefl_preparation_id;


            foreach ($select_m_sub_toefl_preparation as $data2) {

                $update_m_sub_toefl_preparation_no_urut = DB::table('m_sub_toefl_preparation')
                    ->where('m_sub_toefl_preparation.id', '=', $data2->id)
                    ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $m_toefl_preparation_id)
                    ->update([
                        'no_urut' => $data2->no_urut - 1
                    ]);
            }

            $delete_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                ->where('m_sub_toefl_preparation.id', '=', $sub_toefl_preparation_id)
                ->delete();

            $delete_m_pembahasan_materi = DB::table('m_pembahasan_materi')
                ->where('m_pembahasan_materi.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
                ->delete();

            $delete_m_latihan_materi = DB::table('m_latihan_materi')
                ->where('m_latihan_materi.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
                ->delete();

            $delete_tb_reading_materi = DB::table('tb_reading_materi')
                ->where('tb_reading_materi.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
                ->delete();

            $select_m_latihan_soal = DB::table('m_latihan_soal')
                ->select(DB::raw("m_latihan_soal.*"))
                ->where('m_latihan_soal.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
                ->get();

            foreach ($select_m_latihan_soal as $valuedbsoal) {
                $id_sub_soal = $valuedbsoal->id;

                $delete_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                    ->where('m_latihan_jawaban.m_latihan_soal_id', '=', $id_sub_soal)
                    ->delete();

                $delete_m_latihan_soal = DB::table('m_latihan_soal')
                    ->where('m_latihan_soal.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
                    ->delete();
            }

            // $select_m_sub_toefl_preparation_order_by_no_urut = DB::table('m_sub_toefl_preparation')
            //     ->select(DB::raw("m_sub_toefl_preparation.*"))
            //     ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $m_toefl_preparation_id)
            //     ->orderBy('m_sub_toefl_preparation.no_urut', 'ASC')
            //     ->get();

            // $no = 1;
            // foreach ($select_m_sub_toefl_preparation_order_by_no_urut as $data2) {

            //     $update_m_sub_toefl_preparation_no_urut = DB::table('m_sub_toefl_preparation')
            //         ->where('m_sub_toefl_preparation.id', '=', $data2->id)
            //         ->update([
            //             'no_urut' => $no++
            //         ]);
            // }

            return response()->json(['success' => 'Data Berhasil Didelete', 'kode' => 201]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function urut_atas(Request $request)
    {
        $id_toeflpreparation = $request->id_toeflpreparation_urut_atas;
        $select_m_sub_toefl_preparation_no_urut = DB::table('m_sub_toefl_preparation')
            ->select(DB::raw("m_sub_toefl_preparation.no_urut, m_sub_toefl_preparation.m_toefl_preparation_id"))
            ->where('m_sub_toefl_preparation.id', '=', $id_toeflpreparation)
            ->get();
        $m_toefl_preparation_id = $select_m_sub_toefl_preparation_no_urut->first()->m_toefl_preparation_id;
        $no_urut_asli = $select_m_sub_toefl_preparation_no_urut->first()->no_urut;

        $no_urut_asli_min_1 = $no_urut_asli - 1;

        $select_m_sub_toefl_preparation_no_urut_atasnya_id_toeflpreparation = DB::table('m_sub_toefl_preparation')
            ->select(DB::raw("m_sub_toefl_preparation.id"))
            ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $m_toefl_preparation_id)
            ->where('m_sub_toefl_preparation.no_urut', '=', $no_urut_asli_min_1)
            ->get();

        $id_toeflpreparation_setelahnya = $select_m_sub_toefl_preparation_no_urut_atasnya_id_toeflpreparation->first()->id;

        $update_m_sub_toefl_preparation_21 = DB::table('m_sub_toefl_preparation')
            ->where('m_sub_toefl_preparation.id', '=', $id_toeflpreparation)
            ->update([
                'no_urut' => $no_urut_asli_min_1
            ]);

        $update_m_sub_toefl_preparation_no_urut_12 = DB::table('m_sub_toefl_preparation')
            ->where('m_sub_toefl_preparation.id', '=', $id_toeflpreparation_setelahnya)
            ->update([
                'no_urut' => $no_urut_asli
            ]);

        return response()->json(['success' => 'Nomer urut telah diupdate', 'kode' => 201]);
    }
    public function urut_bawah(Request $request)
    {
        $id_toeflpreparation = $request->id_toeflpreparation_urut_bawah;
        $select_m_sub_toefl_preparation_no_urut = DB::table('m_sub_toefl_preparation')
            ->select(DB::raw("m_sub_toefl_preparation.no_urut, m_sub_toefl_preparation.m_toefl_preparation_id"))
            ->where('m_sub_toefl_preparation.id', '=', $id_toeflpreparation)
            ->get();
        $m_toefl_preparation_id = $select_m_sub_toefl_preparation_no_urut->first()->m_toefl_preparation_id;
        $no_urut_asli = $select_m_sub_toefl_preparation_no_urut->first()->no_urut;
        $no_urut_asli_plus_1 = $no_urut_asli + 1;

        $select_m_sub_toefl_preparation_no_urut_bawahnya_id_toeflpreparation = DB::table('m_sub_toefl_preparation')
            ->select(DB::raw("m_sub_toefl_preparation.id"))
            ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $m_toefl_preparation_id)
            ->where('m_sub_toefl_preparation.no_urut', '=', $no_urut_asli_plus_1)
            ->get();

        $id_toeflpreparation_sebelumnya = $select_m_sub_toefl_preparation_no_urut_bawahnya_id_toeflpreparation->first()->id;

        $update_m_sub_toefl_preparation_21 = DB::table('m_sub_toefl_preparation')
            ->where('m_sub_toefl_preparation.id', '=', $id_toeflpreparation)
            ->update([
                'no_urut' => $no_urut_asli_plus_1
            ]);

        $update_m_sub_toefl_preparation_no_urut_12 = DB::table('m_sub_toefl_preparation')
            ->where('m_sub_toefl_preparation.id', '=', $id_toeflpreparation_sebelumnya)
            ->update([
                'no_urut' => $no_urut_asli
            ]);

        return response()->json(['success' => 'Nomer urut telah diupdate', 'kode' => 201]);
    }

    public function tambahDokumenToeflPreparation(Request $request)
    {
        // dd($request->all());
        // dd($request->file('dokumen_toefl_preparation'));

        $dokumen_toefl_preparation = $request->file('dokumen_toefl_preparation');

        $id_sub_toefl_preparation_dokumen = $request->id_sub_toefl_preparation_dokumen;
        $deskripsi_toefl_preparation = $request->deskripsi_toefl_preparation;

        $filename_dengan_extension = $dokumen_toefl_preparation->getClientOriginalName();
        $filename_tanpa_extension = pathinfo($filename_dengan_extension, PATHINFO_FILENAME);

        $file_path = $filename_tanpa_extension . '' . rand(1000, 9999) . '.' . $dokumen_toefl_preparation->getClientOriginalExtension();

        $destinationPath = public_path() . '/file_dokumen_tp/';
        $dokumen_toefl_preparation->move($destinationPath, $file_path);

        $tambah_m_sub_toefl_preparation_dokumen = DB::table('m_sub_toefl_preparation_dokumen')
            ->insert([
                'id_m_sub_toefl_preparation' => $id_sub_toefl_preparation_dokumen,
                'dokumen_file' => $file_path,
                'file_extension' => $dokumen_toefl_preparation->getClientOriginalExtension(),
                'created_at' => Carbon::now()
            ]);

        return response()->json(['success' => 'Data berhasil disimpan', 'kode' => 201]);
    }

    public function getDokumenToeflPreparation(Request $request, $m_sub_toefl_preparation_id_latihan)
    {

        $limit = is_null($request["length"]) ? 25 : $request["length"];
        $offset = is_null($request["start"]) ? 0 : $request["start"];
        $dirs = array("asc", "desc");
        $draw = $request["draw"];
        $searchs = $request["search.value"];
        $resultData = array();
        $data_arr    = [
            'limit' => $limit,
            'offset' => $offset,
            'searchs' => $searchs,
            'dirs' => $dirs,
        ];

        $tb_m_sub_toefl_preparation_dokumen = DB::table('m_sub_toefl_preparation_dokumen')
            ->select(DB::raw("m_sub_toefl_preparation_dokumen.*"))
            ->where('m_sub_toefl_preparation_dokumen.id_m_sub_toefl_preparation', '=', $m_sub_toefl_preparation_id_latihan);

        $total_data = $tb_m_sub_toefl_preparation_dokumen->count();

        $m_sub_toefl_preparation_dokumen = $tb_m_sub_toefl_preparation_dokumen
            ->limit($limit)
            ->offset($offset)
            ->get();

        $datas = [];

        $no = $offset + 1;

        if (count($m_sub_toefl_preparation_dokumen) > 0) {

            foreach ($m_sub_toefl_preparation_dokumen as $value) {

                $datas[] = array(
                    'no' => $no++,
                    'dokumen_file' => $value->dokumen_file,
                    'aksi' =>  '<button type="button" name="delete_dokumen" id="delete_dokumen" data-sub_toefl_preparation_id_dokuemn="' . $value->id . '" class="btn btn-danger btn-xs" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button>'
                );
            }
        } else {
            $datas = array();
        }

        // dd($datas);
        $recordsTotal = is_null($total_data) ? 0 : $total_data;
        $recordsFiltered = is_null($total_data) ? 0 : $total_data;
        $data = $datas;

        return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
    }

    public function delete_dokumen($id_m_sub_toefl_preparation_dokumen)
    {
        // dd($id_m_sub_toefl_preparation_dokumen);
        $tb_m_sub_toefl_preparation_dokumen = DB::table('m_sub_toefl_preparation_dokumen')
            ->select(DB::raw("m_sub_toefl_preparation_dokumen.*"))
            ->where('m_sub_toefl_preparation_dokumen.id', '=', $id_m_sub_toefl_preparation_dokumen)
            ->get();

        $file_delete = public_path() . '/file_dokumen_tp/' . $tb_m_sub_toefl_preparation_dokumen->first()->dokumen_file;
        File::delete($file_delete);

        $tb_saldo = DB::table('m_sub_toefl_preparation_dokumen')
            ->where('m_sub_toefl_preparation_dokumen.id', '=', $id_m_sub_toefl_preparation_dokumen)
            ->delete();

        return response()->json(['success' => 'Data berhasil disimpan', 'kode' => 201]);
    }

    public function getDataDeskripsi(Request $request, $m_sub_toefl_preperation_id)
    {
        $get_data_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
            ->select(DB::raw("m_sub_toefl_preparation.*"))
            ->where('m_sub_toefl_preparation.id', '=', $m_sub_toefl_preperation_id)
            ->get()
            ->first();

        // dd($get_data_kunci_jawaban);
        return response()->json([
            'kode' => 201,
            'data' => $get_data_m_sub_toefl_preparation
        ]);
    }

    public function tambahDeskripsi(Request $request)
    {
        // dd($request->sub_toefl_preparation_id);

        $sub_toefl_preparation_id = $request->sub_toefl_preparation_id;
        $tambah_deskripsi_sub_toefl_preparation_quill = $request->tambah_deskripsi_sub_toefl_preparation_quill;

        if ($tambah_deskripsi_sub_toefl_preparation_quill == '<div class="ql-editor ql-blank" data-gramm="false" contenteditable="true"><p><br></p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>') {
            $tambah_deskripsi_sub_toefl_preparation_quill = null;
        } else {
            $tambah_deskripsi_sub_toefl_preparation_quill = $tambah_deskripsi_sub_toefl_preparation_quill;
        }

        $update_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
            ->where('m_sub_toefl_preparation.id', '=', $sub_toefl_preparation_id)
            ->update([
                'deskripsi' => $tambah_deskripsi_sub_toefl_preparation_quill,
            ]);

        // dd($get_data_kunci_jawaban);
        return response()->json([
            'kode' => 201,
            'success' => 'data berhasil ditambah'
        ]);
    }
}
