<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;
use SebastianBergmann\Environment\Console;
use File;

class toeflPreparationController extends Controller
{
    public function index()
    {

        $data['m_toefl_preparation'] = DB::table('m_toefl_preparation')
            ->select(DB::raw("m_toefl_preparation.*"))
            ->get();

        return view('pages.toefl_preparation.index', $data);
    }

    public function getDataToeflPreparation(Request $request)
    {

        $limit = is_null($request["length"]) ? 25 : $request["length"];
        $offset = is_null($request["start"]) ? 0 : $request["start"];
        $dirs = array("asc", "desc");
        $draw = $request["draw"];
        $searchs = $request["search.value"];
        $resultData = array();
        $data_arr    = [
            'limit' => $limit,
            'offset' => $offset,
            'searchs' => $searchs,
            'dirs' => $dirs,
        ];


        // dd($limit);

        // $tb_users = DB::table('users')
        //     ->select(DB::raw("users.*"));
        // $token = headerToken()['api-token'];
        // $client = new Client(['verify' => public_path('ssl/cacert.pem')]);
        // $myBody = array(
        //     "api-token" => $token,
        // );
        // $url = url_api_helpdesk('menu/get');
        // $response = $client->get($url, ['headers' => $myBody, 'query' => $data_arr]);
        // $response_server = \GuzzleHttp\json_decode($response->getBody(), true);
        // $data_users = $response_server['result']['message'];
        // $data_users_count = count($data_users);
        // dd($data_users);
        // dd($response_server);

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $tb_m_toefl_preparation = DB::table('m_toefl_preparation')
                ->select(DB::raw("m_toefl_preparation.*"))
                ->orderBy('m_toefl_preparation.created_at', 'DESC');

            // $datax['total_data'] = $tb_users->first();
            $total_data = $tb_m_toefl_preparation->count();


            $m_toefl_preparation = $tb_m_toefl_preparation
                ->limit($limit)
                ->offset($offset)
                ->get();


            // dd($response_server['result']['message']);

            // $tb_users_count = $tb_users->count();
            // $tb_users = $tb_users->limit($limit);
            // $tb_users = $tb_users->offset($offset);
            // $tb_users = $tb_users->get();


            $datas = [];

            $no = $offset + 1;

            if (count($m_toefl_preparation) > 0) {

                foreach ($m_toefl_preparation as $value) {
                    // dd($value['nama']);

                    $datas[] = array(

                        'no' => $no++,
                        'idpaket_toefl_preparation' => $value->id,
                        'nama_toefl_preparation' => $value->nama_toefl_preparation,
                        'aksi' =>
                        '<button type="button" name="edit" id="edit" 
                        class="btn btn-warning waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormEditToeflPreparation"
                        data-nama_toefl_preparation="' . $value->nama_toefl_preparation . '" 
                        data-toefl_preparation_id="' . $value->id . '">
                        <i class="dripicons-pencil"></i> Edit </button> &nbsp;' .
                            '<button type="button" name="delete" id="delete" data-toefl_preparation_id="' . $value->id . '" class="btn btn-danger btn-xs" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button> &nbsp;' .
                            '<button type="button" name="duplikat" id="duplikat" data-toefl_preparation_id="' . $value->id . '" class="btn btn-info btn-xs" href=' . '' . '><i class="dripicons-copy"></i> Duplikat </button> &nbsp;' .
                            "<a type='button' class='btn btn-secondary btn-xs' href='" . url("sub_toefl_preparation/index/$value->id") . "'><i class='dripicons-document'></i> Sub MT Exclusive</a>"
                    );
                }
            } else {
                $datas = array();
            }

            // dd($datas);
            $recordsTotal = is_null($total_data) ? 0 : $total_data;
            $recordsFiltered = is_null($total_data) ? 0 : $total_data;
            $data = $datas;

            return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function tambah(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $nama_toefl_preparation = $request->nama_toefl_preparation;

        if (validateSessionToken($get_session_token)) {

            $tb_m_toefl_preparation = DB::table('m_toefl_preparation')
                ->select(DB::raw("m_toefl_preparation.*"))
                ->get();

            $tb_m_toefl_preparation_count = count($tb_m_toefl_preparation);

            if ($tb_m_toefl_preparation_count == 0) {

                $tambah_toefl_preparation = DB::table('m_toefl_preparation')
                    ->insert([
                        'id' => 'T000001',
                        'nama_toefl_preparation' => $nama_toefl_preparation,
                        'created_at' => Carbon::now()
                    ]);
                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            } else {

                $tb_m_toefl_preparation_get_id_terakhir = DB::table('m_toefl_preparation')
                    ->select(DB::raw("m_toefl_preparation.*"))
                    ->orderby('m_toefl_preparation.id', 'DESC')
                    // ->limit(1)
                    ->get()
                    ->first();

                $tambah_toefl_preparation = DB::table('m_toefl_preparation')
                    ->insert([
                        'id' => next_value_nuber_6_digit($tb_m_toefl_preparation_get_id_terakhir->id),
                        'nama_toefl_preparation' => $nama_toefl_preparation,
                        'created_at' => Carbon::now()
                    ]);
                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            }



            return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function update(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $toefl_preparation_id_edit = $request->toefl_preparation_id_edit;
        $nama_toefl_preparation_edit = $request->nama_toefl_preparation_edit;

        if (validateSessionToken($get_session_token)) {

            $update_m_toefl_preparation = DB::table('m_toefl_preparation')
                ->where('m_toefl_preparation.id', '=', $toefl_preparation_id_edit)
                ->update([
                    'nama_toefl_preparation' => $nama_toefl_preparation_edit,
                ]);

            return response()->json(['success' => 'Data Berhasil Diupdate', 'kode' => 201]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function delete(Request $request, $id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

        $delete_m_toefl_preparation = DB::table('m_toefl_preparation')
        ->where('m_toefl_preparation.id', '=', $id)
        ->delete();

        $select_m_toefl_preparation = DB::table('m_sub_toefl_preparation')
        ->select(DB::raw("m_sub_toefl_preparation.*"))
        ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $id)
        ->get();

        $id_sub = "";

        foreach ($select_m_toefl_preparation as $valuedb) {
            $id_sub = $valuedb->id;
        $delete_m_pembahasan_materi = DB::table('m_pembahasan_materi')
         ->where('m_pembahasan_materi.m_sub_toefl_preparation_id', '=', $id_sub)
         ->delete();

         $delete_m_latihan_materi = DB::table('m_latihan_materi')
         ->where('m_latihan_materi.m_sub_toefl_preparation_id', '=', $id_sub)
         ->delete();

         $delete_tb_reading_materi = DB::table('tb_reading_materi')
         ->where('tb_reading_materi.m_sub_toefl_preparation_id', '=', $id_sub)
         ->delete();

         $select_m_latihan_soal = DB::table('m_sub_toefl_preparation_dokumen')
        ->select(DB::raw("m_sub_toefl_preparation_dokumen.*"))
        ->where('m_sub_toefl_preparation_dokumen.id_m_sub_toefl_preparation', '=', $id_sub)
        ->get();

        foreach($select_m_latihan_soal as $datas){
            $file_delete = public_path() . '/file_dokumen_tp/' . $datas->dokumen_file;
            File::delete($file_delete);
        }

         $delete_m_sub_toefl_preparation_dokumen = DB::table('m_sub_toefl_preparation_dokumen')
         ->where('m_sub_toefl_preparation_dokumen.id_m_sub_toefl_preparation', '=', $id_sub)
         ->delete();

         $delete_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
        ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $id)
        ->delete();

        $select_m_latihan_soal = DB::table('m_latihan_soal')
        ->select(DB::raw("m_latihan_soal.*"))
        ->where('m_latihan_soal.m_sub_toefl_preparation_id', '=', $id_sub)
        ->get();

         foreach ($select_m_latihan_soal as $valuedbsoal) {
            $id_sub_soal = $valuedbsoal->id;

        $delete_m_latihan_jawaban = DB::table('m_latihan_jawaban')
         ->where('m_latihan_jawaban.m_latihan_soal_id', '=', $id_sub_soal)
         ->delete();

         $delete_m_latihan_soal = DB::table('m_latihan_soal')
         ->where('m_latihan_soal.m_sub_toefl_preparation_id', '=', $id_sub)
         ->delete();
         }
        }

            return response()->json(['success' => 'Data Berhasil Didelete', 'kode' => 201]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function urut_atas(Request $request){
        dd("atas terpencet");
    }
    public function urut_bawah(Request $request){
        dd("bawah terpencet");
    }

    public function duplikatDataToeflPreparation(Request $request){
        // dd($request->all());
        $toefl_preparation_id = $request->toefl_preparation_id;

        try {

            // ================ tambah data di table m_toefl_preparation ================

            $get_m_toefl_preparation = DB::table('m_toefl_preparation')
            ->select(DB::raw("m_toefl_preparation.*"))
            ->where('m_toefl_preparation.id', '=', $toefl_preparation_id)
            ->get()
            ->first();

            $tb_m_toefl_preparation_get_id_terakhir = DB::table('m_toefl_preparation')
                ->select(DB::raw("m_toefl_preparation.*"))
                ->orderby('m_toefl_preparation.id', 'DESC')
                ->get()
                ->first();

            $tambah_m_toefl_preparation = DB::table('m_toefl_preparation')
                ->insert([
                    'id' => next_value_nuber_6_digit($tb_m_toefl_preparation_get_id_terakhir->id),
                    'nama_toefl_preparation' => $get_m_toefl_preparation->nama_toefl_preparation.'_copy',
                    'created_at' => Carbon::now()
                ]);

            // dd($tambah_m_toefl_preparation);

            // ================ tutup tambah data di table m_toefl_preparation ================

            // ================ tambah data di table m_sub_toefl_preparation ================
            
            $get_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                ->select(DB::raw("m_sub_toefl_preparation.*"))
                ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $toefl_preparation_id)
                ->get();

            foreach($get_m_sub_toefl_preparation as $datas){

                $tb_m_sub_toefl_preparation_get_id_terakhir = DB::table('m_sub_toefl_preparation')
                    ->select(DB::raw("m_sub_toefl_preparation.*"))
                    ->orderby('m_sub_toefl_preparation.id', 'DESC')
                    ->get()
                    ->first();

                $tambah_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                ->insert([
                    'id' => next_value_nuber_6_digit($tb_m_sub_toefl_preparation_get_id_terakhir->id),
                    'm_toefl_preparation_id' => next_value_nuber_6_digit($tb_m_toefl_preparation_get_id_terakhir->id),
                    'nama_sub_toefl_preparation' => $datas->nama_sub_toefl_preparation,
                    'no_urut' => $datas->no_urut,
                    'deskripsi' => $datas->deskripsi,
                    'created_at' => Carbon::now()
                ]);

                    // ================ tambah data di table m_sub_toefl_preparation_dokumen ================

                    $get_m_pembahasan_materi = DB::table('m_pembahasan_materi')
                        ->select(DB::raw("m_pembahasan_materi.*"))
                        ->where('m_pembahasan_materi.m_sub_toefl_preparation_id', '=', $datas->id)
                        ->get();

                    foreach($get_m_pembahasan_materi as $datassssssss){

                        $m_pembahasan_materi_get_id_terakhir = DB::table('m_pembahasan_materi')
                            ->select(DB::raw("m_pembahasan_materi.*"))
                            ->orderby('m_pembahasan_materi.id', 'DESC')
                            ->get()
                            ->first();

                        $tambah_m_pembahasan_materi = DB::table('m_pembahasan_materi')
                        ->insert([
                            'id' => next_value_nuber_6_digit($m_pembahasan_materi_get_id_terakhir->id),
                            'm_sub_toefl_preparation_id' => next_value_nuber_6_digit($tb_m_sub_toefl_preparation_get_id_terakhir->id),
                            'url_vdeo_pembahasan_materi' => $datassssssss->url_vdeo_pembahasan_materi,
                            'created_at' => Carbon::now()
                        ]);
                    }

                    // ================ tutup tambah data di table m_sub_toefl_preparation_dokumen ================

                    // ================ tambah data di table m_sub_toefl_preparation_dokumen ================

                    $get_tb_reading_materi = DB::table('tb_reading_materi')
                        ->select(DB::raw("tb_reading_materi.*"))
                        ->where('tb_reading_materi.m_sub_toefl_preparation_id', '=', $datas->id)
                        ->get();

                    foreach($get_tb_reading_materi as $datasssssss){

                        $tb_reading_materi_get_id_terakhir = DB::table('tb_reading_materi')
                            ->select(DB::raw("tb_reading_materi.*"))
                            ->orderby('tb_reading_materi.id', 'DESC')
                            ->get()
                            ->first();

                        $tambah_tb_reading_materi = DB::table('tb_reading_materi')
                        ->insert([
                            'id' => next_value_nuber_6_digit($tb_reading_materi_get_id_terakhir->id),
                            'm_sub_toefl_preparation_id' => next_value_nuber_6_digit($tb_m_sub_toefl_preparation_get_id_terakhir->id),
                            'text_reading' => $datasssssss->text_reading,
                            'created_at' => Carbon::now()
                        ]);
                    }

                    // ================ tutup tambah data di table m_sub_toefl_preparation_dokumen ================

                    // ================ tambah data di table m_sub_toefl_preparation_dokumen ================

                    $get_m_latihan_materi = DB::table('m_latihan_materi')
                        ->select(DB::raw("m_latihan_materi.*"))
                        ->where('m_latihan_materi.m_sub_toefl_preparation_id', '=', $datas->id)
                        ->get();

                    foreach($get_m_latihan_materi as $datassssss){

                        $tb_m_latihan_materi_get_id_terakhir = DB::table('m_latihan_materi')
                            ->select(DB::raw("m_latihan_materi.*"))
                            ->orderby('m_latihan_materi.id', 'DESC')
                            ->get()
                            ->first();

                        $tambah_m_latihan_materi = DB::table('m_latihan_materi')
                        ->insert([
                            'id' => next_value_nuber_6_digit($tb_m_latihan_materi_get_id_terakhir->id),
                            'm_sub_toefl_preparation_id' => next_value_nuber_6_digit($tb_m_sub_toefl_preparation_get_id_terakhir->id),
                            'url_vdeo_latihan_materi' => $datassssss->url_vdeo_latihan_materi,
                            'created_at' => Carbon::now()
                        ]);
                    }

                    // ================ tutup tambah data di table m_sub_toefl_preparation_dokumen ================

                    // ================ tambah data di table m_sub_toefl_preparation_dokumen ================

                    $get_m_sub_toefl_preparation_dokumen = DB::table('m_sub_toefl_preparation_dokumen')
                        ->select(DB::raw("m_sub_toefl_preparation_dokumen.*"))
                        ->where('m_sub_toefl_preparation_dokumen.id_m_sub_toefl_preparation', '=', $datas->id)
                        ->get();

                    foreach($get_m_sub_toefl_preparation_dokumen as $datass){

                        $tb_m_sub_toefl_preparation_dokumen_get_id_terakhir = DB::table('m_sub_toefl_preparation_dokumen')
                            ->select(DB::raw("m_sub_toefl_preparation_dokumen.*"))
                            ->orderby('m_sub_toefl_preparation_dokumen.id', 'DESC')
                            ->get()
                            ->first();

                        $source = public_path('file_dokumen_tp/'.$datass->dokumen_file);
                        $dokumen_file_replace_extension = str_replace(".".$datass->file_extension, '', $datass->dokumen_file);
                        $file_duplikat = $dokumen_file_replace_extension.'_'.mt_rand(1000, 9999).'.'.$datass->file_extension;
                        $destination = public_path('file_dokumen_tp/'.$file_duplikat);
                        
                        File::copy($source, $destination);

                        $tambah_m_sub_toefl_preparation_dokumen = DB::table('m_sub_toefl_preparation_dokumen')
                        ->insert([
                            // 'id' => next_value_nuber_6_digit($tb_m_sub_toefl_preparation_dokumen_get_id_terakhir->id),
                            'dokumen_file' => $file_duplikat,
                            'deskripsi' => $datass->deskripsi,
                            'id_m_sub_toefl_preparation' => next_value_nuber_6_digit($tb_m_sub_toefl_preparation_get_id_terakhir->id),
                            'file_extension' => $datass->file_extension,
                            'created_at' => Carbon::now()
                        ]);
                    }

                    // ================ tutup tambah data di table m_sub_toefl_preparation_dokumen ================
            
            
                    // ================ tambah data di table m_latihan_soal ================

                    $get_m_latihan_soal = DB::table('m_latihan_soal')
                        ->select(DB::raw("m_latihan_soal.*"))
                        ->where('m_latihan_soal.m_sub_toefl_preparation_id', '=', $datas->id)
                        ->get();

                    foreach($get_m_latihan_soal as $datasss){

                        $tb_m_latihan_soal_get_id_terakhir = DB::table('m_latihan_soal')
                            ->select(DB::raw("m_latihan_soal.*"))
                            ->orderby('m_latihan_soal.id', 'DESC')
                            ->get()
                            ->first();

                        $tambah_m_latihan_soal = DB::table('m_latihan_soal')
                        ->insert([
                            'id' => next_value_nuber_6_digit($tb_m_latihan_soal_get_id_terakhir->id),
                            'm_sub_toefl_preparation_id' => next_value_nuber_6_digit($tb_m_sub_toefl_preparation_get_id_terakhir->id),
                            'm_latihan_jawaban_id' => $datasss->m_latihan_jawaban_id,
                            'soal_point' => $datasss->soal_point,
                            'soal' => $datasss->soal,
                            'deskripsi' => $datasss->deskripsi,
                            'kategori_soal' => $datasss->kategori_soal,
                            'created_at' => Carbon::now()
                        ]);

                            $get_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                                ->select(DB::raw("m_latihan_jawaban.*"))
                                ->where('m_latihan_jawaban.m_latihan_soal_id', '=', $datasss->id)
                                ->get();

                            foreach($get_m_latihan_jawaban as $datassss){

                                $tb_m_latihan_jawaban_get_id_terakhir = DB::table('m_latihan_jawaban')
                                    ->select(DB::raw("m_latihan_jawaban.*"))
                                    ->orderby('m_latihan_jawaban.id', 'DESC')
                                    ->get()
                                    ->first();

                                $tambah_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                                ->insert([
                                    'id' => next_value_nuber_6_digit($tb_m_latihan_jawaban_get_id_terakhir->id),
                                    'm_latihan_soal_id' => next_value_nuber_6_digit($tb_m_latihan_soal_get_id_terakhir->id),
                                    'huruf' => $datassss->huruf,
                                    'jawaban' => $datassss->jawaban,
                                    'created_at' => Carbon::now()
                                ]);
                            }

                    }

                    // ================ tutup tambah data di table m_latihan_soal ================
            
            }

            // ================ tutup tambah data di table m_sub_toefl_preparation ================

            DB::commit();
            return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
        }catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage(), $e->getTraceAsString());
            return response()->json(['success' => $e, 'kode' => 401, 'trace' => $e->getTraceAsString()]);
        }
       
    }
}
