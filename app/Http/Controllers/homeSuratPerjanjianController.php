<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use File;
use Illuminate\Support\Facades\Session;
use DB;
use Response;

class homeSuratPerjanjianController extends Controller
{
    public function index()
    {
        // dd(Session::get('user_app')['users_id']);
        $get_session_users_id = Session::get('user_app')['users_id'];
        $get_tanda_tangan_users = db::table('users')
            ->select(DB::raw('users.*'))
            ->where('users.id', '=', $get_session_users_id)
            ->get()
            ->first();
        $tanda_tangan = $get_tanda_tangan_users->tanda_tangan;
        // dd($tanda_tangan);
        return view('pages.home_surat_perjanjian.index', compact('tanda_tangan'));
    }

    public function surat_perjanjian(Request $request)
    {
        // dd(public_path("image/ttd_contoh.png"));
        $get_session_users_id = Session::get('user_app')['users_id'];
        $get_session_token = Session::get('user_app')['token'];
        $get_session_nama = Session::get('user_app')['nama'];
        $get_session_username = Session::get('user_app')['username'];

        $get_users = db::table('users')
            ->select(DB::raw('users.*'))
            ->where('users.id', '=', $get_session_users_id)
            ->get()
            ->first();
        // $pdf_data = [];
        // dd($get_users);
        $pdf_data['tanda_tangan'] = $get_users->tanda_tangan;
        $pdf_data['email'] = $get_users->email;
        $pdf_data['username'] = $get_session_username;
        $pdf_data['nama'] = $get_session_nama;

        // dd($pdf_data);


        $options = array("format" => "A4", "defaultFont" => "arial");
        $pdf = PDF::loadView('pages.home_surat_perjanjian.surat_perjanjian_file', $pdf_data, [], $options);
        $pdf->stream('surat_perjanjian_' . $get_session_username . '.pdf');
    }

    public function surat_perjanjian_download_pdf(Request $request)
    {
        // dd(public_path("image/ttd_contoh.png"));
        // dd($request->all());

        $get_users_id = $request->users_id;

        $get_users = db::table('users')
            ->select(DB::raw('users.*'))
            ->where('users.id', '=', $get_users_id)
            ->get()
            ->first();
        // $pdf_data = [];
        $pdf_data['tanda_tangan'] = $get_users->tanda_tangan;
        $pdf_data['email'] = $get_users->email;
        $pdf_data['username'] = $get_users->username;
        $pdf_data['nama'] = $get_users->nama;

        // dd($pdf_data);


        $options = array("format" => "A4", "defaultFont" => "arial");
        $pdf = PDF::loadView('pages.home_surat_perjanjian.surat_perjanjian_file', $pdf_data, [], $options);
        $pdf->stream('surat_perjanjian_' . $get_users->username . '.pdf');

        // $headers = array(
        //     'Content-Type: application/pdf',
        // );

        // return Response::download($pdf, 'surat_perjanjian_' . $get_users->username . '.pdf', $headers);
    }

    public function update_tanda_tangan(Request $request)
    {
        // dd(public_path("image/ttd_contoh.png"));
        // dd($request->all());
        $get_session_users_id = Session::get('user_app')['users_id'];
        $get_session_token = Session::get('user_app')['token'];

        if (validateSessionToken($get_session_token)) {
            $tanda_tangan = $request->tanda_tangan;
            // dd($get_session_users_id);
            // dd($tanda_tangan);

            $update_users = DB::table('users')
                ->where('users.id', '=', $get_session_users_id)
                ->update([
                    'tanda_tangan' => $tanda_tangan,
                ]);

            $get_users = db::table('users')
                ->select(DB::raw('users.*'))
                ->where('users.id', '=', $get_session_users_id)
                ->get()
                ->first();


            return response()->json(['success' => 'Data Berhasil Diupdate', 'kode' => 201, 'tanda_tangan' => $get_users->tanda_tangan]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        };
    }

    public function simpan_surat_perjanjian(Request $request)
    {
        // dd(public_path("image/ttd_contoh.png"));
        // dd($request->all());
        $get_session_users_id = Session::get('user_app')['users_id'];
        $get_session_token = Session::get('user_app')['token'];

        if (validateSessionToken($get_session_token)) {
            $tanda_tangan = $request->tanda_tangan;
            // dd($get_session_users_id);
            // dd($tanda_tangan);

            $update_users = DB::table('users')
                ->where('users.id', '=', $get_session_users_id)
                ->update([
                    'status_tanda_tangan' => "2",
                ]);


            return response()->json(['success' => 'Data Berhasil Diupdate', 'kode' => 201]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        };
    }
}
