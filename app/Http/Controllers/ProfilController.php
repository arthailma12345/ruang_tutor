<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class ProfilController extends Controller
{
    public function index()
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];
        $get_session_username = $get_session['username'];
        $get_session_nama = $get_session['nama'];
        $get_session_flag = $get_session['flag'];
        $get_session_users_id = $get_session['users_id'];
        if (validateSessionToken($get_session_token)) {

            $tb_user = DB::table('m_role')
            ->select(DB::raw("
            m_role.id, 
            m_role.nama_role,
            m_role.flag,
            users.role,
            users.created_at,
            users.updated_at,
            users.email"))
            ->join('users', 'users.role', '=', 'm_role.id')
            ->where('users.id', '=', $get_session_users_id)
            ->limit(1)
            ->get();

            // dd($tb_user);

            return view('pages.profil.index',compact("get_session_username","get_session_nama","tb_user", "get_session_flag"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function udpateemail(Request $request){
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];
        $get_session_users_id = $get_session['users_id'];

        if (validateSessionToken($get_session_token)) {

            $datetime = date('Y-m-d H:i:s');
            $inputupdateemail = $request->inputeemail;

            $update_paket = DB::table('users')
                ->where('users.id', '=', $get_session_users_id)
                ->update([
                    'created_at' => $datetime,
                    'email' => $inputupdateemail,
                ]);

            $response = [
                "message" => "data berhasil diupdate",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];
            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function udpatepassowrd(Request $request){
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];
        $get_session_users_id = $get_session['users_id'];

        if (validateSessionToken($get_session_token)) {

            $datetime = date('Y-m-d H:i:s');
            $inputpasslama = $request->inputpasslama;
            $inputpassbaru1 = $request->inputpassbaru1;

            $get_tb_users = db::table('users')
            ->where('users.id', '=', $get_session_users_id)
            ->get()
            ->first();
        if ($get_tb_users != null) {
            if (Hash::check($inputpasslama,$get_tb_users->password)) {
                $update_paket = DB::table('users')
                ->where('users.id', '=', $get_session_users_id)
                ->update([
                    'created_at' => $datetime,
                    'password' => Hash::make($inputpassbaru1),
                ]);

                $response = [
                    "message" => "data berhasil diupdate",
                    "kode"    => 201,
                    "result"  => [
                        "token" => '',
                    ]
                ];
                return response()->json($response, $response['kode']);
            }else{
                $response = [
                    "message" => "data gagal diupdate",
                    "kode"    => 202,
                    "result"  => [
                        "token" => '',
                    ]
                ];
                return response()->json($response, $response['kode']);
            }
        }
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }
}
