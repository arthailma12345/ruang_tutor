<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class toeflPreparationPesertaController extends Controller
{
    public function index()
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {
        // $tb_m_toefl_preparation = DB::table('m_toefl_preparation')
        //     ->select(DB::raw("m_toefl_preparation.*"))
        //     ->get();

        // $data = [];
        // $data['tb_m_toefl_preparation'] = $tb_m_toefl_preparation;

        // return view('pages.toefl_preparation_peserta.index', $data);
        return view('pages.toefl_preparation_peserta.index');
    } else {
        return response()->json(['success' => 'anda belum login', 'kode' => 401]);
    }
    }

    public function getListDataToeflpreparation()
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];
        $get_session_users_id = $get_session['users_id'];

        if (validateSessionToken($get_session_token)) {

            // $tb_m_toefl_preparation = DB::table('m_toefl_preparation')
            //     ->select(DB::raw("m_toefl_preparation.*"))
            //     ->get();

            $tb_m_toefl_preparation = DB::table('tb_roll_akses_teofl_preparation')
                ->select(DB::raw("
                tb_roll_akses_teofl_preparation.id_username, 
                tb_roll_akses_teofl_preparation.id_paket,
                m_toefl_preparation.id as id_tp,
                m_toefl_preparation.nama_toefl_preparation"))
                ->join('m_toefl_preparation', 'm_toefl_preparation.id', '=', 'tb_roll_akses_teofl_preparation.id_paket')
                ->where('tb_roll_akses_teofl_preparation.id_username', '=',  $get_session_users_id)
                ->get();

            $datas = [];
            $no = 1;

            if (count($tb_m_toefl_preparation) > 0) {

                foreach ($tb_m_toefl_preparation as $value) {
                    // dd($value->id_tp);

                    $tb_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                        ->select(DB::raw("m_sub_toefl_preparation.id"))
                        ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $value->id_tp)
                        ->get();

                    $tb_m_sub_toefl_preparation_count = count($tb_m_sub_toefl_preparation);

                    $datas[] = array(

                        'no' => $no++,
                        'id_tb_m_toefl_preparation' => $value->id_tp,
                        'nama_toefl_preparation' => $value->nama_toefl_preparation,
                        'tb_m_sub_toefl_preparation_count' => $tb_m_sub_toefl_preparation_count

                    );
                }
            } else {
                $datas = array();
            }

            $data = $datas;

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }
}
