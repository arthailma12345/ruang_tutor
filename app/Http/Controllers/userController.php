<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\BeforeExport;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;

use Illuminate\Support\Collection;

class userController extends Controller
{
    public function index()
    {

        $data['m_role'] = DB::table('m_role')
            ->select(DB::raw("m_role.*"))
            ->get();

        return view('pages.user.index', $data);
    }

    public function getDataUser(Request $request)
    {

        $limit = is_null($request["length"]) ? 25 : $request["length"];
        $offset = is_null($request["start"]) ? 0 : $request["start"];
        $dirs = array("asc", "desc");
        $draw = $request["draw"];
        $searchs = $request["search.value"];
        $resultData = array();
        $data_arr    = [
            'limit' => $limit,
            'offset' => $offset,
            'searchs' => $searchs,
            'dirs' => $dirs,
        ];
        // dd($limit);

        // $tb_users = DB::table('users')
        //     ->select(DB::raw("users.*"));
        // $token = headerToken()['api-token'];
        // $client = new Client(['verify' => public_path('ssl/cacert.pem')]);
        // $myBody = array(
        //     "api-token" => $token,
        // );
        // $url = url_api_helpdesk('user/get');
        // $response = $client->get($url, ['headers' => $myBody, 'query' => $data_arr]);
        // $response_server = \GuzzleHttp\json_decode($response->getBody(), true);
        // $data_users = $response_server['result']['message'];
        // $data_users_count = count($data_users);
        // dd($data_users);
        // dd($response_server);


        // dd($response_server['result']['message']);

        // $tb_users_count = $tb_users->count();
        // $tb_users = $tb_users->limit($limit);
        // $tb_users = $tb_users->offset($offset);
        // $tb_users = $tb_users->get();

        // dd($request->all());

        $Nama_Search  = is_null($request->Nama_Search) ? '' : $request->Nama_Search;
        $Email_Search  = is_null($request->Email_Search) ? '' : $request->Email_Search;
        $Username_Search  = is_null($request->Username_Search) ? '' : $request->Username_Search;
        $Role_Search  = is_null($request->Role_Search) ? '' : $request->Role_Search;
        $Tanggal_Kadaluwarsa_Search_1_search  = is_null($request->Tanggal_Kadaluwarsa_Search_1_search) ? '0000-00-00' : $request->Tanggal_Kadaluwarsa_Search_1_search;
        $Tanggal_Kadaluwarsa_Search_2_search  = is_null($request->Tanggal_Kadaluwarsa_Search_2_search) ? '9999-99-99' : $request->Tanggal_Kadaluwarsa_Search_2_search;

        $Tanggal_Start_Search_1_search  = is_null($request->Tanggal_Start_Search_1_search) ? '0000-00-00' : $request->Tanggal_Start_Search_1_search;
        $Tanggal_Start_Search_2_search  = is_null($request->Tanggal_Start_Search_2_search) ? '9999-99-99' : $request->Tanggal_Start_Search_2_search;

        $Tanggal_created_at_1_search  = is_null($request->Tanggal_created_at_1_search) ? '0000-00-00' : $request->Tanggal_created_at_1_search;
        $Tanggal_created_at_2_search  = is_null($request->Tanggal_created_at_2_search) ? '9999-99-99' : $request->Tanggal_created_at_2_search;
        // dd($Tanggal_Kadaluwarsa_Search_1_search);


        $tb_users = DB::table('users')
            ->select(DB::raw("users.*, m_role.nama_role as m_role_nama_role"))
            ->leftJoin('m_role', 'm_role.id', '=', 'users.role')
            // ->orWhereNull('users.tanggal_expired')
            ->orWhere(function ($query) use (
                $Nama_Search,
                $Email_Search,
                $Username_Search,
                $Role_Search,
                $Tanggal_Kadaluwarsa_Search_1_search,
                $Tanggal_Kadaluwarsa_Search_2_search,
                $Tanggal_Start_Search_1_search,
                $Tanggal_Start_Search_2_search,
                $Tanggal_created_at_1_search,
                $Tanggal_created_at_2_search
            ) {
                $query
                    ->where('users.nama', 'ilike', '%' . $Nama_Search . '%')
                    ->where('users.email', 'ilike', '%' . $Email_Search . '%')
                    ->where('users.username', 'ilike', '%' . $Username_Search . '%')
                    ->where('users.role', 'ilike', '%' . $Role_Search . '%')
                    // ->where('users.tanggal_expired', '>=', $Tanggal_Kadaluwarsa_Search_1_search)
                    // ->where('users.tanggal_expired', '<=', $Tanggal_Kadaluwarsa_Search_2_search)
                    ->whereBetween('users.tanggal_start', [$Tanggal_Start_Search_1_search, $Tanggal_Start_Search_2_search])
                    ->whereBetween('users.tanggal_expired', [$Tanggal_Kadaluwarsa_Search_1_search, $Tanggal_Kadaluwarsa_Search_2_search])
                    ->whereBetween('users.created_at', [$Tanggal_created_at_1_search, $Tanggal_created_at_2_search]);
            });

        // $datax['total_data'] = $tb_users->first();
        $total_data = $tb_users->count();


        $users = $tb_users
            ->limit($limit)
            ->offset($offset)
            ->get();


        $datas = [];

        // $no = $offset + 1;

        $no = 0;
        if (count($users) > 0) {

            foreach ($users as $value) {
                // dd($value['nama']);
                $no++;
                $tanda_tangan = '';
                if ($value->tanda_tangan == null || $value->tanda_tangan == '') {
                    $tanda_tangan = '';
                } else {
                    $tanda_tangan = '<img id="sig-image" src="' . $value->tanda_tangan . '" style="width: 200px; height: 120px" />';
                }

                $button_set_kadaluwarsa = "";
                $button_downnload_surat_perjanjian = "";
                if ($value->role == "R003") {
                    $button_set_kadaluwarsa = '<button type="button" name="edit_kadaluwarsa" id="edit_kadaluwarsa" class="btn btn-warning waves-effect waves-light m-1" data-bs-toggle="modal" data-bs-target="#modalFormEditKadaluwarsa" data-users_id="' . $value->id . '" data-tanggal_kadaluwarsa="' . $value->tanggal_expired . '" data-tanggal_start="' . $value->tanggal_start . '"> <i class="dripicons-link-broken"></i> Kadaluwarsa </button>';
                    $button_downnload_surat_perjanjian = '<a href=' . url("home_surat_perjanjian/surat_perjanjian_download_pdf" . '?users_id=' . $value->id) . ' name="downnload_surat_perjanjian" id="downnload_surat_perjanjian" target="_blank" class="btn btn-info waves-effect waves-light m-1"> <i class="dripicons-download"></i> download surat perjanjian </a>';
                } else {
                    $button_set_kadaluwarsa = "";
                    $button_downnload_surat_perjanjian = "";
                }

                // $qrcode = base64_encode(QrCode::format('png')
                //     // ->merge(public_path('image/Petro_logo.png'), 0.3, true)
                //     ->size(500)->errorCorrection("H")
                //     ->generate($value->token_login_qr_code));

                $datas[] = array(

                    'no' =>

                    '<table>' .
                        '<tr>' .

                        '<td>' .
                        $no .
                        '</td>' .
                        '<td style="margin:1px; width:70px;">' .
                        '&nbsp; &nbsp;<input style="transform: scale(1.7);" class="form-check-input users_id_checkbox_' . $value->id . '" type="checkbox" id="users_id_checkbox_' . $no . '" name="users_id_checkbox[]" value="' . $value->id . '">' .
                        '</td>' .
                        '</tr>' .

                        '</table>',


                    'email' => $value->email,
                    'nama' => $value->nama,
                    'username' => $value->username,
                    'password' => $value->password,
                    'role' => $value->role,
                    'm_role_nama_role' => $value->m_role_nama_role,
                    'tanggal_start' => $value->tanggal_start,
                    'tanggal_expired' => $value->tanggal_expired,
                    'tanda_tangan' => $tanda_tangan,
                    'created_at' => $value->created_at,
                    // 'qrcode' => $qrcode,
                    'token_login_qr_code' => $value->token_login_qr_code,
                    'aksi' =>
                    // '<button type="button" name="edit" id="edit" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalFormEditUser" data-nama="' . $value['nama'] . '"  data-username="' . $value['username'] . '" data-password="' . $value['password'] . '" data-role="' . $value['role'] . '" data-users_id="' . $value['id'] . '" href="' . $value['id'] . '"><i class="dripicons-pencil"></i> Edit </button> &nbsp;' .
                    '<button type="button" name="edit" id="edit" class="btn btn-primary waves-effect waves-light m-1" data-bs-toggle="modal" data-bs-target="#modalFormEditUser" 
                    data-email="' . $value->email . '" 
                    data-nama="' . $value->nama . '"  
                    data-username="' . $value->username . '" 
                    data-password="' . $value->password . '" 
                    data-role="' . $value->role . '" 
                    data-users_id="' . $value->id . '" 
                    href="' . $value->id . '"> 
                    <i class="dripicons-pencil"></i> Edit </button> &nbsp;' .
                        '<button type="button" name="delete" id="delete" data-users_id="' . $value->id . '" class="btn btn-danger btn-xs m-1" href=' . '' . '><i class="dripicons-trash"></i> hapus </button> &nbsp;' .
                        $button_set_kadaluwarsa .
                        $button_downnload_surat_perjanjian
                );
            }
        } else {
            $datas = array();
        }

        // dd($datas);
        $recordsTotal = is_null($total_data) ? 0 : $total_data;
        $recordsFiltered = is_null($total_data) ? 0 : $total_data;
        $data = $datas;

        return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
    }


    public function getDataUserCheckAll(Request $request)
    {

        $Nama_Search  = is_null($request->Nama_Search) ? '' : $request->Nama_Search;
        $Email_Search  = is_null($request->Email_Search) ? '' : $request->Email_Search;
        $Username_Search  = is_null($request->Username_Search) ? '' : $request->Username_Search;
        $Role_Search  = is_null($request->Role_Search) ? '' : $request->Role_Search;
        $Tanggal_Kadaluwarsa_Search_1_search  = is_null($request->Tanggal_Kadaluwarsa_Search_1_search) ? '0000-00-00' : $request->Tanggal_Kadaluwarsa_Search_1_search;
        $Tanggal_Kadaluwarsa_Search_2_search  = is_null($request->Tanggal_Kadaluwarsa_Search_2_search) ? '9999-99-99' : $request->Tanggal_Kadaluwarsa_Search_2_search;
        // dd($Tanggal_Kadaluwarsa_Search_1_search);


        $tb_users = DB::table('users')
            ->select(DB::raw("users.id as users_id"))
            ->leftJoin('m_role', 'm_role.id', '=', 'users.role')
            // ->orWhereNull('users.tanggal_expired')
            ->orWhere(function ($query) use (
                $Nama_Search,
                $Email_Search,
                $Username_Search,
                $Role_Search,
                $Tanggal_Kadaluwarsa_Search_1_search,
                $Tanggal_Kadaluwarsa_Search_2_search
            ) {
                $query
                    ->where('users.nama', 'ilike', '%' . $Nama_Search . '%')
                    ->where('users.email', 'ilike', '%' . $Email_Search . '%')
                    ->where('users.username', 'ilike', '%' . $Username_Search . '%')
                    ->where('users.role', 'ilike', '%' . $Role_Search . '%')
                    // ->where('users.tanggal_expired', '>=', $Tanggal_Kadaluwarsa_Search_1_search)
                    // ->where('users.tanggal_expired', '<=', $Tanggal_Kadaluwarsa_Search_2_search)
                    ->whereBetween('users.tanggal_expired', [$Tanggal_Kadaluwarsa_Search_1_search, $Tanggal_Kadaluwarsa_Search_2_search]);
            });

        // $datax['total_data'] = $tb_users->first();
        $total_data = $tb_users->count();


        $data = $tb_users
            ->get();

        $response = [
            "message" => "data berhasil ditambah",
            "kode"    => 201,
            "data"    => $data,
            "result"  => [
                "token" => '',
            ]
        ];

        // dd($response);

        return response()->json($response, $response['kode']);
    }



    public function tambah(Request $request)
    {

        $token = headerToken()['api-token'];
        // $data['data'] = $request->all();
        // $data['token'] = $token;
        $users_id = Session::get('user_app')['users_id'];
        $request->request->add(['token' => $token, 'token' => $token, 'users_id' => $users_id]);
        $data = $request->all();

        // dd($data);

        $client = loadfile();

        // try {
        //     $url = url_api_helpdesk('user/tambah');
        //     $response = $client->post($url, ['form_params' => $data]);
        //     $response_server = \GuzzleHttp\json_decode($response->getBody(), true);
        //     // dd($response_server);
        //     return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
        // } catch (RequestException $e) {
        //     // return response()->json(['success' => $e, 'kode' => 401]);
        //     dd($e->getMessage());
        //     // return response()->json(['success' => $e, 'kode' => 201]);
        // } catch (\Exception $e) {
        //     dd($e->getMessage());
        //     // return response()->json(['success' => $e, 'kode' => 201]);
        // }

        $nama = $request->nama;
        $username = $request->username;
        $email = $request->email;
        // return response()->json($request->data['nama']);
        $password = Hash::make($request->password);
        $role = $request->role;

        $kadaluwarsa = "";

        if ($role == "R003") {
            $kadaluwarsa = $request->kadaluwarsa;
        } else {
            $kadaluwarsa = "0000-00-00";
        }

        if($username != "" && $password != ""){
            $tambah_users = DB::table('users')
            ->insert([
                'nama' => $nama,
                'email' => $email,
                'username' => $username,
                'password' => $password,
                'role' => $role,
                'tanggal_expired' => $kadaluwarsa,
                'token_login_qr_code' => encrypt_($username),
                'status_tanda_tangan' => 1,
                'created_at' => Carbon::now()
            ]);

        $response = [
            "message" => "data berhasil ditambah",
            "kode"    => 201,
            "result"  => [
                "token" => '',
            ]
        ];
        }else{
            $response = [
                "message" => "data gagal ditambah",
                "kode"    => 401,
                "result"  => [
                    "token" => '',
                ]
            ];
        }
        

        return response()->json($response, $response['kode']);
    }

    public function update(Request $request)
    {

        $token = headerToken()['api-token'];
        // $data['data'] = $request->all();
        // $data['token'] = $token;
        // dd($request->all());
        // $users_id = Session::get('user_app')['users_id'];
        // $request->request->add(['token' => $token, 'token' => $token, 'users_id' => $users_id]);
        // $data = $request->all();

        // // dd('coba');
        // // dd($request->all());

        // $client = loadfile();

        // try {
        //     $url = url_api_helpdesk('user/update');
        //     $response = $client->post($url, ['form_params' => $data]);
        //     $response_server = \GuzzleHttp\json_decode($response->getBody(), true);
        //     // dd($response_server);
        //     return response()->json(['success' => 'Data Berhasil Diupdate', 'kode' => 201]);
        // } catch (RequestException $e) {
        //     // return response()->json(['success' => $e, 'kode' => 401]);
        //     dd($e->getMessage());
        //     // return response()->json(['success' => $e, 'kode' => 201]);
        // } catch (\Exception $e) {
        //     dd($e->getMessage());
        //     // return response()->json(['success' => $e, 'kode' => 201]);
        // }

        $users_id_edit = $request->users_id_edit;
        $email_edit = $request->email_edit;
        $nama_edit = $request->nama_edit;
        $username_edit = $request->username_edit;
        $password_edit = $request->password_edit;
        $role_edit = $request->role_edit;

        if ($password_edit == null || $password_edit == '') {
            $update_users = DB::table('users')
                ->where('users.id', '=', $users_id_edit)
                ->update([
                    'email' => $email_edit,
                    'nama' => $nama_edit,
                    'username' => $username_edit,
                    'role' => $role_edit,
                ]);
        } else {
            $update_users = DB::table('users')
                ->where('users.id', '=', $users_id_edit)
                ->update([
                    'email' => $email_edit,
                    'nama' => $nama_edit,
                    'username' => $username_edit,
                    'password' => Hash::make($password_edit),
                    'role' => $role_edit,
                ]);
        }

        $response = [
            "message" => "data berhasil diupdate",
            "kode"    => 201,
            "result"  => [
                "token" => '',
            ]
        ];

        return response()->json($response, $response['kode']);

        // dd($response_server);
        // dd();

    }

    public function updateKadaluwarsa(Request $request)
    {
        // dd($request->all());

        $token = headerToken()['api-token'];

        $users_id_edit_tanggal_kadaluwarsa = $request->users_id_edit_tanggal_kadaluwarsa;
        $tanggal_kadaluwarsa_edit = $request->tanggal_kadaluwarsa_edit;
        $tanggal_start_edit = $request->tanggal_start_edit;

        $update_users = DB::table('users')
            ->where('users.id', '=', $users_id_edit_tanggal_kadaluwarsa)
            ->update([
                'tanggal_expired' => $tanggal_kadaluwarsa_edit,
                'tanggal_start' => $tanggal_start_edit,
            ]);

        $response = [
            "message" => "data berhasil diupdate",
            "kode"    => 201,
            "result"  => [
                "token" => '',
            ]
        ];

        return response()->json($response, $response['kode']);

        // dd($response_server);
        // dd();

    }

    public function delete(Request $request, $id)
    {

        $token = headerToken()['api-token'];
        // $data['data'] = $request->all();
        // $data['token'] = $token;
        // dd($request->all());
        // $users_id = Session::get('user_app')['users_id'];
        // $request->request->add(['token' => $token, 'token' => $token, 'users_id' => $users_id]);
        // $data = $request->all();

        // dd('coba');
        // dd($request->all());
        // $array = array("users_id" => $id);
        // $request->request->add(['token' => $token, 'users_id' => $id]);
        // dd($request->all());
        // $data_arr    = [
        //     'token' => $token
        // ];

        // $client = loadfile();

        // try {
        //     $url = url_api_helpdesk("user/delete/$id");
        //     $response = $client->get($url, ['query' => $data_arr]);
        //     $response_server = \GuzzleHttp\json_decode($response->getBody(), true);
        //     // dd($response_server);
        //     return response()->json(['success' => 'Data Berhasil Didelete', 'kode' => 201]);
        // } catch (RequestException $e) {
        //     // return response()->json(['success' => $e, 'kode' => 401]);
        //     dd($e->getMessage());
        //     // return response()->json(['success' => $e, 'kode' => 201]);
        // } catch (\Exception $e) {
        //     dd($e->getMessage());
        //     // return response()->json(['success' => $e, 'kode' => 201]);
        // }

        $delete_users = DB::table('users')
            ->where('users.id', '=', $id)
            ->delete();

        $delete_tb_roll_akses_teofl_preparation = DB::table('tb_roll_akses_teofl_preparation')
            ->where('tb_roll_akses_teofl_preparation.id_username', '=', $id)
            ->delete();

        $delete_tb_roll_akses_teofl_test = DB::table('tb_roll_akses_teofl_test')
            ->where('tb_roll_akses_teofl_test.id_username', '=', $id)
            ->delete();

        $response = [
            "message" => "data berhasil diupdate",
            "kode"    => 201,
            "result"  => [
                "token" => '',
            ]
        ];

        return response()->json($response, $response['kode']);

        // dd($response_server);
        // dd();

    }

    public function delete_checkbox(Request $request)
    {
        // dd($request->all());
        $array_users_id_checkbox = explode(",", $request->array_users_id_checkbox);

        $delete_users = DB::table('users')
            ->whereIn('users.id', $array_users_id_checkbox)
            ->delete();

        $delete_tb_roll_akses_teofl_preparation = DB::table('tb_roll_akses_teofl_preparation')
            ->whereIn('tb_roll_akses_teofl_preparation.id_username', $array_users_id_checkbox)
            ->delete();

        $delete_tb_roll_akses_teofl_test = DB::table('tb_roll_akses_teofl_test')
            ->whereIn('tb_roll_akses_teofl_test.id_username', $array_users_id_checkbox)
            ->delete();

        // $token = headerToken()['api-token'];

        // $delete_users = DB::table('users')
        //     ->where('users.id', '=', $id)
        //     ->delete();

        $response = [
            "message" => "data berhasil didelete",
            "kode"    => 201,
            "result"  => [
                "token" => '',
            ]
        ];

        return response()->json($response, $response['kode']);

        // dd($response_server);
        // dd();

    }

    public function download_template_excel(Request $request)
    {
        $data = array();
        return Excel::download(new downloadTemplateExcel($data), 'Template_Import_User_Ruang_Tutor.xlsx');
    }

    public function import_excel_user(Request $request)
    {
        // dd('coba');

        $rows = Excel::toArray(new importExcelUser, $request->file('file_excel_import_user'));
        // dd($rows);

        $data_user_yang_behasil_di_input = [];
        $data_user_yang_tidak_behasil_di_input = [];

        if (count($rows[0]) == 0) {
            return response()->json([
                'success' => 'File Yang Anda Import Kosong',
                'kode' => 401,
            ]);
        } else {



            for ($i = 0; $i < count($rows[0]); $i++) {

                try {

                    if (
                        $rows[0][$i]['nama'] == null &&
                        $rows[0][$i]['username'] == null &&
                        $rows[0][$i]['password'] == null
                    ) {
                    } else {
                        $data_user = DB::table('users')
                            ->select(DB::raw("users.*"))
                            ->where('users.username', '=', $rows[0][$i]['username'])
                            ->get();

                        // dd($data_user);

                        if (count($data_user) == 0) {
                            // dd(count($data_user));
                            $data_user_yang_behasil_di_input[] = array(
                                'nama' => $rows[0][$i]['nama'],
                                'username' => $rows[0][$i]['username'],
                                'password' => Hash::make($rows[0][$i]['password']),
                                'tanggal_start' => integer_excel_to_date_ymd($rows[0][$i]['tanggal_start']),
                                'tanggal_expired' => integer_excel_to_date_ymd($rows[0][$i]['tanggal_expired']),
                                'paket' => $rows[0][$i]['paket'],
                            );

                            $tambah_users = DB::table('users')
                                ->insert([
                                    'nama' => $rows[0][$i]['nama'],
                                    'username' => $rows[0][$i]['username'],
                                    'password' => Hash::make($rows[0][$i]['password']),
                                    'role' => "R003",
                                    'email' => "",
                                    'tanggal_start' => integer_excel_to_date_ymd($rows[0][$i]['tanggal_start']),
                                    'tanggal_expired' => integer_excel_to_date_ymd($rows[0][$i]['tanggal_expired']),
                                    'status_tanda_tangan' => 1,
                                    'token_login_qr_code' => encrypt_($rows[0][$i]['username']),
                                    'created_at' => Carbon::now()
                                ]);

                            $data_user2 = DB::table('users')
                                ->select(DB::raw("users.*"))
                                ->where('users.username', '=', $rows[0][$i]['username'])
                                ->get();

                            foreach ($data_user2 as $value_ul) {

                                // dd($datas);
                                $idpaket_excel = $rows[0][$i]['paket'];
                                $akses_dokumen_excel = $rows[0][$i]['akses_dokumen'];
                                $substr_id = substr($idpaket_excel, 0, -6);

                                if ($substr_id == "T") {
                                    // dd("true");
                                    $tambah_roll_paket = DB::table('tb_roll_akses_teofl_preparation')
                                        ->insert([
                                            'id_username' => $value_ul->id,
                                            'id_paket' => $idpaket_excel,
                                            'akses_dokumen' => $akses_dokumen_excel,
                                        ]);
                                } else {
                                    $tambah_roll_paket = DB::table('tb_roll_akses_teofl_test')
                                        ->insert([
                                            'id_username' => $value_ul->id,
                                            'id_paket' => $idpaket_excel,
                                        ]);
                                }
                            }
                        } else {

                            $data_user_yang_tidak_behasil_di_input[] = array(
                                'nama' => $rows[0][$i]['nama'],
                                'username' => $rows[0][$i]['username'],
                                'password' => Hash::make($rows[0][$i]['password']),
                                'tanggal_start' => integer_excel_to_date_ymd($rows[0][$i]['tanggal_start']),
                                'tanggal_expired' => integer_excel_to_date_ymd($rows[0][$i]['tanggal_expired']),
                                'paket' => $rows[0][$i]['paket'],
                            );

                            $username_excel = $rows[0][$i]['username'];

                            $data_user3 = DB::table('users')
                                ->select(DB::raw("users.*"))
                                ->where('users.username', '=', $rows[0][$i]['username'])
                                ->get();

                            foreach ($data_user3 as $value_u) {

                                $username_database = $value_u->username;
                                if ($username_excel == $username_database) {
                                    $idpaket_excel = $rows[0][$i]['paket'];
                                    $substr_id = substr($idpaket_excel, 0, -6);
                                    $akses_dokumen_excel = $rows[0][$i]['akses_dokumen'];

                                    if ($substr_id == "T") {
                                        // dd($idpaket_excel);

                                        $get_data_tb_roll_akses_teofl_preparation_cek = DB::table('tb_roll_akses_teofl_preparation')
                                            ->select(DB::raw("tb_roll_akses_teofl_preparation.id_roll_akses_tp"))
                                            ->leftJoin('users', 'users.id', '=', 'tb_roll_akses_teofl_preparation.id_username')
                                            ->where('users.username', '=', $rows[0][$i]['username'])
                                            ->where('tb_roll_akses_teofl_preparation.id_paket', '=', $idpaket_excel)
                                            ->get();

                                        // dd($get_data_tb_roll_akses_teofl_preparation_cek);

                                        if (count($get_data_tb_roll_akses_teofl_preparation_cek) > 0) {
                                            // dd($get_data_tb_roll_akses_teofl_preparation_cek);
                                            $update_roll_paket = DB::table('tb_roll_akses_teofl_preparation')
                                                ->where('tb_roll_akses_teofl_preparation.id_roll_akses_tp', $get_data_tb_roll_akses_teofl_preparation_cek->first()->id_roll_akses_tp)
                                                ->update([
                                                    'id_paket' => $idpaket_excel,
                                                    'akses_dokumen' => $akses_dokumen_excel,
                                                ]);
                                        } else {
                                            $tambah_roll_paket = DB::table('tb_roll_akses_teofl_preparation')
                                                ->insert([
                                                    'id_username' => $value_u->id,
                                                    'id_paket' => $idpaket_excel,
                                                    'akses_dokumen' => $akses_dokumen_excel,
                                                ]);
                                        }
                                    } else {
                                        // dd($idpaket_excel);
                                        $tambah_roll_paket = DB::table('tb_roll_akses_teofl_test')
                                            ->insert([
                                                'id_username' => $value_u->id,
                                                'id_paket' => $idpaket_excel,
                                            ]);
                                    }
                                } else {
                                    dd("false");
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    // dd($rows[0][$i], $i);
                    // dd($e->getMessage());
                    // return $e->getMessage();
                    return response()->json([
                        'success' => $rows[0],
                        'kode' => 401,
                    ]);
                }
            }

            return response()->json([
                'success' => 'Anda Berhasil Input Data',
                'kode' => 201,
                'data_user_yang_behasil_di_input' => $data_user_yang_behasil_di_input,
                'jumlah_data_user_yang_behasil_di_input' => count($data_user_yang_behasil_di_input),
                'data_user_yang_tidak_behasil_di_input' => $data_user_yang_tidak_behasil_di_input,
                'jumlah_data_user_yang_tidak_behasil_di_input' => count($data_user_yang_tidak_behasil_di_input),
            ]);
        }
        // dd($rows);
    }
}


class downloadTemplateExcel implements FromView, WithStyles, ShouldAutoSize, WithEvents, WithColumnWidths
{
    use Exportable;
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        $data = $this->data;
        return view('pages.user.template_export_excel', $data);
    }

    public function styles(Worksheet $sheet) {}

    public function registerEvents(): array
    {
        // dd(count($data['data_barang_investasi_perkiraan_umur_manfaat_null']));
        return [
            // handle by a closure.
            // AfterSheet::class => function (AfterSheet $event) {
            //     $validation = $event->sheet->getCell('B5')
            //         ->getDataValidation();
            //     $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
            //     $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
            //     $validation->setAllowBlank(false);
            //     $validation->setShowInputMessage(true);
            //     $validation->setShowErrorMessage(true);
            //     $validation->setShowDropDown(true);
            //     $validation->setErrorTitle('Input error');
            //     $validation->setError('Value is not in list.');
            //     $validation->setPromptTitle('Pick from list');
            //     $validation->setPrompt('Please pick a value from the drop-down list.');
            //     $validation->setFormula1('"Item A,Item B,Item C"');
            // },
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 40,
            'B' => 40,
            'C' => 40,
            'D' => 30,
        ];
    }
}

class importExcelUser implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        //
    }

    public function headingRow(): int
    {
        return 1;
    }
}
