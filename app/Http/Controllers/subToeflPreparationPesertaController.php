<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use Response;

class subToeflPreparationPesertaController extends Controller
{
    public function index($toefl_preparation_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];
        $get_session_users_id = $get_session['users_id'];

        $tb_m_sub_toefl_preparation = DB::table('m_toefl_preparation')
            ->select(DB::raw("m_toefl_preparation.*"))
            ->where('m_toefl_preparation.id', '=', $toefl_preparation_id)
            ->get();

        $data['toefl_preparation_id'] = $toefl_preparation_id;
        // $data['tb_m_sub_toefl_preparation_nama'] = $tb_m_sub_toefl_preparation->first()->nama_toefl_preparation;

        // dd($data);
        // $tb_roll_akses_teofl_preparation = DB::table('tb_roll_akses_teofl_preparation')
        //     ->select(DB::raw("tb_roll_akses_teofl_preparation.*"))
        //     ->where('tb_roll_akses_teofl_preparation.id_paket', '=', $toefl_preparation_id)
        //     ->where('tb_roll_akses_teofl_preparation.id_username', '=', $get_session_users_id)
        //     ->whereNotNull('tb_roll_akses_teofl_preparation.akses_dokumen')
        //     ->where('tb_roll_akses_teofl_preparation.akses_dokumen', '=', "YA")
        //     ->get();

        // $tb_roll_akses_teofl_preparation = DB::table('tb_roll_akses_teofl_preparation')
        //     ->select(DB::raw("tb_roll_akses_teofl_preparation.*"))
        //     ->where('tb_roll_akses_teofl_preparation.id_paket', '=', $toefl_preparation_id)
        //     ->where('tb_roll_akses_teofl_preparation.id_username', '=', $get_session_users_id)
        //     ->whereNotNull('tb_roll_akses_teofl_preparation.akses_dokumen')
        //     ->where('tb_roll_akses_teofl_preparation.akses_dokumen', '=', "YA")
        //     ->get();

        // dd($tb_roll_akses_teofl_preparation);
        // $data['tb_roll_akses_teofl_preparation'] = $tb_roll_akses_teofl_preparation;

        return view('pages.sub_toefl_preparation_peserta.index', $data);

        // return view('pages.sub_toefl_preparation_peserta.index', compact('toefl_preparation_id'));
    }

    public function getListDataSubToeflpreparation($toefl_preparation_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $tb_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                ->select(DB::raw("m_sub_toefl_preparation.*"))
                ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $toefl_preparation_id)
                // ->orderBy('m_sub_toefl_preparation.nama_sub_toefl_preparation', 'ASC')
                ->orderByRaw("m_sub_toefl_preparation.no_urut ASC")
                ->get();

            $datas = [];
            $no = 1;

            if (count($tb_m_sub_toefl_preparation) > 0) {

                foreach ($tb_m_sub_toefl_preparation as $value) {
                    // dd($value['nama']);

                    $datas[] = array(

                        'no' => $no++,
                        'id_tb_m_sub_toefl_preparation' => $value->id,
                        'no_urut' => $value->no_urut,
                        'nama_sub_toefl_preparation' => $value->nama_sub_toefl_preparation,
                        'deskripsi_sub_toefl_preparation' => $value->deskripsi

                    );
                }
            } else {
                $datas = array();
            }

            $data = $datas;

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getListDataSubToeflpreparationSetelahSebelum($toefl_preparation_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $tb_m_sub_toefl_preparation = DB::table('m_sub_toefl_preparation')
                ->select(DB::raw("m_sub_toefl_preparation.*"))
                ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $toefl_preparation_id)
                // ->orderBy('m_sub_toefl_preparation.nama_sub_toefl_preparation', 'ASC')
                ->orderByRaw("SUBSTRING(m_sub_toefl_preparation.nama_sub_toefl_preparation FROM '([0-9]+)')::BIGINT ASC")
                ->get();

            $datas = [];
            $no = 1;

            if (count($tb_m_sub_toefl_preparation) > 0) {

                foreach ($tb_m_sub_toefl_preparation as $value) {
                    // dd($value['nama']);

                    $datas[] = array(

                        'no' => $no++,
                        'id_tb_m_sub_toefl_preparation' => $value->id,
                        'nama_sub_toefl_preparation' => $value->nama_sub_toefl_preparation

                    );
                }
            } else {
                $datas = array();
            }

            $data = $datas;

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getListDataLatihanMateri($sub_toefl_preparation_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];
        $get_session_users_id = $get_session['users_id'];

        if (validateSessionToken($get_session_token)) {

            $tb_m_latihan_materi = DB::table('m_latihan_materi')
                ->select(DB::raw("m_latihan_materi.*"))
                ->where('m_latihan_materi.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
                ->get();

            $datas = [];
            $no = 1;

            if (count($tb_m_latihan_materi) > 0) {

                foreach ($tb_m_latihan_materi as $value) {
                    // dd($value['nama']);

                    $datas[] = array(

                        'id' => $value->id,
                        'url_vdeo_latihan_materi' => $value->url_vdeo_latihan_materi

                    );
                }
            } else {
                $datas = array();
            }

            $data = $datas;

            $get_id_paket = DB::table('m_sub_toefl_preparation')
                ->select(DB::raw("m_sub_toefl_preparation.m_toefl_preparation_id"))
                ->where('m_sub_toefl_preparation.id', '=', $sub_toefl_preparation_id)
                ->get()
                ->first();

            $tb_roll_akses_teofl_preparation = DB::table('tb_roll_akses_teofl_preparation')
                ->select(DB::raw("tb_roll_akses_teofl_preparation.*"))
                ->where('tb_roll_akses_teofl_preparation.id_paket', '=', $get_id_paket->m_toefl_preparation_id)
                ->where('tb_roll_akses_teofl_preparation.id_username', '=', $get_session_users_id)
                ->whereNotNull('tb_roll_akses_teofl_preparation.akses_dokumen')
                ->where('tb_roll_akses_teofl_preparation.akses_dokumen', '=', "YA")
                ->get();

            $visibility_button_dokumen = '';

            $array_dokumen_file = '';

            if (count($tb_roll_akses_teofl_preparation) > 0) {

                $m_sub_toefl_preparation_dokumen = DB::table('m_sub_toefl_preparation_dokumen')
                    ->select(DB::raw("m_sub_toefl_preparation_dokumen.*"))
                    ->where('m_sub_toefl_preparation_dokumen.id_m_sub_toefl_preparation', '=', $sub_toefl_preparation_id)
                    ->get()
                    ->toArray();
                // dd($m_sub_toefl_preparation_dokumen);

                if (count($m_sub_toefl_preparation_dokumen) > 0) {
                    if (count($tb_roll_akses_teofl_preparation) > 0) {
                        $visibility_button_dokumen = true;
                        $array_dokumen_file = $m_sub_toefl_preparation_dokumen;
                    } else {
                        $visibility_button_dokumen = false;
                        $array_dokumen_file = [];
                    }
                } else {
                    $visibility_button_dokumen = false;
                    $array_dokumen_file = [];
                }
            } else {
                $visibility_button_dokumen = false;
                $array_dokumen_file = [];
            }



            // return response()->json(compact("data, visibility_button_dokumen"));
            return response()->json([
                'data' => $data,
                'visibility_button_dokumen' => $visibility_button_dokumen,
                'array_dokumen_file' => $array_dokumen_file
            ]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getListDataLatihanMateriReading($sub_toefl_preparation_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $tb_tb_reading_materi = DB::table('tb_reading_materi')
                ->select(DB::raw("tb_reading_materi.*"))
                ->where('tb_reading_materi.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
                ->get();

            $datas = [];
            $no = 1;

            if (count($tb_tb_reading_materi) > 0) {

                foreach ($tb_tb_reading_materi as $value) {
                    // dd($value['nama']);

                    $datas[] = array(

                        'id' => $value->id,
                        'text_reading' => $value->text_reading

                    );
                }
            } else {
                $datas = array();
            }

            $data = $datas;

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getListDataLatihanMateriPembahasan($sub_toefl_preparation_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $tb_m_pembahasan_materi = DB::table('m_pembahasan_materi')
                ->select(DB::raw("m_pembahasan_materi.*"))
                ->where('m_pembahasan_materi.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
                ->get();

            $datas = [];
            $no = 1;

            if (count($tb_m_pembahasan_materi) > 0) {

                foreach ($tb_m_pembahasan_materi as $value) {
                    // dd($value['nama']);

                    $datas[] = array(

                        'id' => $value->id,
                        'url_vdeo_pembahasan_materi' => $value->url_vdeo_pembahasan_materi

                    );
                }
            } else {
                $datas = array();
            }

            $data = $datas;

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getListDataLatihanSoal($sub_toefl_preparation_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $tb_m_latihan_materi = DB::table('m_latihan_soal')
                ->select(DB::raw("m_latihan_soal.*"))
                ->where('m_latihan_soal.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
                ->orderBy('m_latihan_soal.id', 'ASC')
                ->get();

            $datas = [];
            $no = 1;

            if (count($tb_m_latihan_materi) > 0) {

                foreach ($tb_m_latihan_materi as $value) {
                    // dd($value['nama']);

                    $datas[] = array(

                        'id' => $value->id,
                        'soal' => $value->soal,
                        'deskripsi' => $value->deskripsi,
                        'm_latihan_jawaban_id' => $value->m_latihan_jawaban_id,
                        'm_sub_toefl_preparation_id' => $value->m_sub_toefl_preparation_id

                    );
                }
            } else {
                $datas = array();
            }

            $data = $datas;

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getListDataLatihanJawaban($latihan_soal_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $tb_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                ->select(DB::raw("m_latihan_jawaban.*, m_latihan_soal.kategori_soal"))
                ->leftjoin('m_latihan_soal', 'm_latihan_soal.id', '=', 'm_latihan_jawaban.m_latihan_soal_id')
                ->where('m_latihan_jawaban.m_latihan_soal_id', '=', $latihan_soal_id)
                ->orderBy('m_latihan_jawaban.id', 'ASC')
                ->get();

            $datas = [];
            $no = 1;

            if (count($tb_m_latihan_jawaban) > 0) {

                foreach ($tb_m_latihan_jawaban as $value) {
                    // dd($value['nama']);

                    $datas[] = array(

                        'id' => $value->id,
                        'huruf' => $value->huruf,
                        'jawaban' => $value->jawaban,
                        'kategori_soal' => $value->kategori_soal,

                    );
                }
            } else {
                $datas = array();
            }

            $data = $datas;

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }


    public function getListSimpanJawaban(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        // dd($request->all());

        if (validateSessionToken($get_session_token)) {

            $nilai = 0;
            // $jumlah_soal = count($request->all());
            $jumlah_jawaban_yang_benar = 0;
            $jawaban_yang_benar_text = '';
            $jawaban_yang_benar_array_id = [];
            $jawaban_yang_benar_array_text = [];

            // dd($request->all());
            // dd(count($request->all()));
            // dd($request->except(['sub_toefl_preparation_peserta_id']));
            if ($request->except(['sub_toefl_preparation_peserta_id']) == 0) {
                $datas = array(
                    'nilai' => 0,
                    'jawaban_yang_benar_text' => '',
                    'jawaban_yang_benar_array_id' => '',
                    'jawaban_yang_benar_array_text' => '',
                    'kode' => 200
                );
            } else {
                // dd($request->except(['sub_toefl_preparation_peserta_id']));
                foreach ($request->except(['sub_toefl_preparation_peserta_id']) as $data => $value) {

                    // dd($data);

                    $explode_data_awal = explode("_", $data);
                    $latian_soal_id_awal = $explode_data_awal[1];

                    // dd($latian_soal_id_awal);

                    $tb_m_latihan_soal_get_awal = DB::table('m_latihan_soal')
                        ->select(DB::raw("m_latihan_soal.*, m_latihan_jawaban.jawaban as m_latihan_jawaban_jawaban"))
                        ->where('m_latihan_soal.id', '=', $latian_soal_id_awal)
                        ->leftjoin('m_latihan_jawaban', 'm_latihan_jawaban.id', '=', 'm_latihan_soal.m_latihan_jawaban_id')
                        ->get();

                    // dd($tb_m_latihan_soal_get_awal);
                    // if (count($tb_m_latihan_soal_get_awal) == 0) {
                    //     dd('data tidak ada');
                    // }

                    if ($tb_m_latihan_soal_get_awal->first()->kategori_soal == 'PILIHAN_GANDA') {

                        $explode_value = explode("_", $value);
                        $latian_soal_id = $explode_value[0];
                        $latian_jawaban_id = $explode_value[1];

                        // dd($latian_soal_id);

                        $tb_m_latihan_soal = DB::table('m_latihan_soal')
                            ->select(DB::raw("m_latihan_soal.*, m_latihan_jawaban.jawaban as m_latihan_jawaban_jawaban"))
                            ->where('m_latihan_soal.id', '=', $latian_soal_id)
                            ->leftjoin('m_latihan_jawaban', 'm_latihan_jawaban.id', '=', 'm_latihan_soal.m_latihan_jawaban_id')
                            ->get();

                        $jawaban_yang_benar_array_id[$tb_m_latihan_soal->first()->id] = $tb_m_latihan_soal->first()->m_latihan_jawaban_id;
                        $jawaban_yang_benar_array_text[$tb_m_latihan_soal->first()->id] = $tb_m_latihan_soal->first()->m_latihan_jawaban_jawaban;

                        $jawaban_yang_benar_text .=  $tb_m_latihan_soal->first()->m_latihan_jawaban_jawaban . ' ';
                        if ($tb_m_latihan_soal->first()->m_latihan_jawaban_id == $latian_jawaban_id) {
                            $jumlah_jawaban_yang_benar += 1;
                        } else {
                            $jumlah_jawaban_yang_benar += 0;
                        }
                    } elseif ($tb_m_latihan_soal_get_awal->first()->kategori_soal == 'ESAI') {

                        $explode_data = explode("_", $data);
                        $latian_soal_id = $explode_data[1];
                        $latian_jawaban_id = $explode_data[2];

                        $tb_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                            ->select(DB::raw("m_latihan_jawaban.jawaban"))
                            ->where('m_latihan_jawaban.m_latihan_soal_id', '=', $latian_soal_id)
                            // ->where('m_latihan_jawaban.jawaban', 'ilike', '%' . $value . '%')
                            ->where('m_latihan_jawaban.jawaban', '=', $value)
                            ->get();

                        if (count($tb_m_latihan_jawaban) > 0) {
                            $jumlah_jawaban_yang_benar += 1;
                        } else {
                            $jumlah_jawaban_yang_benar += 0;
                        }

                        $tb_m_latihan_jawaban_benar_semua_per_soal = DB::table('m_latihan_jawaban')
                            ->select(DB::raw("m_latihan_jawaban.jawaban"))
                            ->where('m_latihan_jawaban.m_latihan_soal_id', '=', $latian_soal_id)
                            ->get();

                        $jawaban_yang_benar_text_esai = '';
                        $jawaban_yang_benar_text_esai_penyambung = '';
                        $no_bantuan_esai = 0;
                        foreach ($tb_m_latihan_jawaban_benar_semua_per_soal as $datass) {
                            $no_bantuan_esai++;
                            if (count($tb_m_latihan_jawaban_benar_semua_per_soal) == $no_bantuan_esai) {
                                $jawaban_yang_benar_text_esai_penyambung = '';
                            } else {
                                $jawaban_yang_benar_text_esai_penyambung = ', ';
                            }
                            $jawaban_yang_benar_text_esai .=  $datass->jawaban . $jawaban_yang_benar_text_esai_penyambung;
                        }

                        $jawaban_yang_benar_array_text[$tb_m_latihan_soal_get_awal->first()->id] = $jawaban_yang_benar_text_esai;
                    }

                    // dd($tb_m_latihan_soal);
                    // // dd($tb_m_latihan_soal->first()->m_latihan_jawaban_id . '  ' . $latian_jawaban_id);
                    // $jawaban_yang_benar_array_id[]= array(
                    //     $tb_m_latihan_soal->first()->id => $tb_m_latihan_soal->first()->m_latihan_jawaban_id,
                    // );

                    // $jawaban_yang_benar_array_text[]= array(
                    //     $tb_m_latihan_soal->first()->id => $tb_m_latihan_soal->first()->m_latihan_jawaban_jawaban,
                    // );


                }

                $jumlah_soal = DB::table('m_latihan_soal')
                    ->select(DB::raw("m_latihan_soal.*"))
                    ->where('m_latihan_soal.m_sub_toefl_preparation_id', '=', $request->sub_toefl_preparation_peserta_id)
                    ->get();

                // dd(count($jumlah_soal));

                $nilai = $jumlah_jawaban_yang_benar / count($jumlah_soal) * 100;

                $jml_soal_total = count($jumlah_soal);
                $jml_jwb_benar = $jumlah_jawaban_yang_benar;
                $jml_jwb_salah = $jml_soal_total - $jml_jwb_benar;
                // dd($nilai);
                $datas = array(
                    'jml_soal_total' => $jml_soal_total,
                    'jml_jwb_benar' => $jml_jwb_benar,
                    'jml_jwb_salah' => $jml_jwb_salah,
                    'nilai' => $nilai,
                    'jawaban_yang_benar_text' => $jawaban_yang_benar_text,
                    'jawaban_yang_benar_array_id' => $jawaban_yang_benar_array_id,
                    'jawaban_yang_benar_array_text' => $jawaban_yang_benar_array_text,
                    'kode' => 200
                );
            }



            // dd($datas);

            return response()->json(compact("datas"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function sub_toefl_preparation_peserta_id_sebelumnya_prev_forw(Request $request)
    {
        // dd($request->all());
        $m_sub_toefl_preparation_id = $request->m_sub_toefl_preparation_id;
        $nama_sub_toefl_preparation = $request->nama_sub_toefl_preparation;
        // dd($nama_sub_toefl_preparation);
        $nama_sub_toefl_preparation_int = (int) filter_var($nama_sub_toefl_preparation, FILTER_SANITIZE_NUMBER_INT);
        // dd($nama_sub_toefl_preparation_int);
        $m_toefl_preparation_id = $request->m_toefl_preparation_id;
        // dd($m_toefl_preparation_id .' - '. $nama_sub_toefl_preparation);

        $m_sub_toefl_preparation_prev = DB::table('m_sub_toefl_preparation')
            ->select(DB::raw("m_sub_toefl_preparation.*"))
            // ->where('m_sub_toefl_preparation.id', '<', $m_sub_toefl_preparation_id)
            ->whereRaw("SUBSTRING(m_sub_toefl_preparation.nama_sub_toefl_preparation FROM '([0-9]+)')::BIGINT < $nama_sub_toefl_preparation_int")
            ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $m_toefl_preparation_id)
            ->orderByRaw("m_sub_toefl_preparation.no_urut DESC")
            ->get();

        $m_sub_toefl_preparation_forw = DB::table('m_sub_toefl_preparation')
            ->select(DB::raw("m_sub_toefl_preparation.*"))
            // ->where('m_sub_toefl_preparation.id', '>', $m_sub_toefl_preparation_id)
            ->whereRaw("SUBSTRING(m_sub_toefl_preparation.nama_sub_toefl_preparation FROM '([0-9]+)')::BIGINT > $nama_sub_toefl_preparation_int")
            ->where('m_sub_toefl_preparation.m_toefl_preparation_id', '=', $m_toefl_preparation_id)
            ->orderByRaw("m_sub_toefl_preparation.no_urut ASC")
            ->get();

        // dd($m_sub_toefl_preparation_prev[1]);

        // dd($m_sub_toefl_preparation_forw);

        $m_sub_toefl_preparation_prev_id = "";
        $m_sub_toefl_preparation_prev_nama = "";
        $m_sub_toefl_preparation_forw_id = "";
        $m_sub_toefl_preparation_forw_nama = "";

        // dd($m_sub_toefl_preparation_prev);
        // dd(count($m_sub_toefl_preparation_prev) .' '. count($m_sub_toefl_preparation_forw));

        if (count($m_sub_toefl_preparation_prev) == 0 && count($m_sub_toefl_preparation_forw) > 0) {
            $m_sub_toefl_preparation_prev_id = $m_sub_toefl_preparation_id;
            $m_sub_toefl_preparation_prev_nama = $nama_sub_toefl_preparation;
            $m_sub_toefl_preparation_forw_id = $m_sub_toefl_preparation_forw[0]->id;
            $m_sub_toefl_preparation_forw_nama = $m_sub_toefl_preparation_forw[0]->nama_sub_toefl_preparation;
            // dd('1');
        } else if (count($m_sub_toefl_preparation_prev) > 0 && count($m_sub_toefl_preparation_forw) == 0) {
            $m_sub_toefl_preparation_prev_id = $m_sub_toefl_preparation_prev[0]->id;
            $m_sub_toefl_preparation_prev_nama = $m_sub_toefl_preparation_prev[0]->nama_sub_toefl_preparation;
            $m_sub_toefl_preparation_forw_id = $m_sub_toefl_preparation_id;
            $m_sub_toefl_preparation_forw_nama = $nama_sub_toefl_preparation;
            // dd('2');
        } else if (count($m_sub_toefl_preparation_forw) == 0 && count($m_sub_toefl_preparation_prev) > 0) {
            $m_sub_toefl_preparation_prev_id = $m_sub_toefl_preparation_prev[0]->id;
            $m_sub_toefl_preparation_prev_nama = $m_sub_toefl_preparation_prev[0]->nama_sub_toefl_preparation;
            $m_sub_toefl_preparation_forw_id = $m_sub_toefl_preparation_id;
            $m_sub_toefl_preparation_forw_nama = $nama_sub_toefl_preparation;
            // dd('3');
        } else if (count($m_sub_toefl_preparation_forw) > 0 && count($m_sub_toefl_preparation_prev) == 0) {
            $m_sub_toefl_preparation_prev_id = $m_sub_toefl_preparation_id;
            $m_sub_toefl_preparation_prev_nama = $nama_sub_toefl_preparation;
            $m_sub_toefl_preparation_forw_id = $m_sub_toefl_preparation_forw[0]->id;
            $m_sub_toefl_preparation_forw_nama = $m_sub_toefl_preparation_forw[0]->nama_sub_toefl_preparation;
            // dd('4');
        } else {
            dd('5');
        }

        $datas = array(
            'm_sub_toefl_preparation_prev_id' => $m_sub_toefl_preparation_prev_id,
            'm_sub_toefl_preparation_prev_nama' => $m_sub_toefl_preparation_prev_nama,
            'm_sub_toefl_preparation_forw_id' => $m_sub_toefl_preparation_forw_id,
            'm_sub_toefl_preparation_forw_nama' => $m_sub_toefl_preparation_forw_nama,
            'kode' => 200
        );
        // dd($datas);

        return response()->json(compact("datas"));
    }

    public function download_dokumen(Request $request, $id_dokumnen)
    {
        // dd($request->all());

        // foreach ($request->array_dokumen_file as $datas) {
        //     $nama_dokumen_download = $datas['dokumen_file'];
        //     // dd($nama_dokumen_download);
        //     // $file = public_path() . "/file_dokumen_tp/toeflmateri4543.pdf";
        //     $file = public_path() . "/file_dokumen_tp/" . $nama_dokumen_download;
        //     $headers = array(
        //         'Content-Type: application/pdf',
        //     );
        // }
        // return Response::download($file, 'filename.pdf', $headers);

        $m_sub_toefl_preparation_dokumen = DB::table('m_sub_toefl_preparation_dokumen')
            ->select(DB::raw("m_sub_toefl_preparation_dokumen.*"))
            ->where('m_sub_toefl_preparation_dokumen.id', '=', $id_dokumnen)
            ->get()
            ->first();

        $file = public_path() . "/file_dokumen_tp/$m_sub_toefl_preparation_dokumen->dokumen_file";
        $headers = array(
            'Content-Type: application/pdf',
        );
        return Response::download($file, $m_sub_toefl_preparation_dokumen->dokumen_file, $headers);



        // $zip = new \ZipArchive();
        // $zipFileName = 'MateriToefl' . rand(10, 100) . '.zip';

        // if ($zip->open(public_path($zipFileName), \ZipArchive::CREATE) === TRUE) {
        //     $filesToZip = [];
        //     foreach ($request->array_dokumen_file as $datas) {
        //         $filesToZip[] = public_path() . "/file_dokumen_tp/" . $datas['dokumen_file'];
        //     }
        //     foreach ($filesToZip as $file) {
        //         $zip->addFile($file, basename($file));
        //     }
        //     $zip->close();
        //     return response()->download(public_path($zipFileName))->deleteFileAfterSend(true);
        // } else {
        //     return "Failed to create the zip file.";
        // }
    }
}
