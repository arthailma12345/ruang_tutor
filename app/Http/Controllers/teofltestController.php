<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;

class teofltestController extends Controller
{
    public function index()
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            return view('pages.teofl_test.index');
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getDataPaket(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $limit = is_null($request["length"]) ? 25 : $request["length"];
            $offset = is_null($request["start"]) ? 0 : $request["start"];
            $dirs = array("asc", "desc");
            $draw = $request["draw"];
            $searchs = $request["search.value"];
            $resultData = array();
            $data_arr    = [
                'limit' => $limit,
                'offset' => $offset,
                'searchs' => $searchs,
                'dirs' => $dirs,
            ];

            $tb_paket = DB::table('tb_paket_teofl_test')
                ->select(DB::raw("tb_paket_teofl_test.*"));

            $total_data = $tb_paket->count();

            $paket = $tb_paket
                ->limit($limit)
                ->offset($offset)
                ->get();

            $datas = [];

            $no = $offset + 1;

            if (count($paket) > 0) {

                foreach ($paket as $value) {
                    $datas[] = array(
                        'no' => $no++,
                        'idpaket' => $value->id_paket,
                        'nama' => $value->nama_paket,
                        'aksi' =>
                        '<button type="button" name="idpakettambahmateri" id="idpakettambahmateri" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormTambahMateri" data-id_paket="' . $value->id_paket . '" href="' . $value->id_paket . '"> <i class="dripicons-plus"></i> Tambah Materi </button> &nbsp;' .

                            '<button type="button" name="lihatmateri" id="lihatmateri" class="btn btn-info waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormLihatMateri" data-nama_paket="' . $value->nama_paket . '" data-id_paket="' . $value->id_paket . '" href="' . $value->id_paket . '"> <i class="dripicons-preview"></i> Lihat Materi </button> &nbsp;' .

                            '<button type="button" name="edit" id="edit" class="btn btn-warning waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormEditPaket" data-nama_paket="' . $value->nama_paket . '" data-id_paket="' . $value->id_paket . '" href="' . $value->id_paket . '"> <i class="dripicons-pencil"></i> Edit Paket </button> &nbsp;' .

                            '<button type="button" name="delete" id="delete" data-id_paket="' . $value->id_paket . '" class="btn btn-danger btn-xs" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button>'
                    );
                }
            } else {
                $datas = array();
            }

            $recordsTotal = is_null($total_data) ? 0 : $total_data;
            $recordsFiltered = is_null($total_data) ? 0 : $total_data;
            $data = $datas;

            return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getDataMateri(Request $request, $id_paket)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $limit = is_null($request["length"]) ? 25 : $request["length"];
            $offset = is_null($request["start"]) ? 0 : $request["start"];
            $dirs = array("asc", "desc");
            $draw = $request["draw"];
            $searchs = $request["search.value"];
            $resultData = array();
            $data_arr    = [
                'limit' => $limit,
                'offset' => $offset,
                'searchs' => $searchs,
                'dirs' => $dirs,
            ];

            $tb_materi = DB::table('tb_paket_teofl_test')
                ->select(DB::raw("
            tb_paket_teofl_test.id_paket,
            tb_paket_materi_teofl_test.id_materi,
            tb_paket_materi_teofl_test.id_paket,
            tb_paket_materi_teofl_test.judul_materi,
            tb_paket_materi_teofl_test.video_materi"))
                ->join('tb_paket_materi_teofl_test', 'tb_paket_materi_teofl_test.id_paket', '=', 'tb_paket_teofl_test.id_paket')
                ->where('tb_paket_teofl_test.id_paket', '=',  $id_paket);

            $total_data = $tb_materi->count();

            $paket = $tb_materi
                ->limit($limit)
                ->offset($offset)
                ->get();

            $datas = [];

            // dd($total_data);

            $no = $offset + 1;

            if (count($paket) > 0) {

                foreach ($paket as $value) {
                    $datas[] = array(
                        'no' => $no++,
                        'judul' => $value->judul_materi,
                        'pertinjau' => $value->video_materi,
                        'aksi' =>

                        '<a href="' . url("study_room/getEditMateri/$value->id_materi") . '" class="btn btn-warning btn-xs"><i class="dripicons-pencil"></i> Edit </a> &nbsp;' .

                            '<button type="button" name="deletemateri" id="deletemateri" data-id_materi="' . $value->id_materi . '" class="btn btn-danger btn-xs" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button> &nbsp;'
                    );
                }
            } else {
                $datas = array();
            }

            // dd($datas);

            $recordsTotal = is_null($total_data) ? 0 : $total_data;
            $recordsFiltered = is_null($total_data) ? 0 : $total_data;
            $data = $datas;

            return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getEditMateri($id_materi)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $tb_materi = DB::table('tb_paket_teofl_test')
                ->select(DB::raw("
            tb_paket_teofl_test.id_paket,
            tb_paket_teofl_test.nama_paket,
            tb_paket_materi_teofl_test.id_materi,
            tb_paket_materi_teofl_test.id_paket,
            tb_paket_materi_teofl_test.judul_materi,
            tb_paket_materi_teofl_test.video_materi"))
                ->join('tb_paket_materi_teofl_test', 'tb_paket_materi_teofl_test.id_paket', '=', 'tb_paket_teofl_test.id_paket')
                ->where('tb_paket_materi_teofl_test.id_materi', '=',  $id_materi)
                ->get();

            $data['id_materi'] = $id_materi;
            $data['tb_materi'] = $tb_materi;
            // dd($data);

            return view('pages.teofl_test.edit_materi', $data);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function updateMateri(Request $request)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $id_materi = $request->id_materi_edit;
            $judul_materi = $request->judul_materi;
            $video_materi = $request->video_materi;

            $videoURL = $video_materi;
            $convertedURL = str_replace("watch?v=", "embed/", $videoURL);

            $update_paket = DB::table('tb_paket_materi_teofl_test')
                ->where('tb_paket_materi_teofl_test.id_materi', '=', $id_materi)
                ->update([
                    'judul_materi' => $judul_materi,
                    'video_materi' => $convertedURL,
                ]);

            $response = [
                "message" => "data berhasil diupdate",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];
            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function tambah(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $nama = $request->nama;

            $tambah_paket = DB::table('tb_paket_teofl_test')
                ->insert([
                    'nama_paket' => $nama
                ]);

            $response = [
                "message" => "data berhasil ditambah",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];

            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function tambahmateri(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $id_paket = $request->id_paket_materi;
            $judul_materi = $request->judul_materi;
            $video_materi = $request->video_materi;

            // $videoURL = $video_materi;
            // $convertedURL = str_replace("watch?v=", "embed/", $videoURL);

            $tambah_paket = DB::table('tb_paket_materi_teofl_test')
                ->insert([
                    'judul_materi' => $judul_materi,
                    'video_materi' => $video_materi,
                    'id_paket' => $id_paket,
                ]);

            $response = [
                "message" => "data berhasil ditambah",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];

            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function update(Request $request)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $id_paket_edit = $request->id_paket_edit;
            $nama_paket_edit = $request->nama_paket_edit;

            $update_paket = DB::table('tb_paket_teofl_test')
                ->where('tb_paket_teofl_test.id_paket', '=', $id_paket_edit)
                ->update([
                    'nama_paket' => $nama_paket_edit,
                ]);

            $response = [
                "message" => "data berhasil diupdate",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];
            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function delete(Request $request, $id)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $delete_paket = DB::table('tb_paket_teofl_test')
                ->where('tb_paket_teofl_test.id_paket', '=', $id)
                ->delete();
            $delete_materi = DB::table('tb_paket_materi_teofl_test')
                ->where('tb_paket_materi_teofl_test.id_paket', '=', $id)
                ->delete();

            $response = [
                "message" => "data berhasil diupdate",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];

            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function deletemateri(Request $request, $id)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {
            $delete_paket = DB::table('tb_paket_materi_teofl_test')
                ->where('tb_paket_materi_teofl_test.id_materi', '=', $id)
                ->delete();

            $response = [
                "message" => "data berhasil diupdate",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];

            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }
}
