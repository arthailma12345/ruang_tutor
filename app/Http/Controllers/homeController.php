<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;

class homeController extends Controller
{
    public function index()
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $datesaatini = date('Y-m-d');

            $total_peserta = DB::table('users')->where('role','=',"R003")->count();

            $total_paket_tt = DB::table('tb_paket_teofl_test')->count();

            $total_paket_tp = DB::table('m_toefl_preparation')->count();

            $total_peserta_aktif = DB::table('users')
            ->select(DB::raw('users.tanggal_expired'))
            ->where('users.tanggal_expired', '>', $datesaatini)
            ->get();
            $count_peserta_aktif = $total_peserta_aktif->count();

            $total_peserta_non_aktif = DB::table('users')
            ->select(DB::raw('users.tanggal_expired'))
            ->where('users.role', '=', 'R003')
            ->where('users.tanggal_expired', '<', $datesaatini)
            
            ->get();
            $count_peserta_non_aktif = $total_peserta_non_aktif->count();

            return view('pages.home.index',compact("total_peserta", "count_peserta_aktif", "count_peserta_non_aktif", "total_paket_tt", "total_paket_tp"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }
}
