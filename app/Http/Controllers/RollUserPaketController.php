<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

class RollUserPaketController extends Controller
{
    public function index()
    {
        $tb_paket_teofl_test = DB::table('tb_paket_teofl_test')
            ->select(DB::raw("tb_paket_teofl_test.*"))
            ->orderBy('tb_paket_teofl_test.id_paket', 'DESC')
            ->get();

        $m_toefl_preparation = DB::table('m_toefl_preparation')
            ->select(DB::raw("m_toefl_preparation.*"))
            ->orderBy('m_toefl_preparation.id', 'DESC')
            ->get();
        return view('pages.user.RollUserPaket', compact('tb_paket_teofl_test', 'm_toefl_preparation'));
    }

    public function getDataUser(Request $request)
    {

        $limit = is_null($request["length"]) ? 25 : $request["length"];
        $offset = is_null($request["start"]) ? 0 : $request["start"];
        $dirs = array("asc", "desc");
        $draw = $request["draw"];
        $searchs = $request["search.value"];
        $resultData = array();
        $data_arr    = [
            'limit' => $limit,
            'offset' => $offset,
            'searchs' => $searchs,
            'dirs' => $dirs,
        ];

        $Username_Search = is_null($request->Username) ? '' : $request->Username;
        $Jumlah_Search = is_null($request->Jumlah) ? '' : $request->Jumlah;
        
        // dd($Jumlah_Search);

        // $tb_paket_teofl_test = DB::table('tb_paket_teofl_test')
        //     ->select(DB::raw("count(tb_roll_akses_teofl_test.id_paket)"))
        //     ->orderBy('tb_paket_teofl_test.id_paket', 'DESC')
        //     ->get();

        // $tb_roll_paket_user = DB::table('users')
        //     ->select(DB::raw("users.id, users.username, count(tb_roll_akses_teofl_test.id_paket) as jumlah_paket"))
        //     ->leftJoin('tb_roll_akses_teofl_test', 'tb_roll_akses_teofl_test.id_username', '=', 'users.id')
        //     ->groupBy('users.username')
        //     ->groupBy('users.id')
        //     ->orWhere(function ($query) use (
        //         $Username_Search,
        //         $Jumlah_Search
        //     ) {
        //         $query
        //             ->where('users.username', 'ilike', '%' . $Username_Search . '%')
        //             // ->orHavingRaw("count(tb_roll_akses_teofl_test.id_paket) > $Jumlah_Search");
        //             ->having('count(tb_roll_akses_teofl_test.id_paket', 'like', "%$Jumlah_Search%");
        //     });

            $peserta = "R003";
            $tb_roll_paket_user = DB::table('users')
            ->select(DB::raw("users.id, users.username, users.role"))
            ->where('users.role', '=', $peserta)
            ->Where(function ($query) use (
                $Username_Search
            ) {
                $query
                    ->where('users.username', 'ilike', '%' . $Username_Search . '%');
            });

        $total_data = $tb_roll_paket_user->count();

        $akses_paket = $tb_roll_paket_user
            ->limit($limit)
            ->offset($offset)
            ->get();

        $datas = [];

        $no = $offset + 1;

        if (count($akses_paket) > 0) {

            foreach ($akses_paket as $value) {

                $tb_roll_akses_teofl_test = DB::table('tb_roll_akses_teofl_test')
                    ->select(DB::raw("count(tb_roll_akses_teofl_test.id_paket) as jumlah"))
                    ->where('tb_roll_akses_teofl_test.id_username', '=', $value->id)
                    ->get();

                    $tb_roll_akses_teofl_preparation = DB::table('tb_roll_akses_teofl_preparation')
                    ->select(DB::raw("count(tb_roll_akses_teofl_preparation.id_paket) as jumlah2"))
                    ->where('tb_roll_akses_teofl_preparation.id_username', '=', $value->id)
                    ->get();

                $datas[] = array(

                    'no' => $no++,
                    'username' => $value->username,
                    'jumlah' => $tb_roll_akses_teofl_test->first()->jumlah,
                    'jumlah2' => $tb_roll_akses_teofl_preparation->first()->jumlah2,
                    'aksi' =>
                    '<button type="button" name="lihat" id="lihat" class="btn btn-info waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormLihatPaket" data-id="' . $value->id . '" href=""> <i class="dripicons-preview"></i> Lihat Paket </button> &nbsp;' .

                        '<button type="button" name="getIDuser" id="getIDuser" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormTambahPaket" data-id="' . $value->id . '" href=""> <i class="dripicons-plus"></i> Tambah Paket </button>'
                );
            }
        } else {
            $datas = array();
        }

        $recordsTotal = is_null($total_data) ? 0 : $total_data;
        $recordsFiltered = is_null($total_data) ? 0 : $total_data;
        $data = $datas;

        return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
    }

    public function getDataLihatPaketTT(Request $request, $id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $limit = is_null($request["length"]) ? 25 : $request["length"];
            $offset = is_null($request["start"]) ? 0 : $request["start"];
            $dirs = array("asc", "desc");
            $draw = $request["draw"];
            $searchs = $request["search.value"];
            $resultData = array();
            $data_arr    = [
                'limit' => $limit,
                'offset' => $offset,
                'searchs' => $searchs,
                'dirs' => $dirs,
            ];

            $tb_paket = DB::table('tb_roll_akses_teofl_test')
                ->join('users', 'tb_roll_akses_teofl_test.id_username', '=', 'users.id')
                ->join('tb_paket_teofl_test', 'tb_roll_akses_teofl_test.id_paket', '=', 'tb_paket_teofl_test.id_paket')
                ->select('tb_roll_akses_teofl_test.*', 'users.id', 'users.username', 'tb_paket_teofl_test.id_paket', 'tb_paket_teofl_test.nama_paket')
                ->where('tb_roll_akses_teofl_test.id_username', '=',  $id);

            $total_data = $tb_paket->count();

            $paket = $tb_paket
                ->limit($limit)
                ->offset($offset)
                ->get();

            $datas = [];

            $no = $offset + 1;

            if (count($paket) > 0) {

                foreach ($paket as $value) {
                    $datas[] = array(
                        'no' => $no++,
                        'namapakett' => $value->nama_paket,
                        'aksi' =>
                        '<button type="button" name="deletepakettt" id="deletepakettt" data-id="' . $value->id_roll_akses_tt . '" class="btn btn-danger btn-xs" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button> &nbsp;'
                    );
                }
            } else {
                $datas = array();
            }

            $recordsTotal = is_null($total_data) ? 0 : $total_data;
            $recordsFiltered = is_null($total_data) ? 0 : $total_data;
            $data = $datas;

            return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getDataLihatPaketTP(Request $request, $id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $limit = is_null($request["length"]) ? 25 : $request["length"];
            $offset = is_null($request["start"]) ? 0 : $request["start"];
            $dirs = array("asc", "desc");
            $draw = $request["draw"];
            $searchs = $request["search.value"];
            $resultData = array();
            $data_arr    = [
                'limit' => $limit,
                'offset' => $offset,
                'searchs' => $searchs,
                'dirs' => $dirs,
            ];

            $tb_paket = DB::table('tb_roll_akses_teofl_preparation')
                ->join('users', 'tb_roll_akses_teofl_preparation.id_username', '=', 'users.id')
                ->join('m_toefl_preparation', 'tb_roll_akses_teofl_preparation.id_paket', '=', 'm_toefl_preparation.id')
                ->select('tb_roll_akses_teofl_preparation.*', 'users.id', 'users.username', 'm_toefl_preparation.id', 'm_toefl_preparation.nama_toefl_preparation')
                ->where('tb_roll_akses_teofl_preparation.id_username', '=',  $id);

            $total_data = $tb_paket->count();

            $paket = $tb_paket
                ->limit($limit)
                ->offset($offset)
                ->get();

            $datas = [];

            $no = $offset + 1;

            if (count($paket) > 0) {

                foreach ($paket as $value) {
                    $datas[] = array(
                        'no' => $no++,
                        'namapaketp' => $value->nama_toefl_preparation,
                        'aksi' =>
                        '<button type="button" name="deletepakettp" id="deletepakettp" data-id="' . $value->id_roll_akses_tp . '" class="btn btn-danger btn-xs" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button> &nbsp;'
                    );
                }
            } else {
                $datas = array();
            }

            $recordsTotal = is_null($total_data) ? 0 : $total_data;
            $recordsFiltered = is_null($total_data) ? 0 : $total_data;
            $data = $datas;

            return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function deletepakettt(Request $request, $id)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {
            $delete_paket = DB::table('tb_roll_akses_teofl_test')
                ->where('tb_roll_akses_teofl_test.id_roll_akses_tt', '=', $id)
                ->delete();

            $response = [
                "message" => "data berhasil diupdate",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];

            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function deletepakettp(Request $request, $id)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {
            $delete_paket = DB::table('tb_roll_akses_teofl_preparation')
                ->where('tb_roll_akses_teofl_preparation.id_roll_akses_tp', '=', $id)
                ->delete();

            $response = [
                "message" => "data berhasil diupdate",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];

            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function tambahceklis1(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $id_username_roll = $request->id_username_roll;
            $paket_toefl_test = $request->paket_toefl_test;

            // dd('Id_username : ',$id_username_roll,'Nama Paket : ',$paket_toefl_test);

            $tambah_roll_paket = DB::table('tb_roll_akses_teofl_test')
                ->insert([
                    'id_username' => $id_username_roll,
                    'id_paket' => $paket_toefl_test,
                ]);

            $response = [
                "message" => "data berhasil ditambah",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];

            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function tambahceklis2(Request $request)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        // dd($request->all());

        if (validateSessionToken($get_session_token)) {

            $id_username_roll_2 = $request->id_username_roll_2;
            $nama_toefl_preparation = $request->nama_toefl_preparation;

            // dd('Id_username : ',$id_username_roll_2,'Nama Paket : ',$paket_toefl_test);

            $tambah_roll_paket = DB::table('tb_roll_akses_teofl_preparation')
                ->insert([
                    'id_username' => $id_username_roll_2,
                    'id_paket' => $nama_toefl_preparation,
                ]);

            $response = [
                "message" => "data berhasil ditambah",
                "kode"    => 201,
                "result"  => [
                    "token" => '',
                ]
            ];

            return response()->json($response, $response['kode']);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }
}
