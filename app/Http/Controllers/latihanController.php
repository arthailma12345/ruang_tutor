<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;

class latihanController extends Controller
{

    public function getDataLatihanSoal(Request $request, $sub_toefl_preparation_id)
    {

        // dd($sub_toefl_preparation_id);

        $limit = is_null($request["length"]) ? 25 : $request["length"];
        $offset = is_null($request["start"]) ? 0 : $request["start"];
        $dirs = array("asc", "desc");
        $draw = $request["draw"];
        $searchs = $request["search.value"];
        $resultData = array();
        $data_arr    = [
            'limit' => $limit,
            'offset' => $offset,
            'searchs' => $searchs,
            'dirs' => $dirs,
        ];

        $tb_m_latihan_soal = DB::table('m_latihan_soal')
            ->select(DB::raw("m_latihan_soal.*"))
            ->where('m_latihan_soal.m_sub_toefl_preparation_id', '=', $sub_toefl_preparation_id)
            ->orderBy('m_latihan_soal.created_at', 'ASC');

        // $datax['total_data'] = $tb_users->first();
        $total_data = $tb_m_latihan_soal->count();


        $m_latihan_soal = $tb_m_latihan_soal
            ->limit($limit)
            ->offset($offset)
            ->get();

        $datas = [];

        // $abjad = 'A';
        // $noabjad = chr(ord($abjad));
        $no = $offset + 1;

        if (count($m_latihan_soal) > 0) {

            foreach ($m_latihan_soal as $value) {
                // dd($value['nama']);

                $get_data_kunci_jawaban = DB::table('m_latihan_soal')
                    ->select(DB::raw("m_latihan_jawaban.huruf as m_latihan_jawaban_huruf, m_latihan_jawaban.jawaban as m_latihan_jawaban_jawaban, m_latihan_soal.id, m_latihan_soal.soal, m_latihan_soal.kategori_soal"))
                    ->where('m_latihan_soal.id', '=', $value->id)
                    ->leftJoin('m_latihan_jawaban', 'm_latihan_jawaban.id', '=', 'm_latihan_soal.m_latihan_jawaban_id')
                    ->get();

                $status_deskripsi = '';
                if ($value->deskripsi == null) {
                    $status_deskripsi = 'belum ada';
                } else {
                    $status_deskripsi = 'sudah ada';
                }

                $datas[] = array(

                    'no' => $no++,
                    'soal' => $value->soal,
                    'soal_point' => $value->soal_point,
                    'deskripsi' => $status_deskripsi,
                    'kategori_soal' => $value->kategori_soal,
                    'aksi' =>

                    '<button type="button" name="jawaban" id="jawaban" data-latihan_soal_id="' . $value->id . '"  data-m_latihan_jawaban_huruf="' . $get_data_kunci_jawaban->first()->m_latihan_jawaban_huruf . '" data-m_latihan_jawaban_jawaban="' . $get_data_kunci_jawaban->first()->m_latihan_jawaban_jawaban . '" data-m_latihan_kategori_soal_view="' . $get_data_kunci_jawaban->first()->kategori_soal . '" class="btn btn-info btn-xs" href=' . '' . '><i class="dripicons-checklist"></i> Jawaban </button> &nbsp;' .

                        '<button type="button" name="editsoallatihan" id="editsoallatihan" data-soal_id="' . $value->id . '" class="btn btn-warning btn-xs" href=' . '' . '><i class="dripicons-pencil"></i> Edit </button> &nbsp;' .

                        '<button type="button" name="tambahdeskripsi" id="tambahdeskripsi" data-soal_id="' . $value->id . '" class="btn btn-secondary btn-xs" href=' . '' . '><i class="dripicons-clipboard"></i> Tambah Deskripsi </button> &nbsp;' .

                        // '<button type="button" name="edit" id="edit" class="btn btn-warning waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormEditPaket" data-soal="' . $value->soal . '" data-id="' . $value->id . '" href="' . $value->id . '"> <i class="dripicons-pencil"></i> Edit </button> &nbsp;' .

                        '<button type="button" name="hapus_soal_jawaban" id="hapus_soal_jawaban" data-hapus_soal_jawaban_id="' . $value->id . '" class="btn btn-danger btn-xs" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button>'
                );
            }
        } else {
            $datas = array();
        }

        // dd($datas);
        $recordsTotal = is_null($total_data) ? 0 : $total_data;
        $recordsFiltered = is_null($total_data) ? 0 : $total_data;
        $data = $datas;

        return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
    }

    public function tambahLatihanMateriReading(Request $request)
    {
        // dd($request->all());
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $m_sub_toefl_preparation_id_latihan = $request->m_sub_toefl_preparation_id_latihan;
        $latihan_materi_reading = $request->soal_reading;

        if (validateSessionToken($get_session_token)) {

            $tb_reading_materi = DB::table('tb_reading_materi')
                ->select(DB::raw("tb_reading_materi.*"))
                ->get();

            $tb_reading_materi_count = count($tb_reading_materi);

            if ($tb_reading_materi_count == 0) {

                $tambah_sub_toefl_preparation = DB::table('tb_reading_materi')
                    ->insert([
                        'id' => 'R000001',
                        'm_sub_toefl_preparation_id' => $m_sub_toefl_preparation_id_latihan,
                        'text_reading' => $latihan_materi_reading,
                        'created_at' => Carbon::now()
                    ]);

                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            } else {
                $tb_reading_materi_get_id_terakhir = DB::table('tb_reading_materi')
                    ->select(DB::raw("tb_reading_materi.*"))
                    ->orderby('tb_reading_materi.id', 'DESC')
                    ->get()
                    ->first();

                $tb_reading_materi_m_sub_toefl_preparation_id = DB::table('tb_reading_materi')
                    ->select(DB::raw("tb_reading_materi.*"))
                    ->where('tb_reading_materi.m_sub_toefl_preparation_id', '=', $m_sub_toefl_preparation_id_latihan)
                    ->get();

                if (count($tb_reading_materi_m_sub_toefl_preparation_id) == 0) {

                    $tambah_m_latihan_materi = DB::table('tb_reading_materi')
                        ->insert([
                            'id' => next_value_nuber_6_digit($tb_reading_materi_get_id_terakhir->id),
                            'm_sub_toefl_preparation_id' => $m_sub_toefl_preparation_id_latihan,
                            'text_reading' => $latihan_materi_reading,
                            'created_at' => Carbon::now()
                        ]);
                } else {

                    $update_m_latihan_materi = DB::table('tb_reading_materi')
                        ->where('tb_reading_materi.m_sub_toefl_preparation_id', '=', $m_sub_toefl_preparation_id_latihan)
                        ->update([
                            'text_reading' => $latihan_materi_reading,
                        ]);
                }


                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            }
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        };
    }

    public function tambahLatihanMateri(Request $request)
    {
        // dd($request->all());
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $m_sub_toefl_preparation_id_latihan = $request->m_sub_toefl_preparation_id_latihan;
        $latihan_materi_url_video = $request->latihan_materi_url_video;

        if (validateSessionToken($get_session_token)) {

            $videoURL = $latihan_materi_url_video;
            //$convertedURL = str_replace("watch?v=", "embed/", $videoURL);

            $tb_m_latihan_materi = DB::table('m_latihan_materi')
                ->select(DB::raw("m_latihan_materi.*"))
                // ->where('m_latihan_materi.m_sub_toefl_preparation_id', '=', $m_sub_toefl_preparation_id_latihan)
                ->get();

            $tb_m_latihan_materi_count = count($tb_m_latihan_materi);

            if ($tb_m_latihan_materi_count == 0) {

                $tambah_sub_toefl_preparation = DB::table('m_latihan_materi')
                    ->insert([
                        'id' => 'M000001',
                        'm_sub_toefl_preparation_id' => $m_sub_toefl_preparation_id_latihan,
                        'url_vdeo_latihan_materi' => $videoURL,
                        'created_at' => Carbon::now()
                    ]);

                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            } else {
                $tb_m_latihan_materi_get_id_terakhir = DB::table('m_latihan_materi')
                    ->select(DB::raw("m_latihan_materi.*"))
                    ->orderby('m_latihan_materi.id', 'DESC')
                    // ->limit(1)
                    ->get()
                    ->first();

                $tb_m_latihan_materi_m_sub_toefl_preparation_id = DB::table('m_latihan_materi')
                    ->select(DB::raw("m_latihan_materi.*"))
                    ->where('m_latihan_materi.m_sub_toefl_preparation_id', '=', $m_sub_toefl_preparation_id_latihan)
                    ->get();

                if (count($tb_m_latihan_materi_m_sub_toefl_preparation_id) == 0) {

                    $tambah_m_latihan_materi = DB::table('m_latihan_materi')
                        ->insert([
                            'id' => next_value_nuber_6_digit($tb_m_latihan_materi_get_id_terakhir->id),
                            'm_sub_toefl_preparation_id' => $m_sub_toefl_preparation_id_latihan,
                            'url_vdeo_latihan_materi' => $videoURL,
                            'created_at' => Carbon::now()
                        ]);
                } else {

                    $update_m_latihan_materi = DB::table('m_latihan_materi')
                        ->where('m_latihan_materi.m_sub_toefl_preparation_id', '=', $m_sub_toefl_preparation_id_latihan)
                        ->update([
                            'url_vdeo_latihan_materi' => $videoURL,
                        ]);
                }


                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            }
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        };
    }

    public function tambahPembahasanMateri(Request $request)
    {
        // dd($request->all());
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $m_sub_toefl_preparation_id_latihan = $request->m_sub_toefl_preparation_id_latihan;
        $pembahasan_materi_url_video = $request->pembahasan_materi_url_video;


        if (validateSessionToken($get_session_token)) {

            $videoURL = $pembahasan_materi_url_video;
            //$convertedURL = str_replace("watch?v=", "embed/", $videoURL);

            $tb_m_pembahasan_materi = DB::table('m_pembahasan_materi')
                ->select(DB::raw("m_pembahasan_materi.*"))
                // ->where('m_latihan_materi.m_sub_toefl_preparation_id', '=', $m_sub_toefl_preparation_id_latihan)
                ->get();

            $tb_m_pembahasan_materi_count = count($tb_m_pembahasan_materi);

            if ($tb_m_pembahasan_materi_count == 0) {

                $tambah_sub_toefl_preparation = DB::table('m_pembahasan_materi')
                    ->insert([
                        'id' => 'M000001',
                        'm_sub_toefl_preparation_id' => $m_sub_toefl_preparation_id_latihan,
                        'url_vdeo_pembahasan_materi' => $videoURL,
                        'created_at' => Carbon::now()
                    ]);

                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            } else {
                $tb_m_pembahasan_materi_get_id_terakhir = DB::table('m_pembahasan_materi')
                    ->select(DB::raw("m_pembahasan_materi.*"))
                    ->orderby('m_pembahasan_materi.id', 'DESC')
                    // ->limit(1)
                    ->get()
                    ->first();

                $tb_m_pembahasan_materi_m_sub_toefl_preparation_id = DB::table('m_pembahasan_materi')
                    ->select(DB::raw("m_pembahasan_materi.*"))
                    ->where('m_pembahasan_materi.m_sub_toefl_preparation_id', '=', $m_sub_toefl_preparation_id_latihan)
                    ->get();

                if (count($tb_m_pembahasan_materi_m_sub_toefl_preparation_id) == 0) {

                    $tambah_m_pembahasan_materi = DB::table('m_pembahasan_materi')
                        ->insert([
                            'id' => next_value_nuber_6_digit($tb_m_pembahasan_materi_get_id_terakhir->id),
                            'm_sub_toefl_preparation_id' => $m_sub_toefl_preparation_id_latihan,
                            'url_vdeo_pembahasan_materi' => $videoURL,
                            'created_at' => Carbon::now()
                        ]);
                } else {

                    $update_m_pembahasan_materi = DB::table('m_pembahasan_materi')
                        ->where('m_pembahasan_materi.m_sub_toefl_preparation_id', '=', $m_sub_toefl_preparation_id_latihan)
                        ->update([
                            'url_vdeo_pembahasan_materi' => $videoURL,
                        ]);
                }


                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            }
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        };
    }

    public function tambahLatihanSoal(Request $request)
    {
        // dd($request->all());
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $m_sub_toefl_preparation_id_latihan = $request->m_sub_toefl_preparation_id_latihan;
        $soal = $request->soal;
        $kategori_soal = $request->kategori_soal;

        if (validateSessionToken($get_session_token)) {

            $tb_m_latihan_soal = DB::table('m_latihan_soal')
                ->select(DB::raw("m_latihan_soal.*"))
                ->get();

            $tb_m_latihan_soal_count = count($tb_m_latihan_soal);

            if ($tb_m_latihan_soal_count == 0) {
                $tambah_sub_toefl_preparation = DB::table('m_latihan_soal')
                    ->insert([
                        'id' => 'L000001',
                        'm_sub_toefl_preparation_id' => $m_sub_toefl_preparation_id_latihan,
                        'soal' => $soal,
                        'kategori_soal' => $kategori_soal,
                        'created_at' => Carbon::now()
                    ]);
                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            } else {
                $tb_m_latihan_soal_get_id_terakhir = DB::table('m_latihan_soal')
                    ->select(DB::raw("m_latihan_soal.*"))
                    ->orderby('m_latihan_soal.id', 'DESC')
                    // ->limit(1)
                    ->get()
                    ->first();

                $tambah_sub_toefl_preparation = DB::table('m_latihan_soal')
                    ->insert([
                        'id' => next_value_nuber_6_digit($tb_m_latihan_soal_get_id_terakhir->id),
                        'm_sub_toefl_preparation_id' => $m_sub_toefl_preparation_id_latihan,
                        'soal' => $soal,
                        'kategori_soal' => $kategori_soal,
                        'created_at' => Carbon::now()
                    ]);
                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            }
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        };
    }

    public function editLatihanSoal(Request $request, $m_latihan_soal_id)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $tb_m_latihan_soal = DB::table('m_latihan_soal')
            ->select(DB::raw("m_latihan_soal.*"))
            ->where('m_latihan_soal.id', '=', $m_latihan_soal_id)
            ->get()
            ->first();
        return response()->json(['success' => 'Data Berhasil Disimpan', 'data' => $tb_m_latihan_soal, 'kode' => 201]);
    }

    public function editLatihanSoalReading($sub_latihan_id)
    {
        $m_sub_toefl_preparation = DB::table('tb_reading_materi')
            ->select(DB::raw("tb_reading_materi.*"))
            ->where('tb_reading_materi.m_sub_toefl_preparation_id', '=', $sub_latihan_id)
            ->get()
            ->first();

        $m_sub_toefl_preparation_soal_prepro = preprocessing_get_string($m_sub_toefl_preparation->text_reading);
        return response()->json(['success' => 'Database :', 'm_sub_toefl_preparation_soal_prepro' => $m_sub_toefl_preparation_soal_prepro, 'kode' => 201]);
    }

    public function updateLatihanJawaban(Request $request)
    {
        $update_m_latihan_soal = DB::table('m_latihan_jawaban')
            ->where('m_latihan_jawaban.id', '=', $request->update_latihan_jawaban_id)
            ->update([
                'jawaban' => $request->update_latihan_jawaban,
            ]);
        return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
    }

    public function updateLatihanSoal(Request $request)
    {
        $update_m_latihan_soal = DB::table('m_latihan_soal')
            ->where('m_latihan_soal.id', '=', $request->soal_id)
            ->update([
                'soal' => $request->edit_soal_quill,
                // 'kategori_soal' => $request->edit_kategori_soal
            ]);
        return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
    }

    public function tambahJawaban(Request $request)
    {
        // dd($request->all());
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        $latihan_soal_id_input_text = $request->latihan_soal_id_input_text;
        $huruf_input_text = $request->huruf_input_text;
        $jawaban_input_text = $request->jawaban_input_text;

        if (validateSessionToken($get_session_token)) {

            $tb_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                ->select(DB::raw("m_latihan_jawaban.*"))
                ->get();

            $tb_m_latihan_jawaban_count = count($tb_m_latihan_jawaban);

            if ($tb_m_latihan_jawaban_count == 0) {
                $tambah_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                    ->insert([
                        'id' => 'J000001',
                        'm_latihan_soal_id' => $latihan_soal_id_input_text,
                        'huruf' => $huruf_input_text,
                        'jawaban' => $jawaban_input_text,
                        'created_at' => Carbon::now()
                    ]);
                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            } else {
                $tb_m_latihan_soal_get_id_terakhir = DB::table('m_latihan_jawaban')
                    ->select(DB::raw("m_latihan_jawaban.*"))
                    ->orderby('m_latihan_jawaban.id', 'DESC')
                    // ->limit(1)
                    ->get()
                    ->first();

                $tambah_sub_toefl_preparation = DB::table('m_latihan_jawaban')
                    ->insert([
                        'id' => next_value_nuber_6_digit($tb_m_latihan_soal_get_id_terakhir->id),
                        'm_latihan_soal_id' => $latihan_soal_id_input_text,
                        'huruf' => $huruf_input_text,
                        'jawaban' => $jawaban_input_text,
                        'created_at' => Carbon::now()
                    ]);
                return response()->json(['success' => 'Data Berhasil Disimpan', 'kode' => 201]);
            }
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        };
    }

    public function getDataJawabanSoal(Request $request, $m_latihan_jawaban)
    {

        $limit = is_null($request["length"]) ? 25 : $request["length"];
        $offset = is_null($request["start"]) ? 0 : $request["start"];
        $dirs = array("asc", "desc");
        $draw = $request["draw"];
        $searchs = $request["search.value"];
        $resultData = array();
        $data_arr    = [
            'limit' => $limit,
            'offset' => $offset,
            'searchs' => $searchs,
            'dirs' => $dirs,
        ];

        $tb_m_latihan_soal = DB::table('m_latihan_jawaban')
            ->select(DB::raw("m_latihan_jawaban.*, m_latihan_soal.kategori_soal"))
            ->where('m_latihan_jawaban.m_latihan_soal_id', '=', $m_latihan_jawaban)
            ->leftJoin('m_latihan_soal', 'm_latihan_soal.id', '=', 'm_latihan_jawaban.m_latihan_soal_id')
            ->orderBy('m_latihan_jawaban.id', 'ASC');

        // $datax['total_data'] = $tb_users->first();
        $total_data = $tb_m_latihan_soal->count();


        $m_latihan_soal = $tb_m_latihan_soal
            ->limit($limit)
            ->offset($offset)
            ->get();

        $datas = [];

        $abjad = 'A';
        $noabjad = chr(ord($abjad));
        // $no = $offset + 1;

        if (count($m_latihan_soal) > 0) {

            foreach ($m_latihan_soal as $value) {
                // dd($value['nama']);
                $kategori_soal_button = $value->kategori_soal;

                // dd($kategori_soal_button);

                if ($kategori_soal_button == 'PILIHAN_GANDA') {
                    $datas[] = array(

                        'no' => $noabjad++,
                        'huruf' => $value->huruf,
                        'jawaban' => $value->jawaban,
                        'aksi' =>
                        '<button type="button" name="simpan_kunci_jawaban" id="simpan_kunci_jawaban" data-jawaban_id="' . $value->id . '" data-kategori_soal="' . $value->kategori_soal . '" class="btn btn-info btn-xs m-1" href=' . '' . '><i class="dripicons-pin"></i> Atur sebagai kunci jawaban </button>' .
                            '<button type="button" name="edit_jawaban" id="edit_jawaban" data-jawaban="' . $value->jawaban . '" data-jawaban_id="' . $value->id . '" class="btn btn-warning btn-xs m-1" href=' . '' . '><i class="dripicons-pencil"></i> Edit </button>' .
                            '<button type="button" name="hapus_jawaban" id="hapus_jawaban" data-jawaban_id="' . $value->id . '" data-kategori_soal="' . $value->kategori_soal . '" class="btn btn-danger btn-xs m-1" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button>'
                    );
                } else if ($kategori_soal_button == 'ESAI') {
                    $datas[] = array(

                        'no' => $noabjad++,
                        'huruf' => $value->huruf,
                        'jawaban' => $value->jawaban,
                        'aksi' =>
                        // '<button type="button" name="simpan_kunci_jawaban" id="simpan_kunci_jawaban" data-jawaban_id="' . $value->id . '" data-kategori_soal="' . $value->kategori_soal . '" class="btn btn-info btn-xs m-1" href=' . '' . '><i class="dripicons-pin"></i> Atur sebagai kunci jawaban </button>' .
                        '<button type="button" name="edit_jawaban" id="edit_jawaban" data-jawaban="' . $value->jawaban . '" data-jawaban_id="' . $value->id . '" class="btn btn-warning btn-xs m-1" href=' . '' . '><i class="dripicons-pencil"></i> Edit </button>' .
                            '<button type="button" name="hapus_jawaban" id="hapus_jawaban" data-jawaban_id="' . $value->id . '" data-kategori_soal="' . $value->kategori_soal . '" class="btn btn-danger btn-xs m-1" href=' . '' . '><i class="dripicons-trash"></i> Hapus </button>'
                    );
                }
            }
        } else {
            $datas = array();
        }

        // dd($datas);
        $recordsTotal = is_null($total_data) ? 0 : $total_data;
        $recordsFiltered = is_null($total_data) ? 0 : $total_data;
        $data = $datas;

        return response()->json(compact("data", "draw", "recordsTotal", "recordsFiltered"));
    }

    public function deleteSoalJawaban(Request $request, $hapus_soal_jawaban_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            $delete_m_latihan_soal = DB::table('m_latihan_soal')
                ->where('m_latihan_soal.id', '=', $hapus_soal_jawaban_id)
                ->delete();

            $delete_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                ->where('m_latihan_jawaban.m_latihan_soal_id', '=', $hapus_soal_jawaban_id)
                ->delete();

            return response()->json(['success' => 'Data Berhasil Didelete', 'kode' => 201]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function deleteJawaban(Request $request, $jawaban_id)
    {

        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        // dd($request->all());

        if (validateSessionToken($get_session_token)) {

            if ($request->kategori_soal == 'PILIHAN_GANDA') {
                $delete_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                    ->where('m_latihan_jawaban.id', '=', $jawaban_id)
                    ->delete();

                $update_m_latihan_materi = DB::table('m_latihan_soal')
                    ->where('m_latihan_soal.id', '=', $request->latihan_soal_id)
                    ->update([
                        'm_latihan_jawaban_id' => null,
                    ]);
            } else if ($request->kategori_soal == 'ESAI') {

                $delete_m_latihan_jawaban = DB::table('m_latihan_jawaban')
                    ->where('m_latihan_jawaban.id', '=', $jawaban_id)
                    ->delete();

                // dd($request->latihan_soal_id);

                $get_data_m_latihan_soal_baru = DB::table('m_latihan_soal')
                    ->select(DB::raw("m_latihan_soal.m_latihan_jawaban_id"))
                    ->where('m_latihan_soal.id', '=', $request->latihan_soal_id)
                    ->get()
                    ->first();

                // dd($get_data_m_latihan_soal_baru);

                $data_m_latihan_jawaban_id_explode = explode('|', $get_data_m_latihan_soal_baru->m_latihan_jawaban_id);

                $data_m_latihan_jawaban_id_explode_diff = array_diff($data_m_latihan_jawaban_id_explode, array(0 => $jawaban_id));

                // dd($data_m_latihan_jawaban_id_explode_diff);

                $no_bantuan = 0;
                $value_final = null;

                foreach ($data_m_latihan_jawaban_id_explode_diff as $datas_1 => $value) {
                    $no_bantuan++;
                    if ($no_bantuan == count($data_m_latihan_jawaban_id_explode_diff)) {
                        $value_final .= $value;
                    } else {
                        $value_final .= $value . '|';
                    }
                }

                // dd($value_final);

                $update_m_latihan_materi = DB::table('m_latihan_soal')
                    ->where('m_latihan_soal.id', '=', $request->latihan_soal_id)
                    ->update([
                        'm_latihan_jawaban_id' => $value_final,
                    ]);
            }

            return response()->json(['success' => 'Data Berhasil Didelete', 'kode' => 201]);
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }



        // dd($response_server);
        // dd();

    }

    public function updateKunciJawaban(Request $request)
    {

        // dd($request->all());

        if ($request->kategori_soal == 'PILIHAN_GANDA') {
            $update_m_latihan_materi = DB::table('m_latihan_soal')
                ->where('m_latihan_soal.id', '=', $request->latihan_soal_id)
                ->update([
                    'm_latihan_jawaban_id' => $request->jawaban_id,
                ]);

            $get_data_kunci_jawaban = DB::table('m_latihan_soal')
                ->select(DB::raw("m_latihan_jawaban.huruf as m_latihan_jawaban_huruf, m_latihan_jawaban.jawaban as m_latihan_jawaban_jawaban"))
                ->where('m_latihan_soal.id', '=', $request->latihan_soal_id)
                ->leftJoin('m_latihan_jawaban', 'm_latihan_jawaban.id', '=', 'm_latihan_soal.m_latihan_jawaban_id')
                ->get();

            $m_latihan_jawaban_huruf_lempar = $get_data_kunci_jawaban->first()->m_latihan_jawaban_huruf;
            $m_latihan_jawaban_jawaban = $get_data_kunci_jawaban->first()->m_latihan_jawaban_jawaban;
        } else if ($request->kategori_soal == 'ESAI') {

            $get_data_kunci_jawaban = DB::table('m_latihan_soal')
                ->select(DB::raw("m_latihan_jawaban.huruf as m_latihan_jawaban_huruf, m_latihan_jawaban.jawaban as m_latihan_jawaban_jawaban, m_latihan_soal.m_latihan_jawaban_id"))
                ->where('m_latihan_soal.id', '=', $request->latihan_soal_id)
                ->leftJoin('m_latihan_jawaban', 'm_latihan_jawaban.id', '=', 'm_latihan_soal.m_latihan_jawaban_id')
                ->get();

            $kunci_jawaban = '';
            if (strpos($get_data_kunci_jawaban->first()->m_latihan_jawaban_id, "|")) {
                $kunci_jawaban_value = $get_data_kunci_jawaban->first()->m_latihan_jawaban_id;
                $kunci_jawaban_value_array = explode("|", $kunci_jawaban_value);
                // dd($kunci_jawaban_value_array, $request->jawaban_id);
                if (in_array($request->jawaban_id, $kunci_jawaban_value_array)) {
                    $kunci_jawaban = $get_data_kunci_jawaban->first()->m_latihan_jawaban_id;
                } else {
                    $kunci_jawaban = $get_data_kunci_jawaban->first()->m_latihan_jawaban_id . '|' . $request->jawaban_id;
                }
            } else {
                if ($get_data_kunci_jawaban->first()->m_latihan_jawaban_id == null || $get_data_kunci_jawaban->first()->m_latihan_jawaban_id == '') {
                    $kunci_jawaban = $request->jawaban_id;
                } else {
                    $kunci_jawaban_value = $get_data_kunci_jawaban->first()->m_latihan_jawaban_id;
                    $kunci_jawaban_value_array = explode("|", $kunci_jawaban_value);
                    if (in_array($request->jawaban_id, $kunci_jawaban_value_array)) {
                        $kunci_jawaban = $get_data_kunci_jawaban->first()->m_latihan_jawaban_id;
                    } else {
                        $kunci_jawaban = $get_data_kunci_jawaban->first()->m_latihan_jawaban_id . '|' . $request->jawaban_id;
                    }
                }
            }

            $update_m_latihan_materi = DB::table('m_latihan_soal')
                ->where('m_latihan_soal.id', '=', $request->latihan_soal_id)
                ->update([
                    'm_latihan_jawaban_id' => $kunci_jawaban,
                ]);

            $get_data_m_latihan_soal_baru = DB::table('m_latihan_soal')
                ->select(DB::raw("m_latihan_soal.m_latihan_jawaban_id"))
                ->where('m_latihan_soal.id', '=', $request->latihan_soal_id)
                ->get()
                ->first();

            $data_m_latihan_jawaban_id_explode = explode('|', $get_data_m_latihan_soal_baru->m_latihan_jawaban_id);

            $m_latihan_jawaban_huruf_lempar = '';
            $m_latihan_jawaban_jawaban = '';

            $no_bantuan = 0;
            $pemisah_value = '';

            foreach ($data_m_latihan_jawaban_id_explode as $datas_1 => $value) {
                $no_bantuan++;
                $get_data_m_latihan_jawaban_baru = DB::table('m_latihan_jawaban')
                    ->select(DB::raw("m_latihan_jawaban.jawaban"))
                    ->where('m_latihan_jawaban.id', '=', $value)
                    ->get();


                if ($no_bantuan == count($data_m_latihan_jawaban_id_explode)) {
                    $pemisah_value =  '';
                } else {
                    $pemisah_value =  ', ';
                }

                $m_latihan_jawaban_jawaban .= $get_data_m_latihan_jawaban_baru->first()->jawaban . $pemisah_value;
            }
        }

        return response()->json([
            'success' => 'Data Berhasil Disimpan',
            'kode' => 201,
            'm_latihan_jawaban_huruf' => $m_latihan_jawaban_huruf_lempar,
            'm_latihan_jawaban_jawaban' => $m_latihan_jawaban_jawaban,
        ]);
    }

    public function getDataDeskripsi(Request $request, $soal_id)
    {
        $get_data_kunci_jawaban = DB::table('m_latihan_soal')
            ->select(DB::raw("m_latihan_soal.*"))
            ->where('m_latihan_soal.id', '=', $soal_id)
            ->get()
            ->first();

        // dd($get_data_kunci_jawaban);
        return response()->json([
            'kode' => 201,
            'data' => $get_data_kunci_jawaban
        ]);
    }

    public function tambahDeskripsi(Request $request)
    {
        // dd($request->tambah_deskripsi_quill);
        $soal_id = $request->soal_id;
        $tambah_deskripsi_quill = $request->tambah_deskripsi_quill;

        if ($tambah_deskripsi_quill == '<div class="ql-editor ql-blank" data-gramm="false" contenteditable="true"><p><br></p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>') {
            $tambah_deskripsi_quill = null;
        } else {
            $tambah_deskripsi_quill = $tambah_deskripsi_quill;
        }

        $update_m_latihan_materi = DB::table('m_latihan_soal')
            ->where('m_latihan_soal.id', '=', $soal_id)
            ->update([
                'deskripsi' => $tambah_deskripsi_quill,
            ]);

        // dd($get_data_kunci_jawaban);
        return response()->json([
            'kode' => 201,
            'success' => 'data berhasil ditambah'
        ]);
    }
}
