<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;

class homePesertaController extends Controller
{
    public function index()
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];

        if (validateSessionToken($get_session_token)) {

            return view('pages.home.index_peserta');
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getMateriIndexTT(Request $request)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];
        $get_session_users_id = $get_session['users_id'];
        $search_materi_tt = is_null($request->search_materi_tt) ? '' : $request->search_materi_tt;

        if (validateSessionToken($get_session_token)) {

            $tb_materi = DB::table('tb_roll_akses_teofl_test')
                ->select(DB::raw("
            tb_roll_akses_teofl_test.id_username,
            tb_roll_akses_teofl_test.id_paket,
            tb_paket_materi_teofl_test.judul_materi,
            tb_paket_materi_teofl_test.video_materi,
            tb_paket_teofl_test.nama_paket,
            tb_paket_materi_teofl_test.id_paket"))
                ->join('tb_paket_materi_teofl_test', 'tb_paket_materi_teofl_test.id_paket', '=', 'tb_roll_akses_teofl_test.id_paket')
                ->join('tb_paket_teofl_test', 'tb_paket_teofl_test.id_paket', '=', 'tb_roll_akses_teofl_test.id_paket')
                ->orWhere(function ($query) use (
                    $get_session_users_id,
                    $search_materi_tt
                ) {
                    $query
                        ->where('tb_roll_akses_teofl_test.id_username', '=',  $get_session_users_id)
                        ->where('tb_paket_materi_teofl_test.judul_materi', 'ilike', '%' . $search_materi_tt . '%');
                })
                ->get();

            // $data['tb_materi'] = $tb_materi;

            $data_uncal = [];

            foreach ($tb_materi as $urlvideo) {

                // convert get id
                $urlDB =  $urlvideo->video_materi;
                // $explodeyt = explode('/', $urlDB);
                // $dataurl = $explodeyt[4];

                // convert replace embed
                // $datareplace =  $urlvideo->video_materi;
                // $convertedURLEmbed = str_replace("embed/", "watch?v=", datareplace);

                $data_uncal[] = array(
                    "nama_paket" => $urlvideo->nama_paket,
                    "judul_materi" => $urlvideo->judul_materi,
                    "video_materi" => $urlvideo->video_materi,
                    // "convertedURLEmbed" => $datareplace,
                    "dataurl" => $urlDB,
                );
            }

            // $data['dataurl'] = $dataurl;
            $data['data_uncal'] = $data_uncal;

            // dd($data_uncal);

            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }

    public function getMateriIndexTP(Request $request)
    {
        $get_session = Session::get('user_app');
        $get_session_token = $get_session['token'];
        $get_session_users_id = $get_session['users_id'];
        $search_materi_tp = is_null($request->search_materi_tp) ? '' : $request->search_materi_tp;

        if (validateSessionToken($get_session_token)) {

            $tb_materi_tp = DB::table('tb_roll_akses_teofl_preparation')
                ->select(DB::raw("
            tb_roll_akses_teofl_preparation.id_username,
            tb_roll_akses_teofl_preparation.id_paket,
            m_toefl_preparation.nama_toefl_preparation,
            m_toefl_preparation.id"))
                ->join('m_toefl_preparation', 'm_toefl_preparation.id', '=', 'tb_roll_akses_teofl_preparation.id_paket')
                ->orWhere(function ($query) use (
                    $get_session_users_id,
                    $search_materi_tp
                ) {
                    $query
                        ->where('tb_roll_akses_teofl_preparation.id_username', '=',  $get_session_users_id)
                        ->where('m_toefl_preparation.nama_toefl_preparation', 'ilike', '%' . $search_materi_tp . '%');
                })
                ->get();

            $data['tb_materi_tp'] = $tb_materi_tp;


            return response()->json(compact("data"));
        } else {
            return response()->json(['success' => 'anda belum login', 'kode' => 401]);
        }
    }
}
