<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Session;
use DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class loginController extends Controller
{
    public function index()
    {
        return view('login.index');
    }

    public function login(Request $request)
    {
        // dd('coba');
        $username = $request->username;
        $password = $request->password;

        $get_username_users = db::table('users')
            ->where('users.username', '=', $username)
            ->get()
            ->first();
        if ($get_username_users != null) {
            // dd('ini data admin unit kerja');
            if (Hash::check($password, $get_username_users->password)) {
                // dd('sukses login admin');

                $users_cek_kadaluwarsa = db::table('users')
                    ->where("users.username", '=', $get_username_users->username)
                    ->get()
                    ->first();

                $tanggal_kadaluwarsa_peserta = $users_cek_kadaluwarsa->tanggal_expired;
                $tanggal_start_peserta = $users_cek_kadaluwarsa->tanggal_start;
                $tanggal_sekarang = date("Y-m-d");

                // dd($users_cek_kadaluwarsa);

                if ($users_cek_kadaluwarsa->role == "R003") {
                    // dd($users_cek_kadaluwarsa);
                    if ($tanggal_sekarang > $tanggal_kadaluwarsa_peserta) {
                        return response()->json(['success' => 'Akun Anda Sudah Kadaluwarsa, Silahkan Hubungi Admin Meet Tutor, Terimakasih', 'kode' => 401]);
                    } else if ($tanggal_sekarang < $tanggal_start_peserta) {
                        return response()->json(['success' => 'Akun Anda Aktif Pada Tanggal ' . $tanggal_start_peserta . ', Silahkan Hubungi Admin Meet Tutor, Terimakasih', 'kode' => 401]);
                    } else {
                    }
                } else{}

                $users = db::table('users')
                    ->where("users.username", '=', $get_username_users->username)
                    ->get()
                    ->first();

                $users_id = $users ? $users->id : NULL;
                $role = $users ? $users->role : NULL;
                // $unit_id = $users ? $users->unitid : NULL;
                $nama = $users ? $users->nama : NULL;
                $username = $users ? $users->username : NULL;
                // $foto = NULL;

                $updated_at = $users ? $users->updated_at : NULL;

                $ambil_role = db::table('m_role')
                    ->where("m_role.id", '=', $role)
                    ->get()
                    ->first();

                // dd($ambil_role);

                $flag = $ambil_role ? $ambil_role->flag : NULL;

                $now = new \DateTime(date("Y-m-d h:i:s"));
                $date1 = new \DateTime($updated_at);
                $diff = $now->diff($date1);

                $newtoken  = $this->generateRandomString();

                $token_buat_sendiri = $newtoken;

                // dd($username);

                $update_users = DB::table('users')
                    ->where('users.username', '=', $username)
                    ->update([
                        'remember_token' => $token_buat_sendiri,
                        'updated_at' => Carbon::now()
                    ]);

                // dd($role);

                $get_mapping_menu_sub_menu_group_by = db::table('m_mapping_menu')
                    ->select(DB::raw('m_sub_menu.m_menu_id, m_menu.nama_menu'))
                    ->where("m_mapping_menu.m_role_id", '=', $role)
                    ->join('m_sub_menu', 'm_sub_menu.id', '=', 'm_mapping_menu.m_sub_menu_id')
                    ->join('m_menu', 'm_menu.id', '=', 'm_sub_menu.m_menu_id')
                    ->groupBy('m_sub_menu.m_menu_id', 'm_menu.nama_menu')
                    ->get();

                // dd($get_mapping_menu_sub_menu_group_by);
                $array_menu = [];
                foreach ($get_mapping_menu_sub_menu_group_by as $data) {
                    $get_mapping_menu_sub_menu = db::table('m_mapping_menu')
                        ->select(DB::raw('m_mapping_menu.*, m_menu.id as m_menu_id, m_menu.nama_menu as m_menu_nama_menu, m_sub_menu.nama_sub_menu as m_sub_menu_nama_sub_menu, m_sub_menu.url_sub_menu as m_sub_menu_url_sub_menu'))
                        ->where("m_mapping_menu.m_role_id", '=', $users->role)
                        ->where("m_sub_menu.m_menu_id", '=', $data->m_menu_id)
                        ->join('m_sub_menu', 'm_sub_menu.id', '=', 'm_mapping_menu.m_sub_menu_id')
                        ->join('m_menu', 'm_menu.id', '=', 'm_sub_menu.m_menu_id')
                        ->get()
                        ->toArray();
                    foreach ($get_mapping_menu_sub_menu as $data2) {
                        // if($data->m_menu_id == $data2->m_menu_id){
                        $array_menu[$data->nama_menu] = $get_mapping_menu_sub_menu;
                        // }
                    }
                }
                // dd($array_menu);

                $data_user = [];
                $data_user['users_id']                  = $users_id;
                $data_user['nik']                       = null;
                $data_user['username']                  = $username;
                $data_user['nama']                      = $nama;
                $data_user['role']                      = $role;
                $data_user['flag']                      = $flag;
                $data_user['token']                     = $token_buat_sendiri;
                $data_user['menu']                      = $array_menu;

                Session::put('user_app', $data_user);
                // dd(Session::get('user_app'));
                return response()->json(['success' => 'Anda Berhasil Login', 'kode' => 200, 'role' => $role]);
            } else {
                // dd('gagal login admin');
                return response()->json(['success' => 'Username Atau Password Anda Salah', 'kode' => 401]);
            }
        } else {
            // dd('data tidak ditemukan');
            return response()->json(['success' => 'Anda Gagal Login', 'kode' => 401]);
        }

        // return response()->json(['success' => 'Anda Gagal Login', 'kode' => 401]);
        // }

        // dd($login);


    }

    public function loginQRCode(Request $request)
    {
        // dd('coba');
        // dd($request->all());
        $token_login_qr_code = $request->token_login_qr_code;

        $get_username_users = db::table('users')
            ->where('users.token_login_qr_code', '=', $token_login_qr_code)
            ->get()
            ->first();
        if ($get_username_users != null) {
            // dd('ini data admin unit kerja');
            // if (Hash::check($password, $get_username_users->password)) {
            // dd('sukses login admin');

            $users_cek_kadaluwarsa = db::table('users')
                ->where("users.username", '=', $get_username_users->username)
                ->get()
                ->first();

            $tanggal_kadaluwarsa_peserta = $users_cek_kadaluwarsa->tanggal_expired;
            $tanggal_start_peserta = $users_cek_kadaluwarsa->tanggal_start;
            $tanggal_sekarang = date("Y-m-d");

            // dd($users_cek_kadaluwarsa);

            if ($users_cek_kadaluwarsa->role == "R003") {
                // dd($users_cek_kadaluwarsa);
                if ($tanggal_sekarang > $tanggal_kadaluwarsa_peserta) {
                    return response()->json(['success' => 'Akun Anda Sudah Kadaluwarsa, Silahkan Hubungi Admin Meet Tutor, Terimakasih', 'kode' => 401]);
                } else if ($tanggal_sekarang < $tanggal_start_peserta) {
                    return response()->json(['success' => 'Akun Anda Aktif Pada Tanggal ' . $tanggal_start_peserta . ', Silahkan Hubungi Admin Meet Tutor, Terimakasih', 'kode' => 401]);
                } else {
                }
            } else{}

            $users = db::table('users')
                ->where("users.username", '=', $get_username_users->username)
                ->get()
                ->first();

            $users_id = $users ? $users->id : NULL;
            $role = $users ? $users->role : NULL;
            // $unit_id = $users ? $users->unitid : NULL;
            $nama = $users ? $users->nama : NULL;
            $username = $users ? $users->username : NULL;
            // $foto = NULL;

            $updated_at = $users ? $users->updated_at : NULL;

            $ambil_role = db::table('m_role')
                ->where("m_role.id", '=', $role)
                ->get()
                ->first();

            // dd($ambil_role);

            $flag = $ambil_role ? $ambil_role->flag : NULL;

            $now = new \DateTime(date("Y-m-d h:i:s"));
            $date1 = new \DateTime($updated_at);
            $diff = $now->diff($date1);

            $newtoken  = $this->generateRandomString();

            $token_buat_sendiri = $newtoken;

            // dd($username);

            $update_users = DB::table('users')
                ->where('users.username', '=', $username)
                ->update([
                    'remember_token' => $token_buat_sendiri,
                    'updated_at' => Carbon::now()
                ]);

            // dd($role);

            $get_mapping_menu_sub_menu_group_by = db::table('m_mapping_menu')
                ->select(DB::raw('m_sub_menu.m_menu_id, m_menu.nama_menu'))
                ->where("m_mapping_menu.m_role_id", '=', $role)
                ->join('m_sub_menu', 'm_sub_menu.id', '=', 'm_mapping_menu.m_sub_menu_id')
                ->join('m_menu', 'm_menu.id', '=', 'm_sub_menu.m_menu_id')
                ->groupBy('m_sub_menu.m_menu_id', 'm_menu.nama_menu')
                ->get();

            // dd($get_mapping_menu_sub_menu_group_by);
            $array_menu = [];
            foreach ($get_mapping_menu_sub_menu_group_by as $data) {
                $get_mapping_menu_sub_menu = db::table('m_mapping_menu')
                    ->select(DB::raw('m_mapping_menu.*, m_menu.id as m_menu_id, m_menu.nama_menu as m_menu_nama_menu, m_sub_menu.nama_sub_menu as m_sub_menu_nama_sub_menu, m_sub_menu.url_sub_menu as m_sub_menu_url_sub_menu'))
                    ->where("m_mapping_menu.m_role_id", '=', $users->role)
                    ->where("m_sub_menu.m_menu_id", '=', $data->m_menu_id)
                    ->join('m_sub_menu', 'm_sub_menu.id', '=', 'm_mapping_menu.m_sub_menu_id')
                    ->join('m_menu', 'm_menu.id', '=', 'm_sub_menu.m_menu_id')
                    ->get()
                    ->toArray();
                foreach ($get_mapping_menu_sub_menu as $data2) {
                    // if($data->m_menu_id == $data2->m_menu_id){
                    $array_menu[$data->nama_menu] = $get_mapping_menu_sub_menu;
                    // }
                }
            }
            // dd($array_menu);

            $data_user = [];
            $data_user['users_id']                  = $users_id;
            $data_user['nik']                       = null;
            $data_user['username']                  = $username;
            $data_user['nama']                      = $nama;
            $data_user['role']                      = $role;
            $data_user['flag']                      = $flag;
            $data_user['token']                     = $token_buat_sendiri;
            $data_user['menu']                      = $array_menu;

            Session::put('user_app', $data_user);
            // dd(Session::get('user_app'));
            return response()->json(['success' => 'Anda Berhasil Login', 'kode' => 200, 'role' => $role]);
            // } else {
            //     // dd('gagal login admin');
            //     return response()->json(['success' => 'Username Atau Password Anda Salah', 'kode' => 401]);
            // }
        } else {
            // dd('data tidak ditemukan');
            return response()->json(['success' => 'Anda Gagal Login', 'kode' => 401]);
        }

        // return response()->json(['success' => 'Anda Gagal Login', 'kode' => 401]);
        // }

        // dd($login);


    }

    function generateRandomString($length = 80)
    {
        $karakkter = '012345678dssd9abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $panjang_karakter = strlen($karakkter);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $karakkter[rand(0, $panjang_karakter - 1)];
        }
        return $str;
    }

    public function logout()
    {
        $session_username = Session::get('user_app')['username'];

        // dd($session_nik);

        $users = db::table('users')->where("username", $session_username)->first();
        // dd($user);
        $ara = array('remember_token' => NULL, 'updated_at' => date("Y-m-d H:i:s"));
        if ($users) {
            DB::table('users')->where("username", $session_username)->update($ara);
        } else {
        }

        Session::flush();
    }

    public function resetPasword(Request $request)
    {
        // dd($request->all());
        $username_reset_password = $request->username_reset_password;
        $email_reset_password = $request->email_reset_password;
        $random_reset_password = 'RTR' . rand(100, 999);

        $cek_username_dan_email = db::table('users')
            ->select(DB::raw('users.id'))
            ->where("users.username", '=', $username_reset_password)
            ->where("users.email", '=', $email_reset_password)
            ->get();

        if (count($cek_username_dan_email) > 0) {
            Mail::send('login.email_letter', ['username_reset_password' => $username_reset_password, 'random_reset_password' => $random_reset_password], function ($message) use ($username_reset_password, $email_reset_password, $random_reset_password) {
                $message->to($email_reset_password, "Meet Tutor")->subject('Reset Password Meet Tutor');
            });

            $update_users = DB::table('users')
                ->where('users.username', '=', $username_reset_password)
                ->update([
                    'password' => Hash::make($random_reset_password),
                ]);

            return response()->json(['success' => 'Reset Password Berhasil, Silahkan Cek Pesan Di Email Anda', 'kode' => 200]);
        } else {
            return response()->json(['success' => 'username atau email tidak diketahui', 'kode' => 401]);
        }
    }
    public function logout_lwt_menu()
    {
        $session_username = Session::get('user_app')['username'];

        // dd($session_nik);

        $users = db::table('users')->where("username", $session_username)->first();
        // dd($user);
        $ara = array('remember_token' => NULL, 'updated_at' => date("Y-m-d H:i:s"));
        if ($users) {
            DB::table('users')->where("username", $session_username)->update($ara);
        } else {
        }

        Session::flush();

        return view('login.index');
    }
}
