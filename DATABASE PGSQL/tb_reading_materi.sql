-- Adminer 4.8.1 PostgreSQL 11.16 dump

DROP TABLE IF EXISTS "tb_reading_materi";
DROP SEQUENCE IF EXISTS tb_reading_materi_id_reading_seq;
CREATE SEQUENCE tb_reading_materi_id_reading_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."tb_reading_materi" (
    "id" character varying DEFAULT 'nextval(''tb_reading_materi_id_reading_seq'')' NOT NULL,
    "m_sub_toefl_preparation_id" character varying,
    "text_reading" character varying,
    "created_at" character varying,
    "update_at" character varying,
    CONSTRAINT "tb_reading_materi_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "tb_reading_materi" ("id", "m_sub_toefl_preparation_id", "text_reading", "created_at", "update_at") VALUES
('R000001',	'S000001',	'<div class="ql-editor" data-gramm="false" contenteditable="true"><p>coba bos</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-10-27 12:24:28',	NULL),
('R000002',	'S000002',	'<div class="ql-editor" data-gramm="false" contenteditable="true" data-placeholder="Isikan soal text Kamu disini"><p>	<span style="color: rgb(44, 62, 80);">Setibanya pak Usman di restoran kecil sepulang dari sekolah, larasati segera memulai pembicaraan.</span></p><p>	<span style="color: rgb(44, 62, 80);">Sebelum membicarakan soal diah, saya perlu menjelaskan menggapa saya tidak mau membicarakan hal ini di sekolah karena saya ingin saya bicarakan adalah masalah yang harus diselesaikan dengan kacamata kemanusiaan, bukan kedinasan</span></p><p><span style="color: rgb(44, 62, 80);">Saya khawatir , keinginan bapak untuk menghabisi Diah itu karena kebencian bapak terhadap saya. Selama ini orang kan tahu saya sangat perhatin terhadap diah. Dia anak yang lemah pak, sudah mengalami berbagai cobaan hidup, sering murung karena menerima beban yang terlalu banyak dalam hidupnya</span></p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden" style="margin-top: 0px;"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-10-27 13:09:16',	NULL);

-- 2022-10-28 17:40:20.015509+07
