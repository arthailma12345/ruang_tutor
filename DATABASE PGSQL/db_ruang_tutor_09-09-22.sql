-- Adminer 4.8.1 PostgreSQL 11.16 dump

DROP TABLE IF EXISTS "m_flag";
DROP SEQUENCE IF EXISTS m_flag_id_seq;
CREATE SEQUENCE m_flag_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."m_flag" (
    "id" smallint DEFAULT nextval('m_flag_id_seq') NOT NULL,
    "nama_flag" character varying(100),
    "keterangan" character varying(100),
    CONSTRAINT "m_flag_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "m_flag" ("id", "nama_flag", "keterangan") VALUES
(1,	'AP',	'Admin Programmer'),
(2,	'AS',	'Admin Super'),
(3,	'PE',	'Peserta');

DROP TABLE IF EXISTS "m_latihan_jawaban";
CREATE TABLE "public"."m_latihan_jawaban" (
    "id" character varying,
    "m_latihan_soal_id" character varying,
    "huruf" character varying,
    "jawaban" character varying,
    "created_at" character varying,
    "updated_at" character varying
) WITH (oids = false);

INSERT INTO "m_latihan_jawaban" ("id", "m_latihan_soal_id", "huruf", "jawaban", "created_at", "updated_at") VALUES
('J000001',	'L000020',	'a',	'coba1',	'2022-08-27 14:32:41',	NULL),
('J000002',	'L000018',	'b',	'ya',	'2022-08-27 15:08:41',	NULL),
('J000003',	'L000020',	'c',	'af',	'2022-08-29 15:32:39',	NULL),
('J000004',	'L000019',	'a',	'diferent',	'2022-08-29 16:46:20',	NULL),
('J000005',	'L000020',	'b',	'gh',	'2022-08-29 17:17:37',	NULL);

DROP TABLE IF EXISTS "m_latihan_materi";
CREATE TABLE "public"."m_latihan_materi" (
    "id" character varying,
    "m_sub_toefl_preparation_id" character varying,
    "url_vdeo_latihan_materi" character varying,
    "created_at" character varying,
    "update_at" character varying
) WITH (oids = false);

INSERT INTO "m_latihan_materi" ("id", "m_sub_toefl_preparation_id", "url_vdeo_latihan_materi", "created_at", "update_at") VALUES
('M000001',	'S000001',	'coba2',	'2022-08-17 18:14:49',	NULL),
('M000002',	'S000002',	'ini 3',	'2022-08-17 18:17:32',	NULL);

DROP TABLE IF EXISTS "m_latihan_soal";
CREATE TABLE "public"."m_latihan_soal" (
    "id" character varying,
    "m_sub_toefl_preparation_id" character varying,
    "soal" text,
    "created_at" character varying,
    "update_at" character varying,
    "m_latihan_jawaban_id" character varying,
    "soal_point" smallint
) WITH (oids = false);

INSERT INTO "m_latihan_soal" ("id", "m_sub_toefl_preparation_id", "soal", "created_at", "update_at", "m_latihan_jawaban_id", "soal_point") VALUES
('L000002',	'S000001',	'coba 2',	NULL,	NULL,	NULL,	NULL),
('L000003',	'S000001',	'coba 3',	NULL,	NULL,	NULL,	NULL),
('L000004',	'S000001',	'coba 4',	NULL,	NULL,	NULL,	NULL),
('L000005',	'S000001',	'coba 5',	NULL,	NULL,	NULL,	NULL),
('L000006',	'S000001',	'coba 6',	NULL,	NULL,	NULL,	NULL),
('L000007',	'S000001',	'coba 7',	NULL,	NULL,	NULL,	NULL),
('L000008',	'S000001',	'coba 8',	NULL,	NULL,	NULL,	NULL),
('L000009',	'S000001',	'coba 9',	NULL,	NULL,	NULL,	NULL),
('L000010',	'S000001',	'coba 10',	NULL,	NULL,	NULL,	NULL),
('L000011',	'S000001',	'coba 11',	NULL,	NULL,	NULL,	NULL),
('L000012',	'S000001',	'coba 12',	NULL,	NULL,	NULL,	NULL),
('L000001',	'S000001',	'coba1',	NULL,	NULL,	NULL,	NULL),
('L000013',	'S000001',	'<div class="ql-editor" data-gramm="false" contenteditable="true"><p>cban 5</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-08-17 17:16:10',	NULL,	NULL,	NULL),
('L000014',	'S000001',	'<div class="ql-editor" data-gramm="false" contenteditable="true"><p>dssd</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-08-17 17:30:21',	NULL,	NULL,	NULL),
('L000015',	'S000001',	'<div class="ql-editor" data-gramm="false" contenteditable="true"><p>dssdsd</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-08-17 17:30:32',	NULL,	NULL,	NULL),
('L000016',	'S000001',	'<div class="ql-editor" data-gramm="false" contenteditable="true"><p>dssdsdafafd</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-08-17 17:30:45',	NULL,	NULL,	NULL),
('L000017',	'S000002',	'<div class="ql-editor" data-gramm="false" contenteditable="true"><p>ini coba pertanyaan</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-08-17 18:32:54',	NULL,	NULL,	NULL),
('L000018',	'S000003',	'<div class="ql-editor" data-gramm="false" contenteditable="true"><p>soal 1</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-08-26 00:34:48',	NULL,	NULL,	NULL),
('L000019',	'S000003',	'<div class="ql-editor" data-gramm="false" contenteditable="true"><p>soal 2</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-08-26 00:38:34',	NULL,	NULL,	NULL),
('L000020',	'S000003',	'<div class="ql-editor" data-gramm="false" contenteditable="true"><p>coba1 </p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>',	'2022-08-27 05:40:05',	NULL,	'J000003',	NULL);

DROP TABLE IF EXISTS "m_mapping_menu";
DROP SEQUENCE IF EXISTS m_mapping_menu_id_seq;
CREATE SEQUENCE m_mapping_menu_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."m_mapping_menu" (
    "id" smallint DEFAULT nextval('m_mapping_menu_id_seq'),
    "m_role_id" character varying,
    "m_sub_menu_id" character varying(100),
    "created_at" character varying(100),
    "updated_at" character varying(100)
) WITH (oids = false);

INSERT INTO "m_mapping_menu" ("id", "m_role_id", "m_sub_menu_id", "created_at", "updated_at") VALUES
(198,	'R002',	'S002',	'2022-08-13 13:50:55',	NULL),
(199,	'R002',	'S003',	'2022-08-13 13:50:55',	NULL),
(200,	'R002',	'S004',	'2022-08-13 13:50:55',	NULL),
(201,	'R002',	'S015',	'2022-08-13 13:50:55',	NULL),
(202,	'R002',	'S008',	'2022-08-13 13:50:55',	NULL),
(203,	'R002',	'S014',	'2022-08-13 13:50:55',	NULL),
(204,	'R002',	'S017',	'2022-08-13 13:50:55',	NULL),
(209,	'R001',	'S002',	'2022-08-13 13:51:08',	NULL),
(210,	'R001',	'S003',	'2022-08-13 13:51:08',	NULL),
(211,	'R001',	'S004',	'2022-08-13 13:51:08',	NULL),
(212,	'R001',	'S015',	'2022-08-13 13:51:08',	NULL),
(213,	'R001',	'S008',	'2022-08-13 13:51:08',	NULL),
(214,	'R001',	'S014',	'2022-08-13 13:51:08',	NULL),
(215,	'R001',	'S017',	'2022-08-13 13:51:08',	NULL),
(216,	'R003',	'S008',	'2022-08-13 13:51:14',	NULL),
(217,	'R003',	'S014',	'2022-08-13 13:51:14',	NULL),
(218,	'R003',	'S016',	'2022-08-13 13:51:14',	NULL);

DROP TABLE IF EXISTS "m_menu";
CREATE TABLE "public"."m_menu" (
    "id" character varying,
    "nama_menu" character varying(100),
    "created_at" character varying(100),
    "updated_at" character varying(100)
) WITH (oids = false);

INSERT INTO "m_menu" ("id", "nama_menu", "created_at", "updated_at") VALUES
('M003',	'Admin Panel',	'2022-07-07 00:58:45',	NULL),
('M001',	'Home',	'2022-07-07 00:59:12',	NULL),
('M006',	'About',	'2022-07-08 03:09:48',	NULL),
('M007',	'Surat Perjanjian',	'2022-08-04 17:31:52',	NULL),
('M008',	'Toefl',	'2022-08-13 13:49:59',	NULL);

DROP TABLE IF EXISTS "m_pembahasan_materi";
CREATE TABLE "public"."m_pembahasan_materi" (
    "id" character varying,
    "m_sub_toefl_preparation_id" character varying,
    "url_vdeo_pembahasan_materi" character varying,
    "created_at" character varying,
    "update_at" character varying
) WITH (oids = false);

INSERT INTO "m_pembahasan_materi" ("id", "m_sub_toefl_preparation_id", "url_vdeo_pembahasan_materi", "created_at", "update_at") VALUES
('M000001',	'S000001',	'coba3',	'2022-08-17 18:32:08',	NULL),
('M000002',	'S000002',	'coba4',	'2022-08-17 18:32:34',	NULL);

DROP TABLE IF EXISTS "m_role";
CREATE TABLE "public"."m_role" (
    "id" character varying,
    "nama_role" character varying(16),
    "flag" character varying(2),
    "created_at" character varying(100),
    "updated_at" character varying(100)
) WITH (oids = false);

INSERT INTO "m_role" ("id", "nama_role", "flag", "created_at", "updated_at") VALUES
('R001',	'admin super',	'AS',	'2022-07-06 06:52:28',	NULL),
('R002',	'admin programmer',	'AP',	'2022-07-06 06:53:08',	NULL),
('R003',	'peserta',	'PE',	'2022-07-06 06:53:21',	NULL);

DROP TABLE IF EXISTS "m_sub_menu";
DROP SEQUENCE IF EXISTS m_sub_menu_id_seq;
CREATE SEQUENCE m_sub_menu_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."m_sub_menu" (
    "id" character varying DEFAULT 'nextval(''m_sub_menu_id_seq'')',
    "m_menu_id" character varying,
    "nama_sub_menu" character varying(100),
    "url_sub_menu" character varying(100),
    "created_at" character varying(100),
    "updated_at" character varying(100)
) WITH (oids = false);

INSERT INTO "m_sub_menu" ("id", "m_menu_id", "nama_sub_menu", "url_sub_menu", "created_at", "updated_at") VALUES
('S002',	'M003',	'Menu',	'menu/index',	'2022-07-07 01:46:28',	NULL),
('S008',	'M001',	'Home',	'home/index',	'2022-07-08 03:02:48',	NULL),
('S009',	'M002',	'Issues',	'issues/index',	'2022-07-08 03:06:49',	NULL),
('S011',	'M004',	'Report Aging Issues GOSAP',	'report_aging_issues_gosap',	'2022-07-08 03:11:26',	NULL),
('S010',	'M004',	'Report Aging Issues HELPDESK',	'report_aging_issues_helpdesk',	'2022-07-08 03:10:36',	NULL),
('S012',	'M004',	'Response and Resolution Report',	'report_and_resolution_report',	'2022-07-08 03:12:53',	NULL),
('S014',	'M006',	'about',	'about/index',	'2022-07-08 03:13:52',	NULL),
('S013',	'M005',	'Created Issues',	'tiket/index',	'2022-07-08 03:13:31',	NULL),
('S003',	'M003',	'Mapping Menu',	'mapping_menu/index',	'2022-07-07 03:20:41',	NULL),
('S004',	'M003',	'Role',	'role/index',	'2022-07-07 01:51:11',	NULL),
('S015',	'M003',	'User',	'user/index',	'2022-07-24 06:23:32',	NULL),
('S016',	'M007',	'Surat Perjanjian',	'home_surat_perjanjian/index',	'2022-08-04 17:32:25',	NULL),
('S017',	'M008',	'Toefl Preparation',	'toefl_preparation/index',	'2022-08-13 13:50:47',	NULL);

DROP TABLE IF EXISTS "m_sub_toefl_preparation";
CREATE TABLE "public"."m_sub_toefl_preparation" (
    "id" character varying,
    "m_toefl_preparation_id" character varying,
    "nama_sub_toefl_preparation" character varying,
    "created_at" character varying,
    "updated_at" character varying
) WITH (oids = false);

INSERT INTO "m_sub_toefl_preparation" ("id", "m_toefl_preparation_id", "nama_sub_toefl_preparation", "created_at", "updated_at") VALUES
('S000001',	'T000002',	'Tips 1',	'2022-08-08 17:36:08',	NULL),
('S000002',	'T000002',	'Tips 2',	'2022-08-08 17:36:19',	NULL),
('S000003',	'T000003',	'Tips 1',	'2022-08-13 14:06:22',	NULL);

DROP TABLE IF EXISTS "m_toefl_preparation";
CREATE TABLE "public"."m_toefl_preparation" (
    "id" character varying,
    "nama_toefl_preparation" character varying,
    "created_at" character varying,
    "updated_at" character varying
) WITH (oids = false);

INSERT INTO "m_toefl_preparation" ("id", "nama_toefl_preparation", "created_at", "updated_at") VALUES
('T000001',	'Listening 1',	'2022-08-08 17:35:26',	''),
('T000002',	'Listening 2',	'2022-08-08 17:35:43',	''),
('T000003',	'Listening 3',	'2022-08-13 14:05:51',	''),
('T000000',	'-',	'',	'');

DROP TABLE IF EXISTS "migrations";
DROP SEQUENCE IF EXISTS migrations_id_seq;
CREATE SEQUENCE migrations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."migrations" (
    "id" smallint DEFAULT nextval('migrations_id_seq'),
    "migration" character varying(46),
    "batch" smallint
) WITH (oids = false);

INSERT INTO "migrations" ("id", "migration", "batch") VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1);

DROP TABLE IF EXISTS "password_resets";
CREATE TABLE "public"."password_resets" (
    "email" character varying(1),
    "token" character varying(1),
    "created_at" character varying(1)
) WITH (oids = false);


DROP TABLE IF EXISTS "tb_paket_materi_teofl_test";
DROP SEQUENCE IF EXISTS tb_paket_materi_teofl_test_id_materi_seq;
CREATE SEQUENCE tb_paket_materi_teofl_test_id_materi_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."tb_paket_materi_teofl_test" (
    "id_materi" integer DEFAULT nextval('tb_paket_materi_teofl_test_id_materi_seq') NOT NULL,
    "judul_materi" character(50) NOT NULL,
    "video_materi" text NOT NULL,
    "id_paket" integer NOT NULL
) WITH (oids = false);

INSERT INTO "tb_paket_materi_teofl_test" ("id_materi", "judul_materi", "video_materi", "id_paket") VALUES
(15,	'Ojo dibandingke                                   ',	'https://www.youtube.com/embed/sYJ_t2MiJF0',	10),
(16,	'Mendung tanpo udan                                ',	'https://www.youtube.com/embed/r8T61-29o10',	13),
(17,	'Tak ingat usia                                    ',	'https://www.youtube.com/embed/FB1YNEOspyA',	13),
(12,	'Dunia Tipu Tipu                                   ',	'https://www.youtube.com/embed/sYJ_t2MiJF0',	10),
(13,	'Hati Hati di jalan raya                           ',	'https://www.youtube.com/embed/_N6vSc_mT6I',	10);

DROP TABLE IF EXISTS "tb_paket_teofl_test";
DROP SEQUENCE IF EXISTS tb_teofl_test_id_seq;
CREATE SEQUENCE tb_teofl_test_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."tb_paket_teofl_test" (
    "id_paket" integer DEFAULT nextval('tb_teofl_test_id_seq') NOT NULL,
    "nama_paket" character(50) NOT NULL
) WITH (oids = false);

INSERT INTO "tb_paket_teofl_test" ("id_paket", "nama_paket") VALUES
(1,	'Paket A                                           '),
(2,	'Paket AB                                          '),
(0,	'-                                                 ');

DROP TABLE IF EXISTS "tb_roll_akses_teofl_preparation";
DROP SEQUENCE IF EXISTS tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq;
CREATE SEQUENCE tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."tb_roll_akses_teofl_preparation" (
    "id_roll_akses_tp" integer DEFAULT nextval('tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq') NOT NULL,
    "id_username" integer NOT NULL,
    "id_paket" text NOT NULL,
    CONSTRAINT "tb_roll_akses_teofl_preparation_pkey" PRIMARY KEY ("id_roll_akses_tp")
) WITH (oids = false);

INSERT INTO "tb_roll_akses_teofl_preparation" ("id_roll_akses_tp", "id_username", "id_paket") VALUES
(2,	1,	'T000001'),
(3,	1,	'T000002'),
(4,	2,	'T000001'),
(5,	2,	'T000003');

DROP TABLE IF EXISTS "tb_roll_akses_teofl_test";
DROP SEQUENCE IF EXISTS tb_roll_akses_teofl_test_id_roll_akses_tt_seq;
CREATE SEQUENCE tb_roll_akses_teofl_test_id_roll_akses_tt_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."tb_roll_akses_teofl_test" (
    "id_roll_akses_tt" integer DEFAULT nextval('tb_roll_akses_teofl_test_id_roll_akses_tt_seq') NOT NULL,
    "id_username" integer NOT NULL,
    "id_paket" integer NOT NULL,
    CONSTRAINT "tb_roll_akses_teofl_test_pkey" PRIMARY KEY ("id_roll_akses_tt")
) WITH (oids = false);

INSERT INTO "tb_roll_akses_teofl_test" ("id_roll_akses_tt", "id_username", "id_paket") VALUES
(3,	1,	1),
(4,	2,	1),
(5,	1,	2);

DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."users" (
    "id" bigint DEFAULT nextval('users_id_seq'),
    "nama" character varying(500),
    "username" character varying(500),
    "password" character varying(500),
    "remember_token" character varying(500),
    "role" character varying(500),
    "created_at" character varying(500),
    "updated_at" character varying(500),
    "tanda_tangan" text,
    "email" character varying(500),
    "status_tanda_tangan" character varying(500) DEFAULT '1 = draft, 2 = selesai',
    "tanggal_expired" character varying(500)
) WITH (oids = false);

INSERT INTO "users" ("id", "nama", "username", "password", "remember_token", "role", "created_at", "updated_at", "tanda_tangan", "email", "status_tanda_tangan", "tanggal_expired") VALUES
(1,	'Irhas',	'irhas',	'$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai',	'',	'R003',	'2022-07-24 06:52:17',	'',	'',	'',	'1',	''),
(2,	'arta',	'arta',	'$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai',	'n6FVaKDCk4zygUyVnAYdjbkssUsu4Ojo4RbdWisyNqZqG144R6IolY9qFrnqLlbn1q7sHHh1NGgCHx7Y',	'R003',	'2022-07-24 06:59:26',	'2022-07-24 07:13:12',	'',	'',	'1',	''),
(3,	'fadila',	'fadila',	'$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai',	'5JppmGoScNUMyKaJ4kIhg2fKzST4LnJrDoQycTj9AahPj1Kz4Guhm5W9s5SAq1sidN7iOgHRMpxiggY6',	'R003',	'2022-07-18 03:17:57',	'2022-07-24 07:16:38',	'',	'',	'1',	''),
(4,	'bapak',	'bapak',	'$2y$10$3IDkG/4Xhc0cTAuu/7O2leK2kMQ75HnghO83qju3u2XrpIs1OryZC',	'd1HmARql4S5sVSRRW6D9mTRtgqbqHwGsdnjjUl0MfrH3TBN4dLuRroDGM0qdJdC3zhtd2b8fSpdXdnFw',	'R003',	'2022-08-04 16:25:08',	'2022-08-06 13:05:47',	'',	'',	'2',	''),
(5,	'ibuk',	'ibuk',	'$2y$10$0AjnW7hc.7PYLglf/M6PreWVtxCNh6XhSqLwhk6XftmPBkPW41ByS',	'',	'R003',	'2022-08-13 13:36:52',	'2022-08-13 13:45:39',	'',	'',	'1',	''),
(6,	'adik',	'adik',	'$2y$10$nuKXxJyExbt9fd369tz6Le8hK./F06WrjAMe86mC8tPV5Uahm/gKO',	'',	'R003',	'2022-08-13 14:04:32',	'2022-08-13 14:11:50',	'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAAAXNSR0IArs4c6QAADURJREFUeF7tnc2P3EgZh6s8ne4ogIBFoCXdmWnXRIBAICTgQjgDewCJj8si4AYnLtyREOIf4MIFToD4EAiEuPBxReS0CAkEYlfp8nRm+Fi0fC1ESSbTbeQwHfV4bLfd7bLrtZ85Tper3vf5vf7JVS7bWvEHAQhAQAgBLSROwoQABCCgMCyKAAIQEEMAwxIjFYFCAAIYFjUAAQiIIYBhiZGKQCEAAQyLGoAABMQQwLDESEWgEIAAhkUNQAACYghgWGKkIlAIQADDogYgAAExBDAsMVIRKAS2J2CM+aK19kvb9+DHkRiWHzoQBQScEAjD8CCKorkxJrbWij/fxSfgRGU6hUAHCBhj3qeU+qVS6o5S6iaG1QFRSQECXSVgjLFKqXAtvx9baz8iOV+usCSrR+wQyCEQhuHHtdY/SP38eWvtVyRDw7Akq0fsEMghYIx5pJQarP1831p7TTowDEu6gsQPgRQBY8ypUupK6t9ftdZ+TjosDEu6gsQPgTUCWWaltb49m81udQEUhtUFFckBAkqpnCsrpbW+NZvNbncBEobVBRXJofcE+mBWicgYVu9LHQDSCfTFrDAs6ZVK/L0n0CezwrB6X+4AkEwgDMN7WutLWxW6tGaV1ocpoeSKJfbeEjDGLJRSwaUTukML7FniYli9LXkSl0rAGPMXpdTTfTMrpoRSK5a4e0vAGPO8UupNaQBBEHzgzp07v+g6GK6wuq4w+XWGgDHmO0qpZzMS+q619hOdSbQgEQyrDyqTo3gCh4eHX4jj+MsZibxgrX2z+ARLJoBhlQRFMwi0ReDmzZvvXy6XP88Y/6/W2je2FVcb42JYbVBnTAhUIJC8LTRjgf1sNpulH3Cu0KvMphiWTN2IuicEcrYvLK21ez1BcCFNDKuPqpOzCALGmAdKqVE62L7cEcwSCcMSUboE2TcCeXut+mxWSQ1gWH07E8jXewJsX8iXCMPyvnwJsE8EMKtitTGsPp0N5Oo1AbYvbJYHw9rMiBYQcE6gwKxetta+2nkAQgbAsIQIRZjdJWCM+ahS6ocZGfZ2+0Ke2hhWd88DMhNAoMCsVBe+1Fy3BBhW3UTpDwIlCYRh+FOt9QezrqyCIHimD29fKInqSTMMqyox2kOgBgIFZpX0/jFr7Y9qGKZzXWBYnZOUhHwnYIz5l1IqbyEdsyoQEMPyvbqJr1MECswqttZeeuVxp5KvIRkMqwaIdAGBMgTy3sMex/EiiqJBmT763gbD6nsFkH8jBIwxy5xH4f5trX1NI0F0YBAMqwMikoK/BIq2LWit/zybzcb+Ru9fZBiWf5oQUUcIFJlVHMc/i6LomY6k2lgaGFZjqNsfKAzDZRRFLOw2IEXRtgXMansBMKzt2Yk6MjErrbWO4/jx63YxLnfyscfKHVsMyx1bb3pemdV6QDz24UYe9li54brqFcNyy7f13vf3988Gg8Gl93+fnp6+dHJy8vrWA+xQAOyxci8mhuWecasjZH1xJQkomRoyLaxPGvZY1ceyqCcMqxnOrYwShuGZ1jr36ypMC+uRhT1W9XAs0wuGVYaSwDbj8fjl0Wj0qqLQMazdhD08PPxaHMefyeqFPVa7sc07GsNyw7X1XrMW2tNBMS3cXiZjzN+VUk9l9cC2he25bjoSw9pESOjveWtX6+lgWNXFDcPwU1rrb+R9cQqzqs60yhEYVhVagtqmDet8/1WyFetCFkwLy4tqjPmVUuq9eUfAsjzLbVtiWNuS8/i4rOngw4cP/zMcDl+ZdixOsnJCGmNOlVJXclqfWWvzfis3AK1KEcCwSmGS02g6nS6CILjw+E0cx8kjOXuTyeRvw+Hwwt6rs7Ozxd27d3m1SY7E51PAbxZUwG1r7S05FSI7UgxLtn4Xop9MJi8Nh8PXpVNaN6WsqSL7sbKL4PDwcBbHsckrkTiOPx1F0bc6VELep4JheS9R+QCzFtrTC+s8plOOZxiGC6113oPi962118r1RKs6CWBYddJssa+8bQzpNarJZPLicDh8w3qoy+VyeXR0lLvBtMW0Gh+6aG9VEozW+uuz2eyzjQfGgI8JYFgdKISsdaskrbwF9awrMZ8W3xPzXcnS5HS1aG9V8jQT71xv/2TBsNrXYKcIyqxbpQfwfVq4bqhNGOmmvVVaazubzQ53EoqDayGAYdWCsb1OyqxbZUXn4+J7ElPyd2mz2P8f1v52FEWfrJv0pr1VLKzXTXy3/jCs3fi1enTZdasyhlU0hWwiybxp7WpsF7vy2VvVhLL1joFh1cuzsd6qrlulA8s6vonpVxagg4ODe3t7exvvutUVH3urGivT2gfCsGpH6r7DbdatylxltXW3MGdae6q1Hq7HXcdaEnur3NenyxEwLJd0HfW97bpVOhwf1rGyrq5W07+672ayt8pRQTbYLYbVIOw6htpl3So9fhiGyQL3k38n36eIoqjRmigypfQLCLddxzLGPFRKXbhaS125sbeqjuJsoI9Gi7OBfDo9xHg8/u9oNHpFOslt13ba3t6QNswkr7QppQ2tSq6bjIq9VfJOFwxLkGZZVyO7rDu1ufCeZZZZV3gZpvY7a+07imQrYVTJjnX2Vgmq/VWoGJYQ0eqcCq6nnDbBR48evXh8fPy0Syx5dzgXi8X9+Xx+4W5hlavAMkZ1fhXHQ8suBXbYN4blEG5dXe+6hWHD1cjjD6uu/rZdJyqb63Q6fS4Ignel2+eNm7NX6jlr7XuSPowxP1FKfajM+Frr781ms2fLtKWNnwQwLD91eRJVXVsY8tJs+k7hNnc4847J2hGflSdG5XmRVwgPw6oAq42m25zgVeLMWviusrBdcaxllslsGs8Y80ApNaoyVtIWo6pKzP/2GJbHGrlat1pPucoa0S6optPpb4IgeGe6j+Vy+eujo6N3b+o7MdZzEypsmrxdNQiC7zP120RU5u8Ylqe67e/vPxgMBpeuKjZdjVRNJ+tT9nWPcb7WdGGtLPnfNnc4C74GxHvVq4ovsD2G5alodW9hKEozPVbd73kvu4WhjBSrK63zq60XlFLPW2s/XOZY2sgngGF5qGGdJ3iZ9FwuvOdNBV1cxZXJlTayCWBYnuk3Ho8fjEYj51PB9bQzDKu2R3SavFL0TErCcUAAw3IAdZcu2zjBXS28N32luAt3jpVBAMPySKe2TnAXhsVU0KPC6lAoGJYnYo7H4/uj0ehqOpym1np2ecg4C2EbV4qeSEkYDglgWA7hVum67RO8zjuFbV0pVuFNW5kEMCwPdGtig+imNOu6U8hUcBNpft+FAIa1C70ajp1MJv8YDoevbWsquBq3LsNq+0qxBknowmMCGFbL4uQ8K7iMoqjRLzHX8fZRpoItF1MPhsewWhTZh6ngKv1d7xS6fAVOixIxtGcEMKyWBPHtBN/FsKbT6W+DIHh7GuVisXg4n88v3flsCTnDdoAAhtWCiK7fcbVNSru8Ltn1K3C2yYdjukkAw2pBV19P8IzXJf/p+Ph4UoTIp2ltC1IyZMMEMKyGgft8gle9U+jbtLZhKRmuBQIYVoPQfT/Bq3y4lHWrBguHoZ4QwLAaKgYf163SqVdZePd1WtuQnAzTEgEMqyHwZT4a2lAohcOkjSjrraBZuSSdNvXcow+ciKEdAhhWA9x9XrdKp79pHUtSLg1IyxANE8CwHAO/fv36tatXr95LD+Pr1UjRtLDgA6in8/m88ldtHKOn+w4SwLAci5rzuEocRVHgeOitus/7KMXBwcHv9/b23pru1PWHV7dKgoM6SwDDcijtjRs3Hly5cqXR1x3XkU7WOlYQBJcMFrOqgzZ9VCGAYVWhVbFt1p20xWKxmM/ng4pdNdo8b1FdyrS2UVgM1igBDMsR7ul0ehYEwaU3Lvi6drWOYX9/3w4Gg7AIzWKx+MN8Pn+bI3x0C4FMAhiWo8LIuro6PT3958nJyVOOhqyt202GJcF0a4NBR14RwLAcyZHxfilvF9qzEKxMK9mHpc//WLNyVCx0W5oAhlUaVbWG618oTo6Mokg064ODgz/O5/O3VKNAawjUS0D0SVQvCnqDAAR8J4Bh+a4Q8UEAAk8IYFgUAwQgIIYAhiVGKgKFAAQwLGoAAhAQQwDDEiMVgUIAAhgWNQABCIghgGGJkYpAIQABDIsagAAExBDAsMRIRaAQgACGRQ1AAAJiCGBYYqQiUAhAAMOiBiAAATEEMCwxUhEoBCCAYVEDEICAGAIYlhipCBQCEMCwqAEIQEAMAQxLjFQECgEIYFjUAAQgIIYAhiVGKgKFAAQwLGoAAhAQQwDDEiMVgUIAAhgWNQABCIghgGGJkYpAIQABDIsagAAExBDAsMRIRaAQgACGRQ1AAAJiCGBYYqQiUAhAAMOiBiAAATEEMCwxUhEoBCCAYVEDEICAGAIYlhipCBQCEMCwqAEIQEAMAQxLjFQECgEIYFjUAAQgIIYAhiVGKgKFAAQwLGoAAhAQQwDDEiMVgUIAAhgWNQABCIghgGGJkYpAIQABDIsagAAExBDAsMRIRaAQgACGRQ1AAAJiCGBYYqQiUAhA4H++xi7iGjJaHgAAAABJRU5ErkJggg==',	'',	'2',	''),
(7,	'admin_super',	'admin_super',	'$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai',	NULL,	'R001',	'2022-07-18 03:18:19',	'2022-09-09 04:09:35',	'',	'',	'2',	'');

-- 2022-09-09 11:17:34.731736+07
