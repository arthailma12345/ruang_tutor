--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16
-- Dumped by pg_dump version 11.16

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: issues_file_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.issues_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issues_file_id_seq OWNER TO postgres;

--
-- Name: issues_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.issues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issues_id_seq OWNER TO postgres;

--
-- Name: issues_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.issues_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issues_status_id_seq OWNER TO postgres;

--
-- Name: komentar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.komentar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.komentar_id_seq OWNER TO postgres;

--
-- Name: m_flag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_flag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_flag_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: m_flag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_flag (
    id smallint DEFAULT nextval('public.m_flag_id_seq'::regclass) NOT NULL,
    nama_flag character varying(100),
    keterangan character varying(100)
);


ALTER TABLE public.m_flag OWNER TO postgres;

--
-- Name: m_flag_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_flag_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_flag_seq OWNER TO postgres;

--
-- Name: m_latihan_jawaban; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_latihan_jawaban (
    id character varying,
    m_latihan_soal_id character varying,
    huruf character varying,
    jawaban character varying,
    created_at character varying,
    updated_at character varying
);


ALTER TABLE public.m_latihan_jawaban OWNER TO postgres;

--
-- Name: m_latihan_materi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_latihan_materi (
    id character varying,
    m_sub_toefl_preparation_id character varying,
    url_vdeo_latihan_materi character varying,
    created_at character varying,
    update_at character varying
);


ALTER TABLE public.m_latihan_materi OWNER TO postgres;

--
-- Name: m_latihan_soal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_latihan_soal (
    id character varying,
    m_sub_toefl_preparation_id character varying,
    soal text,
    created_at character varying,
    update_at character varying,
    m_latihan_jawaban_id character varying,
    soal_point smallint
);


ALTER TABLE public.m_latihan_soal OWNER TO postgres;

--
-- Name: m_libur_nasional_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_libur_nasional_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_libur_nasional_id_seq OWNER TO postgres;

--
-- Name: m_mapping_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_mapping_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_mapping_menu_id_seq OWNER TO postgres;

--
-- Name: m_mapping_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_mapping_menu (
    id smallint DEFAULT nextval('public.m_mapping_menu_id_seq'::regclass),
    m_role_id character varying,
    m_sub_menu_id character varying(100),
    created_at character varying(100),
    updated_at character varying(100)
);


ALTER TABLE public.m_mapping_menu OWNER TO postgres;

--
-- Name: m_mapping_menu_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_mapping_menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_mapping_menu_seq OWNER TO postgres;

--
-- Name: m_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_menu (
    id character varying,
    nama_menu character varying(100),
    created_at character varying(100),
    updated_at character varying(100)
);


ALTER TABLE public.m_menu OWNER TO postgres;

--
-- Name: m_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_menu_id_seq OWNER TO postgres;

--
-- Name: m_menu_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_menu_seq OWNER TO postgres;

--
-- Name: m_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_migrations_id_seq OWNER TO postgres;

--
-- Name: m_pembahasan_materi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_pembahasan_materi (
    id character varying,
    m_sub_toefl_preparation_id character varying,
    url_vdeo_pembahasan_materi character varying,
    created_at character varying,
    update_at character varying
);


ALTER TABLE public.m_pembahasan_materi OWNER TO postgres;

--
-- Name: m_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_role (
    id character varying,
    nama_role character varying(16) DEFAULT NULL::character varying,
    flag character varying(2) DEFAULT NULL::character varying,
    created_at character varying(100),
    updated_at character varying(100)
);


ALTER TABLE public.m_role OWNER TO postgres;

--
-- Name: m_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_role_id_seq OWNER TO postgres;

--
-- Name: m_role_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_role_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_role_seq OWNER TO postgres;

--
-- Name: m_sub_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_sub_menu (
    id character varying DEFAULT 'nextval(''m_sub_menu_id_seq'')'::character varying,
    m_menu_id character varying,
    nama_sub_menu character varying(100),
    url_sub_menu character varying(100),
    created_at character varying(100),
    updated_at character varying(100)
);


ALTER TABLE public.m_sub_menu OWNER TO postgres;

--
-- Name: m_sub_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_sub_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_sub_menu_id_seq OWNER TO postgres;

--
-- Name: m_sub_menu_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_sub_menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_sub_menu_seq OWNER TO postgres;

--
-- Name: m_sub_toefl_preparation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_sub_toefl_preparation (
    id character varying,
    m_toefl_preparation_id character varying,
    nama_sub_toefl_preparation character varying,
    created_at character varying,
    updated_at character varying
);


ALTER TABLE public.m_sub_toefl_preparation OWNER TO postgres;

--
-- Name: m_toefl_preparation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_toefl_preparation (
    id character varying,
    nama_toefl_preparation character varying,
    created_at character varying,
    updated_at character varying
);


ALTER TABLE public.m_toefl_preparation OWNER TO postgres;

--
-- Name: m_toefl_preparation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_toefl_preparation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_toefl_preparation_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id smallint DEFAULT nextval('public.migrations_id_seq'::regclass),
    migration character varying(46) DEFAULT NULL::character varying,
    batch smallint
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_seq OWNER TO postgres;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(1) DEFAULT NULL::character varying,
    token character varying(1) DEFAULT NULL::character varying,
    created_at character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: pegawai_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pegawai_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pegawai_id_seq OWNER TO postgres;

--
-- Name: tb_paket_materi_teofl_test_id_materi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_paket_materi_teofl_test_id_materi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.tb_paket_materi_teofl_test_id_materi_seq OWNER TO postgres;

--
-- Name: tb_paket_materi_teofl_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_paket_materi_teofl_test (
    id_materi integer DEFAULT nextval('public.tb_paket_materi_teofl_test_id_materi_seq'::regclass) NOT NULL,
    judul_materi character(50) NOT NULL,
    video_materi text NOT NULL,
    id_paket integer NOT NULL
);


ALTER TABLE public.tb_paket_materi_teofl_test OWNER TO postgres;

--
-- Name: tb_teofl_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_teofl_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.tb_teofl_test_id_seq OWNER TO postgres;

--
-- Name: tb_paket_teofl_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_paket_teofl_test (
    id_paket integer DEFAULT nextval('public.tb_teofl_test_id_seq'::regclass) NOT NULL,
    nama_paket character(50) NOT NULL
);


ALTER TABLE public.tb_paket_teofl_test OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_preparation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_roll_akses_teofl_preparation (
    id_roll_akses_tp integer NOT NULL,
    id_username integer NOT NULL,
    id_paket text NOT NULL
);


ALTER TABLE public.tb_roll_akses_teofl_preparation OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq OWNED BY public.tb_roll_akses_teofl_preparation.id_roll_akses_tp;


--
-- Name: tb_roll_akses_teofl_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_roll_akses_teofl_test (
    id_roll_akses_tt integer NOT NULL,
    id_username integer NOT NULL,
    id_paket integer NOT NULL
);


ALTER TABLE public.tb_roll_akses_teofl_test OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_test_id_roll_akses_tt_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_test_id_roll_akses_tt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq OWNED BY public.tb_roll_akses_teofl_test.id_roll_akses_tt;


--
-- Name: tb_roll_paket_user_id_roll_paket_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_roll_paket_user_id_roll_paket_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.tb_roll_paket_user_id_roll_paket_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint DEFAULT nextval('public.users_id_seq'::regclass),
    nama character varying(500),
    username character varying(500),
    password character varying(500),
    remember_token character varying(500),
    role character varying(500),
    created_at character varying(500),
    updated_at character varying(500),
    tanda_tangan text,
    email character varying(500),
    status_tanda_tangan character varying(500) DEFAULT '1 = draft, 2 = selesai'::character varying,
    tanggal_expired character varying(500)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_seq OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_preparation id_roll_akses_tp; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roll_akses_teofl_preparation ALTER COLUMN id_roll_akses_tp SET DEFAULT nextval('public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq'::regclass);


--
-- Name: tb_roll_akses_teofl_test id_roll_akses_tt; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roll_akses_teofl_test ALTER COLUMN id_roll_akses_tt SET DEFAULT nextval('public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq'::regclass);


--
-- Data for Name: m_flag; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_flag (id, nama_flag, keterangan) FROM stdin;
1	AP	Admin Programmer
2	AS	Admin Super
3	PE	Peserta
\.


--
-- Data for Name: m_latihan_jawaban; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_latihan_jawaban (id, m_latihan_soal_id, huruf, jawaban, created_at, updated_at) FROM stdin;
J000001	L000020	a	coba1	2022-08-27 14:32:41	\N
J000002	L000018	b	ya	2022-08-27 15:08:41	\N
J000003	L000020	c	af	2022-08-29 15:32:39	\N
J000004	L000019	a	diferent	2022-08-29 16:46:20	\N
J000005	L000020	b	gh	2022-08-29 17:17:37	\N
\.


--
-- Data for Name: m_latihan_materi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_latihan_materi (id, m_sub_toefl_preparation_id, url_vdeo_latihan_materi, created_at, update_at) FROM stdin;
M000001	S000001	coba2	2022-08-17 18:14:49	\N
M000002	S000002	ini 3	2022-08-17 18:17:32	\N
\.


--
-- Data for Name: m_latihan_soal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_latihan_soal (id, m_sub_toefl_preparation_id, soal, created_at, update_at, m_latihan_jawaban_id, soal_point) FROM stdin;
L000002	S000001	coba 2	\N	\N	\N	\N
L000003	S000001	coba 3	\N	\N	\N	\N
L000004	S000001	coba 4	\N	\N	\N	\N
L000005	S000001	coba 5	\N	\N	\N	\N
L000006	S000001	coba 6	\N	\N	\N	\N
L000007	S000001	coba 7	\N	\N	\N	\N
L000008	S000001	coba 8	\N	\N	\N	\N
L000009	S000001	coba 9	\N	\N	\N	\N
L000010	S000001	coba 10	\N	\N	\N	\N
L000011	S000001	coba 11	\N	\N	\N	\N
L000012	S000001	coba 12	\N	\N	\N	\N
L000001	S000001	coba1	\N	\N	\N	\N
L000013	S000001	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>cban 5</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 17:16:10	\N	\N	\N
L000014	S000001	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>dssd</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 17:30:21	\N	\N	\N
L000015	S000001	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>dssdsd</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 17:30:32	\N	\N	\N
L000016	S000001	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>dssdsdafafd</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 17:30:45	\N	\N	\N
L000017	S000002	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>ini coba pertanyaan</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 18:32:54	\N	\N	\N
L000018	S000003	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>soal 1</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-26 00:34:48	\N	\N	\N
L000019	S000003	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>soal 2</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-26 00:38:34	\N	\N	\N
L000020	S000003	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>coba1 </p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-27 05:40:05	\N	J000003	\N
\.


--
-- Data for Name: m_mapping_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_mapping_menu (id, m_role_id, m_sub_menu_id, created_at, updated_at) FROM stdin;
8	R001	S002	2022-08-29 02:53:25	\N
9	R001	S003	2022-08-29 02:53:25	\N
10	R001	S004	2022-08-29 02:53:25	\N
11	R001	S015	2022-08-29 02:53:25	\N
12	R001	S008	2022-08-29 02:53:25	\N
13	R001	S014	2022-08-29 02:53:25	\N
14	R001	S016	2022-08-29 02:53:25	\N
15	R001	S017	2022-08-29 02:53:25	\N
16	R003	S016	2022-08-29 03:07:31	\N
17	R003	S017	2022-08-29 03:07:31	\N
18	R003	S008	2022-08-29 03:07:31	\N
\.


--
-- Data for Name: m_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_menu (id, nama_menu, created_at, updated_at) FROM stdin;
M007	STUDY ROOM	2022-07-25 16:00:36	\N
M001	DASHBOARD	2022-07-07 00:59:12	\N
M006	AKSES PAKET	2022-07-08 03:09:48	\N
M003	PENGATURAN	2022-07-07 00:58:45	\N
\.


--
-- Data for Name: m_pembahasan_materi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_pembahasan_materi (id, m_sub_toefl_preparation_id, url_vdeo_pembahasan_materi, created_at, update_at) FROM stdin;
M000001	S000001	coba3	2022-08-17 18:32:08	\N
M000002	S000002	coba4	2022-08-17 18:32:34	\N
\.


--
-- Data for Name: m_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_role (id, nama_role, flag, created_at, updated_at) FROM stdin;
R001	admin super	AS	2022-07-06 06:52:28	\N
R003	peserta	PE	2022-07-06 06:53:21	\N
\.


--
-- Data for Name: m_sub_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_sub_menu (id, m_menu_id, nama_sub_menu, url_sub_menu, created_at, updated_at) FROM stdin;
S009	M002	Issues	issues/index	2022-07-08 03:06:49	\N
S011	M004	Report Aging Issues GOSAP	report_aging_issues_gosap	2022-07-08 03:11:26	\N
S010	M004	Report Aging Issues HELPDESK	report_aging_issues_helpdesk	2022-07-08 03:10:36	\N
S012	M004	Response and Resolution Report	report_and_resolution_report	2022-07-08 03:12:53	\N
S013	M005	Created Issues	tiket/index	2022-07-08 03:13:31	\N
S016	M007	TOEFL TEST	study_room/index	2022-07-25 16:02:08	\N
S017	M007	TOEFL PREPARATION	toefl_preparation/index	2022-08-29 02:52:40	\N
S014	M006	PAKET PESERTA	RollUserPaket/index	2022-07-08 03:13:52	\N
S002	M003	MENU	menu/index	2022-07-07 01:46:28	\N
S015	M003	PESERTA	user/index	2022-07-24 06:23:32	\N
S003	M003	PEMETAAN MENU	mapping_menu/index	2022-07-07 03:20:41	\N
S004	M003	AKSES AKUN	role/index	2022-07-07 01:51:11	\N
S008	M001	HOME	home/index	2022-07-08 03:02:48	\N
\.


--
-- Data for Name: m_sub_toefl_preparation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_sub_toefl_preparation (id, m_toefl_preparation_id, nama_sub_toefl_preparation, created_at, updated_at) FROM stdin;
S000001	T000002	Tips 1	2022-08-08 17:36:08	\N
S000002	T000002	Tips 2	2022-08-08 17:36:19	\N
S000003	T000003	Tips 1	2022-08-13 14:06:22	\N
\.


--
-- Data for Name: m_toefl_preparation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_toefl_preparation (id, nama_toefl_preparation, created_at, updated_at) FROM stdin;
T000001	Listening 1	2022-08-08 17:35:26	
T000002	Listening 2	2022-08-08 17:35:43	
T000003	Listening 3	2022-08-13 14:05:51	
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: tb_paket_materi_teofl_test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_paket_materi_teofl_test (id_materi, judul_materi, video_materi, id_paket) FROM stdin;
15	Ojo dibandingke                                   	https://www.youtube.com/embed/sYJ_t2MiJF0	1
16	Mendung tanpo udan                                	https://www.youtube.com/embed/r8T61-29o10	2
17	Tak ingat usia                                    	https://www.youtube.com/embed/FB1YNEOspyA	2
12	Dunia Tipu Tipu                                   	https://www.youtube.com/embed/sYJ_t2MiJF0	1
13	Hati Hati di jalan raya                           	https://www.youtube.com/embed/_N6vSc_mT6I	1
\.


--
-- Data for Name: tb_paket_teofl_test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_paket_teofl_test (id_paket, nama_paket) FROM stdin;
1	Paket A                                           
2	Paket AB                                          
\.


--
-- Data for Name: tb_roll_akses_teofl_preparation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_roll_akses_teofl_preparation (id_roll_akses_tp, id_username, id_paket) FROM stdin;
2	1	T000001
3	1	T000002
4	2	T000001
5	2	T000003
\.


--
-- Data for Name: tb_roll_akses_teofl_test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_roll_akses_teofl_test (id_roll_akses_tt, id_username, id_paket) FROM stdin;
3	1	1
4	2	1
5	1	2
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, nama, username, password, remember_token, role, created_at, updated_at, tanda_tangan, email, status_tanda_tangan, tanggal_expired) FROM stdin;
1	Irhas	irhas	$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai		R003	2022-07-24 06:52:17				1	
2	arta	arta	$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai	n6FVaKDCk4zygUyVnAYdjbkssUsu4Ojo4RbdWisyNqZqG144R6IolY9qFrnqLlbn1q7sHHh1NGgCHx7Y	R003	2022-07-24 06:59:26	2022-07-24 07:13:12			1	
3	fadila	fadila	$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai	5JppmGoScNUMyKaJ4kIhg2fKzST4LnJrDoQycTj9AahPj1Kz4Guhm5W9s5SAq1sidN7iOgHRMpxiggY6	R003	2022-07-18 03:17:57	2022-07-24 07:16:38			1	
4	bapak	bapak	$2y$10$3IDkG/4Xhc0cTAuu/7O2leK2kMQ75HnghO83qju3u2XrpIs1OryZC	d1HmARql4S5sVSRRW6D9mTRtgqbqHwGsdnjjUl0MfrH3TBN4dLuRroDGM0qdJdC3zhtd2b8fSpdXdnFw	R003	2022-08-04 16:25:08	2022-08-06 13:05:47			2	
5	ibuk	ibuk	$2y$10$0AjnW7hc.7PYLglf/M6PreWVtxCNh6XhSqLwhk6XftmPBkPW41ByS		R003	2022-08-13 13:36:52	2022-08-13 13:45:39			1	
6	adik	adik	$2y$10$nuKXxJyExbt9fd369tz6Le8hK./F06WrjAMe86mC8tPV5Uahm/gKO		R003	2022-08-13 14:04:32	2022-08-13 14:11:50	data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAAAXNSR0IArs4c6QAADURJREFUeF7tnc2P3EgZh6s8ne4ogIBFoCXdmWnXRIBAICTgQjgDewCJj8si4AYnLtyREOIf4MIFToD4EAiEuPBxReS0CAkEYlfp8nRm+Fi0fC1ESSbTbeQwHfV4bLfd7bLrtZ85Tper3vf5vf7JVS7bWvEHAQhAQAgBLSROwoQABCCgMCyKAAIQEEMAwxIjFYFCAAIYFjUAAQiIIYBhiZGKQCEAAQyLGoAABMQQwLDESEWgEIAAhkUNQAACYghgWGKkIlAIQADDogYgAAExBDAsMVIRKAS2J2CM+aK19kvb9+DHkRiWHzoQBQScEAjD8CCKorkxJrbWij/fxSfgRGU6hUAHCBhj3qeU+qVS6o5S6iaG1QFRSQECXSVgjLFKqXAtvx9baz8iOV+usCSrR+wQyCEQhuHHtdY/SP38eWvtVyRDw7Akq0fsEMghYIx5pJQarP1831p7TTowDEu6gsQPgRQBY8ypUupK6t9ftdZ+TjosDEu6gsQPgTUCWWaltb49m81udQEUhtUFFckBAkqpnCsrpbW+NZvNbncBEobVBRXJofcE+mBWicgYVu9LHQDSCfTFrDAs6ZVK/L0n0CezwrB6X+4AkEwgDMN7WutLWxW6tGaV1ocpoeSKJfbeEjDGLJRSwaUTukML7FniYli9LXkSl0rAGPMXpdTTfTMrpoRSK5a4e0vAGPO8UupNaQBBEHzgzp07v+g6GK6wuq4w+XWGgDHmO0qpZzMS+q619hOdSbQgEQyrDyqTo3gCh4eHX4jj+MsZibxgrX2z+ARLJoBhlQRFMwi0ReDmzZvvXy6XP88Y/6/W2je2FVcb42JYbVBnTAhUIJC8LTRjgf1sNpulH3Cu0KvMphiWTN2IuicEcrYvLK21ez1BcCFNDKuPqpOzCALGmAdKqVE62L7cEcwSCcMSUboE2TcCeXut+mxWSQ1gWH07E8jXewJsX8iXCMPyvnwJsE8EMKtitTGsPp0N5Oo1AbYvbJYHw9rMiBYQcE6gwKxetta+2nkAQgbAsIQIRZjdJWCM+ahS6ocZGfZ2+0Ke2hhWd88DMhNAoMCsVBe+1Fy3BBhW3UTpDwIlCYRh+FOt9QezrqyCIHimD29fKInqSTMMqyox2kOgBgIFZpX0/jFr7Y9qGKZzXWBYnZOUhHwnYIz5l1IqbyEdsyoQEMPyvbqJr1MECswqttZeeuVxp5KvIRkMqwaIdAGBMgTy3sMex/EiiqJBmT763gbD6nsFkH8jBIwxy5xH4f5trX1NI0F0YBAMqwMikoK/BIq2LWit/zybzcb+Ru9fZBiWf5oQUUcIFJlVHMc/i6LomY6k2lgaGFZjqNsfKAzDZRRFLOw2IEXRtgXMansBMKzt2Yk6MjErrbWO4/jx63YxLnfyscfKHVsMyx1bb3pemdV6QDz24UYe9li54brqFcNyy7f13vf3988Gg8Gl93+fnp6+dHJy8vrWA+xQAOyxci8mhuWecasjZH1xJQkomRoyLaxPGvZY1ceyqCcMqxnOrYwShuGZ1jr36ypMC+uRhT1W9XAs0wuGVYaSwDbj8fjl0Wj0qqLQMazdhD08PPxaHMefyeqFPVa7sc07GsNyw7X1XrMW2tNBMS3cXiZjzN+VUk9l9cC2he25bjoSw9pESOjveWtX6+lgWNXFDcPwU1rrb+R9cQqzqs60yhEYVhVagtqmDet8/1WyFetCFkwLy4tqjPmVUuq9eUfAsjzLbVtiWNuS8/i4rOngw4cP/zMcDl+ZdixOsnJCGmNOlVJXclqfWWvzfis3AK1KEcCwSmGS02g6nS6CILjw+E0cx8kjOXuTyeRvw+Hwwt6rs7Ozxd27d3m1SY7E51PAbxZUwG1r7S05FSI7UgxLtn4Xop9MJi8Nh8PXpVNaN6WsqSL7sbKL4PDwcBbHsckrkTiOPx1F0bc6VELep4JheS9R+QCzFtrTC+s8plOOZxiGC6113oPi962118r1RKs6CWBYddJssa+8bQzpNarJZPLicDh8w3qoy+VyeXR0lLvBtMW0Gh+6aG9VEozW+uuz2eyzjQfGgI8JYFgdKISsdaskrbwF9awrMZ8W3xPzXcnS5HS1aG9V8jQT71xv/2TBsNrXYKcIyqxbpQfwfVq4bqhNGOmmvVVaazubzQ53EoqDayGAYdWCsb1OyqxbZUXn4+J7ElPyd2mz2P8f1v52FEWfrJv0pr1VLKzXTXy3/jCs3fi1enTZdasyhlU0hWwiybxp7WpsF7vy2VvVhLL1joFh1cuzsd6qrlulA8s6vonpVxagg4ODe3t7exvvutUVH3urGivT2gfCsGpH6r7DbdatylxltXW3MGdae6q1Hq7HXcdaEnur3NenyxEwLJd0HfW97bpVOhwf1rGyrq5W07+672ayt8pRQTbYLYbVIOw6htpl3So9fhiGyQL3k38n36eIoqjRmigypfQLCLddxzLGPFRKXbhaS125sbeqjuJsoI9Gi7OBfDo9xHg8/u9oNHpFOslt13ba3t6QNswkr7QppQ2tSq6bjIq9VfJOFwxLkGZZVyO7rDu1ufCeZZZZV3gZpvY7a+07imQrYVTJjnX2Vgmq/VWoGJYQ0eqcCq6nnDbBR48evXh8fPy0Syx5dzgXi8X9+Xx+4W5hlavAMkZ1fhXHQ8suBXbYN4blEG5dXe+6hWHD1cjjD6uu/rZdJyqb63Q6fS4Ignel2+eNm7NX6jlr7XuSPowxP1FKfajM+Frr781ms2fLtKWNnwQwLD91eRJVXVsY8tJs+k7hNnc4847J2hGflSdG5XmRVwgPw6oAq42m25zgVeLMWviusrBdcaxllslsGs8Y80ApNaoyVtIWo6pKzP/2GJbHGrlat1pPucoa0S6optPpb4IgeGe6j+Vy+eujo6N3b+o7MdZzEypsmrxdNQiC7zP120RU5u8Ylqe67e/vPxgMBpeuKjZdjVRNJ+tT9nWPcb7WdGGtLPnfNnc4C74GxHvVq4ovsD2G5alodW9hKEozPVbd73kvu4WhjBSrK63zq60XlFLPW2s/XOZY2sgngGF5qGGdJ3iZ9FwuvOdNBV1cxZXJlTayCWBYnuk3Ho8fjEYj51PB9bQzDKu2R3SavFL0TErCcUAAw3IAdZcu2zjBXS28N32luAt3jpVBAMPySKe2TnAXhsVU0KPC6lAoGJYnYo7H4/uj0ehqOpym1np2ecg4C2EbV4qeSEkYDglgWA7hVum67RO8zjuFbV0pVuFNW5kEMCwPdGtig+imNOu6U8hUcBNpft+FAIa1C70ajp1MJv8YDoevbWsquBq3LsNq+0qxBknowmMCGFbL4uQ8K7iMoqjRLzHX8fZRpoItF1MPhsewWhTZh6ngKv1d7xS6fAVOixIxtGcEMKyWBPHtBN/FsKbT6W+DIHh7GuVisXg4n88v3flsCTnDdoAAhtWCiK7fcbVNSru8Ltn1K3C2yYdjukkAw2pBV19P8IzXJf/p+Ph4UoTIp2ltC1IyZMMEMKyGgft8gle9U+jbtLZhKRmuBQIYVoPQfT/Bq3y4lHWrBguHoZ4QwLAaKgYf163SqVdZePd1WtuQnAzTEgEMqyHwZT4a2lAohcOkjSjrraBZuSSdNvXcow+ciKEdAhhWA9x9XrdKp79pHUtSLg1IyxANE8CwHAO/fv36tatXr95LD+Pr1UjRtLDgA6in8/m88ldtHKOn+w4SwLAci5rzuEocRVHgeOitus/7KMXBwcHv9/b23pru1PWHV7dKgoM6SwDDcijtjRs3Hly5cqXR1x3XkU7WOlYQBJcMFrOqgzZ9VCGAYVWhVbFt1p20xWKxmM/ng4pdNdo8b1FdyrS2UVgM1igBDMsR7ul0ehYEwaU3Lvi6drWOYX9/3w4Gg7AIzWKx+MN8Pn+bI3x0C4FMAhiWo8LIuro6PT3958nJyVOOhqyt202GJcF0a4NBR14RwLAcyZHxfilvF9qzEKxMK9mHpc//WLNyVCx0W5oAhlUaVbWG618oTo6Mokg064ODgz/O5/O3VKNAawjUS0D0SVQvCnqDAAR8J4Bh+a4Q8UEAAk8IYFgUAwQgIIYAhiVGKgKFAAQwLGoAAhAQQwDDEiMVgUIAAhgWNQABCIghgGGJkYpAIQABDIsagAAExBDAsMRIRaAQgACGRQ1AAAJiCGBYYqQiUAhAAMOiBiAAATEEMCwxUhEoBCCAYVEDEICAGAIYlhipCBQCEMCwqAEIQEAMAQxLjFQECgEIYFjUAAQgIIYAhiVGKgKFAAQwLGoAAhAQQwDDEiMVgUIAAhgWNQABCIghgGGJkYpAIQABDIsagAAExBDAsMRIRaAQgACGRQ1AAAJiCGBYYqQiUAhAAMOiBiAAATEEMCwxUhEoBCCAYVEDEICAGAIYlhipCBQCEMCwqAEIQEAMAQxLjFQECgEIYFjUAAQgIIYAhiVGKgKFAAQwLGoAAhAQQwDDEiMVgUIAAhgWNQABCIghgGGJkYpAIQABDIsagAAExBDAsMRIRaAQgACGRQ1AAAJiCGBYYqQiUAhA4H++xi7iGjJaHgAAAABJRU5ErkJggg==		2	
7	admin_super	admin_super	$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai	LQorZEbCGhuTFHgcycJaWXvR5Wn7FIZjTfUTaaenfIaae5dXNx0KF8Rc0IRlyTcmLKdfJI3MxKYK2duV	R001	2022-07-18 03:18:19	2022-09-12 12:12:11			2	
\.


--
-- Name: issues_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.issues_file_id_seq', 59, true);


--
-- Name: issues_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.issues_id_seq', 108, true);


--
-- Name: issues_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.issues_status_id_seq', 46, true);


--
-- Name: komentar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.komentar_id_seq', 232, true);


--
-- Name: m_flag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_flag_id_seq', 4, true);


--
-- Name: m_flag_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_flag_seq', 2, true);


--
-- Name: m_libur_nasional_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_libur_nasional_id_seq', 4, true);


--
-- Name: m_mapping_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_mapping_menu_id_seq', 218, true);


--
-- Name: m_mapping_menu_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_mapping_menu_seq', 1, false);


--
-- Name: m_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_menu_id_seq', 5, true);


--
-- Name: m_menu_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_menu_seq', 1, false);


--
-- Name: m_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_migrations_id_seq', 3, true);


--
-- Name: m_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_role_id_seq', 10, true);


--
-- Name: m_role_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_role_seq', 1, false);


--
-- Name: m_sub_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_sub_menu_id_seq', 13, true);


--
-- Name: m_sub_menu_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_sub_menu_seq', 1, false);


--
-- Name: m_toefl_preparation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_toefl_preparation_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 2, true);


--
-- Name: migrations_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_seq', 1, false);


--
-- Name: pegawai_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pegawai_id_seq', 44094, true);


--
-- Name: tb_paket_materi_teofl_test_id_materi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_paket_materi_teofl_test_id_materi_seq', 1, false);


--
-- Name: tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq', 5, true);


--
-- Name: tb_roll_akses_teofl_test_id_roll_akses_tt_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq', 5, true);


--
-- Name: tb_roll_paket_user_id_roll_paket_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_roll_paket_user_id_roll_paket_seq', 12, true);


--
-- Name: tb_teofl_test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_teofl_test_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 12681, true);


--
-- Name: users_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_seq', 1, false);


--
-- Name: m_flag m_flag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_flag
    ADD CONSTRAINT m_flag_pkey PRIMARY KEY (id);


--
-- Name: tb_roll_akses_teofl_preparation tb_roll_akses_teofl_preparation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roll_akses_teofl_preparation
    ADD CONSTRAINT tb_roll_akses_teofl_preparation_pkey PRIMARY KEY (id_roll_akses_tp);


--
-- Name: tb_roll_akses_teofl_test tb_roll_akses_teofl_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roll_akses_teofl_test
    ADD CONSTRAINT tb_roll_akses_teofl_test_pkey PRIMARY KEY (id_roll_akses_tt);


--
-- PostgreSQL database dump complete
--

