--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16
-- Dumped by pg_dump version 11.16

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: issues_file_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.issues_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issues_file_id_seq OWNER TO postgres;

--
-- Name: issues_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.issues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issues_id_seq OWNER TO postgres;

--
-- Name: issues_status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.issues_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issues_status_id_seq OWNER TO postgres;

--
-- Name: komentar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.komentar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.komentar_id_seq OWNER TO postgres;

--
-- Name: m_flag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_flag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_flag_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: m_flag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_flag (
    id smallint DEFAULT nextval('public.m_flag_id_seq'::regclass) NOT NULL,
    nama_flag character varying(100),
    keterangan character varying(100)
);


ALTER TABLE public.m_flag OWNER TO postgres;

--
-- Name: m_flag_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_flag_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_flag_seq OWNER TO postgres;

--
-- Name: m_latihan_jawaban; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_latihan_jawaban (
    id character varying,
    m_latihan_soal_id character varying,
    huruf character varying,
    jawaban character varying,
    created_at character varying,
    updated_at character varying
);


ALTER TABLE public.m_latihan_jawaban OWNER TO postgres;

--
-- Name: m_latihan_materi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_latihan_materi (
    id character varying,
    m_sub_toefl_preparation_id character varying,
    url_vdeo_latihan_materi character varying,
    created_at character varying,
    update_at character varying
);


ALTER TABLE public.m_latihan_materi OWNER TO postgres;

--
-- Name: m_latihan_soal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_latihan_soal (
    id character varying,
    m_sub_toefl_preparation_id character varying,
    soal text,
    created_at character varying,
    update_at character varying,
    m_latihan_jawaban_id character varying,
    soal_point smallint
);


ALTER TABLE public.m_latihan_soal OWNER TO postgres;

--
-- Name: m_libur_nasional_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_libur_nasional_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_libur_nasional_id_seq OWNER TO postgres;

--
-- Name: m_mapping_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_mapping_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_mapping_menu_id_seq OWNER TO postgres;

--
-- Name: m_mapping_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_mapping_menu (
    id smallint DEFAULT nextval('public.m_mapping_menu_id_seq'::regclass),
    m_role_id character varying,
    m_sub_menu_id character varying(100),
    created_at character varying(100),
    updated_at character varying(100)
);


ALTER TABLE public.m_mapping_menu OWNER TO postgres;

--
-- Name: m_mapping_menu_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_mapping_menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_mapping_menu_seq OWNER TO postgres;

--
-- Name: m_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_menu (
    id character varying,
    nama_menu character varying(100),
    created_at character varying(100),
    updated_at character varying(100)
);


ALTER TABLE public.m_menu OWNER TO postgres;

--
-- Name: m_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_menu_id_seq OWNER TO postgres;

--
-- Name: m_menu_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_menu_seq OWNER TO postgres;

--
-- Name: m_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_migrations_id_seq OWNER TO postgres;

--
-- Name: m_pembahasan_materi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_pembahasan_materi (
    id character varying,
    m_sub_toefl_preparation_id character varying,
    url_vdeo_pembahasan_materi character varying,
    created_at character varying,
    update_at character varying
);


ALTER TABLE public.m_pembahasan_materi OWNER TO postgres;

--
-- Name: m_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_role (
    id character varying,
    nama_role character varying(16) DEFAULT NULL::character varying,
    flag character varying(2) DEFAULT NULL::character varying,
    created_at character varying(100),
    updated_at character varying(100)
);


ALTER TABLE public.m_role OWNER TO postgres;

--
-- Name: m_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_role_id_seq OWNER TO postgres;

--
-- Name: m_role_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_role_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_role_seq OWNER TO postgres;

--
-- Name: m_sub_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_sub_menu (
    id character varying DEFAULT 'nextval(''m_sub_menu_id_seq'')'::character varying,
    m_menu_id character varying,
    nama_sub_menu character varying(100),
    url_sub_menu character varying(100),
    created_at character varying(100),
    updated_at character varying(100)
);


ALTER TABLE public.m_sub_menu OWNER TO postgres;

--
-- Name: m_sub_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_sub_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_sub_menu_id_seq OWNER TO postgres;

--
-- Name: m_sub_menu_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_sub_menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_sub_menu_seq OWNER TO postgres;

--
-- Name: m_sub_toefl_preparation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_sub_toefl_preparation (
    id character varying,
    m_toefl_preparation_id character varying,
    nama_sub_toefl_preparation character varying,
    created_at character varying,
    updated_at character varying
);


ALTER TABLE public.m_sub_toefl_preparation OWNER TO postgres;

--
-- Name: m_toefl_preparation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.m_toefl_preparation (
    id character varying,
    nama_toefl_preparation character varying,
    created_at character varying,
    updated_at character varying
);


ALTER TABLE public.m_toefl_preparation OWNER TO postgres;

--
-- Name: m_toefl_preparation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.m_toefl_preparation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.m_toefl_preparation_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id smallint DEFAULT nextval('public.migrations_id_seq'::regclass),
    migration character varying(46) DEFAULT NULL::character varying,
    batch smallint
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_seq OWNER TO postgres;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(1) DEFAULT NULL::character varying,
    token character varying(1) DEFAULT NULL::character varying,
    created_at character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: pegawai_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pegawai_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pegawai_id_seq OWNER TO postgres;

--
-- Name: tb_paket_materi_teofl_test_id_materi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_paket_materi_teofl_test_id_materi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.tb_paket_materi_teofl_test_id_materi_seq OWNER TO postgres;

--
-- Name: tb_paket_materi_teofl_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_paket_materi_teofl_test (
    id_materi integer DEFAULT nextval('public.tb_paket_materi_teofl_test_id_materi_seq'::regclass) NOT NULL,
    judul_materi character(50) NOT NULL,
    video_materi text NOT NULL,
    id_paket integer NOT NULL
);


ALTER TABLE public.tb_paket_materi_teofl_test OWNER TO postgres;

--
-- Name: tb_teofl_test_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_teofl_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.tb_teofl_test_id_seq OWNER TO postgres;

--
-- Name: tb_paket_teofl_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_paket_teofl_test (
    id_paket integer DEFAULT nextval('public.tb_teofl_test_id_seq'::regclass) NOT NULL,
    nama_paket character(50) NOT NULL
);


ALTER TABLE public.tb_paket_teofl_test OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_preparation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_roll_akses_teofl_preparation (
    id_roll_akses_tp integer NOT NULL,
    id_username integer NOT NULL,
    id_paket text NOT NULL
);


ALTER TABLE public.tb_roll_akses_teofl_preparation OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq OWNED BY public.tb_roll_akses_teofl_preparation.id_roll_akses_tp;


--
-- Name: tb_roll_akses_teofl_test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tb_roll_akses_teofl_test (
    id_roll_akses_tt integer NOT NULL,
    id_username integer NOT NULL,
    id_paket integer NOT NULL
);


ALTER TABLE public.tb_roll_akses_teofl_test OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_test_id_roll_akses_tt_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_test_id_roll_akses_tt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq OWNED BY public.tb_roll_akses_teofl_test.id_roll_akses_tt;


--
-- Name: tb_roll_paket_user_id_roll_paket_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tb_roll_paket_user_id_roll_paket_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.tb_roll_paket_user_id_roll_paket_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint DEFAULT nextval('public.users_id_seq'::regclass),
    nama character varying(500),
    username character varying(500),
    password character varying(500),
    remember_token character varying(500),
    role character varying(500),
    created_at character varying(500),
    updated_at character varying(500),
    tanda_tangan text,
    email character varying(500),
    status_tanda_tangan character varying(500) DEFAULT '1 = draft, 2 = selesai'::character varying,
    tanggal_expired character varying(500)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_seq OWNER TO postgres;

--
-- Name: tb_roll_akses_teofl_preparation id_roll_akses_tp; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roll_akses_teofl_preparation ALTER COLUMN id_roll_akses_tp SET DEFAULT nextval('public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq'::regclass);


--
-- Name: tb_roll_akses_teofl_test id_roll_akses_tt; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roll_akses_teofl_test ALTER COLUMN id_roll_akses_tt SET DEFAULT nextval('public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq'::regclass);


--
-- Data for Name: m_flag; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_flag (id, nama_flag, keterangan) FROM stdin;
1	AP	Admin Programmer
2	AS	Admin Super
3	PE	Peserta
\.


--
-- Data for Name: m_latihan_jawaban; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_latihan_jawaban (id, m_latihan_soal_id, huruf, jawaban, created_at, updated_at) FROM stdin;
J000001	L000020	a	coba1	2022-08-27 14:32:41	\N
J000002	L000018	b	ya	2022-08-27 15:08:41	\N
J000003	L000020	c	af	2022-08-29 15:32:39	\N
J000004	L000019	a	diferent	2022-08-29 16:46:20	\N
J000005	L000020	b	gh	2022-08-29 17:17:37	\N
J000006	L000021	A.	Jawaban 1	2022-09-17 10:32:44	\N
J000007	L000021	B.	Jawaban 2	2022-09-17 10:33:19	\N
J000008	L000021	C.	Jawaban 3	2022-09-17 10:33:30	\N
J000009	L000022	A.	Jawaban 90	2022-09-17 10:34:32	\N
J000010	L000022	B.	Jawaban 91	2022-09-17 10:34:38	\N
J000011	L000023	A.	jawaban 1	2022-09-17 10:40:33	\N
\.


--
-- Data for Name: m_latihan_materi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_latihan_materi (id, m_sub_toefl_preparation_id, url_vdeo_latihan_materi, created_at, update_at) FROM stdin;
M000001	S000001	coba2	2022-08-17 18:14:49	\N
M000002	S000002	ini 3	2022-08-17 18:17:32	\N
M000003	S000004	https://www.youtube.com/embed/OAPrNLVCEGU	2022-09-17 10:32:11	\N
M000004	S000005	https://www.youtube.com/embed/OAPrNLVCEGU	2022-09-17 10:41:05	\N
\.


--
-- Data for Name: m_latihan_soal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_latihan_soal (id, m_sub_toefl_preparation_id, soal, created_at, update_at, m_latihan_jawaban_id, soal_point) FROM stdin;
L000002	S000001	coba 2	\N	\N	\N	\N
L000003	S000001	coba 3	\N	\N	\N	\N
L000004	S000001	coba 4	\N	\N	\N	\N
L000005	S000001	coba 5	\N	\N	\N	\N
L000006	S000001	coba 6	\N	\N	\N	\N
L000007	S000001	coba 7	\N	\N	\N	\N
L000008	S000001	coba 8	\N	\N	\N	\N
L000009	S000001	coba 9	\N	\N	\N	\N
L000010	S000001	coba 10	\N	\N	\N	\N
L000011	S000001	coba 11	\N	\N	\N	\N
L000012	S000001	coba 12	\N	\N	\N	\N
L000001	S000001	coba1	\N	\N	\N	\N
L000013	S000001	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>cban 5</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 17:16:10	\N	\N	\N
L000014	S000001	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>dssd</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 17:30:21	\N	\N	\N
L000015	S000001	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>dssdsd</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 17:30:32	\N	\N	\N
L000016	S000001	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>dssdsdafafd</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 17:30:45	\N	\N	\N
L000017	S000002	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>ini coba pertanyaan</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-17 18:32:54	\N	\N	\N
L000018	S000003	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>soal 1</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="text" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-26 00:34:48	\N	\N	\N
L000019	S000003	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>soal 2</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-26 00:38:34	\N	\N	\N
L000020	S000003	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>coba1 </p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-08-27 05:40:05	\N	J000003	\N
L000021	S000004	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>soal 1</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-09-17 10:32:26	\N	J000007	\N
L000023	S000005	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>soal 1</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-09-17 10:40:14	\N	J000011	\N
L000022	S000004	<div class="ql-editor" data-gramm="false" contenteditable="true"><p>soal 2</p></div><div class="ql-clipboard" contenteditable="true" tabindex="-1"></div><div class="ql-tooltip ql-hidden"><a class="ql-preview" rel="noopener noreferrer" target="_blank" href="about:blank"></a><input type="hidden" data-formula="e=mc^2" data-link="https://quilljs.com" data-video="Embed URL"><a class="ql-action"></a><a class="ql-remove"></a></div>	2022-09-17 10:34:03	\N	J000009	\N
\.


--
-- Data for Name: m_mapping_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_mapping_menu (id, m_role_id, m_sub_menu_id, created_at, updated_at) FROM stdin;
8	R001	S002	2022-08-29 02:53:25	\N
9	R001	S003	2022-08-29 02:53:25	\N
10	R001	S004	2022-08-29 02:53:25	\N
11	R001	S015	2022-08-29 02:53:25	\N
12	R001	S008	2022-08-29 02:53:25	\N
13	R001	S014	2022-08-29 02:53:25	\N
14	R001	S016	2022-08-29 02:53:25	\N
15	R001	S017	2022-08-29 02:53:25	\N
225	R003	S019	2022-09-29 04:22:18	\N
226	R003	S020	2022-09-29 04:22:18	\N
227	R003	S018	2022-09-29 04:22:18	\N
228	R003	S021	2022-09-29 04:22:18	\N
229	R003	S022	2022-09-29 04:22:18	\N
\.


--
-- Data for Name: m_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_menu (id, nama_menu, created_at, updated_at) FROM stdin;
M007	STUDY ROOM	2022-07-25 16:00:36	\N
M001	DASHBOARD	2022-07-07 00:59:12	\N
M006	AKSES PAKET	2022-07-08 03:09:48	\N
M003	PENGATURAN	2022-07-07 00:58:45	\N
M008	AKUN	2022-09-29 04:07:33	\N
\.


--
-- Data for Name: m_pembahasan_materi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_pembahasan_materi (id, m_sub_toefl_preparation_id, url_vdeo_pembahasan_materi, created_at, update_at) FROM stdin;
M000001	S000001	coba3	2022-08-17 18:32:08	\N
M000002	S000002	coba4	2022-08-17 18:32:34	\N
M000003	S000004	https://www.youtube.com/embed/OAPrNLVCEGU	2022-09-17 10:32:13	\N
M000004	S000005	https://www.youtube.com/embed/OAPrNLVCEGU	2022-09-17 10:41:03	\N
\.


--
-- Data for Name: m_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_role (id, nama_role, flag, created_at, updated_at) FROM stdin;
R001	admin super	AS	2022-07-06 06:52:28	\N
R003	peserta	PE	2022-07-06 06:53:21	\N
\.


--
-- Data for Name: m_sub_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_sub_menu (id, m_menu_id, nama_sub_menu, url_sub_menu, created_at, updated_at) FROM stdin;
S009	M002	Issues	issues/index	2022-07-08 03:06:49	\N
S011	M004	Report Aging Issues GOSAP	report_aging_issues_gosap	2022-07-08 03:11:26	\N
S010	M004	Report Aging Issues HELPDESK	report_aging_issues_helpdesk	2022-07-08 03:10:36	\N
S012	M004	Response and Resolution Report	report_and_resolution_report	2022-07-08 03:12:53	\N
S013	M005	Created Issues	tiket/index	2022-07-08 03:13:31	\N
S016	M007	TOEFL TEST	study_room/index	2022-07-25 16:02:08	\N
S017	M007	TOEFL PREPARATION	toefl_preparation/index	2022-08-29 02:52:40	\N
S014	M006	PAKET PESERTA	RollUserPaket/index	2022-07-08 03:13:52	\N
S002	M003	MENU	menu/index	2022-07-07 01:46:28	\N
S015	M003	PESERTA	user/index	2022-07-24 06:23:32	\N
S003	M003	PEMETAAN MENU	mapping_menu/index	2022-07-07 03:20:41	\N
S004	M003	AKSES AKUN	role/index	2022-07-07 01:51:11	\N
S008	M001	HOME	home/index	2022-07-08 03:02:48	\N
S018	M001	MENU	home/index_peserta	2022-09-15 15:20:58	\N
S019	M007	TEOFL TEST	study_room_peserta/index	2022-09-17 04:27:05	\N
S020	M007	TEOFL PREPARATION	toefl_preparation_peserta/index	2022-09-17 04:27:55	\N
S021	M008	LOGOUT	/logout_lwt	2022-09-29 04:20:14	\N
S022	M008	PROFIL	/profil_akun	2022-09-29 04:21:59	\N
\.


--
-- Data for Name: m_sub_toefl_preparation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_sub_toefl_preparation (id, m_toefl_preparation_id, nama_sub_toefl_preparation, created_at, updated_at) FROM stdin;
S000001	T000002	Tips 1	2022-08-08 17:36:08	\N
S000002	T000002	Tips 2	2022-08-08 17:36:19	\N
S000003	T000003	Tips 1	2022-08-13 14:06:22	\N
S000004	T000001	Tips 1	2022-09-17 10:31:35	\N
S000005	T000001	Tips 2	2022-09-17 10:39:59	\N
\.


--
-- Data for Name: m_toefl_preparation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.m_toefl_preparation (id, nama_toefl_preparation, created_at, updated_at) FROM stdin;
T000001	Listening 1	2022-08-08 17:35:26	
T000002	Listening 2	2022-08-08 17:35:43	
T000003	Listening 3	2022-08-13 14:05:51	
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: tb_paket_materi_teofl_test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_paket_materi_teofl_test (id_materi, judul_materi, video_materi, id_paket) FROM stdin;
15	Ojo dibandingke                                   	https://www.youtube.com/embed/sYJ_t2MiJF0	1
16	Mendung tanpo udan                                	https://www.youtube.com/embed/r8T61-29o10	2
17	Tak ingat usia                                    	https://www.youtube.com/embed/FB1YNEOspyA	2
12	Dunia Tipu Tipu                                   	https://www.youtube.com/embed/sYJ_t2MiJF0	1
13	Hati Hati di jalan raya                           	https://www.youtube.com/embed/_N6vSc_mT6I	1
\.


--
-- Data for Name: tb_paket_teofl_test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_paket_teofl_test (id_paket, nama_paket) FROM stdin;
1	Paket A                                           
2	Paket AB                                          
6	PAKET CD                                          
\.


--
-- Data for Name: tb_roll_akses_teofl_preparation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_roll_akses_teofl_preparation (id_roll_akses_tp, id_username, id_paket) FROM stdin;
4	2	T000001
5	2	T000003
14	4	T000002
15	6	T000001
16	1	T000002
\.


--
-- Data for Name: tb_roll_akses_teofl_test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tb_roll_akses_teofl_test (id_roll_akses_tt, id_username, id_paket) FROM stdin;
3	1	1
5	1	2
6	2	1
11	3	1
12	4	2
13	4	1
15	3	2
16	2	2
20	6	1
21	1	6
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, nama, username, password, remember_token, role, created_at, updated_at, tanda_tangan, email, status_tanda_tangan, tanggal_expired) FROM stdin;
2	arta	arta	$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai	n6FVaKDCk4zygUyVnAYdjbkssUsu4Ojo4RbdWisyNqZqG144R6IolY9qFrnqLlbn1q7sHHh1NGgCHx7Y	R003	2022-07-24 06:59:26	2022-07-24 07:13:12			1	
3	fadila	fadila	$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai	5JppmGoScNUMyKaJ4kIhg2fKzST4LnJrDoQycTj9AahPj1Kz4Guhm5W9s5SAq1sidN7iOgHRMpxiggY6	R003	2022-07-18 03:17:57	2022-07-24 07:16:38			1	
4	bapak	bapak	$2y$10$3IDkG/4Xhc0cTAuu/7O2leK2kMQ75HnghO83qju3u2XrpIs1OryZC	d1HmARql4S5sVSRRW6D9mTRtgqbqHwGsdnjjUl0MfrH3TBN4dLuRroDGM0qdJdC3zhtd2b8fSpdXdnFw	R003	2022-08-04 16:25:08	2022-08-06 13:05:47			2	
5	ibuk	ibuk	$2y$10$0AjnW7hc.7PYLglf/M6PreWVtxCNh6XhSqLwhk6XftmPBkPW41ByS		R003	2022-08-13 13:36:52	2022-08-13 13:45:39			1	
6	adik	adik	$2y$10$nuKXxJyExbt9fd369tz6Le8hK./F06WrjAMe86mC8tPV5Uahm/gKO		R003	2022-08-13 14:04:32	2022-08-13 14:11:50	data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAAAXNSR0IArs4c6QAADURJREFUeF7tnc2P3EgZh6s8ne4ogIBFoCXdmWnXRIBAICTgQjgDewCJj8si4AYnLtyREOIf4MIFToD4EAiEuPBxReS0CAkEYlfp8nRm+Fi0fC1ESSbTbeQwHfV4bLfd7bLrtZ85Tper3vf5vf7JVS7bWvEHAQhAQAgBLSROwoQABCCgMCyKAAIQEEMAwxIjFYFCAAIYFjUAAQiIIYBhiZGKQCEAAQyLGoAABMQQwLDESEWgEIAAhkUNQAACYghgWGKkIlAIQADDogYgAAExBDAsMVIRKAS2J2CM+aK19kvb9+DHkRiWHzoQBQScEAjD8CCKorkxJrbWij/fxSfgRGU6hUAHCBhj3qeU+qVS6o5S6iaG1QFRSQECXSVgjLFKqXAtvx9baz8iOV+usCSrR+wQyCEQhuHHtdY/SP38eWvtVyRDw7Akq0fsEMghYIx5pJQarP1831p7TTowDEu6gsQPgRQBY8ypUupK6t9ftdZ+TjosDEu6gsQPgTUCWWaltb49m81udQEUhtUFFckBAkqpnCsrpbW+NZvNbncBEobVBRXJofcE+mBWicgYVu9LHQDSCfTFrDAs6ZVK/L0n0CezwrB6X+4AkEwgDMN7WutLWxW6tGaV1ocpoeSKJfbeEjDGLJRSwaUTukML7FniYli9LXkSl0rAGPMXpdTTfTMrpoRSK5a4e0vAGPO8UupNaQBBEHzgzp07v+g6GK6wuq4w+XWGgDHmO0qpZzMS+q619hOdSbQgEQyrDyqTo3gCh4eHX4jj+MsZibxgrX2z+ARLJoBhlQRFMwi0ReDmzZvvXy6XP88Y/6/W2je2FVcb42JYbVBnTAhUIJC8LTRjgf1sNpulH3Cu0KvMphiWTN2IuicEcrYvLK21ez1BcCFNDKuPqpOzCALGmAdKqVE62L7cEcwSCcMSUboE2TcCeXut+mxWSQ1gWH07E8jXewJsX8iXCMPyvnwJsE8EMKtitTGsPp0N5Oo1AbYvbJYHw9rMiBYQcE6gwKxetta+2nkAQgbAsIQIRZjdJWCM+ahS6ocZGfZ2+0Ke2hhWd88DMhNAoMCsVBe+1Fy3BBhW3UTpDwIlCYRh+FOt9QezrqyCIHimD29fKInqSTMMqyox2kOgBgIFZpX0/jFr7Y9qGKZzXWBYnZOUhHwnYIz5l1IqbyEdsyoQEMPyvbqJr1MECswqttZeeuVxp5KvIRkMqwaIdAGBMgTy3sMex/EiiqJBmT763gbD6nsFkH8jBIwxy5xH4f5trX1NI0F0YBAMqwMikoK/BIq2LWit/zybzcb+Ru9fZBiWf5oQUUcIFJlVHMc/i6LomY6k2lgaGFZjqNsfKAzDZRRFLOw2IEXRtgXMansBMKzt2Yk6MjErrbWO4/jx63YxLnfyscfKHVsMyx1bb3pemdV6QDz24UYe9li54brqFcNyy7f13vf3988Gg8Gl93+fnp6+dHJy8vrWA+xQAOyxci8mhuWecasjZH1xJQkomRoyLaxPGvZY1ceyqCcMqxnOrYwShuGZ1jr36ypMC+uRhT1W9XAs0wuGVYaSwDbj8fjl0Wj0qqLQMazdhD08PPxaHMefyeqFPVa7sc07GsNyw7X1XrMW2tNBMS3cXiZjzN+VUk9l9cC2he25bjoSw9pESOjveWtX6+lgWNXFDcPwU1rrb+R9cQqzqs60yhEYVhVagtqmDet8/1WyFetCFkwLy4tqjPmVUuq9eUfAsjzLbVtiWNuS8/i4rOngw4cP/zMcDl+ZdixOsnJCGmNOlVJXclqfWWvzfis3AK1KEcCwSmGS02g6nS6CILjw+E0cx8kjOXuTyeRvw+Hwwt6rs7Ozxd27d3m1SY7E51PAbxZUwG1r7S05FSI7UgxLtn4Xop9MJi8Nh8PXpVNaN6WsqSL7sbKL4PDwcBbHsckrkTiOPx1F0bc6VELep4JheS9R+QCzFtrTC+s8plOOZxiGC6113oPi962118r1RKs6CWBYddJssa+8bQzpNarJZPLicDh8w3qoy+VyeXR0lLvBtMW0Gh+6aG9VEozW+uuz2eyzjQfGgI8JYFgdKISsdaskrbwF9awrMZ8W3xPzXcnS5HS1aG9V8jQT71xv/2TBsNrXYKcIyqxbpQfwfVq4bqhNGOmmvVVaazubzQ53EoqDayGAYdWCsb1OyqxbZUXn4+J7ElPyd2mz2P8f1v52FEWfrJv0pr1VLKzXTXy3/jCs3fi1enTZdasyhlU0hWwiybxp7WpsF7vy2VvVhLL1joFh1cuzsd6qrlulA8s6vonpVxagg4ODe3t7exvvutUVH3urGivT2gfCsGpH6r7DbdatylxltXW3MGdae6q1Hq7HXcdaEnur3NenyxEwLJd0HfW97bpVOhwf1rGyrq5W07+672ayt8pRQTbYLYbVIOw6htpl3So9fhiGyQL3k38n36eIoqjRmigypfQLCLddxzLGPFRKXbhaS125sbeqjuJsoI9Gi7OBfDo9xHg8/u9oNHpFOslt13ba3t6QNswkr7QppQ2tSq6bjIq9VfJOFwxLkGZZVyO7rDu1ufCeZZZZV3gZpvY7a+07imQrYVTJjnX2Vgmq/VWoGJYQ0eqcCq6nnDbBR48evXh8fPy0Syx5dzgXi8X9+Xx+4W5hlavAMkZ1fhXHQ8suBXbYN4blEG5dXe+6hWHD1cjjD6uu/rZdJyqb63Q6fS4Ignel2+eNm7NX6jlr7XuSPowxP1FKfajM+Frr781ms2fLtKWNnwQwLD91eRJVXVsY8tJs+k7hNnc4847J2hGflSdG5XmRVwgPw6oAq42m25zgVeLMWviusrBdcaxllslsGs8Y80ApNaoyVtIWo6pKzP/2GJbHGrlat1pPucoa0S6optPpb4IgeGe6j+Vy+eujo6N3b+o7MdZzEypsmrxdNQiC7zP120RU5u8Ylqe67e/vPxgMBpeuKjZdjVRNJ+tT9nWPcb7WdGGtLPnfNnc4C74GxHvVq4ovsD2G5alodW9hKEozPVbd73kvu4WhjBSrK63zq60XlFLPW2s/XOZY2sgngGF5qGGdJ3iZ9FwuvOdNBV1cxZXJlTayCWBYnuk3Ho8fjEYj51PB9bQzDKu2R3SavFL0TErCcUAAw3IAdZcu2zjBXS28N32luAt3jpVBAMPySKe2TnAXhsVU0KPC6lAoGJYnYo7H4/uj0ehqOpym1np2ecg4C2EbV4qeSEkYDglgWA7hVum67RO8zjuFbV0pVuFNW5kEMCwPdGtig+imNOu6U8hUcBNpft+FAIa1C70ajp1MJv8YDoevbWsquBq3LsNq+0qxBknowmMCGFbL4uQ8K7iMoqjRLzHX8fZRpoItF1MPhsewWhTZh6ngKv1d7xS6fAVOixIxtGcEMKyWBPHtBN/FsKbT6W+DIHh7GuVisXg4n88v3flsCTnDdoAAhtWCiK7fcbVNSru8Ltn1K3C2yYdjukkAw2pBV19P8IzXJf/p+Ph4UoTIp2ltC1IyZMMEMKyGgft8gle9U+jbtLZhKRmuBQIYVoPQfT/Bq3y4lHWrBguHoZ4QwLAaKgYf163SqVdZePd1WtuQnAzTEgEMqyHwZT4a2lAohcOkjSjrraBZuSSdNvXcow+ciKEdAhhWA9x9XrdKp79pHUtSLg1IyxANE8CwHAO/fv36tatXr95LD+Pr1UjRtLDgA6in8/m88ldtHKOn+w4SwLAci5rzuEocRVHgeOitus/7KMXBwcHv9/b23pru1PWHV7dKgoM6SwDDcijtjRs3Hly5cqXR1x3XkU7WOlYQBJcMFrOqgzZ9VCGAYVWhVbFt1p20xWKxmM/ng4pdNdo8b1FdyrS2UVgM1igBDMsR7ul0ehYEwaU3Lvi6drWOYX9/3w4Gg7AIzWKx+MN8Pn+bI3x0C4FMAhiWo8LIuro6PT3958nJyVOOhqyt202GJcF0a4NBR14RwLAcyZHxfilvF9qzEKxMK9mHpc//WLNyVCx0W5oAhlUaVbWG618oTo6Mokg064ODgz/O5/O3VKNAawjUS0D0SVQvCnqDAAR8J4Bh+a4Q8UEAAk8IYFgUAwQgIIYAhiVGKgKFAAQwLGoAAhAQQwDDEiMVgUIAAhgWNQABCIghgGGJkYpAIQABDIsagAAExBDAsMRIRaAQgACGRQ1AAAJiCGBYYqQiUAhAAMOiBiAAATEEMCwxUhEoBCCAYVEDEICAGAIYlhipCBQCEMCwqAEIQEAMAQxLjFQECgEIYFjUAAQgIIYAhiVGKgKFAAQwLGoAAhAQQwDDEiMVgUIAAhgWNQABCIghgGGJkYpAIQABDIsagAAExBDAsMRIRaAQgACGRQ1AAAJiCGBYYqQiUAhAAMOiBiAAATEEMCwxUhEoBCCAYVEDEICAGAIYlhipCBQCEMCwqAEIQEAMAQxLjFQECgEIYFjUAAQgIIYAhiVGKgKFAAQwLGoAAhAQQwDDEiMVgUIAAhgWNQABCIghgGGJkYpAIQABDIsagAAExBDAsMRIRaAQgACGRQ1AAAJiCGBYYqQiUAhA4H++xi7iGjJaHgAAAABJRU5ErkJggg==		2	
1	Irhas	irhas	$2y$10$7HJZiNNfUXYzcea3o5ATQuH2rkjfOgPpCI1xN2d3PrEcWL/0nfelG	\N	R003	2022-07-24 06:52:17	2022-09-29 10:15:49	data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAAAXNSR0IArs4c6QAAEehJREFUeF7tXTuPJEkRruqe6b47DoFwkGhp6aoZBwMJCQsQmDxcfISFkBA4+Hg4eDgIn58ADsJCiANM+AFTPTdSCxyEcbC73TtdhbLVNVtXk6/IyqzMqPrG253MyMgvIr+JiHxUnuEHCAABIMAEgZyJnlATCAABIJCBsOAEQAAIsEEAhMXGVFAUCAABEBZ8AAgAATYIgLDYmAqKAgEgAMKCDwABIMAGARAWG1NBUSAABEBY8AEgAATYIADCYmMqKAoEgAAICz4ABIAAGwRAWGxMBUWBABAAYcEHgAAQYIMACIuNqaAoEAACICz4ABAAAmwQAGGxMRUUBQJAAIQFHwACQIANAiAsNqaCokAACICw4ANAAAiwQQCExcZUUBQIAAEQFnwACAABNgiAsDybqiiKOs/zvKoqYOsZW4gDAlhUHn2gJatWJEjLI7gQBQSyDB+h8OUFfbK6yP1DVVXf8jUG5ACBuSOACMuTB2y329NisVj0xSHK8gQwxAABRFh+fWC73T4uFotlV2pd1/+5v7//jN+RIA0IzBMBRFie7V6WZYMoyzOoEAcELgiAsDy7gqyWhbTQM8gQN1sEQFieTV8Uxb+yLPtsnr+Ftmmac9S12+2e1bg8Dz8rcWVZ/j3Lsi9UVbWe1cRnPFkQViDjF0VxyvP8TFAXvmpAWP7A7kWy/6iq6kv+pENSqgiAsAJZpizLD5umeSH4CkTlF2TUCf3iyUkaCIuTtQLoKo5jiJP5GtHid92NhP6/+11NvxcRZ/PmzZs3+/3+HZcp3dzcfNQ0zfuSvn+pquprLjLRhwcCICwednLWUhBSXdfNcrk8p6cGcnIeZ0hHQWDi5/7+/mNHQnQyZVHWpf1jVVXXQ/RB33QRAGGlaxtnzW5vb9en0+lVS06ihqYPopyHCtLRZld1u93+cLFY/CLLsk/1lDhmWfaqqqpPB1EOQqMiAMKKCr/fwV+8ePEoIqkUoyjKTEW0ZXOBvCiK7+V5/huZ7KZpfr/b7b5DGRdt00cAhJW+jZQaCoJaLM78dP4ZMpXLTuYQEVZ9qWo+Pj6eHh4ergzp4b+zLHt2m8AmUrNSGo2SQWCQkyczi5koMiSC6hDSuYB+Op1qExGEhrUt+NuQrYm4bm5u9k3TfE6i8z+rqpL9f+jpQX4ABEBYAUD1LVIQ1dXVlXVBuju+aaH71nWIPNUFciHTJk3UFOJFDe/Pd3d3Xx+iH/rGRwCEFd8GSg02m83r1Wq1solAukLE4j4ej0fXYwMxIdGRVquXKtUry/K7TdP8IM/zb/fnkOf5z+7u7n4ec24YezgCIKzhGAaRoHhfSzmWIKkU0jxfYJjmX9d1LTsGcXt7+826rn+bZVn/us45Fa6qCtejfBkpghwQVgTQVUMSI6pTVVXaYvSl5iVSSbZkZkoTVbcIVMSFQnxCDu+gCgjLATSfXSiFZxFFUa75yKIUqgyfc3WVZUoTdSQkqWv9qKqqX7vqgn5xEQBhRcDflO70VRI7fMfj8UCtSakIS8inEF8EiJ4NKaLP9XotfZVBR8JlWdbdhyqbpvnbbrf7Sgpzgg50BEBYdMyce+h2sVRCh+zy6YiRY6QlMFLN6XA4SAm9LMuXWZa928P3V1VV/djZkOgYDQEQ1gjQUyMqoZJqAVLUbWtYqsOaPsag6OOrrSxFVBFwURR/zfP8y1mWiVpee5/yJ03TXFVV9UtfOkHOOAiAsALjbEtWoXf5VHpwLUIXRSGu7zxZzxQx9lLD845h0zTFbrf7MLALQLxHBEBYHsHsizKR1djnpfqLvNVXdUQgIDSDRVM2FMqy/GKWZb/LsuzznYEFaf0UUdZgU4wqAIQVAG7XAnEAVZ6JVNXRTBHKGLpRxpClu6Z6383NzR+bpvlGnud/quv6+4iuKIin0RaE5dkOqRe6VWe9uL473yVg0xyKojhHWCAqz04/ojgQliewbdK/VI4S6CJAToV41Ty41uU8ueKkxYCwBpq3LMvXkmsgH5OaarqlqAOJCISNX+CzagMdmFl3No6ZIq6mqEronHrEIivEp65z1xdAWCmujHA6gbAcsTWRFZedN1VaxUV/EJajAzPtBsJyMJyvwrogC+p1Gwd1jV1Uxx1STWURYRlNOtkGICyiaVVkZbu4+6e0UygQC+K8vr6+XiwWH3t6xbTrRoQuSHNEWEFgTVYoCItgmqFkJXs51HR2iKDeoKYcdw4ph0cHgYPOySAAwrI0xVCyaodJ/avFXK7wcNHT0r3QzBIBEJYFUL7ISgzFISpIvabl0x4W5keThBAAYRmMEWJxyGSmdJRAdRpepK+n0+kx5kZBCHsktB6higEBEJYGINNuYNuVeoJd9YJmCgX4dk66Vz5j6QmyAp+BsCQ+oCtA95vb7g72+6nSrlhkIFsKl0hr3X9PK8ZGAcgKZCUQAGH1/ID6DUBXgtF9cMJVZiiX7m8UiCebx7y+A7IKZVl+ckFYHZuZTq/7iq5MaZdr1BbK/WLW3KZMViLtln2qLJQdpyAXhHWx4thkZSKtlIrwQldJlEX6go/LYpkqWXXnldofJxc7jdln9oRFqVe1hvHtZIo3ykdNu0xOJ6u5hUpdTZsd1E0O09zG+j3Ojg1HetaEtd1uHxeLhfg4gdWPuBAsPhsfYsGk/mrCGKf0Td8f9P2HwsroHhqZoneu8/IADVnEbAnL5ESSelXQiEcV6aXkzLJT+j70M0W5HO40ylaerY9xeRmDzC4BOsyOsIqieMzz3Cqq8rEYKTaTRVkhozqKbqJtiFTNZleW04I2RYldzMf2L6q9U2w/K8ISC04YQaR1JmPEKHqLSEPoJfvCcQx9VBgpIi2Ba0apa5kiEC4LerPZfLRarT5h41cCUy7zMq2RGL83LtwYSoUa0+bLy2OfMerPVXVYM7ZerZ4q/brzsCGtENFaKL9RyTURrqSsEHxndWwMxh5vNoRlSVbJOFTqRXgTcalIS6RMIhJRRSMpRZKyxUhJ+dr+MW4GjE0kY403C8Iqy/KDLMu+qgM1tTBdVohOJcrq4qhawO1HYler1comVUoN/+4cqSkf0r5w9DULwjKF7qn+VZdFWSn8tW6vFQmS0UVLtm6bKlmZ/EaW8h2Px//t9/tP2s4d7WgIzIKwTOlgqgtGt90/BnFdvq7cPpssfOVMUDQXU7dOaQe01dIl5eO0i+nLdrHkeHO+WBPQjWs639P2Tfmcj+5Vh1AfsWgjizaCCmHblNJbpHwhLBxG5mQJayp/KXVHHYRL2OzImVxHYFXXdbNcLhc+I6juuG1NK+bjf30ckPKZPCO930+KsNoURrfo2vSP270ulb6uqWH/eRsR8bhke6Kf4qc5nU71w8PDVUpuP5U/ZClhOqYukyAs2xSmX1zn+BrAkI9Y2BC6zPl6pJQkERlKA6SDndjlG5OCaGOxJixCjUp5D1BXkE+xGC8jWV1aeCGpJTV6SrEgTnNt/VUiBTE32OWjojxue7aEZVN/sFl0qo+ICjO4pluhTdidu4qsbMm8V2d62ocI8SJFaFyEfKR8Y6AcbwyWhKVL5UT96pLCWJ9adz21Hcts7aKUkZVL2pcqMdvii10+W6T4t2NHWLrIauh5GBVx+diJ8+0q3Z09IZuyuycIXXyvK7WCOBUjmyi7F0Ei5aOCnFh7VoQ1VpE8xnPANn7Rj54oO3sp1uNs5txvg5TPBbXp9GFFWIqnhK1TP1uz6QrxY0ZbLumdbI6pXj2ytYcLDlMhaFuM5tKODWHJnDaUU5qePhHOEbIobXtMQ+ekKZ0kd1lMm83mv6vV6j1iqouUzwVsRn3YEBZ1O9+HDWIceXCoyzzt7IUmUh+YmmRQ5y/kDa1dmnTC79NBAISlsYVp8fhMtWyeCn5ipqbJjsfjIaVrLkNc2qUuFSq6HjIP9A2PAAjLEuNQV3ls6jMpX862hO9ZM5t59ztd7iO+3O/377uOi368EQBhEewnezlh6F96U70sZK2MMHUvTR3rUpM4guEFQAjJQFhEJ5DVtVxqKLoIw+aEPlHtqM1NqbVMORdMo04Sg4+CAAiLCLPqygv1uIPqjt/QiI04nWDNKTW5Tm3O+xGVYBOE4CgIgLAcYFdFDNQivO4grFArxedZdHA5pnziKALqUg5+OMcuICxHq6uOPFAiJNvdsUvR/fxEcUfdp3/HJjbbeXSh5n5/0dFt0G0gAiAsRwD7D+B1xYQgLRs1xyYBam2KgovNfNFmfgiAsAbY3PSEi21dq41Qhr6hbjvegCmfu1KICkcRhqKN/l0EQFge/EH1oQghmkIibSFe9KM+uCf6hD6vZUtUQo/YaaoHs0JEggiAsDwZJcSzN4LAhHrL5XKpUlNGbL5TLwpRTencmCfXgBiPCICwPIKpKz77JpFWbd0TyEPPMoGoPDoHRHlBAITlBca3Qkw7ZpQUkaKaj13LdjwQFQV5tB0TARBWALR9FeNtVdM98Uypa4GobBFHu1gIgLACIq8jgBCRlo4odekhiCqgE0C0VwRAWF7hfC5MlyKGIC2hge3LEiCqwMaHeO8IgLC8Q5ouabWvkIKoRjA6hgiCAAgrCKz2pBVq91BooHsxVTftkDqNBDeGmSgCbAhLtn1PvWwc24aq9HDo8QPVvEzF/34/EFVsD8H4JgTYEJaqNhOqDmQCzvX3KtIKMQ+kfq5WQr9UEWBFWLLFPvaFXx+GDP19RdNZsHYOiKh8WBMyxkSAFWHJ6jJcF12IL/JsNpuX6/X6XVsH4pZS284L7aaLADvCivG5rxDmF/Wl6+vr68VisejLF1Hj6XR6pHwVxzb9647FlexD2AMyeSDAjrBkhWSuC093Ql24j01dyyaqavHpvyrBFTceSwtahkCAHWHJ0kLbxR0CQB8yVcRlqs+Zoqo+IU0lOvWBOWTwRIAlYckW3hTqMbK6liwKsomqDofDq/1+/17XLUFYPBcptH6LAEvCUhTfs91ux3Y+Yk4yQhGn08WbV216SI2qQFhY7lNCgO0C5xhltd8iFA6ke+jO9YS6LKoCYU1puWIubAlLEWUl+V27lly7b7arPpZqKsTLXNa2eI6UEAueOwKsCUv2lrrNztqYRjOlcKprObaHP01RFSKsMa2NsUIjwJqwZF8XNu2shQbURBCy8WUkayI626jKpE9qBD+mfTAWPwRYE5YsLRT/l8IiNBFO31XaXU7XHUAb10NKaIMS2qSMAHvCSnERmu4K6t5fzzXf93KJqhBhpbz8oBsVAfaEJSYsI4hQT7aYAFZ9EbpLNrqvRqvkU2pVMhmKIxNJblKYMMbv54vAJAhLVaAeGpG4uAXh8Ofr9Xq9No3hYw62TyabdMHvgUBsBCZBWAJEWQG+BXfMU/B9wtIRjqnOFZKsfMiO7bwYf34ITIawTK9rjkFatvU0HbnKXNCVXEy1tPm5O2bMHYHJEFZrCF3UErquJUsH+zuWpqhK51AU4gJZcV+a0F+GwOQIy5QeUhY9xWVkBNE9E7bZbF6t1+t3VDKFXsfj8bhardaajcJzd90cdJFmqLlTcEJbIDAEgUkSlgBkzIVrKmqbTq0fDofX+/3+6aVQ2+s57VedLw4gbNmojkWArIYsE/RNBYHJElYLcIiniLvGM5HVhTyl0ZWJRGyJy+RMpnFM/fF7IJAKApMnLAG0qW7ksqCpEVxfB0o9bQhxucwtFeeEHkCgj8AsCEtM2pSWtbWhDkDnFKubbmVZ9vR/LqmXiPbary+7uGKXuLovP6hkjbEz6jIP9AECrgjMhrDaupZNUdsVzDGjmfbDshJdcXrd1YDolzwCsyKs1hpDUiyVRcckq+S9CgoCgUAIzJKwZMRlk2Ih9QrkhRALBCwRmDVhdTHSpFjKoEr3zLEl/mgGBIAAAQEQFgEsNAUCQCAuAiCsuPhjdCAABAgIgLAIYKEpEAACcREAYcXFH6MDASBAQACERQALTYEAEIiLAAgrLv4YHQgAAQICICwCWGgKBIBAXARAWHHxx+hAAAgQEABhEcBCUyAABOIiAMKKiz9GBwJAgIAACIsAFpoCASAQFwEQVlz8MToQAAIEBEBYBLDQFAgAgbgIgLDi4o/RgQAQICAAwiKAhaZAAAjERQCEFRd/jA4EgAABARAWASw0BQJAIC4C/wfzl5k844yxPwAAAABJRU5ErkJggg==		2	
7	admin_super	admin_super	$2y$10$CK2vDkmiCpuKhDsj7ggv9.gXYUZ9zmWxb4uy5GNBu3nPdpWNPE5Ai	\N	R001	2022-07-18 03:18:19	2022-09-29 10:28:41			2	
12682	IRHAS KARUMA FITRODIN	trodin	$2y$10$xuoWwN1HHQgc4pcMYuLRiOdlhXY.np68by.cDs8pqw/VAQEa9UiQS	\N	R003	2022-09-15 15:11:59	2022-09-29 10:48:13	data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAAAXNSR0IArs4c6QAAE+JJREFUeF7tXc0LpjcRn7ra1q6Kq3Yrfh0ULKKiBUHxpAhePOjBk+JJwYsn/QvEmx69eNCLePDkUVBExIsoiIpYsCCIitSFtqutbf2WYd/ZzmaTJ5Nkkid5nt8Lyy77JpmZ3yS/dzL5uofwAQJAAAgsgsA9i+gJNYEAEAACBMJCJwACQGAZBEBYy7gKigIBIADCQh8AAkBgGQRAWMu4CooCASAAwkIfAAJAYBkEQFjLuAqKAgEgAMJCHwACQGAZBEBYy7gKigIBIADCQh8AAkBgGQRAWMu4CooCASAAwkIfAAJAYBkEQFjLuAqKAgEgAMJCHwACQGAZBEBYy7gKiu6IwP+IiP+8aEcdIJoItzWgFwCBDAL/vYwTENYEXQUR1gROgApTI8BExR8Q1gRuAmFN4ASoMC0CEl2BsCZxEQhrEkdAjSkRkOiKlcNYmcBFcMIEToAKUyLwLyJ68UWzZ4no6pRankwpENbJHA5zzQjIdJD/vmKuhYJdEQBhdYUXjS+MALYyTOg8ENaEToFKuyMg0RXGx+6uuFMBOGQyh0Cd3RH4z2WDKLYx7O6KuxUAYU3oFKi0KwKyMvhTInrfrppA+F0IgLDQKYDACwjofVcYGxP2DDhlQqdApV0QkKkgC8fK4C4uyAsFYeUxWrUE7yPiz0tWNWCw3rIqyGMC42Iw+FZxcIwVqfXK6V3aSCDn/Yczg3mMdi8BwtrdBd0UkIiBBYif4e843P9Wm0N/RkTv7eYVNNyEADpwE3xTV9YbH2VA8t+YIt7tNiTbp+7KLygHwlrEURVqhju1MeVJgyjYINle0dFGVgFhjUS7r6x/EtG9SkTsaIm+jI5XxRBt3QJMCAvjoW8fbW4dDmqGcIoGmKyYfCS5vnW0BHc83ekymS5jYWKKrrytBAhrAScZVAyne5aDu5YyBtHLF8G5wYVcCMJayFkbqmrCuklE14iIo677MnUQVdyKSoHDIuMAhLWIozJqasLiopbNj4gsiGR3O/8tl/Udo0cc1AoQ1vqOlfwVW3KDiK4XRAxnjy6QbF+s/4OwFnNYoK5OtrMveQBaoitp5syEpTeLYhwsMg7gqEUcFVEzJCue4vFDnyX5mDMTFvalLdj3QVgLOu2SUJdtDJKzKo2uuN5ZCUtPo2WRYs2ecDKtQVhrOlwTjb5loCS6OjNh6YPhGAMLjQE4ayFnXVR9Mti2oAmr1J9njLD+TkQPXLDEUZzF+n9pB1/MvEOqG25HaMnFnJGwcNB54WEBwlrPeZ6Hms9GWB8mou9dXF46fV6vpxxQYxDWWk59ioheGexiR4Rl96GOrnInAeytouQwBEBYw6B2ERTbnQ7CskPbku+zS0HJbgiAsLpB26Xh2BQOhGWDGrdU2HCauhQIa2r33KFc6uyfEFbNebgz5bAQXa3T15OagrDWcWKMXFqPl5yFsBBdrdPPNzUFYa3hyFR01bpEfxbC0tFVTSS6Ri85gZYgrPmd/BwR3Z943LMlf8WWn4Gw9LXQJQfD5+8ZJ9QQhDW/04WUYr4CYeX913J0Kd86SgxFAIQ1FO5iYXLBHL/irB+YkIb2JqzfEdGbiOjPRPQ6IvrDRbG3FFvap4JEV4zjFbzo3Afkka2CsEaiXS4rN2UTwqrdBMn1W87TMWG9OWHWDH1LR1es5gw6lfcC1LiNABw4b2ewXGG8NV3MWSaHgL37wONE9BAR8bUtf7oo8c6cMh2+l+hULjZsIeYO6qHJGgS8O2uNDqgTRyAXXYVPe5XiqAd0ad1ceX19i5T9CRG9P1fR8XvBTyIr9HVHcPdqCk7cC/ltubnoSl9AV3uINyfDExlNYLX6lugjZMyHnb9feAtriRyUHYwACGsw4AZx3ySiT2UGmcfKVy6CM6haXES2aHDFVxMR3+3V46Nta83T9dAPbVYiAMKqBK5jNUvkI4OQ73BvSbiPiHZiULXk3nLQy+7/py8rg3xZH/p5DrVFvocj53IUb1/g9/G2dmNLspwHfctGyD0iLEH700T0dSJ6hohe7uwCTYY983TOaqM5CwIgLAtK48pYIg8ZhKxVbYTUmrD3QOT5y8vUnlNDIXyJOi3RqoctaGMQAiCsQUAbxAgR/fVySV+qSusldDOQldjmnV8KCX/PKNLgchQpRQCEVYpYv/KWwRW+RVjjP4ucflbe2bKQL0dbL20U+o/LaQDOYfETaBKBYv9VI7AzVa/p8DPpfxRdrFMXr9XBmW4s+CMRvaFh8UBHa/xv6dO9NsaO6HMz/aiMsNcsA4RlhqpbQZkKWkhEE1bN6qCsoM3m998S0VsbVvM4eX81WKxYOeEOwkoMt9k6bjdWmLjhks6pCavGd9ZIbg+42LZvENFnKoTH7JrZ1i0TZ8oxVriib5WaTt9Xo3O1XjqoJKlcuzpYQo6jPcG6pW6l2NLlc0T01cgh7pltTdlzg4gebFj9He2z4fJAWMMhvy2wZCoY5mlqCKuUHEcjw3g8QUTXCwXH7Apfxy5scpfiss2jxre7KLyHUBDWHqjfklkTAbREWDXyRqLDtzzw57WFQmNbI2Yn59BEfSsqn17AJ4EACGufrlE7oISwSpfqV4w4LJ7ZeklolkiFVyv15Yuco5IP/z+fbJAfMJBVxusgLMuw8C1TMxUUDSw74WPa1hKkr+X+rcWixtnIWbZXbFn/QSL6kT88x2sRhDXepy1TsxrCOup0I/eSEPr2+L7dXSKc2h3iOwS0RjolhKV/2WeZHnminSL+lh8ET/3QVgcEQFgdQE00+bfLzQSl+SfdXMlgFHJ7jIgeHmfmEEkp4p9tOjgEjDMJAWGN83ZJdJTSqpSwjhhZSYI6ZltrBDuuN0BSFQIgrCrYiitJor3mOI0I0/dgWVaTSsit2KAdK2yR0lFt3hHuuUSDsMb4w2MgCelZoyYPmWPQKZOyZddRbS5D6MClQVj9nes1TSlZ7fOS2R+dMgk5u0BYZXguVxqE1ddlfBnfKxofKxUNS3a5H3Xg5uzKfd/X22i9OwIgrL4QeyTaWUO5+lcSzls5rFwU0tfifq1brosBYfXDf4qWQVj93OCRaBft9LXIWzms1NUkTHj6Izdy9rPev+UU+YttbBMIyx/3qVoEYfVzh+fg4baYtDiyShFWjKx4Ssqv0oR+tiTuLSSnyaIfkkSyQirXH/OetpcFdrFNbKfFtp66ou2OCICw+oDrNRVk7fQUL0aCTFR8gDY2WEUPvVnVMmXUL0vrHJpMRbVMjaAHWcSIMtRZ5/NkqwgIq09fnqpVEJa/O2Rw1VxGF9NGkxT/W1+lrIkl3EGfIqZnLw8+lERqWoeYTI58mMw8IhwhI02U3K7YF7NL9LPI94x8/XsPWtxEAITl20FyZFAqTR81uXJ5yVh8tnWVbu5GCBn0PySiDymlUm3KIJcpWYrstsggjJxYbJhLi0VSEjmx3Sm7UoQVm7KCsEp74UTlQVi+zvCcCobTQT2Yc/d+WwZlWGZrn5c1gknJjU0x2T5NfDEy0lM/Lp+KoESueJOfqde5O5GTw823N6A1dwRAWH6QWnJDpdJSkc0WIVn10G3kNqVq4tjaUpHSK/b/mmSkH2oC0yujglvs4Li2V7+KLYQvCxVCeCFRlvoE5XdEAITlA74MlJabGLbyVzqy2CKkkghCR00sO/XMWEmbrBt/NKml9NW5Kv43y7mfXlgR1HikcNW6aUIK813SlkwvU1Nan96AVrohAMLygdYyBauRFItsctMu62DUhJHqB3oqZ2k3JKdU5CbvI2qS0aSisUodGA9fwZaIim2RP9rG7xLRRy7TUO8flhrfok4FAiCsCtCCKtYpWI2kkLBSskqioJAYLGQVI5aUPayz7BkTEuEFgzBi0it/8p2eBko0FNMvJCtNpnIq4CkiunZpWNpY+TXomv5zuDogrHaX9oquWLOQsGKyasgql7MKCYFXP/ll5dzHesup2BXb0Cp2xwiNvwsT+LGprJ7u6u8tx3tyNuL7HREAYbWB3zO6CqON2JaCGrLS07HUNE8PeOv0KdQ3lZyXcrGpnuTAJKKL9U/LVDZFyD391daTUNuEAAjLBFOyUO/oSqZFehqniaBGviajGGGVHLRmvcKoKrb1QPZD8dSwZXOnJsWtvvtJIvpW5JhODV5tPQS1XREAYdXD2fPXOlzS54H2KyJ6RKmbmlZtWaR1Tg1eLdtyQ6ro8SgRvU0lvLUeOirKJe9T0zZrBKfJXcvCfe/2vl56WaS95caSIKx6AHv9WscSz+EglzLW3BJbGT6CkdJfR2C5/hGS9labkpvKXe8cthGL4HJ6sazUiqWlbn2vOE5N3Q+fI6IHZjANzqvzQo/oijsF70PiD//C8fRJpoSasCQHZc0t6YhDckNCHikitJBLuDN9CxPJTeXISmy7qVb4dHRm0SuM7FhPboMPiOeiu7recNxafGzrBxfzGDvGUj67XFEEwqrrbJ7RlU6CxwglzPnUyA7v5kol60umAqIH65+6LaIU3dg0VyfQZXOptd0WsrPKOEO58ASB/hHk74aRFwirvLt5RVdhJ9DL76l8TU3eKoymalYWYyjpqaNMwcL9ViXofpaIvrax476kLZTtiwD3Vdmcq39k+0qNXOzWXeDiAjweQw3J6JnLve/hVEamg0JkUq/02hprnqnUNUKeXtMsrx+CUjtQPo+Avj4o7KffJqJP5JvwKYEIqwzH2ggnvPkzN8jDxHfLtTV6Cum5UlYzNd1C27u9Ms+itEbgeSK6N3FT7dApYOgWEJa9o9bc0R4SlTVRHhJWLVH2XCmzJtItCCO6sqDkW+bLRPQFRUqpTbrc91qm+q5ag7DscJZEALVEJdro6ZbkCGp8FepcYoMdmfaSs+rVbtn+LXAKQQhnqw9Jn9s1gsrBVTMIcm0e8XtrBNBKVIydznHxv3krgDUyC7EXIpDl6BmX9nG+r33EcL+TRzkspMT96V1E9Jt20WNbAGHl8S5JtMuvVC3ByMOrrJUk3XP5ri0LYjvmc3uh8oj4lkB0ZcPzK0T0+cwUTkfojOvjRPR6W/NrlAJh5f1kzR9Zo7AtiXrfkBDWDSJ6KK/mkiVkD9pjRPTwkhb4K32oKZw3PCCsbUStifbcow8Wv2lZvEIjhHVkH1l/DCz4rVSGV+F4s6XeyxTTX0fsS07hvJ1y5MHggdXWdCV8zLNl6iZTQG4j3EA6zQqNB6CqDbkVwnLA2ln0kOa+Q0QfPfsUzhtpEFYa0dyvv17JKz0yEkoNc03y/ZH9k8PXu6/3ak82VXL7uYS3/CBxBI1PBQJHHhAVcNyuYrkNwTNZrPddeSTbW2wfUVc2wqYevhihQ4kM/kGSu7xypMTtcv/hFVl8nBEAYd0NqOU2BI8Eu5Ysv7x6+ndk33jj5zEsfk9EbzRM4XRk/Qsieo+HcLRhQ+DIg8KGwN2l5GBnzft7NTKFIDmnI6feS+65qpG5dx0e9LVbP1p0fwcR/VI9Q5aLllhP9s99LUJR1w8BEFY5lt7RQZi/2mMgl6NQX8Mbv5gm2BpQ75+pa4Kwyt3jmbti6dKeaDLbxs5yhLZreEVXNVsDvkhEX/I2CO2NQwCEVYa198rWiGijzMK+pVvslagpdUhXyJ/fI3xNXzPQ+l4IgLDsyLcMtpgUj82mdu3nKFkSnW7dwcTtvJuIfj2HWdBiFAIgLBvSQlaey/Alg9em5dylcoQf3mIp1ghOR91AO7fXJtMOhJV3iKzite5k15Jygzev1XolNEHzLvCPXUwI+6DkuLCPaT0fd9cYhJWHuEck1KPNvCX7lUjt5Je8E+ensHVgP/8sIxmElXdVD3IJzwzmtVirxFb+iS35MRF9YC2ToO0MCICw8l7wJiyZYh4J+1z+ibdqeOb/8l5DiUMicKRB08tB3oR1hPzVFkGF5+hwo2ivnnnCdkFYead7E5Z3e3kL2krwMSF+kTq1/yl3B/gRCLoNQdR2QwCElYfSk2BWGbxbzzw9QUQP5mG7XcITvwKxKHpEBEBYea96DjjPtvKal5WI7SRnfW8S0avKmrqjNLdx1Ev6GmBB1RoEQFh51LyiIu9jPXnN8yViq3msJ++T+ni+erYE8ldZiFCgBAEQVh4tucq3BSvLhYB5TXxKxBLmnptitZZeZO9jOVpZHoGWQbi88QUGtNwwYLkQsECVqqIS6ejKvUgqlDFCThUoqLQeAiAsm89ack8tdW3axUvFSGrkXVt/IaLrRPQoEb29xRDUBQKCAAjL1hdqpzZPEtG1gUnn2BGYkSSF6aCtP6FUJQIgLBtwtXmsWqKzaUX0CBH9PNgjNcvh4b0iSyt2KLcgAiAsu9NK81i8lM93tHvncJ4moqsRkspt4LRb6lOyFC8fqWjl0AiAsOzulWhpi4C8H1fV2oU5KdaDE/ozvnEnZI3+Ze9fKGlAAB3KAJIqonNEcuOCbkHucJLNknykxfrhaWfsI+/h8XciU17XsbY9ulzvqfBoeyBvEgRAWHWOyN3vVPOQhOTJYhp5TyvrrLbXQv7KjhVKFiAAwioAC0XNCCB/ZYYKBUsQAGGVoIWyFgSOeN+XxW6UGYAACGsAyCcTgfzVyRw+0lwQ1ki0zyEL+atz+HkXK0FYu8B+aKGykolXbw7t5n2MA2Htg/tRpSJ/dVTPTmIXCGsSRxxEDeSvDuLIWc0AYc3qmTX1Qv5qTb8tozUIaxlXLaEo8ldLuGldJUFY6/puNs2Rv5rNIwfUB4R1QKfuZBLyVzsBfyaxIKwzedvfViEpjq54G8NqZx79EUGLXREAYXWF9xSNy2tAbCwI6xQu389IENZ+2EMyEAAChQj8HwPk99NB5S5aAAAAAElFTkSuQmCC	irhas.cepunk@gmail.com	2	\N
\.


--
-- Name: issues_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.issues_file_id_seq', 59, true);


--
-- Name: issues_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.issues_id_seq', 108, true);


--
-- Name: issues_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.issues_status_id_seq', 46, true);


--
-- Name: komentar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.komentar_id_seq', 232, true);


--
-- Name: m_flag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_flag_id_seq', 4, true);


--
-- Name: m_flag_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_flag_seq', 2, true);


--
-- Name: m_libur_nasional_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_libur_nasional_id_seq', 4, true);


--
-- Name: m_mapping_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_mapping_menu_id_seq', 229, true);


--
-- Name: m_mapping_menu_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_mapping_menu_seq', 1, false);


--
-- Name: m_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_menu_id_seq', 5, true);


--
-- Name: m_menu_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_menu_seq', 1, false);


--
-- Name: m_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_migrations_id_seq', 3, true);


--
-- Name: m_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_role_id_seq', 10, true);


--
-- Name: m_role_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_role_seq', 1, false);


--
-- Name: m_sub_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_sub_menu_id_seq', 13, true);


--
-- Name: m_sub_menu_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_sub_menu_seq', 1, false);


--
-- Name: m_toefl_preparation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.m_toefl_preparation_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 2, true);


--
-- Name: migrations_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_seq', 1, false);


--
-- Name: pegawai_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pegawai_id_seq', 44094, true);


--
-- Name: tb_paket_materi_teofl_test_id_materi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_paket_materi_teofl_test_id_materi_seq', 2, true);


--
-- Name: tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_roll_akses_teofl_preparation_id_roll_akses_tp_seq', 16, true);


--
-- Name: tb_roll_akses_teofl_test_id_roll_akses_tt_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_roll_akses_teofl_test_id_roll_akses_tt_seq', 21, true);


--
-- Name: tb_roll_paket_user_id_roll_paket_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_roll_paket_user_id_roll_paket_seq', 12, true);


--
-- Name: tb_teofl_test_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tb_teofl_test_id_seq', 6, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 12682, true);


--
-- Name: users_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_seq', 1, false);


--
-- Name: m_flag m_flag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.m_flag
    ADD CONSTRAINT m_flag_pkey PRIMARY KEY (id);


--
-- Name: tb_roll_akses_teofl_preparation tb_roll_akses_teofl_preparation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roll_akses_teofl_preparation
    ADD CONSTRAINT tb_roll_akses_teofl_preparation_pkey PRIMARY KEY (id_roll_akses_tp);


--
-- Name: tb_roll_akses_teofl_test tb_roll_akses_teofl_test_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tb_roll_akses_teofl_test
    ADD CONSTRAINT tb_roll_akses_teofl_test_pkey PRIMARY KEY (id_roll_akses_tt);


--
-- PostgreSQL database dump complete
--

