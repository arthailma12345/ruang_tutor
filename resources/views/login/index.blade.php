<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="A powerful and conceptual apps base dashboard template that especially build for developers and programmers.">
    <!-- Fav Icon  -->
    <!-- <link rel="shortcut icon" href="./images/favicon.png"> -->
    <!-- Page Title  -->
    <title>Login | Meet Tutor</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="{{asset('public/assets/css/dashlite.css?ver=2.9.0')}}">
    <link href="{{ asset('public/assets/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="shortcut icon" href="{{asset('public/assets/images/logo1.jpg')}}">
    <style>
        .ndokmu {
            background-image:url({{ asset('public/image/bgteofl1.jpg')}});
        background-repeat: no-repeat;
        background-size: cover;
        }

        .nk-block-head {
            padding-bottom: 0px;
        }

        form {
            padding: 10px 0;
            position: relative;
            z-index: 2;
        }

        @keyframes square {
            0% {
                transform: translateY(0);
            }

            100% {
                transform: translateY(-90vh);
            }
        }

        @keyframes square {
            0% {
                transform: translateY(0);
            }

            100% {
                transform: translateY(-90vh);
            }
        }
    </style>
    <!-- <style>
        .grad1 {
        height: 200px;
        background-color: red; /* For browsers that do not support gradients */
        background-image: linear-gradient(to top, yellow , blue);
        }
</style> -->
    <style>
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            font-weight: 300;
        }

        body {}

        .card {
            background-color: rgba(255, 255, 255, 1)
        }

        .card.Lo {
            height: 0%;
            z-index: 2;
        }

        .wrapper {
            /* background: #ffffff;
            background: linear-gradient(top left, #31baff 0%, #d6ffd6 100%);
            background: linear-gradient(to bottom right, #d6ffd6 0%, #31baff 100%); */
            /* background-image: url("{{ asset('public/image/bg-login.jpg') }}"); */
            background-repeat: no-repeat;
            background-size: 100% 100%;
            /* position: absolute; */
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .container1 {
            /* max-width: 30%; */
            /* margin-left: auto;
            margin-right: auto;
            margin-top: 6vh;
            margin-bottom: auto; */
            height: 80%;
            text-align: center;
            z-index: 1;
        }

        form {
            position: relative;
            z-index: 2;
        }

        .partner>img {
            max-width: 20%;
            margin-right: 5px;
        }

        .logos {
            background-color: white;
            border-radius: 50px;
            padding-right: 2%;
            padding-left: 2%;
            margin-left: 2%;
            margin-right: 2%;
        }

        .logos>img {
            max-width: 95%;
            margin: 3px;
        }

        @media only screen and (min-width: 1200px) {
            #banner {
                margin-top: 8vh !important;
            }
        }

        @media only screen and (min-width: 2200px) {
            .container1>div {
                max-width: 20% !important;
            }

            /* .logo-link>img {
                max-width: 50% !important;
            } */
        }
    </style>
</head>

<body class="ndokmu">

    <!-- <div class="nk-app-root">
        <div class="nk-main">
            <div class="nk-wrap nk-wrap-nosidebar">
                <div class="nk-content ">
                    <div class="wrapper">
                        <div class="container">
                            <div class="nk-block nk-block-middle nk-auth-body wide-xs py-4" style="height: 100%">
                                <div class="brand-logo py-1 text-center">
                                    <a class="logo-link" style="z-index: 2;">
                                        <img style="width: 75%;" src="{{ asset('public/image/meet_tutor_logo.png') }}" alt="description of myimage">
                                    </a>
                                    
                                </div>
                                <div class="card Lo">
                                    <div class="card-inner">
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">
                                                <h4 class="nk-block-title">Sign In</h4>
                                            </div>
                                        </div>
                                        <form action="javascript:;" name="formLogin" id="formLogin" enctype="multipart/form-data" style="display: none">
                                            <div class="form-group">
                                                <div class="form-label-group">
                                                    <label class="form-label" for="default-01">Username</label>
                                                </div>
                                                <div class="form-control-wrap">
                                                    <input type="text" class="form-control form-control-lg" name="username" id="username" placeholder="Enter your username">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-label-group">
                                                    <label class="form-label" for="password">Password</label>
                                                </div>
                                                <div class="form-control-wrap">
                                                    <a href="#" class="form-icon form-icon-right passcode-switch xl" data-target="password">
                                                        <em class="passcode-icon icon-show "><i class='bx bx-show bx-xs'></i></em>
                                                        <em class="passcode-icon icon-hide "><i class='bx bxs-hide bx-xs'></i></em>
                                                    </a>
                                                    <input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Enter your passcode">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <button id="btnLogin" name="btnLogin" type="submit" class="btn btn-primary waves-effect waves-light col-12"><i class='bx bx-log-in-circle'></i> Login </button>
                                                <hr>
                                                <button id="btnLoginQRCode" name="btnLoginQRCode" type="button" class="btn btn-outline-light waves-effect waves-light btnLoginQRCode" data-bs-toggle="modal" data-bs-target="#modalScanQRCodeLogin"> <i class='bx bx-search'></i> Login QRCOde
                                                </button>
                                                <button type="button" class="col-12 btn btn-outline-light waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalResetPassword"> <i class='bx bx-reset'></i> Reset Password </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> -->

    <div class="wrapper">
        <!-- <div class="row g-0">
            <div class="col-md-4 mt-2">
                <div class="logos d-flex justify-content-center">
                    <img src="{{asset('public/image/logos.png')}}" alt="">
                </div>
            </div>
        </div> -->
        <div class="container1 d-flex align-items-center justify-content-center align-self-center mt-3">
            <div class="col-md-4">
                <a class="logo-link " style="z-index: 2;">
                    <img style="width: 70%;" src="{{ asset('public/image/meet_tutor_logo.png') }}" alt="description of myimage">
                </a>
                <div class="col-md-12">
                    <div class="card Lo">
                        <div class="card-body">
                            <div class="card-title">
                                <h4>Sign In</h4>
                            </div>
                            <form action="javascript:;" name="formLogin" id="formLogin" style="display:none" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div class="form-label-group">
                                        <label class="form-label" for="default-01">Username</label>
                                    </div>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control form-control-lg" name="username" id="username" placeholder="Enter your username">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-label-group">
                                        <label class="form-label" for="password">Password</label>
                                    </div>
                                    <div class="form-control-wrap">
                                        <a href="#" class="form-icon form-icon-right passcode-switch xl" data-target="password">
                                            <em class="passcode-icon icon-show "><i class='bx bx-show bx-xs'></i></em>
                                            <em class="passcode-icon icon-hide "><i class='bx bxs-hide bx-xs'></i></em>
                                        </a>
                                        <input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Enter your passcode">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <!-- <button class="btn btn-lg btn-primary btn-block">Sign in</button> -->
                                    <button id="btnLogin" name="btnLogin" type="submit" class="btn btn-primary waves-effect waves-light col-12"><i class='bx bx-log-in-circle'></i> Login </button>
                                    <hr>
                                    <!-- <button id="btnLoginQRCode" name="btnLoginQRCode" type="button" class="btn btn-outline-light waves-effect waves-light btnLoginQRCode" data-bs-toggle="modal" data-bs-target="#modalScanQRCodeLogin"> <i class='bx bx-search'></i> Login QRCOde
                                    </button> -->
                                    <button type="button" class="col-12 btn btn-outline-light waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalResetPassword"> <i class='bx bx-reset'></i> Reset Password </button>
                                    <!-- <button id="btnModalScanQRCodeTiketIssuesKamera" name="btnModalScanQRCodeTiketIssuesKamera" type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalScanQRCodeTiketIssuesKamera">ScanQRCode</button> -->
                                </div>
                            </form>
                            <!-- <p class="mt-1" style="font-size: 12px;">Partner App:</p> -->
                            <!-- <div class="d-flex justify-content-center partner">
                                <img src="{{asset('public/image/simasti_logo.png')}}" alt="">
                                <img src="{{asset('public/image/logo_dof.jpeg')}}" alt="">
                            </div> -->
                            <div style="width: 100%" id="reader"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalResetPassword">
        <div class="modal-dialog modal-dialog-top modal-md" style="min-width: 35%" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Reset Password</h5>
                </div>
                <div class="modal-body">
                    <form action="#" class="form-validate is-alter" name="formResetPassword" id="formResetPassword">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="form-label" for="full-name">Masukan Username</label>
                            <div class="form-control-wrap">
                                <input type="text" class="form-control" id="username_reset_password" name="username_reset_password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="full-name">Masukan Email</label>
                            <div class="form-control-wrap">
                                <input type="text" class="form-control" id="email_reset_password" name="email_reset_password" required>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer bg-light">
                    <button type="button" id="button_reset_password" namme="button_reset_password" class="btn btn-md btn-primary">Reset Password</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalScanQRCodeLogin">
        <div class="modal-dialog modal-dialog-top modal-md" style="min-width: 35%" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Scan QR Code</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <video id="camera" style="width: 100%"></video>
                        <div id="qrcode"></div>
                    </div>
                </div>
                <div class="modal-footer bg-light">
                    <!-- <span class="sub-text">Modal Footer Text</span> -->
                    <button type="button" id="kembali" namme="kembali" class="btn btn-md btn-primary">
                        Kembali</button>
                    <button type="button" id="kamera_depan" namme="kamera_depan" class="btn btn-md btn-primary">Kamera
                        Depan</button>
                    <button type="button" id="kamera_belakang" namme="kamera_belakang" class="btn btn-md btn-primary">Kamera Belakang</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('public/assets_frontend/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/assets/js/bundle.js?ver=2.9.0')}}"></script>
    <script src="{{asset('public/assets/js/scripts.js?ver=2.9.0')}}"></script>
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <script src="{{asset('public/assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
    <!-- select region modal -->

    <!-- qrhtml5 -->
    <!-- <script src="{{asset('public/assets/js/html5-qrcode.min.js')}}"></script> -->
    <!-- <script src="https://unpkg.com/html5-qrcode"></script> -->
    <!-- <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.0.3/highlight.min.js"></script>
    <script src="https://blog.minhazav.dev/assets/research/html5qrcode/html5-qrcode.min.v2.3.0.js"></script>

    <script>
        $(document).ready(function() {

            // let config = {
            //     fps: 10,
            //     qrbox: {width: 100, height: 100},
            //     useBarCodeDetectorIfSupported: true,
            //     rememberLastUsedCamera: true,
            //     aspectRatio: 4/3,
            //     showTorchButtonIfSupported: true,
            //     showZoomSliderIfSupported: true,
            //     defaultZoomValueIfSupported: 2,
            //     // supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_CAMERA],
            // };
            // let html5QrcodeScanner = new Html5QrcodeScanner(
            // "reader", config);
            // html5QrcodeScanner.render(onScanSuccess);

            // ----------------------------------------------------------------------------------
            var qrboxFunction = function(viewfinderWidth, viewfinderHeight) {
                // Square QR Box, with size = 80% of the min edge.
                var minEdgeSizeThreshold = 250;
                var edgeSizePercentage = 0.75;
                var minEdgeSize = (viewfinderWidth > viewfinderHeight) ?
                    viewfinderHeight : viewfinderWidth;
                var qrboxEdgeSize = Math.floor(minEdgeSize * edgeSizePercentage);
                if (qrboxEdgeSize < minEdgeSizeThreshold) {
                    if (minEdgeSize < minEdgeSizeThreshold) {
                        return {
                            width: minEdgeSize,
                            height: minEdgeSize
                        };
                    } else {
                        return {
                            width: minEdgeSizeThreshold,
                            height: minEdgeSizeThreshold
                        };
                    }
                }
                return {
                    width: qrboxEdgeSize,
                    height: qrboxEdgeSize
                };
            }

            let html5QrcodeScanner = new Html5QrcodeScanner(
                "reader", {
                    fps: 10,
                    qrbox: qrboxFunction,
                    experimentalFeatures: {
                        useBarCodeDetectorIfSupported: true
                    },
                    rememberLastUsedCamera: false,
                    showTorchButtonIfSupported: true
                });

            // html5QrcodeScanner.clear();

            html5QrcodeScanner.render(onScanSuccess);

            // function onScanSuccess(decodedText, decodedResult) {
            //     // Handle on success condition with the decoded text or result.
            //     alert("Berhasil = "+decodedText);
            //     html5QrcodeScanner.clear();
            // }

            // ----------------------------------------------------------------------------------

            function onScanSuccess(decodedText, decodedResult) {
                // Handle on success condition with the decoded text or result.
                console.log(`Scan result: ${decodedText}`, decodedResult);
                // ...
                // alert(decodedText);
                // loginQRCode(decodedText);
                // html5QrcodeScanner.clear();
                // ^ this will stop the scanner (video feed) and clear the scan area.
                var formLoginQRCode =
                    // $("#formSuratPerjanjianIssues").serialize() +
                    "token_login_qr_code=" + decodedText;
                console.log(formLoginQRCode);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: formLoginQRCode,
                    url: "{{url('loginQRCode')}}",
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        console.log(data.kode);
                        if (data.kode == 200) {
                            // toastr.clear();
                            // toastr.success('Anda Berhasil Login');
                            // $(".card.Lo").animate({
                            //     height: '-=50%'
                            // });
                            // $('form').css('display', 'none');
                            $('#formLogin').hide('slow', 'linear')
                            setTimeout(function() {
                                Swal.fire({
                                    position: "top",
                                    icon: "success",
                                    title: data.success,
                                    showConfirmButton: !1,
                                    timer: 1500
                                })

                                if (data.role == 'R003') {
                                    document.location = "{{ url('/home/index_peserta') }}"
                                } else {
                                    document.location = "{{ url('/home/index') }}"
                                }

                            }, 1000);

                            // $('#modalScanQRCodeLogin').modal('hide');
                            // scanner.stop();
                            html5QrcodeScanner.clear();

                        } else if (data.kode == 401) {
                            // $('#formLogin').trigger('reset');
                            // toastr.warning('adf');
                            // console.log(toastr.warning('adf'));
                            // console.log('salah');
                            // toastr.clear();
                            // toastr.error('Username Atau Password Anda Salah');
                            // toastr.error('Anda Berhasil Login');
                            Swal.fire({
                                position: "top",
                                icon: "error",
                                title: data.success,
                                showConfirmButton: !1,
                                timer: 1500
                            });

                            // $('#modalScanQRCodeLogin').modal('hide');
                            // scanner.stop();

                        } else {

                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#modalPenghargaan').modal('show');
                    }
                });
            }
        });
    </script>

    <script>
        $(".card.Lo").css('height', '100%');
        setTimeout(function() {
            $('#formLogin').show(800);
        }, 500);
    </script>

    <script>
        $('#formLogin').submit(function(event) {
            // console.log('coba');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: $('#formLogin').serialize(),
                url: "{{url('login')}}",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    // console.log('data serialize :' + data.username);
                    // console.log( $( this ).serialize() );
                    // console.log(data.kode);
                    if (data.kode == 200) {
                        // toastr.clear();
                        // toastr.success('Anda Berhasil Login');
                        // $('#formLogin').hide('slow', 'linear')
                        // setTimeout(function() {
                        //     $('#formLogin').hide('slow', 'linear');
                        // }, 500);
                        // $('form').css('display', 'none');
                        $('#formLogin').hide('slow', 'linear')
                        setTimeout(function() {
                            Swal.fire({
                                position: "top",
                                icon: "success",
                                title: data.success,
                                showConfirmButton: !1,
                                timer: 1500
                            })

                            if (data.role == 'R003') {
                                document.location = "{{ url('/home/index_peserta') }}"
                            } else if(data.role == 'R001' || data.role == 'R012') {
                                document.location = "{{ url('/home/index') }}"
                            }else{
                             
                            }

                        }, 1000);

                    } else if (data.kode == 401) {
                        // $('#formLogin').trigger('reset');
                        // toastr.warning('adf');
                        // console.log(toastr.warning('adf'));
                        // console.log('salah');
                        // toastr.clear();
                        // toastr.error('Username Atau Password Anda Salah');
                        // toastr.error('Anda Berhasil Login');
                        Swal.fire({
                            position: "top",
                            icon: "error",
                            title: data.success,
                            showConfirmButton: !1,
                            timer: 1500
                        });
                    } else {

                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#modalPenghargaan').modal('show');
                }
            });
        });
    </script>

    <script>
        $(document).on('click', '#button_reset_password', function() {
            console.log('coba');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: $('#formResetPassword').serialize(),
                url: "{{url('resetPasword')}}",
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    if (data.kode == 200) {

                        Swal.fire({
                            position: "top",
                            icon: "success",
                            title: data.success,
                            showConfirmButton: !1,
                            timer: 1500
                        })

                        // $('#modalResetPassword').hide();
                        $('#modalResetPassword').modal('hide');

                    } else if (data.kode == 401) {

                        Swal.fire({
                            position: "top",
                            icon: "error",
                            title: data.success,
                            showConfirmButton: !1,
                            timer: 1500
                        });

                        // $('#modalResetPassword').hide();

                    } else {

                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#modalPenghargaan').modal('show');
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#modalScanQRCodeLogin').modal({
                backdrop: 'static',
                keyboard: false
            });

            $(document).on('click', '#kembali', function() {
                $('#modalScanQRCodeLogin').modal('hide');
                scanner.stop();
            });

            let scanner = new Instascan.Scanner({
                video: document.getElementById("camera"),
                mirror: false
            });
            let resultado = document.getElementById("qrcode");
            $(document).on('click', '#btnLoginQRCode', function() {
                console.log('coba');
                scanner.stop();
                scanner.addListener("scan", function(content) {
                    // resultado.innerText = content;
                    console.log(content);
                    loginQRCode(content);
                    scanner.stop();
                });
                Instascan.Camera.getCameras()
                    .then(function(cameras) {
                        if (cameras.length > 0) {
                            scanner.start(cameras[0]);
                        } else {
                            resultado.innerText = "No cameras found.";
                        }
                    })
                    .catch(function(e) {
                        // resultado.innerText = e;
                        console.log(e);
                    });
            });

            $(document).on('click', '#kamera_depan', function() {
                scanner.stop();
                scanner.addListener("scan", function(content) {
                    // resultado.innerText = content;
                    console.log(content);
                    loginQRCode(content);
                    scanner.stop();
                });
                Instascan.Camera.getCameras()
                    .then(function(cameras) {
                        // alert(cameras.length);
                        if (cameras.length > 0) {
                            scanner.start(cameras[0]);
                        } else {
                            resultado.innerText = "No cameras found.";
                        }
                    })
                    .catch(function(e) {
                        // resultado.innerText = e;
                        console.log(e);
                    });
            });

            $(document).on('click', '#kamera_belakang', function() {
                scanner.stop();
                scanner.addListener("scan", function(content) {
                    // resultado.innerText = content;
                    console.log(content);
                    loginQRCode(content);
                    scanner.stop();
                });
                Instascan.Camera.getCameras()
                    .then(function(cameras) {
                        // alert(cameras.length);
                        if (cameras.length > 0) {
                            // scanner.start(cameras[3]);
                            var selectedCam;
                            // $.each(cameras, (i, c) => {
                            //     if (c.name.indexOf('back') != -1) {
                            //         selectedCam = c;
                            //         return false;
                            //     }
                            // });
                            var coba = [];
                            for (var i = 0; i < cameras.length; i++) {
                                // console.log(cameras[i].name.indexOf('back'));
                                coba.push(cameras[i].name.indexOf('back'));
                                if (cameras[i].name.indexOf('back') != -1) {
                                    selectedCam = cameras[i + 1];
                                    // return false;
                                    break;
                                } else {
                                    // alert('kamera beakang tidak ditemukan');
                                }
                            }
                            // alert(selectedCam.name.indexOf('back'));
                            // console.log(coba);
                            scanner.start(selectedCam);

                        } else {
                            resultado.innerText = "No cameras found.";
                        }
                    })
                    .catch(function(e) {
                        // resultado.innerText = e;
                        console.log(e);
                    });
            });

            function loginQRCode(token_login_qr_code) {
                var formLoginQRCode =
                    // $("#formSuratPerjanjianIssues").serialize() +
                    "token_login_qr_code=" + token_login_qr_code;
                console.log(token_login_qr_code);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: formLoginQRCode,
                    url: "{{url('loginQRCode')}}",
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        console.log(data.kode);
                        if (data.kode == 200) {
                            // toastr.clear();
                            // toastr.success('Anda Berhasil Login');
                            // $(".card.Lo").animate({
                            //     height: '-=50%'
                            // });
                            // $('form').css('display', 'none');
                            $('#formLogin').hide('slow', 'linear')
                            setTimeout(function() {
                                Swal.fire({
                                    position: "top",
                                    icon: "success",
                                    title: data.success,
                                    showConfirmButton: !1,
                                    timer: 1500
                                })

                                if (data.role == 'R003') {
                                    document.location = "{{ url('/home/index_peserta') }}"
                                } else if(data.role == 'R001' || data.role == 'R012') {
                                    document.location = "{{ url('/home/index') }}"
                                }else{
                                    console.log('gagal login');
                                    // document.location = "{{ url('/login/index') }}"
                                }

                            }, 1000);

                            $('#modalScanQRCodeLogin').modal('hide');
                            scanner.stop();

                        } else if (data.kode == 401) {
                            // $('#formLogin').trigger('reset');
                            // toastr.warning('adf');
                            // console.log(toastr.warning('adf'));
                            // console.log('salah');
                            // toastr.clear();
                            // toastr.error('Username Atau Password Anda Salah');
                            // toastr.error('Anda Berhasil Login');
                            Swal.fire({
                                position: "top",
                                icon: "error",
                                title: data.success,
                                showConfirmButton: !1,
                                timer: 1500
                            });

                            $('#modalScanQRCodeLogin').modal('hide');
                            scanner.stop();

                        } else {

                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#modalPenghargaan').modal('show');
                    }
                });
            }




        });
    </script>
</body>

</html>