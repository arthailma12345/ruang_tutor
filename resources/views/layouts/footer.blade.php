<footer class="footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <script>
          document.write(new Date().getFullYear())
        </script> &copy; Meet Tutor
      </div>
      <div class="col-sm-6">
        <div class="text-sm-end d-none d-sm-block">
          Create <i class="mdi mdi-heart text-danger"></i> by <a href="https://www.instagram.com/arirprogramming_id/" target="_blank" class="text-reset">ARIR Programming</a>
        </div>
      </div>
    </div>
  </div>
</footer>