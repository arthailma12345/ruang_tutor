<script src="{{asset('public/assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('public/assets/libs/metismenujs/metismenujs.min.js')}}"></script>
<script src="{{asset('public/assets/libs/simplebar/simplebar.min.js')}}"></script>
<!-- <script src="{{asset('public/assets/libs/feather-icons/feather.min.js')}}"></script> -->

<!-- apexcharts -->
<!-- <script src="{{asset('public/assets/libs/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{asset('public/assets/moment/min/moment.min.js')}}"></script>
<script src="{{asset('public/assets/js/pages/apexcharts.init.js')}}"></script> -->
<!-- Chart JS -->
<script src="{{asset('public/assets/js/pages/chartjs.js')}}"></script>

<!-- <script src="{{asset('public/assets/js/pages/dashboard.init.js')}}"></script> -->

<!-- choices js -->
<script src="{{asset('public/assets/libs/choices.js/public/assets/scripts/choices.min.js')}}"></script>

<!-- color picker js -->
<!-- <script src="{{asset('public/assets/libs/%40simonwep/pickr/pickr.min.js')}}"></script> -->
<!-- <script src="{{asset('public/assets/libs/%40simonwep/pickr/pickr.es5.min.js')}}"></script> -->

<!-- datepicker js -->
<!-- <script src="{{asset('public/assets/libs/flatpickr/flatpickr.min.js')}}"></script> -->

<!-- init js -->
<!-- <script src="{{asset('public/assets/js/pages/form-advanced.init.js')}}"></script> -->

<!-- ckeditor -->
<!-- <script src="{{asset('public/assets/libs/%40ckeditor/ckeditor5-build-classic/build/ckeditor.js')}}"></script> -->

<!-- quill js -->
<!-- <script src="{{asset('public/assets/libs/quill/quill.min.js')}}"></script> -->
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/quill-image-resize-module@3.0.0/image-resize.min.js"></script>

<!-- init js -->
<!-- <script src="{{asset('public/assets/js/pages/form-editor.init.js')}}"></script> -->

<!-- <script src="{{asset('public/assets/js/pages/modal.init.js')}}"></script> -->

<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

<!-- <script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script> -->
<script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-5-theme/1.3.0/select2-bootstrap-5-theme.min.css"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.full.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-business-days/1.2.0/index.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-business-days/1.2.0/index.d.ts"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-business-days/1.2.0/index.min.js"></script>

<!-- <script src="https://cdn.jsdelivr.net/npm/moment-business-time@2.0.0/index.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment-business-time@2.0.0/lib/business-hours.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment-business-time@2.0.0/package.json"></script>
<script src="https://cdn.jsdelivr.net/npm/moment-business-time@2.0.0/index.js"></script> -->

<!-- <script src="https://cdn.jsdelivr.net/npm/@tototares/moment-business-time@1.0.2/index.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@tototares/moment-business-time@1.0.2/.travis.yml"></script>
<script src="https://cdn.jsdelivr.net/npm/@tototares/moment-business-time@1.0.2/LICENSE"></script>
<script src="https://cdn.jsdelivr.net/npm/@tototares/moment-business-time@1.0.2/package.json"></script>
<script src="https://cdn.jsdelivr.net/npm/@tototares/moment-business-time@1.0.2/package-lock.json"></script>
<script src="https://cdn.jsdelivr.net/npm/@tototares/moment-business-time@1.0.2/README.md"></script> -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/webui-popover/1.2.18/jquery.webui-popover.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webui-popover/1.2.18/jquery.webui-popover.js"></script> -->


<!-- <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.2.4/js/dataTables.fixedHeader.min.js"></script>

<script>
    $("#btnLogout").click(function() {
        console.log('logout');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('logout')}}",
            type: "GET",
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                // console.log(data.kode);
                // if (data.kode == 200) {
                //     toastr.clear();
                //     NioApp.Toast('Anda Berhasil Logout', 'success', {
                //         position: 'top-right'
                //     });
                //     document.location = "{{ url('/') }}";
                // } else {
                //     toastr.clear();
                //     NioApp.Toast('Anda Gagal Logout', 'error', {
                //         position: 'top-right'
                //     });
                // }
                // console.log(data);
                document.location = "{{ url('/') }}";
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
                document.location = "{{ url('/') }}";
            }
        });
    });
</script>
<!-- 
<script>
    console.log('cobaan apa ini..?');
</script> -->