<head>

  <meta charset="utf-8" />
  <title>RUANGAN TUTOR</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
  <meta content="Themesbrand" name="author" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- App favicon -->
  <link rel="shortcut icon" href="{{asset('public/assets/images/logo1.jpg')}}">

  <!-- choices css -->
  <link href="{{asset('public/assets/libs/choices.js/public/assets/styles/choices.min.css')}}" rel="stylesheet" type="text/css" />

  <!-- color picker css -->
  <link rel="stylesheet" href="{{asset('public/assets/libs/%40simonwep/pickr/themes/classic.min.css')}}" /> <!-- 'classic' theme -->
  <link rel="stylesheet" href="{{asset('public/assets/libs/%40simonwep/pickr/themes/monolith.min.css')}}" /> <!-- 'monolith' theme -->
  <link rel="stylesheet" href="{{asset('public/assets/libs/%40simonwep/pickr/themes/nano.min.css')}}" /> <!-- 'nano' theme -->

  <!-- <link href="{{asset('public/assets/libs/quill/quill.core.css')}}" rel="stylesheet" type="text/css" /> -->
  <link href="//cdn.quilljs.com/1.3.6/quill.core.css" rel="stylesheet">
  <!-- <link href="{{asset('public/assets/libs/quill/quill.bubble.css')}}" rel="stylesheet" type="text/css" /> -->
  <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
  <!-- <link href="{{asset('public/assets/libs/quill/quill.snow.css')}}" rel="stylesheet" type="text/css" /> -->
  <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

  <!-- datepicker css -->
  <link rel="stylesheet" href="{{asset('public/assets/libs/flatpickr/flatpickr.min.css')}}">

  <!-- Bootstrap Css -->
  <link href="{{asset('public/assets/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
  <!-- Icons Css -->
  <link href="{{asset('public/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
  <!-- App Css-->
  <link href="{{asset('public/assets/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" />

  <!-- <link href="//cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" id="app-style" rel="stylesheet" type="text/css" /> -->
  <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" id="app-style" rel="stylesheet" type="text/css" /> -->
  <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" id="app-style" rel="stylesheet" type="text/css" />

  <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/> -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-5-theme/1.3.0/select2-bootstrap-5-theme.min.css"></script> -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />

  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />

  <link href="https://cdnjs.cloudflare.com/ajax/libs/webui-popover/1.2.18/jquery.webui-popover.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/webui-popover/1.2.18/jquery.webui-popover.min.css" rel="stylesheet" />

  <link href="https://datatables.net/release-datatables/extensions/FixedHeader/js/dataTables.fixedHeader.js" rel="stylesheet" />
  <link href="https://cdn.datatables.net/fixedcolumns/4.1.0/css/fixedColumns.dataTables.min.css" rel="stylesheet" />

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />

  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet" />

  <link href="https://fonts.googleapis.com/css?family=Calibri" rel="stylesheet">

  <style>
    .ql-font-calibri {
      font-family: 'Calibri', Calibri;
    }
  </style>

  <style>
    .popover {
      z-index: 1060 !important;
    }
  </style>

  <style>
    .swal2-container {
      z-index: 1000000 !important;
    }
  </style>
  <style>
    .swal-wide-850 {
      width: 850px !important;
    }
  </style>
  <style>
    .div_scroll_none::-webkit-scrollbar {
      display: none;
    }

    .div_scroll_none {
      -ms-overflow-style: none;
      /* IE and Edge */
      scrollbar-width: none;
      /* Firefox */
    }

    /* Works on Firefox */
    * {
      scrollbar-width: thin;
      scrollbar-color: white orange;
    }

    /* Works on Chrome, Edge, and Safari */
    *::-webkit-scrollbar {
      width: 12px;
    }

    *::-webkit-scrollbar-track {
      background: white;
    }

    *::-webkit-scrollbar-thumb {
      background-color: blue;
      border-radius: 20px;
      border: 3px solid white;
    }
  </style>

  <style>
    .dtfh-floatingparenthead {
      background-color: white;
      font-size: 14pt;
    }

    .dtfh-floatingparent {
      overflow-x: scroll !important;
      overflow-y: hidden !important;
      height: 270px !important;
    }
  </style>

</head>