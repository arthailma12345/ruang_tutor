<!-- Left Sidebar End -->
<header id="page-topbar" class="ishorizontal-topbar" style="background-color: white !important; color:black;">
  <div class="navbar-header">
    <div class="d-flex">
      <!-- LOGO -->
      <div class="navbar-brand-box">
        <a href="#" class="logo logo-light">
          <span class="logo-sm">
            <img src="{{ asset('public/image/meet_tutor_logo1.png') }}" alt="" style="height: 12%; width: 12%">
          </span>
          <!-- <span class="logo-lg">
            <span class="logo-txt" style="color: blue">RUANGAN TUTOR</span> -->
            <!-- <img src="{{ asset('public/image/logo_ruang_tutor.png') }}" alt="description of myimage" style="height: 12%; width: 12%"> -->
          <!-- </span> -->
        </a>
      </div>

      <div class="topnav">
        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

          <div class="collapse navbar-collapse" id="topnav-menu-content">
            <ul class="navbar-nav">

              @foreach(Session::get('user_app')['menu'] as $datas => $value)

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-admin_panel" role="button">
                  <i class='bx bx-grid-alt'></i>
                  <span data-key="t-apps">{{$datas}}</span>
                  <div class="arrow-down"></div>
                </a>
                <!-- {{count($value)}} -->
                <div class="dropdown-menu" aria-labelledby="topnav-admin_panel">
                  @php
                  for($i = 0; $i < count($value); $i++){ @endphp <a href="{{url($value[$i]->m_sub_menu_url_sub_menu)}}" class="dropdown-item" data-key="t-calendar">{{$value[$i]->m_sub_menu_nama_sub_menu}}</a>

                    @php
                    }
                    @endphp
                </div>

              </li>

              @endforeach

            </ul>
          </div>
        </nav>
      </div>
    </div>

    <div class="d-flex">
      <?php

      $foto_url = asset('public/assets/images/users/avatar-1.jpg');

      ?>

      <div class="dropdown d-inline-block">
        <button type="button" class="btn header-item user text-start d-flex align-items-center" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img class="rounded-circle header-profile-user" src="{{$foto_url}}" alt="Header Avatar">
        </button>
        <div class="dropdown-menu dropdown-menu-end pt-0">
          <button onclick="location.href='{{url('/index_profil')}}'" type="button" class="dropdown-item"><i class='bx bx-user-pin text-muted font-size-18 align-middle me-1'></i> <span class="align-middle">Profil</span></button>
          <hr>
          <button id="btnLogout" name="btnLogout" type="button" class="dropdown-item"><i class='bx bx-log-out text-muted font-size-18 align-middle me-1'></i> <span class="align-middle">Logout</span></button>
        </div>
      </div>
    </div>
  </div>
</header>