@extends('layouts_frontend.app')
@section('content')
<header>
    <div class="inner-page-header_home inner-header-bg-2"></div>
</header>

<!-- product-section -->
<section class="product-section bg-dark pt-100">
    <h1 style="font-weight: bolder; text-align: center;">MT Class</h1>
            <div class="container">
                <div class="border-bottom pb-100">
                <div class="row" id="append_html">
                        <!-- konten                     -->
                    </div>
                </div>
            </div>
        </section>
        <!-- .end product-section -->

@endsection

@section('script')
<!-- script irhas -->
<script>
    $(document).ready(function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // data: $('#FormEditUser').serialize(),
            url: "{{ url('study_room_peserta/getDataPaket') }}",
            type: "GET",
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                // console.log("Log Cek Data",data.data[0].id_paket);
                var view_produk = '';
                for (var i = 0; i < data.data.length; i++) {
                    // console.log(data.data[0].id_tb_m_toefl_preparation);
                    view_produk +=
                        '<div style="width:50%" class="col-md-6 col-lg-2 pb-30">'+
                        '<div class="product-card product-card-white">'+
                        '<div class="product-card-thumb">'+
                        '<a href="' + "{{url('study_room_peserta/materiIndex/')}}" + '/' + data.data[i].id_paket + '"><img src=' + "{{ asset('public/image/new_icon1.png') }}" + ' alt="product"></a>'+
                        '<a href="' + "{{url('study_room_peserta/materiIndex/')}}" + '/' + data.data[i].id_paket + '" class="product-cart main-btn"><th>Lihat</th></a>'+
                        '</div>'+
                        '<div class="product-card-details">'+
                        '<h3><a href="' + "{{url('study_room_peserta/materiIndex/')}}" + '/' + data.data[i].id_paket + '"><th>' + data.data[i].nama + '</th></a></h3>'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                }



                $("#append_html").html(view_produk);


            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

@endsection