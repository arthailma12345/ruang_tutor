@extends('layouts_frontend.app')

@section('content')
<header>
    <div class="inner-page-header_home inner-header-bg-2"></div>
</header>
<!-- product-section -->
<section class="product-section bg-dark pt-100">
    <?php $count = 0; ?>
    @foreach ($tb_materi as $data)
    <?php if ($count == 1) {
        break;
    } ?>
    <h1 style="font-weight: bolder; text-align: center;"><span> {{ $data->nama_paket }}</span></h1>
    <?php $count++; ?>
    @endforeach
    <div class="container">
        <div class="border-bottom pb-100">
            <div class="product-list-header">
                <div class="product-list-header-item">
                    <div class="product-list-result product-list-result-white">
                        <!-- <p>Showing <span>1-12</span> out of <span>32</span></p> -->
                    </div>
                </div>
                <div class="product-list-header-item">
                    <div class="product-list-action">
                        <div class="product-list-search product-list-search-white">
                            <form>
                                <div class="form-group">
                                    <input id="search_materi" name="search_materi" type="text" placeholder="Search your product" class="form-control form-control-white">
                                    <button><i class="flaticon-magnifying-glass"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="append_html">
                <!-- conten -->
            </div>
        </div>
    </div>
</section>
<!-- .end product-section -->
@endsection

@section('script')
<!-- script irhas -->
<script>
    $(document).ready(function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // data: $('#FormEditUser').serialize(),
            url: "{{ url('study_room_peserta/getDataMateri') }}" + '/' + "{{ $id_paket }}",
            type: "GET",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                // console.log("Log Cek Data",data.data[0].id_paket);
                console.log(data.data.data_uncal);
                var view_produk = '';
                for (var i = 0; i < data.data.data_uncal.length; i++) {
                    // console.log(data.data[0].id_tb_m_toefl_preparation);
                    view_produk +=
                        '<div style="width:50%" class="col-md-6 col-lg-3 pb-30">' +
                        '<div class="news-card news-card-dark news-card-video">' +
                        // '<div class="card-body">' +
                        data.data.data_uncal[i].dataurl +
                        // '<object data=' + data.data.data_uncal[i].dataurl +
                        // 'type="video/mp4"></object>' +
                        // '<div class="news-video-icon">' +
                        // '<a href="' + data.data.data_uncal[i].convertedURLEmbed +
                        // '" id="video-popup_' + i + '"><i class="flaticon-play-button"></i></a>' +
                        // '</div>' +
                        // '</div>' +
                        '<div class="news-card-content">' +
                        '<div class="news-content-flex">' +
                        '<h5>' + data.data.data_uncal[i].judul_materi + '</h5>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }

                $("#append_html").html(view_produk);

                for (var j = 0; j < data.data.data_uncal.length; j++) {
                    $("#video-popup_" + j).magnificPopup({
                        disableOn: 0,
                        type: 'iframe',
                        mainClass: 'mfp-fade',
                        removalDelay: 160,
                        preloader: false,
                        fixedContentPos: false
                    });
                };

            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>


<script>
    $(document).ready(function() {
        $('#search_materi').on('keyup', function() {

            // console.log(this.val);
            var search_materi = $("#search_materi").val();
            // console.log(search_materi);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // data: $('#FormEditUser').serialize(),
                url: "{{ url('study_room_peserta/getDataMateri') }}" + '/' +
                    "{{ $id_paket }}" + '?search_materi=' + search_materi,
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    // console.log("Log Cek Data",data.data[0].id_paket);
                    console.log(data.data.data_uncal);
                    var view_produk = '';
                    for (var i = 0; i < data.data.data_uncal.length; i++) {
                        // console.log(data.data[0].id_tb_m_toefl_preparation);
                        view_produk +=
                            '<div style="width:50%" class="col-md-6 col-lg-3 pb-30">' +
                            '<div class="news-card news-card-dark news-card-video">' +
                            // '<div style="width:"500px"; height:"400px";>' +
                            data.data.data_uncal[i].dataurl +
                            // '<object data=' + data.data.data_uncal[i].dataurl +
                            // 'type="video/mp4"></object>' +
                            // '<div class="news-video-icon">' +
                            // '<a href="' + data.data.data_uncal[i].convertedURLEmbed +
                            // '" id="video-popup_' + i + '"><i class="flaticon-play-button"></i></a>' +
                            // '</div>' +
                            // '</div>' +
                            '<div class="news-card-content">' +
                            '<div class="news-content-flex">' +
                            '<h5>' + data.data.data_uncal[i].judul_materi + '</h5>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    }

                    $("#append_html").html(view_produk);

                    // for (var j = 0; j < data.data.data_uncal.length; j++) {
                    //     $("#video-popup_" + j).magnificPopup({
                    //         disableOn: 0,
                    //         type: 'iframe',
                    //         mainClass: 'mfp-fade',
                    //         removalDelay: 160,
                    //         preloader: false,
                    //         fixedContentPos: false
                    //     });
                    // };

                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });

        });

    });
</script>

<!-- <script>
        $(document).ready(function() {
            var interval_alert;
            interval_alert = setInterval(fun, 3000);


            function fun() {
                var html_panduan =
                    '<p>Simak video dengan menggunakan headphone agar suara lebih jelas dan konsentrasi, tonton video secara berurutan agar kalian lebih paham alur pembelajaran kalin ini di paket Toefl Tes A</p>';
                Swal.fire({
                    title: 'PANDUAN BELAJAR',
                    // input: input,
                    html: true,
                    html: html_panduan,
                    customClass: 'swal-wide-850',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCancelButton: false,
                    showConfirmButton: true,
                    confirmButtonText: 'Lanjut Tonton Video',
                }).then(function(result) {
                    if (result.value) {

                        console.log('Modal berhasil keluar');
                    }
                });
                clearInterval(interval_alert);
            }
        });
    </script> -->
@endsection