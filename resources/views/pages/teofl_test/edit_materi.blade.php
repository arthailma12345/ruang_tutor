@extends('layouts.app')

@section('content')

<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    @foreach ($tb_materi as $data)
                    <h4 class="mb-0">{{$data->nama_paket}} </h4>
                    @endforeach
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item">Study Room</a></li>
                            <li class="breadcrumb-item active">Teofl Test</li>
                            <li class="breadcrumb-item active">Lihat Materi</li>
                            <li class="breadcrumb-item active">Edit Materi</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        @foreach ($tb_materi as $data)
                        <h4 class="card-title mb-0">{{$data->judul_materi}}</h4>
                        @endforeach
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="mt-4 mt-xl-0">
                                    <form action="#" class="form-validate is-alter" name="FormEditMateri" id="FormEditMateri">
                                        {{ csrf_field() }}
                                        @foreach ($tb_materi as $data)
                                        <input type="hidden" class="form-control" name="id_materi_edit" value="{{$data->id_materi}}">
                                        <div class="mb-3">
                                            <label class="form-label">Judul Materi</label>
                                            <input type="text" class="form-control" name="judul_materi" value="{{$data->judul_materi}}">
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Url Materi</label>
                                            <input type="text" class="form-control url" name="video_materi" value="{{$data->video_materi}}">
                                        </div>
                                        <div class="mt-4">
                                            @endforeach
                                            <button type="button" id="updatemateri" name="updatemateri" class="btn btn-md btn-primary">Update</button>
                                            <button type="submit" class="btn btn-info">Tinjau Video</button>
                                            <input type="button" class="btn btn-secondary" value="Kembali" onclick="history.back(-1)" />
                                            <p>Sebelum melakukan update video silahkan pertinjau terlebih dahulu untuk memastikan video sudah benar!</p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-5 mt-lg-4 mt-xl-0">
                                    <div class="row mb-4">
                                        <div id="append_html_tinjau">
                                            <h4>Mengurangi load server klik tombol Tinjau Video untuk menampilkan video</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Form Layout -->

    </div>
</div>

@endsection

@section('script')

<script>
    //$('iframe').hide();
    $(document).ready(function() {
        $('form').submit(function(e) {
            e.preventDefault();
            let getURL = $('.url').val();
            // let newURL = getURL.replace("watch?v=", "embed/");
            // $('iframe').attr("src", newURL).show();
            $("#append_html_tinjau").html(getURL);
        });
    });
</script>

<script>
    $(document).on('click', '#updatemateri', function() {
        console.log('update materi');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormEditMateri').serialize(),
            url: "{{url('study_room/updateMateri')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.message);
                } else {
                    toastr.clear();
                    toastr.success(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

@endsection