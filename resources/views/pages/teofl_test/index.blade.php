@extends('layouts.app')

@section('content')

<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">MT Class</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Study Room</a></li>
                            <li class="breadcrumb-item active">MT Class</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="row">
                    <div class="col-lg-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-xl-12">
                                        <!-- <div class="card"> -->
                                        <!-- <div class="card-body"> -->
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="mt-4 mt-xl-0">

                                                    <div class="card-title-group">
                                                        <div class="card-title">
                                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormTambahPaket">Tambah Paket</button>
                                                            </h6>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <table class="table stripe" id="tb_paket" style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th style="color: blue;">ID Paket</th>
                                                                <th>Nama Paket</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div><!-- .card-preview -->
                                        </div> <!-- nk-block -->
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .card-preview -->

<!-- modal tambah paket -->
<div class="modal fade" id="modalFormTambahPaket">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Paket</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="formTambahPaket" id="formTambahPaket">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="form-label" for="full-name">Nama Paket</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="nama" name="nama" required>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
                <button type="button" id="tambah" namme="tambah" class="btn btn-md btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- modal edit paket -->
<div class="modal fade" id="modalFormEditPaket">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Paket</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="FormEditPaket" id="FormEditPaket">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="id_paket_edit" name="id_paket_edit" required>
                    <div class="form-group">
                        <label class="form-label" for="full-name">Nama Paket</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="nama_paket_edit" name="nama_paket_edit" required>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
                <button type="button" id="update" name="update" class="btn btn-md btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- modal tambah materi -->
<div class="modal fade bs-example-modal-xl" id="modalFormTambahMateri">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Materi</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="mt-4 mt-xl-0">
                            <form action="#" class="form-validate is-alter" name="FormTambahMateri" id="FormTambahMateri">
                                {{ csrf_field() }}
                                <input type="hidden" class="form-control" id="id_paket_materi" name="id_paket_materi" required>
                                <div class="mb-3">
                                    <label class="form-label">Judul Materi</label>
                                    <input type="text" class="form-control" id="judul_materi" name="judul_materi" required>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Url Materi</label>
                                    <input class="form-control url" name="video_materi" required>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-info">Tinjau Video</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="mt-5 mt-lg-4 mt-xl-0">
                            <form>
                                <div class="row mb-4">
                                    <div class="ratio ratio-16x9">
                                        <div id="append_html_tinjau">
                                            <!-- conten -->
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Form Layout -->
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
                <button type="button" id="tambahmateri" namme="tambahmateri" class="btn btn-md btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- modal lihat materi -->
<div class="modal fade bs-example-modal-xl" id="modalFormLihatMateri">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Lihat Materi</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="formTambahPaket" id="formTambahPaket">
                    {{ csrf_field() }}
                    <table class="table stripe" id="tb_materi" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>Pertinjau</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
    $(document).ready(function() {
        var tb_paket = $('#tb_paket').DataTable({
            responsive: {
                details: true
            },
            processing: true,
            serverSide: true,
            searching: true,
            responsive: {
                details: true
            },
            sDom: 'lrtip', // untuk hidden search box di datatable
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('study_room/getDataPaket') }}",
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center'
                },
                {
                    data: 'idpaket',
                    name: 'idpaket',
                    className: 'text-center'
                },
                {
                    data: 'nama',
                    name: 'nama',
                    className: 'text-center'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    className: 'text-center'
                },
            ]
        })
        // };
    });
</script>

<script type="text/javascript">

</script>

<script>
    $(document).on('click', '#tambah', function() {
        console.log('tambah');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#formTambahPaket').serialize(),
            url: "{{url('study_room/tambah')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.success(data.message);
                    $("#modalFormTambahPaket").modal('hide');
                    $('#tb_paket').DataTable().ajax.reload(null, false);
                } else {
                    toastr.success(data.message);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        $(document).on('click', '#edit', function() {
            var id_paket = $(this).attr("data-id_paket");
            var nama_paket = $(this).attr("data-nama_paket");

            $("#id_paket_edit").val(id_paket);
            $("#nama_paket_edit").val(nama_paket);

        });
    });
</script>

<script>
    $(document).on('click', '#update', function() {
        console.log('update');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormEditPaket').serialize(),
            url: "{{url('study_room/update')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.message);
                    $("#modalFormEditPaket").modal('hide');
                    $('#tb_paket').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.success(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $(document).on('click', '#delete', function() {
        console.log('delete');
        var id_paket = $(this).attr("data-id_paket");
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('study_room/delete')}}" + '/' + id_paket,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        if (data.kode == 201) {
                            toastr.clear();
                            toastr.success(data.message);
                            $('#tb_paket').DataTable().ajax.reload(null, false);
                        } else {
                            toastr.clear();
                            toastr.success(data.message);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });
</script>

<script>
    $(document).on('click', '#deletemateri', function() {
        console.log('deletemateri');
        var id_materi = $(this).attr("data-id_materi");
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('study_room/deletemateri')}}" + '/' + id_materi,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        if (data.kode == 201) {
                            toastr.clear();
                            toastr.success(data.message);
                            //$("#modalFormLihatMateri").load("#modalFormLihatMateri");
                            $('#tb_materi').DataTable().ajax.reload(null, false);
                        } else {
                            toastr.clear();
                            toastr.success(data.message);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });
</script>

<script>
    $(document).on('click', '#lihatmateri', function() {
        var id_paket = $(this).attr("data-id_paket");
        var nama_paket = $(this).attr("data-nama_paket");

        $("#id_paket_materi").val(id_paket);
        $("#nama_paket_materi").val(nama_paket);
        console.log('Id Paket:', id_paket);
        console.log('Nama Paket:', nama_paket);

        $('#tb_materi').DataTable().destroy();

        var tb_materi = $('#tb_materi').DataTable({
            responsive: {
                details: true
            },
            processing: true,
            serverSide: true,
            searching: true,
            responsive: {
                details: true
            },
            sDom: 'lrtip', // untuk hidden search box di datatable
            paging: false,
            bInfo: false,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('study_room/getDataMateri') }}" + "/" + id_paket,
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center'
                },
                {
                    data: 'judul',
                    name: 'judul',
                    className: 'text-center'
                },
                {
                    data: 'pertinjau',
                    name: 'pertinjau',
                    className: 'text-center'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    className: 'text-center'
                },
            ]
        })
        // };
    });
</script>

<script>
    $(document).on('click', '#idpakettambahmateri', function() {
        var id_paket = $(this).attr("data-id_paket");

        $("#id_paket_materi").val(id_paket);
        console.log('Id Paket:', id_paket);
    });
</script>

<script>
    $(document).on('click', '#tambahmateri', function() {
        console.log('tambah materi');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormTambahMateri').serialize(),
            url: "{{url('study_room/tambahmateri')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data.kode == 201) {
                    toastr.success(data.message);
                    $("#modalFormTambahMateri").modal('hide');
                    $('#tb_paket').DataTable().ajax.reload(null, false);
                } else {
                    toastr.success(data.message);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $('iframe').hide();
    $(document).ready(function() {
        $('form').submit(function(e) {
            e.preventDefault();
            var getURL = $('.url').val();
            // let newURL = getURL.replace("watch?v=", "embed/");
            $("#append_html_tinjau").html(getURL);
        });
    });
</script>

@endsection