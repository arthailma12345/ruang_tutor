@extends('layouts_frontend.app')

@section('content')
<header>
    <div class="inner-page-header_home inner-header-bg-3"></div>
</header>
<!-- product-section -->
<section class="product-section bg-dark pt-100">
    <h2 style="font-weight: bolder; text-align: center;">MT Exclusive</h2>
            <div class="container">
                <div class="border-bottom pb-100">
                    <div class="product-list-header">
                        <div class="product-list-header-item">
                            <div class="product-list-action">
                            </div>
                        </div>
                    </div>
                    <div class="row" id="append_html">
                        <!-- konten                     -->
                    </div>
                </div>
            </div>
        </section>
        <!-- .end product-section -->

@endsection

@section('script')
<!-- script irhas -->
<script>
    $(document).ready(function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // data: $('#FormEditUser').serialize(),
            url: "{{url('toefl_preparation_peserta/getListDataToeflpreparation')}}",
            type: "GET",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.data[0].id_tb_m_toefl_preparation);

                var html_jawaban = '';
                for (var i = 0; i < data.data.length; i++) {
                    // console.log(data.data[0].id_tb_m_toefl_preparation);
                    html_jawaban +=

                        '<div style="width:50%" class="col-md-6 col-lg-2 pb-30">'+
                        '<div class="product-card product-card-white">'+
                        '<div class="product-card-thumb">'+
                        '<a href="' + "{{url('sub_toefl_preparation_peserta/index')}}" + '/' + data.data[i].id_tb_m_toefl_preparation + '"><img src=' + "{{ asset('public/image/new_icon2.png') }}" + ' alt="product"></a>'+
                        '<a href="' + "{{url('sub_toefl_preparation_peserta/index')}}" + '/' + data.data[i].id_tb_m_toefl_preparation + '" class="product-cart main-btn"><th>Lihat</th></a>'+
                        '</div>'+
                        '<div class="product-card-details">'+
                        '<h3><a href="' + "{{url('sub_toefl_preparation_peserta/index')}}" + '/' + data.data[i].id_tb_m_toefl_preparation + '"><th>' + data.data[i].nama_toefl_preparation + '</th></a></h3>'+
                        '<h6 class="product-price"><a href="' + "{{url('sub_toefl_preparation_peserta/index')}}" + '/' + data.data[i].id_tb_m_toefl_preparation + '"><th>Jumlah Kuis ' + data.data[i].tb_m_sub_toefl_preparation_count + '</th></a></h6>'+
                        '</div>'+
                        '</div>'+
                        '</div>';
                }



                $("#append_html").html(html_jawaban);


            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>
<!-- <script>
    $(document).ready(function() {
    var interval_alert;  
        interval_alert = setInterval(fun, 3000);
        
    
    function fun() {
        var html_panduan = 
        '<h5>Gunakan Komputer untuk hasil maksimal, atau miringkan (horizontal) smarphone kamu</h5>';
        Swal.fire({
            width: '50%',
            title: '<h4>PERINGATAN.!!!</h4>',
            html: true,
            html: html_panduan,
            allowOutsideClick: false,
            allowEscapeKey: false,
            showCancelButton: true,
            showConfirmButton: false,
            confirmButtonText: 'JOIN NOW!',
        }).then(function(result) {
            if (result.value) {
                // window.location.href ("https://www.bukukampunginggris.com/");  
                // window.open('https://www.bukukampunginggris.com/', '_blank');
                console.log('Modal berhasil keluar');
            }
        });
        clearInterval(interval_alert);
    }
});
</script> -->
@endsection