<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Abaz">
    <meta name="keywords" content="HTML,CSS,JavaScript">
    <meta name="author" content="HiBootstrap">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>RUANGAN TUTOR</title>
    <link rel="shortcut icon" href="{{asset('public/assets/images/logo1.jpg')}}">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/bootstrap.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/bootstrap-reboot.min.css')}}" type="text/css" media="all" />
    <!-- animate css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/animate.min.css')}}" type="text/css" media="all" />
    <!-- swiper css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/slick.css')}}" type="text/css" media="all" />
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/meanmenu.min.css')}}" type="text/css" media="all" />
    <!-- magnific popup css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/magnific-popup.min.css')}}" type="text/css" media="all" />
    <!-- boxicons css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/boxicons.min.css')}}" type="text/css" media="all" />
    <!-- flaticon css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/flaticon.css')}}" type="text/css" media="all" />
    <!-- flaticon css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/bookblock.css')}}" type="text/css" media="all" />
    <!-- revolution slider css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/settings.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/layers.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/navigation.css')}}" type="text/css" media="all" />
    <!-- nice select css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/nice-select.css')}}" type="text/css" media="all" />
    <!-- style css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/style.css')}}" type="text/css" media="all" />
    <!-- responsive css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/responsive.css')}}" type="text/css" media="all" />
    <!-- theme dark css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/theme-dark.css')}}" type="text/css" media="all" />
    <!-- quill editor -->
    <link href="{{asset('public/assets/libs/quill/quill.core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/assets/libs/quill/quill.bubble.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/assets/libs/quill/quill.snow.css')}}" rel="stylesheet" type="text/css" />
    <!-- css ku -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/ikf.css')}}" type="text/css" media="all" />
    <style>
        .boxreading {
            inline-size: 100%;
        }
    </style>
</head>

<body>
    <!-- preloader -->
    <div class="preloader bg-dark">
        <div class="preloader-wrapper">
            <div class="preloader-content">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <!-- .end preloader -->
    <!-- navbar -->

    <!-- .end navbar -->
    <!-- product-details-section -->
    <div class="product-details-section">
        <!-- page-grid -->
        <div class="page-grid">

            <!-- split kir -->
            <div class="split left">
                <div class="fixed-top-half">
                    <div class="">
                        <!-- <div class="mobile-nav mobile-nav-black">
                <div class="navbar-option navbar-option-black">
                    <div class="navbar-option-item navbar-option-search dropdown">
                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropdownMenuButton6" data-bs-toggle="dropdown">
                            <i class="flaticon-magnifying-glass"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton6">
                            <form>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="flaticon-magnifying-glass"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Search article">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
                        <div style="background: white;" class="main-nav main-nav-black">
                            <div class="container-fluid">
                                <nav class="navbar navbar-expand-md navbar-light">
                                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent6">
                                        <ul class="navbar-nav mx-auto">
                                            @foreach(Session::get('user_app')['menu'] as $datas => $value)
                                            <li class="nav-item">
                                                <a href="#" class="nav-link dropdown-toggle">{{$datas}}</a>
                                                <ul class="dropdown-menu">
                                                    @php
                                                    for($i = 0; $i < count($value); $i++) { @endphp <li class="nav-item">
                                                        <a href="{{url($value[$i]->m_sub_menu_url_sub_menu)}}" class="nav-link">{{$value[$i]->m_sub_menu_nama_sub_menu}}</a>
                                            </li>
                                            @php
                                            }
                                            @endphp
                                        </ul>
                                        </li>
                                        @endforeach
                                        <li>
                                            <button style="max-width: 88%; margin:10px ;" type="button" class="btn main-btn full-width" data-toggle="modal" data-target="#exampleModal">List Latihan Soal</button>
                                        </li>
                                        <button style='display:none; height: 100%; margin:10px ;' id="button_download_dokumen" name="button_download_dokumen" type="button" class="btn main-btn full-width">
                                            Download File
                                        </button>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="page-header-inner">
                        <div class="page-header-content page-header-justify-center">
                            <div class="product-details-carousel-area">
                                <div class="page-header-content-white">
                                    <!-- tampilan reading -->
                                    <div id="append_latihan_materi"></div>
                                    <div id="append_pembahasan_materi"></div>
                                    <div class="boxreading" id="append_latihan_materi_reading"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end split kiri -->

            <!-- split kanan -->
            <div class="split right">
                <h5 style="color:white ; background-color: #bf8888; font-weight: bolder;" align="center">
                    <input type="hidden" id="sub_toefl_preparation_peserta_id_input_text" name="sub_toefl_preparation_peserta_id_input_text">
                    <div id="tipsToeflPreparationSekarangAppend"></div>
                </h5>
                <h6 style="margin-top: -2%;">
                    <div id="tipsToeflPreparationSekarangDeskripsiAppend"></div>
                </h6>
                <div class="">
                    <div class="container page-body-inner">
                        <div class="product-details-caption">
                            <form action="javascript:;" name="form_latihan_soal_jawaban" id="form_latihan_soal_jawaban" enctype="multipart/form-data">
                                <!-- <ol>
                                    <div id="append_latihan_soal_jawaban"></div>
                                </ol> -->
                                <li style="list-style-type: none;">
                                    <div id="append_latihan_soal_jawaban"></div>
                                </li>
                            </form>
                            <!-- tampilan selesai dan keluar kunci jawaban -->
                            <div style="margin-bottom: 70px;">
                                <button id="button_jawab" name="button_jawab" type="button" class="btn main-btn full-width" style="display: none;">
                                    SELESAI
                                </button>
                                <hr>
                                <div class="container">
                                    <div id="sebelum_sesudah_tips"></div>
                                </div>
                                <div style="color:#000000; font-weight: bold; margin-bottom: 70px;" id="nilai_latihan"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end split kanan -->
        </div>
        <!-- .end page-grid -->
    </div>
    <!-- .end product-details-section -->

    <!-- scroll-top -->
    <div class="scroll-top" id="scrolltop">
        <div class="scroll-top-inner">
            <span><i class="bx bx-up-arrow-alt"></i></span>
        </div>
    </div>
    <!-- .end scroll-top -->
    <!-- modal -->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">List Latihan Soal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="sub_toefl_preparation_peserta_append"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
    <!-- essential js -->
    <script src="{{asset('public/assets_frontend/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/bootstrap.bundle.min.js')}}"></script>
    <!-- magnific popup js -->
    <script src="{{asset('public/assets_frontend/js/jquery.magnific-popup.min.js')}}"></script>
    <!-- counter js -->
    <script src="{{asset('public/assets_frontend/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/jquery.counterup.min.js')}}"></script>
    <!-- slick js -->
    <script src="{{asset('public/assets_frontend/js/slick.min.js')}}"></script>
    <!-- nice select js -->
    <script src="{{asset('public/assets_frontend/js/jquery.nice-select.min.js')}}"></script>
    <!-- bookblock js -->
    <script src="{{asset('public/assets_frontend/js/modernizr.custom.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/jquerypp.custom.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/jquery.bookblock.js')}}"></script>
    <!-- revolution slider js -->
    <script src="{{asset('public/assets_frontend/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/revolution.addon.slicey.min.js')}}"></script>
    <!-- slider revolution extension scripts. only needed for local testing -->
    <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.video.min.js')}}"></script>
    <!-- form ajazchimp js -->
    <script src="{{asset('public/assets_frontend/js/jquery.ajaxchimp.min.js')}}"></script>
    <!-- form validator js  -->
    <script src="{{asset('public/assets_frontend/js/form-validator.min.js')}}"></script>
    <!-- contact form js -->
    <script src="{{asset('public/assets_frontend/js/contact-form-script.js')}}"></script>
    <!-- meanmenu js -->
    <script src="{{asset('public/assets_frontend/js/jquery.meanmenu.min.js')}}"></script>
    <!-- main js -->
    <script src="{{asset('public/assets_frontend/js/script.js')}}"></script>
    <!-- quill editor -->
    <script src="{{asset('public/assets/libs/quill/quill.min.js')}}"></script>
    <!-- alert -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.8/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <!-- btn logout -->
    <script>
        $("#btnLogout").click(function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('logout')}}",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    document.location = "{{ url('/') }}";
                },
                error: function(data) {
                    console.log('Error:', data);
                    document.location = "{{ url('/') }}";
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('sub_toefl_preparation_peserta/getListDataSubToeflpreparation')}}" + '/' + "{{$toefl_preparation_id}}",
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    var sub_toefl_preparation_peserta_append = '';

                    var id_tb_m_sub_toefl_preparation = "";
                    var nama_sub_toefl_preparation = "";
                    var no_urut = "";

                    var tips_sebelumnya_id = "";
                    var tips_sebelumnya_nama = "";
                    var tips_sesudahnya_id = "";
                    var tips_sesudahnya_nama = "";

                    for (var i = 0; i < data.data.length; i++) {

                        id_tb_m_sub_toefl_preparation = data.data[i].id_tb_m_sub_toefl_preparation;
                        nama_sub_toefl_preparation = data.data[i].nama_sub_toefl_preparation;
                        no_urut = data.data[i].no_urut;
                        deskripsi_sub_toefl_preparation = data.data[i].deskripsi_sub_toefl_preparation;

                        if (deskripsi_sub_toefl_preparation == null) {
                            deskripsi_sub_toefl_preparation = '';
                        } else {
                            deskripsi_sub_toefl_preparation = deskripsi_sub_toefl_preparation;
                        }

                        sub_toefl_preparation_peserta_append +=
                            '<button style="margin:6px; font-size:33px;" type="button" class="btn main-btn btn-yellow btn-md sub_toefl_preparation_peserta_id_class"' +
                            'data-sub_toefl_preparation_peserta_id="' + id_tb_m_sub_toefl_preparation + '" ' +
                            'data-nama_sub_toefl_preparation="' + nama_sub_toefl_preparation + '" ' +
                            'data-no_urut="' + no_urut + '" ' +
                            "data-deskripsi_sub_toefl_preparation='" + deskripsi_sub_toefl_preparation + "' " +
                            '>' +
                            nama_sub_toefl_preparation + '</button>'
                    }

                    $("#sub_toefl_preparation_peserta_append").html(sub_toefl_preparation_peserta_append);
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $(document).on('click', '.sub_toefl_preparation_peserta_id_class', function() {
                var sub_toefl_preparation_peserta_id = $(this).attr("data-sub_toefl_preparation_peserta_id");
                var nama_sub_toefl_preparation = $(this).attr("data-nama_sub_toefl_preparation");
                var deskripsi_sub_toefl_preparation = $(this).attr("data-deskripsi_sub_toefl_preparation");

                if (deskripsi_sub_toefl_preparation == 'undefined') {
                    deskripsi_sub_toefl_preparation = '';
                } else {
                    deskripsi_sub_toefl_preparation = deskripsi_sub_toefl_preparation;
                }
                let no_urut = $(this).attr("data-no_urut");
                let no_urut_tambah_satu = parseInt(no_urut) + 1;
                let no_urut_kurang_satu = parseInt(no_urut) - 1;
                console.log(sub_toefl_preparation_peserta_id);
                console.log(nama_sub_toefl_preparation);
                console.log(no_urut);
                console.log(no_urut_tambah_satu);
                console.log(no_urut_kurang_satu);
                $("#sub_toefl_preparation_peserta_id_input_text").val(sub_toefl_preparation_peserta_id);
                $("#tipsToeflPreparationSekarangAppend").html(nama_sub_toefl_preparation);
                $("#tipsToeflPreparationSekarangDeskripsiAppend").html(deskripsi_sub_toefl_preparation);
                $("#nilai_latihan").html('');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('sub_toefl_preparation_peserta/getListDataSubToeflpreparation')}}" + '/' + "{{$toefl_preparation_id}}",
                    type: "GET",
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        var sebelum_sesudah_tips_append = '';

                        var id_tb_m_sub_toefl_preparation_2 = "";
                        var nama_sub_toefl_preparation_2 = "";
                        var no_urut_2 = "";
                        var deskripsi_sub_toefl_preparation_2 = "";

                        sebelum_sesudah_tips_append += '<div class="row">';
                        for (var i = 0; i < data.data.length; i++) {

                            id_tb_m_sub_toefl_preparation_2 = data.data[i].id_tb_m_sub_toefl_preparation;
                            nama_sub_toefl_preparation_2 = data.data[i].nama_sub_toefl_preparation;
                            no_urut_2 = data.data[i].no_urut;
                            deskripsi_sub_toefl_preparation_2 = data.data[i].deskripsi_sub_toefl_preparation;

                            if (deskripsi_sub_toefl_preparation_2 == null) {
                                deskripsi_sub_toefl_preparation_2 = '';
                            } else {
                                deskripsi_sub_toefl_preparation_2 = deskripsi_sub_toefl_preparation_2;
                            }

                            if (no_urut_2 == no_urut_tambah_satu) {

                                if (no_urut == 1) {

                                    sebelum_sesudah_tips_append +=
                                        '<div class="col-sm-6" style="width:50%;">' +
                                        '<button class="buttonkukiri visible_true_false_button_prev_forw_' + no_urut + ' sub_toefl_preparation_peserta_id_class m-2"' +
                                        'style="width:47%" ' +
                                        'data-sub_toefl_preparation_peserta_id="' + sub_toefl_preparation_peserta_id + '" ' +
                                        'data-nama_sub_toefl_preparation="' + nama_sub_toefl_preparation + '" ' +
                                        'data-no_urut="' + no_urut + '" ' +
                                        "data-deskripsi_sub_toefl_preparation='" + deskripsi_sub_toefl_preparation + "' " +
                                        '>' +
                                        '<span>' +
                                        nama_sub_toefl_preparation +
                                        '</span>' +
                                        '</button>' +
                                        '</div>';

                                    sebelum_sesudah_tips_append +=
                                        '<div class="col-sm-6" style="width:50%;">' +
                                        '<button class="buttonkukanan visible_true_false_button_prev_forw_' + no_urut_2 + ' sub_toefl_preparation_peserta_id_class m-2"' +
                                        'style="width:47%" ' +
                                        'data-sub_toefl_preparation_peserta_id="' + id_tb_m_sub_toefl_preparation_2 + '" ' +
                                        'data-nama_sub_toefl_preparation="' + nama_sub_toefl_preparation_2 + '" ' +
                                        'data-no_urut="' + no_urut_2 + '" ' +
                                        "data-deskripsi_sub_toefl_preparation='" + deskripsi_sub_toefl_preparation_2 + "' " +
                                        '>' +
                                        '<span>' +
                                        nama_sub_toefl_preparation_2 +
                                        '</span>' +
                                        '</button>' +
                                        '</div>';
                                } else {
                                    sebelum_sesudah_tips_append +=
                                        '<div class="col-sm-6" style="width:50%;">' +
                                        '<button class="buttonkukanan visible_true_false_button_prev_forw_' + no_urut_2 + ' sub_toefl_preparation_peserta_id_class m-2"' +
                                        'style="width:47%" ' +
                                        'data-sub_toefl_preparation_peserta_id="' + id_tb_m_sub_toefl_preparation_2 + '" ' +
                                        'data-nama_sub_toefl_preparation="' + nama_sub_toefl_preparation_2 + '" ' +
                                        'data-no_urut="' + no_urut_2 + '" ' +
                                        "data-deskripsi_sub_toefl_preparation='" + deskripsi_sub_toefl_preparation_2 + "' " +
                                        '>' +
                                        '<span>' +
                                        nama_sub_toefl_preparation_2 +
                                        '</span>' +
                                        '</button>' +
                                        '</div>';

                                }



                            } else if (no_urut_2 == no_urut_kurang_satu) {

                                sebelum_sesudah_tips_append +=
                                    '<div class="col-sm-6" style="width:50%;">' +
                                    '<button class="buttonkukiri visible_true_false_button_prev_forw_' + no_urut_2 + ' sub_toefl_preparation_peserta_id_class m-2"' +
                                    'style="width:47%" ' +
                                    'data-sub_toefl_preparation_peserta_id="' + id_tb_m_sub_toefl_preparation_2 + '" ' +
                                    'data-nama_sub_toefl_preparation="' + nama_sub_toefl_preparation_2 + '" ' +
                                    'data-no_urut="' + no_urut_2 + '" ' +
                                    "data-deskripsi_sub_toefl_preparation='" + deskripsi_sub_toefl_preparation_2 + "' " +
                                    '>' +
                                    '<span>' +
                                    nama_sub_toefl_preparation_2 +
                                    '</span>' +
                                    '</button>' +
                                    '</div>';

                            } else {

                            }


                        }
                        sebelum_sesudah_tips_append += '</div>';

                        $("#sebelum_sesudah_tips").html(sebelum_sesudah_tips_append);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });


                const alpha = Array.from(Array(26)).map((e, i) => i + 65);
                const alphabet = alpha.map((x) => String.fromCharCode(x));

                // listening
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('sub_toefl_preparation_peserta/getListDataLatihanMateri')}}" + '/' + sub_toefl_preparation_peserta_id,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        console.log(data.array_dokumen_file);

                        var array_dokumen_file = [];
                        for (var i = 0; i < data.array_dokumen_file.length; i++) {
                            array_dokumen_file.push(data.array_dokumen_file[i]);
                        }

                        console.log(array_dokumen_file);

                        var html_latihan_maetri = "";
                        if (data.data.length == 0) {
                            var html_latihan_maetri = "";
                        } else {
                            var html_latihan_maetri =
                                '<h5 style="text-align: center; font-weight: bold;">Video Materi</h5>' +
                                data.data[0].url_vdeo_latihan_materi
                            // '<iframe style="padding:15px; width:100%; height:440px;" allowfullscreen ' +
                            // 'src="' + data.data[0].url_vdeo_latihan_materi + '">' +
                            // '</iframe>';

                            // var html_latihan_maetri = "";
                        }
                        $("#exampleModal").modal('hide');
                        $("#append_pembahasan_materi").html("");
                        $("#append_latihan_materi").html(html_latihan_maetri);
                        $("#button_jawab").show();

                        if (data.visibility_button_dokumen == true) {
                            $('#button_download_dokumen').show();

                            $("#button_download_dokumen").click(function(data1) {
                                // console.log(data);
                                // console.log('coba');
                                // console.log(array_dokumen_file);
                                // $.ajax({
                                //     headers: {
                                //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                //     },
                                //     url: "{{url('sub_toefl_preparation_peserta/download_dokumen')}}",
                                //     type: "POST",
                                //     // dataType: 'json',
                                //     data: {
                                //         array_dokumen_file: array_dokumen_file
                                //     },
                                //     success: function(data) {},
                                //     error: function(data) {}
                                // });
                                for (var k = 0; k < array_dokumen_file.length; k++) {
                                    var alamat = "<?php echo url('sub_toefl_preparation_peserta/download_dokumen') ?>" + '/' + array_dokumen_file[k]['id'];
                                    // window.location.href = alamat;
                                    window.open(
                                        alamat,
                                        '_blank' // <- This is what makes it open in a new window.
                                    );
                                }


                            });
                        } else {
                            $('#button_download_dokumen').hide();
                        }

                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });

                // reading
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('sub_toefl_preparation_peserta/getListDataLatihanMateriReading')}}" + '/' + sub_toefl_preparation_peserta_id,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        var html_LatihanMateriReading = "";
                        if (data.data.length == 0) {
                            var html_LatihanMateriReading = "";
                        } else {
                            var html_LatihanMateriReading =
                                '<h5 style="text-align: center; font-weight: bold;">Soal Text</h5>' +
                                data.data[0].text_reading;
                        }
                        $("#append_latihan_materi_reading").html(html_LatihanMateriReading);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });

                // tampillan soal
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // data: $('#FormEditUser').serialize(),
                    url: "{{url('sub_toefl_preparation_peserta/getListDataLatihanSoal')}}" + '/' + sub_toefl_preparation_peserta_id,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        var html_latihan_soal = '';
                        console.log('data_deskripsi' + data.data);
                        for (var i = 0; i < data.data.length; i++) {

                            var deskripsi_soal = data.data[i].deskripsi;

                            if (deskripsi_soal == null) {
                                deskripsi_soal = '';
                            } else {
                                deskripsi_soal = deskripsi_soal;
                            }

                            html_latihan_soal +=
                                '<div style="color:#000000;">' +
                                '<hr>' +
                                '<li>' + '<div>' + deskripsi_soal + ' ' + data.data[i].soal.replace('style="color: #000000;"') + '</div>' + '</li>' +
                                '<div style="font-weight: bold; color:#000000;" id="append_jawaban_yang_benar_' + data.data[i].id + '" class="append_jawaban_yang_benar_' + data.data[i].id + '"></div>' +
                                '<div class="row" id="html_latihan_jawaban">';

                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: "{{url('sub_toefl_preparation_peserta/getListDataLatihanJawaban')}}" + '/' + data.data[i].id,
                                type: "GET",
                                dataType: 'json',
                                async: false,
                                success: function(data2) {
                                    console.log('getListDataLatihanJawaban');
                                    console.log(data2);
                                    var no_bantuan_jawaban_esai = 0;
                                    for (var j = 0; j < data2.data.length; j++) {

                                        console.log('kategori_soal = ' + data2.data[j].kategori_soal);

                                        if (data2.data[j].kategori_soal == 'PILIHAN_GANDA') {
                                            html_latihan_soal +=
                                                '<div class="col-lg-6">' +
                                                '<div style="margin-left:45px" class="checkout-payment-list">' +
                                                '<div class="input-radio">' +
                                                '<input type="radio" value=" ' + data.data[i].id + '_' + data2.data[j].id + '" id="jawaban_' + i + '_' + (j + 1) + '" name="jawaban_' + data.data[i].id + '">' +
                                                '<label for="jawaban_' + i + '_' + (j + 1) + '"></label>' +
                                                '</div>' +
                                                '<div class="checkout-payment-text">' +
                                                alphabet[0 + j] + '. ' + data2.data[j].jawaban +
                                                '</div>' +
                                                '</div>' +
                                                '</div>';
                                        } else if (data2.data[j].kategori_soal == 'ESAI') {
                                            if (no_bantuan_jawaban_esai == 0) {
                                                html_latihan_soal +=
                                                    '<div class="col-lg-12">' +
                                                    '<div style="margin-left:45px" class="checkout-payment-list">' +

                                                    '<input class="form-control" placeholder="Isikan jawaban kamu disini!" type="text" style="width:100%;" id="jawaban_' + i + '_' + (j + 1) + '" name="jawaban_' + data.data[i].id + '_' + data2.data[j].id + '">' +
                                                    '<label for="jawaban_' + i + '_' + (j + 1) + '"></label>' +

                                                    '</div>' +
                                                    '</div>';
                                            }

                                            no_bantuan_jawaban_esai++;

                                        }


                                    }

                                },
                                error: function(data2) {
                                    console.log('Error:', data2);
                                }
                            });

                            html_latihan_soal +=
                                '</div>' +
                                '</div>';
                        }
                        $("#append_latihan_soal_jawaban").html(html_latihan_soal);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            });
        });
    </script>

    <script>
        $("#button_jawab").click(function() {
            var sub_toefl_preparation_peserta_id_input_text = $("#sub_toefl_preparation_peserta_id_input_text").val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('sub_toefl_preparation_peserta/getListDataLatihanMateriPembahasan')}}" + '/' + sub_toefl_preparation_peserta_id_input_text,
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    var html_latihan_maetri = "";
                    if (data.data.length == 0) {
                        var html_latihan_maetri = "";
                    } else {
                        var html_latihan_maetri_pembahasan =

                            '<h5 style="text-align: center; font-weight: bold;">Video Pembahasan</h5>' +
                            data.data[0].url_vdeo_pembahasan_materi
                        // '<iframe style="padding:15px; width:100%; height:440px;" allowfullscreen ' +
                        // 'src="' + data.data[0].url_vdeo_pembahasan_materi + '">' +
                        // '</iframe>';
                    }
                    // $("#append_latihan_materi").html(html_latihan_maetri);
                    $("#append_pembahasan_materi").html(html_latihan_maetri_pembahasan);
                    // $("#append_latihan_materi_reading").html(html_LatihanMateriReading);
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: $('#form_latihan_soal_jawaban').serialize() + '&sub_toefl_preparation_peserta_id=' + sub_toefl_preparation_peserta_id_input_text,
                url: "{{url('sub_toefl_preparation_peserta/getListSimpanJawaban')}}",
                type: "GET",
                dataType: 'json',
                success: function(datas) {
                    var nilai_latihan_soal = "";
                    console.log(datas);
                    console.log(datas.datas.kode);
                    if (datas.datas.kode == 200) {
                        Object.keys(datas.datas.jawaban_yang_benar_array_text).forEach(function(k) {
                            $("#append_jawaban_yang_benar_" + k).html('jawaban benar = ' + datas.datas.jawaban_yang_benar_array_text[k]);
                        });
                        var nilai_latihan_soal =
                            '<table class="table">' +
                            '<thead>' +
                            '<tr>' +
                            '<th>Total Soal</th>' +
                            '<th>Soal Benar</th>' +
                            '<th>Soal Salah</th>' +
                            '<th>Nilai Kamu</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody>' +
                            '<tr>' +
                            '<td>' + datas.datas.jml_soal_total + '</td>' +
                            '<td>' + datas.datas.jml_jwb_benar + '</td>' +
                            '<td>' + datas.datas.jml_jwb_salah + '</td>' +
                            '<td>' + datas.datas.nilai + '</td>' +
                            '</tr>' +
                            '</tbody>' +
                            '</table>';
                        $("#nilai_latihan").html(nilai_latihan_soal);
                    } else {}
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            var interval_alert;
            interval_alert = setInterval(fun, 3000);


            function fun() {
                var html_panduan =
                    '<h5>Gunakan Komputer untuk hasil maksimal, atau miringkan (horizontal) smarphone kamu</h5>';
                Swal.fire({
                    width: '50%',
                    title: '<h4>PERINGATAN.!!!</h4>',
                    html: true,
                    html: html_panduan,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCancelButton: true,
                    showConfirmButton: true,
                    confirmButtonText: '<button style="background-color: transparent; color: white;" type="button" data-toggle="modal" data-target="#exampleModal">LANJUT</button>',
                }).then(function(result) {
                    if (result.value) {

                    }
                });
                clearInterval(interval_alert);
            }
        });
    </script>
</body>

</html>