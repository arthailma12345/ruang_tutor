@include('layouts.header')

<div class="modal fade" id="modalFormSuratPerjanjian">
    <div class="modal-dialog modal-dialog-top modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Surat Perjanjian Ruang Tutor</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <form action="javascript:;" class="form-validate is-alter formSuratPerjanjian" name="formSuratPerjanjian" id="formSuratPerjanjian" enctype="multipart/form-data">
                <div class="modal-body">

                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="form-control-wrap">
                            <iframe id="iframe_surat_perjanjian" name="iframe_surat_perjanjian" src='{{asset("public/pdf/web/viewer.html?file="). urlencode(url("home_surat_perjanjian/surat_perjanjian"))}}' title="W3Schools Free Online Web Tutorials" style="width: 100%;" height="450px">
                            </iframe>
                        </div>
                    </div>
                    <br>

                    <div class="row" style="width:100%; height:100%">
                        <div class="col-xl-12">
                            <p align="center">
                                <canvas id="sig-canvas" style="border: 1px solid black; width:'100%' height='100%'">
                                </canvas>
                                <br>
                                <button type="submit" id="lihat_tanda_tangan" namme="lihat_tanda_tangan" class="btn btn-md btn-primary mt-2">lihat tanda tangan</button>
                                <button type="button" id="hapus_tanda_tangan" namme="hapus_tanda_tangan" class="btn btn-md btn-primary mt-2">hapus</button>
                            </p>
                        </div>
                    </div>


                    <!-- <div class="form-group">
                        <br>
                        <div class="form-control-wrap float-right">
                            <div class="d-grid gap-2 col-6 mx-auto">
                                <p align="center">
                                    <canvas id="sig-canvas" style="border: 1px solid black; width:'100%' height='100%'">
                                    </canvas>
                                    <br>
                                    <button type="submit" id="lihat_tanda_tangan" namme="lihat_tanda_tangan" class="btn btn-md btn-primary mt-2">lihat tanda tangan</button>
                                    <button type="button" id="hapus_tanda_tangan" namme="hapus_tanda_tangan" class="btn btn-md btn-primary mt-2">hapus</button>
                                </p>


                            </div>
                        </div>
                    </div> -->

                </div>
                <div class="modal-footer bg-light">
                    <!-- <span class="sub-text">Modal Footer Text</span> -->
                    <a href="{{url('home/index')}}" id="lihat_tanda_tangan" namme="lihat_tanda_tangan" class="btn btn-md btn-primary">kembali ke home</a>
                    <!-- <input type="button" id="btnVerify" value="Verify Canvas" onclick="checkvanvs();" /> -->
                    @if($tanda_tangan == NULL || $tanda_tangan == "")
                    @php
                    $disabled_enabled_button_simpan_surat_perjanjian = "disabled";
                    @endphp
                    @else
                    @php
                    $disabled_enabled_button_simpan_surat_perjanjian = "";
                    @endphp
                    @endif

                    <button {{$disabled_enabled_button_simpan_surat_perjanjian}} type="button" id="simpan_surat_perjanjian" namme="simpan_surat_perjanjian" class="btn btn-md btn-primary">simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@include('layouts.script')

<script>
    $(document).ready(function() {
        $('#modalFormSuratPerjanjian').modal('show');
    });
</script>

<!-- <script>
    $(document).on('click', '#lihat_tanda_tangan', function() {
        console.log('lihat_tanda_tangan');
        loadIframe('iframe_surat_perjanjian', 'http://localhost/ruang_tutor/home_surat_perjanjian/surat_perjanjian/1');
    });
</script> -->

<script>
    function loadIframe(iframeName, url) {
        var $iframe = $('#' + iframeName);
        if ($iframe.length) {
            $iframe.attr('src', url);
            return false;
        }
        return true;
    }
</script>

<script>
    (function() {
        window.requestAnimFrame = (function(callback) {
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimaitonFrame ||
                function(callback) {
                    window.setTimeout(callback, 1000 / 60);
                };
        })();

        var canvas = document.getElementById("sig-canvas");
        var ctx = canvas.getContext("2d");
        ctx.strokeStyle = "#222222";
        ctx.lineWidth = 4;

        var drawing = false;
        var mousePos = {
            x: 0,
            y: 0
        };
        var lastPos = mousePos;

        canvas.addEventListener("mousedown", function(e) {
            drawing = true;
            lastPos = getMousePos(canvas, e);
        }, false);

        canvas.addEventListener("mouseup", function(e) {
            drawing = false;
        }, false);

        canvas.addEventListener("mousemove", function(e) {
            mousePos = getMousePos(canvas, e);
        }, false);

        // Add touch event support for mobile
        canvas.addEventListener("touchstart", function(e) {

        }, false);

        canvas.addEventListener("touchmove", function(e) {
            var touch = e.touches[0];
            var me = new MouseEvent("mousemove", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(me);
        }, false);

        canvas.addEventListener("touchstart", function(e) {
            mousePos = getTouchPos(canvas, e);
            var touch = e.touches[0];
            var me = new MouseEvent("mousedown", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            canvas.dispatchEvent(me);
        }, false);

        canvas.addEventListener("touchend", function(e) {
            var me = new MouseEvent("mouseup", {});
            canvas.dispatchEvent(me);
        }, false);

        function getMousePos(canvasDom, mouseEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: mouseEvent.clientX - rect.left,
                y: mouseEvent.clientY - rect.top
            }
        }

        function getTouchPos(canvasDom, touchEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: touchEvent.touches[0].clientX - rect.left,
                y: touchEvent.touches[0].clientY - rect.top
            }
        }

        function renderCanvas() {
            if (drawing) {
                ctx.moveTo(lastPos.x, lastPos.y);
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                lastPos = mousePos;
            }
        }

        // Prevent scrolling when touching the canvas
        document.body.addEventListener("touchstart", function(e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchend", function(e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchmove", function(e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        }, false);

        (function drawLoop() {
            requestAnimFrame(drawLoop);
            renderCanvas();
        })();

        function clearCanvas() {
            canvas.width = canvas.width;
        }



        // Set up the UI
        var sigText = document.getElementById("sig-dataUrl");
        var sigImage = document.getElementById("sig-image");
        var clearBtn = document.getElementById("sig-clearBtn");
        var submitBtn = document.getElementById("sig-submitBtn");
        // clearBtn.addEventListener("click", function(e) {
        //     clearCanvas();
        //     sigText.innerHTML = "Data URL for your signature will go here!";
        //     sigImage.setAttribute("src", "");
        // }, false);
        // submitBtn.addEventListener("click", function(e) {
        //     var dataUrl = canvas.toDataURL();
        //     sigText.innerHTML = dataUrl;
        //     sigImage.setAttribute("src", dataUrl);
        // }, false);

        function isCanvasEmpty(canvas) {
            const blank = document.createElement('canvas');

            blank.width = canvas.width;
            blank.height = canvas.height;

            return canvas.toDataURL() === blank.toDataURL();
        }

        function checkvanvs() {
            var cnv = document.getElementById('sig-canvas');
            if (isCanvasEmpty(cnv))
                // alert('Empty!');
                return 'canvas tidak terisi'; // jika canvas terisi jadi 0
            else
                // alert('Not Empty!!');
                return 'canvas terisi'; // jika canvas tidak terisi jadi 1
        };

        $('.formSuratPerjanjian').submit(function(event) {
            var formData = new FormData(this);
            console.log('lihat_tanda_tangan');

            console.log(checkvanvs());
            if (checkvanvs() == 'canvas terisi') {
                var dataUrl = canvas.toDataURL();
            } else {
                var dataUrl = "";
            }


            formData.append('tanda_tangan', dataUrl);
            // sigText.innerHTML = dataUrl;
            // sigImage.setAttribute("src", dataUrl);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // contentType: 'multipart/form-data',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                url: "{{url('home_surat_perjanjian/update_tanda_tangan')}}",
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    console.log(data.kode);

                    if (data.kode == 201) {
                        toastr.clear();
                        toastr.success(data.success);
                        // loadIframe('iframe_surat_perjanjian', 'http://localhost/ruang_tutor/home_surat_perjanjian/surat_perjanjian');
                        loadIframe('iframe_surat_perjanjian', '{{asset("public/pdf/web/viewer.html?file=" . urlencode(url("home_surat_perjanjian/surat_perjanjian")))}}');
                        clearCanvas();
                        if (data.tanda_tangan == null || data.tanda_tangan == "") {

                        } else {
                            $("#simpan_surat_perjanjian").removeAttr("disabled");
                        }
                        // document.location = "{{ url('/home/index') }}";
                        // $("#modalFormTambahIssues").modal('hide');
                        // $('#tb_issues').DataTable().ajax.reload(null, false);
                        // $('#tb_issues_unit_kerja').DataTable().ajax.reload(null, false);
                    } else {
                        toastr.clear();
                        toastr.error(data.success);
                    }

                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#modalPenghargaan').modal('show');
                }
            });

        });

        $(document).on('click', '#hapus_tanda_tangan', function() {
            // console.log('simpan');
            clearCanvas();
        });

    })();
</script>

<script>
    $(document).on('click', '#simpan_surat_perjanjian', function() {
        console.log('simpan');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#formTambahMenu').serialize(),
            url: "{{url('home_surat_perjanjian/simpan_surat_perjanjian')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    // document.location = "{{ url('/home/index') }}";
                    // $("#modalformTambahMenu").modal('hide');
                    // $('#tb_menu').DataTable().ajax.reload(null, false);
                    document.location = "{{ url('/home/index_peserta') }}";
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>