@extends('layouts.app')

@section('content')


<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0">User Settings</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Profil</a></li>
                                <li class="breadcrumb-item active">User Settings</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row mb-4">
                <div class="col-xl-8">
                    <div class="card mb-0">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-bs-toggle="tab" href="#about" role="tab">
                                    <i class="bx bx-info-circle font-size-20"></i>
                                    <span class="d-none d-sm-block">Informasi Akun</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#tasks" role="tab">
                                    <i class="bx bx-mail-send font-size-20"></i>
                                    <span class="d-none d-sm-block">Ganti Email</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#messages" role="tab">
                                    <i class="bx bx-key font-size-20"></i>
                                    <span class="d-none d-sm-block">Ganti Password</span>
                                </a>
                            </li>
                        </ul>
                        <!-- Tab content -->
                        <div class="tab-content p-4">
                            <div class="tab-pane active" id="about" role="tabpanel">
                                <div>
                                    <div>
                                        <div class="table-responsive">
                                        <h5 class="font-size-16 mb-4">Informasi 1</h5>
                                            <table id="info1" class="table table-nowrap table-hover mb-0">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Nama</th>
                                                        <th scope="col">Username</th>
                                                        <th scope="col">Email</th>
                                                        <th scope="col">Role</th>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($tb_user as $info1)
                                                    <tr>
                                                        <th scope="row">{{$get_session_nama}}</th>
                                                        <td class="text-dark" scope="row">{{$get_session_username}}</td>
                                                        <td class="text-dark" scope="row">{{$info1->email}}</td>
                                                        <td class="text-dark" scope="row">{{$info1->nama_role}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <br><hr><br>
                                            <h5 class="font-size-16 mb-4">Informasi 2</h5>
                                            <table class="table table-nowrap table-hover mb-0">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Craete Akun</th>
                                                        <th scope="col">Update Akun</th>
                                                        <th scope="col">Akses</th>
                                                        <th scope="col">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($tb_user as $info2)
                                                    <tr>
                                                        <td class="text-dark" scope="row">{{$info2->created_at}}</td>
                                                        <td class="text-dark" scope="row">{{$info2->updated_at}}</td>
                                                        <td class="text-dark" scope="row">{{$get_session_flag}}</td>
                                                        <td>
                                                            <span class="badge badge-soft-primary font-size-12">Aktif</span>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tasks" role="tabpanel">
                                <div>
                                    <form action="#" class="row gx-3 gy-2 align-items-center" name="FormUpdateEmail" id="FormUpdateEmail">
                                    {{ csrf_field() }}
                                        <div class="hstack gap-3">
                                            <input id="inputeemail" name="inputeemail" class="form-control me-auto" type="email" required placeholder="Add your email active">
                                            <button id="btnupdateemail" name="btnupdateemail" type="button" class="btn btn-secondary">Update</button>
                                            <div class="vr"></div>
                                            <button type="reset" class="btn btn-outline-danger">Reset</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane" id="messages" role="tabpanel">
                                <div>
                                <form action="#" name="FormUpdatePassword" id="FormUpdatePassword">
                                    {{ csrf_field() }}
                                        <div class="row mb-4">
                                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Password Lama</label>
                                            <div class="col-sm-9">
                                                <input id="inputpasslama" name="inputpasslama" type="text" class="form-control" placeholder="Enter Password">
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <label for="horizontal-email-input" class="col-sm-3 col-form-label">Password Baru</label>
                                            <div class="col-sm-9">
                                                <input id="inputpassbaru1" name="inputpassbaru1" type="text" class="form-control" placeholder="Enter Password">
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <label for="horizontal-password-input" class="col-sm-3 col-form-label">Konformasi Password</label>
                                            <div class="col-sm-9">
                                                <input id="inputpassbaru2" name="inputpassbaru2" type="text" class="form-control" placeholder="Enter Password">
                                            </div>
                                        </div>

                                        <div class="row justify-content-end">
                                            <div class="col-sm-9">
                                                <div>
                                                    <button id="btnupdatepassword" name="btnupdatepassword" type="button" class="btn btn-primary w-md">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="card overflow-hidden">
                        <div class="profile-user"></div>
                        <div class="card-body">
                            <div class="profile-content text-center">
                                <div class="profile-user-img">
                                    <img src="{{asset('public/assets/images/users/avatar-1.jpg')}}" alt="" class="avatar-lg rounded-circle img-thumbnail">
                                </div>
                                <h5 class="mt-3 mb-1">{{$get_session_username}}</h5>
                                <p class="text-muted">Online</p>
                                <p class="text-muted mb-1">Jangan bagikan password, jika akun tidak bisa dibuka segera resert password</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
    </div>
    <!-- end main content-->
    @endsection
    @section('script')

    <script>
        $(document).ready(function() {
            $("#choices-single-default").select2({
                theme: "bootstrap-5",
                placeholder: "Pilih Apalah"
            });
        });
    </script>

<script>
    $(document).on('click', '#btnupdateemail', function() {
        console.log('update email aktif');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormUpdateEmail').serialize(),
            url: "{{url('/update_email_profil')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.message);
                    location.reload();
                } else {
                    toastr.clear();
                    toastr.success(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $(document).on('click', '#btnupdatepassword', function() {
        console.log('update ps aktif');
        var pass = $('#inputpassbaru1').val();
		var pass2 = $('#inputpassbaru2').val();						
			if (pass != pass2) {				
				alert("Konfirmasi Password Tidak Sama!");
                return false;
			}
                $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormUpdatePassword').serialize(),
            url: "{{url('/update_pass_profil')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.message);
                    window.location.href = "{{ url('/logout_lwt')}}";
                }else if (data.kode == 202) {
                    toastr.clear();
                    toastr.error(data.message);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

    @endsection