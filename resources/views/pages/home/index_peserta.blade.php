@section('style')
<style>
    #swal2-html-container {
        overflow: hidden;
    }
</style>
<!-- modal -->
<!-- <style>
                @import 'https://fonts.googleapis.com/css?family=Prompt:400,700';

                .modal {
                    /* This way it could be display flex or grid or whatever also. */
                    display: block;

                    /* Probably need media queries here */
                    width: 600px;
                    max-width: 100%;

                    height: 400px;
                    max-height: 100%;

                    position: fixed;

                    z-index: 100;

                    left: 50%;
                    top: 50%;

                    /* Use this for centering if unknown width/height */
                    transform: translate(-50%, -50%);

                    /* If known, negative margins are probably better (less chance of blurry text). */
                    /* margin: -200px 0 0 -200px; */

                    background: white;
                    box-shadow: 0 0 60px 10px rgba(0, 0, 0, 0.9);
                }

                .closed {
                    display: none;
                }

                .modal-overlay {
                    position: fixed;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    z-index: 50;

                    background: rgba(0, 0, 0, 0.6);
                }

                .modal-guts {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    overflow: auto;
                    padding: 20px 50px 20px 20px;
                }

                /* body {
                    background-color: #556;
                    background-image: linear-gradient(30deg, #445 12%, transparent 12.5%, transparent 87%, #445 87.5%, #445),
                        linear-gradient(150deg, #445 12%, transparent 12.5%, transparent 87%, #445 87.5%, #445),
                        linear-gradient(30deg, #445 12%, transparent 12.5%, transparent 87%, #445 87.5%, #445),
                        linear-gradient(150deg, #445 12%, transparent 12.5%, transparent 87%, #445 87.5%, #445),
                        linear-gradient(60deg, #99a 25%, transparent 25.5%, transparent 75%, #99a 75%, #99a),
                        linear-gradient(60deg, #99a 25%, transparent 25.5%, transparent 75%, #99a 75%, #99a);
                    background-size: 80px 140px;
                    background-position: 0 0, 0 0, 40px 70px, 40px 70px, 0 0, 40px 70px;
                    border: radius 2%;
                    font-family: 'Prompt', sans-serif;
                } */

                ul {
                    margin: 10px 0 10px 30px;
                }

                li,
                p {
                    margin: 0 0 10px 0;
                }

                h1 {
                    margin: 0 0 20px 0;
                }

                .modal .close-button {
                    position: absolute;

                    /* don't need to go crazy with z-index here, just sits over .modal-guts */
                    z-index: 1;

                    top: 5px;

                    /* needs to look OK with or without scrollbar */
                    right: 5px;

                    border: 0;
                    background: red;
                    color: white;
                    padding: 5px 5px;
                    font-size: 1.0rem;
                }

                .open-button {
                    border: 0;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                    background: lightgreen;
                    color: white;
                    padding: 10px 20px;
                    border-radius: 10px;
                    font-size: 21px;
                }
            </style> -->
@endsection

@extends('layouts_frontend.app')

@section('content')
<header>
    <div class="inner-page-header_home inner-header-bg-1"></div>
</header>

<!-- <div class="modal-overlay" id="modal-overlay"></div>

            <div class="modal" id="modal">
                <button class="close-button" id="close-button">X</button>
                <div class="modal-guts">
                <h5>PEMBELIAN PERTAMA GRATIS 1 PAKET</h5>
                <hr>
                    <div class="row">
                        <div class="col-md-6"><a href=""><img src="{{ asset('public/image/buku_inggris.jpg') }}" alt=""></a></div>
                        <div class="col-md-6">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae expedita corrupti laudantium aperiam, doloremque explicabo ipsum earum dicta saepe delectus totam vitae ipsam doloribus et obcaecati facilis eius assumenda, cumque.</p>
                            <a class="btn btn-info" href="">Join Me</a>
                    </div>
                </div>
            </div>
            </div> -->

<section class="news-section bg-dark pt-80">
    <div class="container">
        <div class="border-bottom pb-100">
            <h2 style="font-weight: bolder; text-align: center;">MT Class</h2>
            <div class="product-list-header">
                <div class="product-list-header-item">
                    <div class="product-list-result product-list-result-white">
                    </div>
                </div>
                <div class="product-list-header-item">
                    <div class="product-list-action">
                        <div class="product-list-search product-list-search-white">
                            <form>
                                <div class="form-group">
                                    <input id="search_materi_tt" name="search_materi_tt" type="text" placeholder="Search your product" class="form-control form-control-white">
                                    <button><i class="flaticon-magnifying-glass"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="news-category">

                        <div class="row" id="append_htmll">
                            <!-- conten -->
                        </div>

                    </div>
                </div>
            </div>
            <hr>
            <h2 style="font-weight: bolder; text-align: center;">MT Exclusive</h2>
            <div class="product-list-header">
                <div class="product-list-header-item">
                    <div class="product-list-result product-list-result-white">
                        <!-- <p>Showing <span>1-12</span> out of <span>32</span></p> -->
                    </div>
                </div>
                <div class="product-list-header-item">
                    <div class="product-list-action">
                        <div class="product-list-search product-list-search-white">
                            <form>
                                <div class="form-group">
                                    <input id="search_materi_tp" name="search_materi_tp" type="text" placeholder="Search your product" class="form-control form-control-white">
                                    <button><i class="flaticon-magnifying-glass"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="news-category">

                        <div class="row" id="append_html">
                            <!-- conten -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
@endsection

@section('script')
<!-- script irhas -->

<!-- TT -->
<script>
    $(document).ready(function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('home/getMateriIndexPesertaTT') }}",
            type: "GET",
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                // console.log("Log Cek Data",data.data.tb_materi[0].id_paket);
                console.log(data);
                var view_produk = '';
                for (var i = 0; i < data.data.data_uncal.length; i++) {
                    // console.log(data.data[0].id_tb_m_toefl_preparation);
                    view_produk +=
                        '<div style="width:50%" class="col-md-6 col-lg-3 pb-30">' +
                        '<div class="news-card news-card-dark news-card-video">' +
                        '<div class="news-card-thumb">' +
                        data.data.data_uncal[i].dataurl +
                        // '<iframe src=' + data.data.data_uncal[i].dataurl + '></iframe>' +
                        // '<div class="news-video-icon">' +
                        // '<a href="' + data.data.data_uncal[i].convertedURLEmbed +
                        // '" id="video-popup_' + i + '"><i class="flaticon-play-button"></i></a>' +
                        // '<a href="' + data.data.data_uncal[i].convertedURLEmbed + '" id="video-popup_' + i + '"></a>' +
                        // '<video src="' + data.data.data_uncal[i].convertedURLEmbed + '" type="video/mp4" controls >< /video >'+
                        '</div>' +
                        '</div>' +
                        '<div class="news-card-content">' +
                        '<div class="news-content-flex">' +
                        '<h4>' + data.data.data_uncal[i].nama_paket + '</h4>' +
                        '<h6>' + data.data.data_uncal[i].judul_materi + '</h6>' +
                        '</div>' +
                        '</div>' +
                        // '</div>' +
                        '</div>';
                }

                $("#append_htmll").html(view_produk);

                for (var j = 0; j < data.data.data_uncal.length; j++) {
                    $("#video-popup_" + j).magnificPopup({
                        disableOn: 0,
                        type: 'iframe',
                        mainClass: 'mfp-fade',
                        removalDelay: 160,
                        preloader: false,
                        fixedContentPos: false
                    });
                };

            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<!-- pencarian -->
<script>
    $(document).ready(function() {
        $('#search_materi_tt').on('keyup', function() {

            // console.log(this.val);
            var search_materi_tt = $("#search_materi_tt").val();
            // console.log(search_materi_tt);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // data: $('#FormEditUser').serialize(),
                url: "{{ url('home/getMateriIndexPesertaTT') }}" + '?search_materi_tt=' +
                    search_materi_tt,
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    // console.log("Log Cek Data",data.data[0].id_paket);
                    console.log(data.data.tb_materi);
                    var view_produk = '';
                    for (var i = 0; i < data.data.data_uncal.length; i++) {
                        // console.log(data.data[0].id_tb_m_toefl_preparation);
                        view_produk +=
                            '<div style="width:50%" class="col-md-6 col-lg-3 pb-30">' +
                            '<div class="news-card news-card-dark news-card-video">' +
                            '<div class="news-card-thumb">' +
                            '<iframe src=' + data.data.data_uncal[i].dataurl +
                            '></iframe>' +
                            '<div class="news-video-icon">' +
                            '<a href="' + data.data.data_uncal[i].convertedURLEmbed +
                            '" id="video-popup_' + i +
                            '"><i class="flaticon-play-button"></i></a>' +
                            '</div>' +
                            '</div>' +
                            '<div class="news-card-content">' +
                            '<div class="news-content-flex">' +
                            '<h5>' + data.data.data_uncal[i].judul_materi + '</h5>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    }

                    $("#append_htmll").html(view_produk);

                    for (var j = 0; j < data.data.data_uncal.length; j++) {
                        $("#video-popup_" + j).magnificPopup({
                            disableOn: 0,
                            type: 'iframe',
                            mainClass: 'mfp-fade',
                            removalDelay: 160,
                            preloader: false,
                            fixedContentPos: false
                        });
                    };

                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });

        });

    });
</script>

<!-- TP -->
<script>
    $(document).ready(function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('home/getMateriIndexPesertaTP') }}",
            type: "GET",
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                // console.log("Log Cek Data",data.data.tb_materi[0].id_paket);
                var view_produk = '';
                for (var i = 0; i < data.data.tb_materi_tp.length; i++) {
                    // console.log(data.data[0].id_tb_m_toefl_preparation);
                    view_produk +=
                        '<div style="width:50%" class="col-md-6 col-lg-2 pb-30">' +
                        '<div class="product-card product-card-white">' +
                        '<div class="product-card-thumb">' +
                        '<a href="' + "{{ url('sub_toefl_preparation_peserta/index/') }}" + '/' +
                        data.data.tb_materi_tp[i].id + '"><img src=' +
                        "{{ asset('public/image/new_icon2.png') }}" + ' alt="product"></a>' +
                        '<a href="' + "{{ url('sub_toefl_preparation_peserta/index/') }}" + '/' +
                        data.data.tb_materi_tp[i].id +
                        '" class="product-cart main-btn"><th>Lihat</th></a>' +
                        '</div>' +
                        '<div class="product-card-details">' +
                        '<h3><a href="' + "{{ url('sub_toefl_preparation_peserta/index/') }}" +
                        '/' + data.data.tb_materi_tp[i].id + '"><th>' + data.data.tb_materi_tp[i]
                        .nama_toefl_preparation + '</th></a></h3>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }



                $("#append_html").html(view_produk);


            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<!-- pencarian -->
<script>
    $(document).ready(function() {
        $('#search_materi_tp').on('keyup', function() {

            // console.log(this.val);
            var search_materi_tp = $("#search_materi_tp").val();
            // console.log(search_materi_tt);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                // data: $('#FormEditUser').serialize(),
                url: "{{ url('home/getMateriIndexPesertaTP') }}" + '?search_materi_tp=' +
                    search_materi_tp,
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    // console.log("Log Cek Data",data.data[0].id_paket);
                    console.log(data.data.tb_materi_tp);
                    var view_produktp = '';
                    for (var i = 0; i < data.data.tb_materi_tp.length; i++) {
                        // console.log(data.data[0].id_tb_m_toefl_preparation);
                        view_produktp +=
                            '<div style="width:50%" class="col-md-6 col-lg-2 pb-30">' +
                            '<div class="product-card product-card-white">' +
                            '<div class="product-card-thumb">' +
                            '<a href="' +
                            "{{ url('sub_toefl_preparation_peserta/index/') }}" + '/' + data
                            .data.tb_materi_tp[i].id + '"><img src=' +
                            "{{ asset('public/image/new_icon2.png') }}" +
                            ' alt="product"></a>' +
                            '<a href="' +
                            "{{ url('sub_toefl_preparation_peserta/index/') }}" + '/' +
                            data.data.tb_materi_tp[i].id +
                            '" class="product-cart main-btn"><th>Lihat</th></a>' +
                            '</div>' +
                            '<div class="product-card-details">' +
                            '<h3><a href="' +
                            "{{ url('sub_toefl_preparation_peserta/index/') }}" + '/' +
                            data.data.tb_materi_tp[i].id + '"><th>' + data.data
                            .tb_materi_tp[i].nama_toefl_preparation + '</th></a></h3>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    }



                    $("#append_html").html(view_produktp);


                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#modalPenghargaan').modal('show');
                }
            });

        });

    });
</script>

<!-- <script>
        window.addEventListener("load", function() {
            setTimeout(
                function open(event) {
                    var modal = document.querySelector("#modal");
                    var modalOverlay = document.querySelector("#modal-overlay");
                    var closeButton = document.querySelector("#close-button");

                    closeButton.addEventListener("click", function() {
                        modal.classList.toggle("closed");
                        modalOverlay.classList.toggle("closed");
                    });
                },
                4000
            )
        });
    </script> -->
<script>
    $(document).ready(function() {
        var interval_alert;
        interval_alert = setInterval(fun, 3000);


        function fun() {
            var html_panduan =
                '<section>' +
                '<div class="row">' +
                '<div class="col-md-6">' +
                '<img style="border-radius:5%;" src="' + '{{ asset('
            public / image / buku_inggris.jpg ') }}' +
                '" alt="Italian Trulli">' +
                '</div>' +
                '<div class="col-md-6">' +
                '<p>Ayok..!!! buruan serbu diskon 75% dari Meet Tutor bagi yang join sekarang, Buku berkualitas,Berkelas dan tidak lupa sudah SNI</p>' +
                '</div>' +
                '</div>' +
                '</section>';
            Swal.fire({
                width: '50%',
                title: '<h4>PROMO TAHUN BARU,SERBUU.!!!</h4>',
                html: true,
                html: html_panduan,
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCancelButton: true,
                showConfirmButton: true,
                confirmButtonText: 'JOIN NOW!',
            }).then(function(result) {
                if (result.value) {
                    // window.location.href ("https://www.bukukampunginggris.com/");
                    window.open('https://www.bukukampunginggris.com/', '_blank');
                    console.log('Modal berhasil keluar');
                }
            });
            clearInterval(interval_alert);
        }
    });
</script>
@endsection