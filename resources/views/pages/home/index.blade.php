@extends('layouts.app')

@section('content')


<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Home</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="card">
                    <div class="card-body">
                        <marquee><h2>SELAMAT DATANG DI APLIKASI MEET TUTOR</h2></marquee>
                    </div>
                    </div>
        <div class="row">
            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="avatar">
                            <span class="avatar-title bg-soft-primary rounded">
                                <i class="mdi mdi-account-multiple text-primary font-size-24"></i>
                            </span>
                        </div>
                        <p class="text-muted mt-4 mb-0">Peserta</p>
                        <h4 class="mt-1 mb-0">{{$total_peserta}}</h4>
                    </div>
                </div>
            </div>

            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="avatar">
                            <span class="avatar-title bg-soft-primary rounded">
                                <i class="mdi mdi-account-multiple-check text-primary font-size-24"></i>
                            </span>
                        </div>
                        <p class="text-muted mt-4 mb-0">Peserta Aktif</p>
                        <h4 class="mt-1 mb-0">{{$count_peserta_aktif}}</h4>
                    </div>
                </div>
            </div>

            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="avatar">
                            <span class="avatar-title bg-soft-primary rounded">
                                <i class="mdi mdi-account-multiple-remove text-primary font-size-24"></i>
                            </span>
                        </div>
                        <p class="text-muted mt-4 mb-0">Peserta Non AKtif</p>
                        <h4 class="mt-1 mb-0">{{$count_peserta_non_aktif}}</h4>
                    </div>
                </div>
            </div>

            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="avatar">
                            <span class="avatar-title bg-soft-primary rounded">
                                <i class="mdi mdi-book-open-page-variant text-primary font-size-24"></i>
                            </span>
                        </div>
                        <p class="text-muted mt-4 mb-0">Paket MT Class</p>
                        <h4 class="mt-1 mb-0">{{$total_paket_tt}}</h4>
                    </div>
                </div>
            </div>

            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="avatar">
                            <span class="avatar-title bg-soft-primary rounded">
                                <i class="mdi mdi-book-open-page-variant text-primary font-size-24"></i>
                            </span>
                        </div>
                        <p class="text-muted mt-4 mb-0">Paket MT Exclusive</p>
                        <h4 class="mt-1 mb-0">{{$total_paket_tp}}</h4>
                    </div>
                </div>
            </div>
        </div>


    </div> <!-- container-fluid -->
</div>


@endsection

@section('script')

<script>
    $(document).ready(function() {
        $("#choices-single-default").select2({
            theme: "bootstrap-5",
            placeholder: "Pilih Apalah"
        });
    });
</script>

@endsection