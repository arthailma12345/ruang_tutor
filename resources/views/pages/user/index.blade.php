@extends('layouts.app')

@section('content')

<div class="page-content" style="font-size: 14pt">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">PESERTA</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item">Pengaturan</li>
                            <li class="breadcrumb-item active">Peserta</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="row">
                    <div class="col-lg-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-xl-12">
                                        <!-- <div class="card"> -->
                                        <!-- <div class="card-body"> -->
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="mt-4 mt-xl-0">

                                                    <div class="card-title-group">
                                                        <div class="card-title">
                                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalFormTambahUser">Tambah Peserta</button>
                                                            <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalImportUser">Import Peserta</button>
                                                            </h6>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <table class="table stripe" id="tb_user" style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Qr Code</th>
                                                                <th>Nama</th>
                                                                <th>Email</th>
                                                                <th>Username</th>
                                                                <th>Tanda Tangan</th>
                                                                <th>Role</th>
                                                                <th>Tanggal Start</th>
                                                                <th>Tanggal Kadaluwarsa</th>
                                                                <th>Tanggal Created At</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                            <tr style="font-size: 14pt">
                                                                <th>No Search</th>
                                                                <th>QR Code Search</th>
                                                                <th>Nama Search</th>
                                                                <th>Email Search</th>
                                                                <th>Username Search</th>
                                                                <th>Tanda Tangan Search</th>
                                                                <th>Role Search</th>
                                                                <th>

                                                                    <input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Awal" id="Tanggal_Start_Search_1" name="Tanggal_Start_Search_1">
                                                                    s/d
                                                                    <input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Akhir" id="Tanggal_Start_Search_2" name="Tanggal_Start_Search_2">

                                                                </th>
                                                                <th>

                                                                    <input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Awal" id="Tanggal_Kadaluwarsa_Search_1" name="Tanggal_Kadaluwarsa_Search_1">
                                                                    s/d
                                                                    <input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Akhir" id="Tanggal_Kadaluwarsa_Search_2" name="Tanggal_Kadaluwarsa_Search_2">

                                                                </th>
                                                                <th>

                                                                    <input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Awal" id="Tanggal_created_at_1" name="Tanggal_created_at_1">
                                                                    s/d
                                                                    <input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Akhir" id="Tanggal_created_at_2" name="Tanggal_created_at_2">

                                                                </th>
                                                                <th>Aksi Search</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div><!-- .card-preview -->
                                        </div> <!-- nk-block -->
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .card-preview -->


<div class="modal fade" id="modalFormTambahUser">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Akun</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="formTambahUser" id="formTambahUser">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="form-label" for="full-name">Nama</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="nama" name="nama" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label mt-2" for="full-name">Email</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="email" name="email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label mt-2" for="full-name">Username</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="username" name="username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label mt-2" for="full-name">Password</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="password" name="password" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label mt-2">Role</label>
                        <div class="form-control-wrap">
                            <select name="role" id="role" style="width:100%;">
                                <option value="">Pilih User</option>
                                <!-- @foreach($m_role as $data)
                                <option value=" {{$data->id}}">{{$data->nama_role}}</option>
                                @endforeach -->
                                <option value="R003">peserta</option>
                                <option value="R001">admin super</option>
                                <option value="R012">admin programmer</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group kadaluwarsa_display" style="display: none">
                        <label class="form-label mt-2" for="full-name">Kadaluwarsa</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="kadaluwarsa" name="kadaluwarsa" required>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
                <button type="button" id="tambah" namme="tambah" class="btn btn-md btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalImportUser">
    <div class="modal-dialog modal-dialog-top modal-md" style="min-width: 35%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Import Peserta</h5>
                <!-- <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <em class="icon ni ni-cross"></em>
                    </a> -->
            </div>
            <div class="modal-body">
                <div class="form-group">

                    <div class="mb-3">
                        <a href="{{ url('user/download_template_excel') }}" target="_blank" style="width: 100%" id="btnDownloadTemplateUser" name="btnDownloadTemplateUser" type="button" class="btn btn-primary waves-effect waves-light">Download Template Import Peserta</a>
                    </div>
                    <div class="mb-3">
                        <!-- <input class="form-control" type="file" id="formFile"> -->
                    </div>
                    <form action="javascript:;" class="modal-content formimportDataUser" name="formimportDataUser" id="formimportDataUser" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="input-group mb-3">
                            <input class="form-control" type="file" id="file_excel_import_user" name="file_excel_import_user">
                            <button id="btnImportUser" name="btnImportUser" type="submit" class="btn btn-primary waves-effect waves-light">Import User</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalFormEditUser">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Akun</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="FormEditUser" id="FormEditUser">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="users_id_edit" name="users_id_edit" required>
                    <div class="form-group">
                        <label class="form-label" for="full-name">Nama</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="nama_edit" name="nama_edit" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="full-name">Email</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="email_edit" name="email_edit" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="full-name">Username</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="username_edit" name="username_edit" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="full-name">Password</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="password_edit" name="password_edit" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Role</label>
                        <div class="form-control-wrap">
                            <select style="width:100% ;" id="role_edit" name="role_edit">
                                <!-- @foreach($m_role as $data)
                                <option value="{{$data->id}}">{{$data->nama_role}}</option>
                                @endforeach -->
                                <option value="R003">peserta</option>
                                <option value="R001">admin super</option>
                                <option value="R012">admin programmer</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
                <button type="button" id="update" namme="update" class="btn btn-md btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalFormEditKadaluwarsa">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Akun</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="FormEditKadaluwarsa" id="FormEditKadaluwarsa">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="users_id_edit_tanggal_kadaluwarsa" name="users_id_edit_tanggal_kadaluwarsa" required>
                    <div class="form-group">
                        <label class="form-label" for="full-name">Tanggal start</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="tanggal_start_edit" name="tanggal_start_edit">
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="form-label" for="full-name">Tanggal Kadaluwarsa</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="tanggal_kadaluwarsa_edit" name="tanggal_kadaluwarsa_edit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
                <button type="button" id="update_tanggal_kadaluwarsa" namme="update_tanggal_kadaluwarsa" class="btn btn-md btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    $(document).ready(function() {

        $.fn.dataTable.ext.errMode = function(settings, helpPage, message) {
            console.log(message);
        };

    });
</script>

<script>
    $(document).ready(function() {
        $("#role").select2({
            theme: "bootstrap-5",
            placeholder: "Pilih Flag"
        });

        $('#role').on("select2:select", function(e) {
            var role = $("#role option:selected").val();

            if (role == "R001") {
                // $('.kadaluwarsa_display').hide();
                $(".kadaluwarsa_display").css("display", "none");
            } else {
                // $('.kadaluwarsa_display').show();
                $(".kadaluwarsa_display").css("display", "block");
            }

        });

        $("#kadaluwarsa").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            language: 'id',
            locale: 'id',
            orientation: 'left bottom',
            todayHighlight: true
        });
    });
</script>

<script>
    $(document).ready(function() {
        $("#role_edit").select2({
            theme: "bootstrap-5",
            placeholder: "Pilih Flag"
        });
    });
</script>

<script>
    $("#tanggal_kadaluwarsa_edit").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        language: 'id',
        locale: 'id',
        orientation: 'left bottom',
        todayHighlight: true
    });
</script>

<script>
    $("#tanggal_start_edit").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        language: 'id',
        locale: 'id',
        orientation: 'left bottom',
        todayHighlight: true
    });
</script>

<script type="text/javascript">
    // var table_riwayat_kenaikan_pangkat = ;
    $(document).ready(function() {
        // NioApp.DataTable.init = function() {
        // NioApp.DataTable('#tb_user', {


        var no_kolom = 0;
        var kolom_search_gabung = '';
        var kolom_title = '';
        var value_kolom = '';
        var searchTimeout;
        $('#tb_user thead tr:eq(1) th').each(function(i) {


            // console.log(no_kolom);
            var title = $(this).text();
            if (no_kolom == 5 || no_kolom == 10) {
                $(this).html('');
            } else if (no_kolom == 0) {
                $(this).html(
                    '<table>' +
                    '<tr>' +
                    '<td>' +
                    '<input style="transform: scale(1.7);" class="form-check-input m-2 users_id_checkbox_all" type="checkbox" id="users_id_checkbox_all" value="">' +
                    '</td>' +
                    '<td>' +
                    '<button style="display:none;" type="button" name="btn_delete_users_array" id="btn_delete_users_array" class="btn btn-primary waves-effect waves-light m-2" data-bs-toggle="modal"> <i class="dripicons-trash"></i></button>' +
                    '</td>' +
                    '</tr>' +
                    '</table>'
                );
                // $(this).html('<input style="transform: scale(1.7);" class="form-check-input users_id_checkbox_all" type="checkbox" id="users_id_checkbox_all" value="">');
            } else if (no_kolom == 5) {
                $(this).html('<select id="Role_Search" name="Role_Search" style="width:100%">' +
                    '<option value=" ">Semua</option>' +
                    '<option value="R001"> Admin Super </option>' +
                    '<option value="R003"> Peserta </option>' +
                    '</select>');

                $("#Role_Search").select2({
                    theme: "bootstrap-5",
                    placeholder: "Search Status",
                });

                $("#Role_Search").on("select2:select", function(e) {
                    // what you would like to happen
                    // console.log(this.value);
                    // $("#eselon_id_selected_input").val(eselon_id).select2();
                    var status_search = this.value;
                    kolom_search_gabung += '&Role_Search=' + status_search;
                    const myArray = kolom_search_gabung.split("&");
                    console.log(kolom_search_gabung);
                    console.log(myArray);
                    console.log('coba');
                    // kolom_gabung = '';
                    // $('#tb_pegawai').DataTable().ajax.url("{{ url('pegawai/getDataPegawai') }}"+'?').load();
                    $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}" + '?' + kolom_search_gabung).load();
                    // console.log(status_search);
                });
            } else if (no_kolom == 9) {
                // $(this).html(
                //     '<input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Awal" id="Tanggal_Kadaluwarsa_Search_1" name="Tanggal_Kadaluwarsa_Search_1">' +
                //     ' s/d ' +
                //     '<input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Akhir" id="Tanggal_Kadaluwarsa_Search_2" name="Tanggal_Kadaluwarsa_Search_2">'
                // );

                $("#Tanggal_created_at_1").datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    language: 'id',
                    locale: 'id',
                    orientation: 'left bottom',
                    todayHighlight: true,
                    clearBtn: true,
                }).on('changeDate', function(e) {
                    // var startDate = document.getElementById('Tanggal_Kadaluwarsa_Search_1').value;
                    // var selectedDate = new Date(startDate);
                    // $('#tanggalakhir').datepicker('setStartDate', startDate);

                    var Tanggal_created_at_1_search = document.getElementById('Tanggal_created_at_1').value;
                    // var Tanggal_Kadaluwarsa_Search_1_search = $('#Tanggal_Kadaluwarsa_Search_1_search').val();
                    // var Tanggal_Kadaluwarsa_Search_1_search = $(this).datepicker('getDate');


                    // var Tanggal_Kadaluwarsa_Search_1_search = this.value;
                    kolom_search_gabung += '&Tanggal_created_at_1_search=' + Tanggal_created_at_1_search;
                    const myArray = kolom_search_gabung.split("&");
                    console.log(kolom_search_gabung);
                    console.log(myArray);
                    console.log('coba');
                    // kolom_gabung = '';
                    // $('#tb_pegawai').DataTable().ajax.url("{{ url('pegawai/getDataPegawai') }}"+'?').load();
                    $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}" + '?' + kolom_search_gabung).load();

                });

                $("#Tanggal_created_at_2").datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    language: 'id',
                    locale: 'id',
                    orientation: 'left bottom',
                    todayHighlight: true,
                    clearBtn: true,
                }).on('changeDate', function(e) {
                    // alert(this.value);

                    var Tanggal_created_at_2_search = document.getElementById('Tanggal_created_at_2').value;
                    // var Tanggal_Kadaluwarsa_Search_2_search = $(this).datepicker('getDate');

                    // var Tanggal_Kadaluwarsa_Search_2_search = this.value;
                    kolom_search_gabung += '&Tanggal_created_at_2_search=' + Tanggal_created_at_2_search;
                    const myArray = kolom_search_gabung.split("&");
                    console.log(kolom_search_gabung);
                    console.log(myArray);
                    console.log('coba');
                    // kolom_gabung = '';
                    // $('#tb_pegawai').DataTable().ajax.url("{{ url('pegawai/getDataPegawai') }}"+'?').load();
                    $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}" + '?' + kolom_search_gabung).load();
                    // console.log(status_search);
                });

            } else if (no_kolom == 8) {
                // $(this).html(
                //     '<input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Awal" id="Tanggal_Kadaluwarsa_Search_1" name="Tanggal_Kadaluwarsa_Search_1">' +
                //     ' s/d ' +
                //     '<input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Akhir" id="Tanggal_Kadaluwarsa_Search_2" name="Tanggal_Kadaluwarsa_Search_2">'
                // );

                $("#Tanggal_Kadaluwarsa_Search_1").datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    language: 'id',
                    locale: 'id',
                    orientation: 'left bottom',
                    todayHighlight: true,
                    clearBtn: true,
                }).on('changeDate', function(e) {
                    // var startDate = document.getElementById('Tanggal_Kadaluwarsa_Search_1').value;
                    // var selectedDate = new Date(startDate);
                    // $('#tanggalakhir').datepicker('setStartDate', startDate);

                    var Tanggal_Kadaluwarsa_Search_1_search = document.getElementById('Tanggal_Kadaluwarsa_Search_1').value;
                    // var Tanggal_Kadaluwarsa_Search_1_search = $('#Tanggal_Kadaluwarsa_Search_1_search').val();
                    // var Tanggal_Kadaluwarsa_Search_1_search = $(this).datepicker('getDate');


                    // var Tanggal_Kadaluwarsa_Search_1_search = this.value;
                    kolom_search_gabung += '&Tanggal_Kadaluwarsa_Search_1_search=' + Tanggal_Kadaluwarsa_Search_1_search;
                    const myArray = kolom_search_gabung.split("&");
                    console.log(kolom_search_gabung);
                    console.log(myArray);
                    console.log('coba');
                    // kolom_gabung = '';
                    // $('#tb_pegawai').DataTable().ajax.url("{{ url('pegawai/getDataPegawai') }}"+'?').load();
                    $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}" + '?' + kolom_search_gabung).load();

                });

                $("#Tanggal_Kadaluwarsa_Search_2").datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    language: 'id',
                    locale: 'id',
                    orientation: 'left bottom',
                    todayHighlight: true,
                    clearBtn: true,
                }).on('changeDate', function(e) {
                    // alert(this.value);

                    var Tanggal_Kadaluwarsa_Search_2_search = document.getElementById('Tanggal_Kadaluwarsa_Search_2').value;
                    // var Tanggal_Kadaluwarsa_Search_2_search = $(this).datepicker('getDate');

                    // var Tanggal_Kadaluwarsa_Search_2_search = this.value;
                    kolom_search_gabung += '&Tanggal_Kadaluwarsa_Search_2_search=' + Tanggal_Kadaluwarsa_Search_2_search;
                    const myArray = kolom_search_gabung.split("&");
                    console.log(kolom_search_gabung);
                    console.log(myArray);
                    console.log('coba');
                    // kolom_gabung = '';
                    // $('#tb_pegawai').DataTable().ajax.url("{{ url('pegawai/getDataPegawai') }}"+'?').load();
                    $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}" + '?' + kolom_search_gabung).load();
                    // console.log(status_search);
                });

            } else if (no_kolom == 7) {
                // $(this).html(
                //     '<input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Awal" id="Tanggal_Kadaluwarsa_Search_1" name="Tanggal_Kadaluwarsa_Search_1">' +
                //     ' s/d ' +
                //     '<input type="text" style="width:100%;" class="form-control" placeholder="Tanggal Akhir" id="Tanggal_Kadaluwarsa_Search_2" name="Tanggal_Kadaluwarsa_Search_2">'
                // );

                $("#Tanggal_Start_Search_1").datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    language: 'id',
                    locale: 'id',
                    orientation: 'left bottom',
                    todayHighlight: true,
                    clearBtn: true,
                }).on('changeDate', function(e) {
                    // var startDate = document.getElementById('Tanggal_Kadaluwarsa_Search_1').value;
                    // var selectedDate = new Date(startDate);
                    // $('#tanggalakhir').datepicker('setStartDate', startDate);

                    var Tanggal_Start_Search_1_search = document.getElementById('Tanggal_Start_Search_1').value;
                    // var Tanggal_Kadaluwarsa_Search_1_search = $('#Tanggal_Kadaluwarsa_Search_1_search').val();
                    // var Tanggal_Kadaluwarsa_Search_1_search = $(this).datepicker('getDate');


                    // var Tanggal_Kadaluwarsa_Search_1_search = this.value;
                    kolom_search_gabung += '&Tanggal_Start_Search_1_search=' + Tanggal_Start_Search_1_search;
                    const myArray = kolom_search_gabung.split("&");
                    console.log(kolom_search_gabung);
                    console.log(myArray);
                    console.log('coba');
                    // kolom_gabung = '';
                    // $('#tb_pegawai').DataTable().ajax.url("{{ url('pegawai/getDataPegawai') }}"+'?').load();
                    $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}" + '?' + kolom_search_gabung).load();

                });

                $("#Tanggal_Start_Search_2").datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    language: 'id',
                    locale: 'id',
                    orientation: 'left bottom',
                    todayHighlight: true,
                    clearBtn: true,
                }).on('changeDate', function(e) {
                    // alert(this.value);

                    var Tanggal_Start_Search_2_search = document.getElementById('Tanggal_Start_Search_2').value;
                    // var Tanggal_Kadaluwarsa_Search_2_search = $(this).datepicker('getDate');

                    // var Tanggal_Kadaluwarsa_Search_2_search = this.value;
                    kolom_search_gabung += '&Tanggal_Start_Search_2_search=' + Tanggal_Start_Search_2_search;
                    const myArray = kolom_search_gabung.split("&");
                    console.log(kolom_search_gabung);
                    console.log(myArray);
                    console.log('coba');
                    // kolom_gabung = '';
                    // $('#tb_pegawai').DataTable().ajax.url("{{ url('pegawai/getDataPegawai') }}"+'?').load();
                    $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}" + '?' + kolom_search_gabung).load();
                    // console.log(status_search);
                });

            } else if (no_kolom == 1) {
                $(this).html('');
            } else {
                $(this).html('<input type="text" style="width:100%;" placeholder="Search ' + title.toLowerCase() + '" class="' + title.toLowerCase().toLowerCase().replace(" ", "_") + "_search form-control" + '" />');
            }



            $('input', this).on('keyup', function() {
                // console.log('&' + title + '=' + this.value);
                // if (table.column(i).search() !== this.value) {
                //     table
                //         .column(i)
                //         .search(this.value)
                //         .draw();
                // }
                // kolom_gabung = '';
                kolom_search_gabung += '&' + title + '=' + this.value;
                const myArray = kolom_search_gabung.split("&");
                console.log(kolom_search_gabung);
                console.log(myArray);
                console.log('coba');
                // kolom_gabung = '';
                // $('#tb_pegawai').DataTable().ajax.url("{{ url('pegawai/getDataPegawai') }}"+'?').load();
                // $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}" + '?' + kolom_search_gabung).load();
                // $('#tb_pegawai').DataTable().ajax.reload(null, false);
                clearTimeout(searchTimeout);
                searchTimeout = setTimeout(function() {
                    $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}" + '?' + kolom_search_gabung).load();
                }, 2000);
            });

            // kolom_gabung = '';

            // console.log('coba = ' + kolom_gabung);
            no_kolom++;

        });

        var array_users_id_checkbox = [];
        var array_users_id_checkbox_uniq = [];
        var array_users_id_checkbox_uniq_uniq = [];
        var t = 1;

        var tb_user = $('#tb_user').DataTable({
            responsive: {
                details: true
            },
            processing: true,
            serverSide: true,
            searching: true,
            scrollX: true,
            ordering: false,
            sDom: 'lrtip', // untuk hidden search box di datatable
            // language: {
            // 	processing: "Sedang diproses...<img src='{{ asset('public/assets/images/load.gif') }}'>"
            // },
            fixedHeader: {
                header: true,
                footer: true,
                headerOffset: $('#page-topbar').height()
            },
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('user/getDataUser') }}",
                type: 'GET',
                async: false,
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    width: "10%",
                    className: 'text-center',
                },
                {
                    data: 'qrcode',
                    name: 'qrcode',
                    className: 'text-center',
                    width: "12%",
                    render: function(data, type, row) {
                        // if (type === 'display') {
                        // return row.tiket_issues_qrcode;

                        // return '<img src"public_path('+'\''+'image/Petro_logo.png'+'\''+')">';
                        // return '<img style="width:200px; height:200px;" src="data:image/png;base64,' + row.qrcode + '" />' +
                        //     // '<a href="data:image/png;base64,{!! base64_encode(QrCode::encoding("UTF-8")->format("png")->size(700)->errorCorrection("H")->generate("' + row.tiket_issues_qrcode + '")) !!}" download="' + row.username + '.png" target="_blank" style="width: 100%; font-size:12px" type="button" class="btn btn-primary waves-effect waves-light mt-1">Download QRCode</a>' +
                        //     '<a href="data:image/png;base64,' + row.qrcode + '" download="' + row.username + '.png" target="_blank" style="width: 100%; font-size:12px" type="button" class="btn btn-primary waves-effect waves-light mt-1">Download QRCode</a>'+
                        //     '<input type="text" class="form-control" id="inputcopy" name="inputcopy" value="' + row.token_login_qr_code + '">'+
                        //     '<button onclick="copyCodeQR()" class="btn btn-info waves-effect waves-light mt-1" style="width: 100%; font-size:12px">Salin Code</button>'
                        // ;
                        return '<input style="width:200px;" type="text" class="form-control" id="inputcopy" name="inputcopy" value="' + row.token_login_qr_code + '">' +
                            '<button style="width:200px;" onclick="copyCodeQR()" class="btn btn-info waves-effect waves-light mt-1" style="width: 100%; font-size:12px">Salin Code</button>';
                        // return 'coba';
                        // console.log(row.tiket_issues);
                        // return row.tiket_issues_qrcode;
                        // return '';
                        // } else if (type === 'sort') {
                        //     return data.LastName;
                        // } else {
                        //     return data;
                        // }
                    }
                },
                {
                    data: 'nama',
                    name: 'nama',
                    className: 'text-center'
                },
                {
                    data: 'email',
                    name: 'email',
                    className: 'text-center'
                },
                {
                    data: 'username',
                    name: 'username',
                    className: 'text-center'
                },
                {
                    data: 'tanda_tangan',
                    name: 'tanda_tangan',
                    className: 'text-center'
                },
                {
                    data: 'm_role_nama_role',
                    name: 'm_role_nama_role',
                    className: 'text-center'
                },
                {
                    data: 'tanggal_start',
                    name: 'tanggal_start',
                    className: 'text-center'
                },
                {
                    data: 'tanggal_expired',
                    name: 'tanggal_expired',
                    className: 'text-center'
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                    className: 'text-center'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    className: 'text-center'
                },

            ],
            drawCallback: function(settings) {
                // alert('DataTables has redrawn the table');
                // alert(settings);
                var api = this.api();
                var api_lenght = api.rows({
                    page: 'current'
                }).data().length;
                // console.log( api.rows( {page:'current'} ).data().length );
                // console.log(api.rows);
                // console.log(api_lenght);

                // var array_users_id_checkbox = [];

                for (t; t <= api_lenght; t++) {
                    // console.log(t);

                    $(document).on('change', '#users_id_checkbox_' + t, function() {
                        var coba = $('#users_id_checkbox_' + t).val();
                        // var coba = $('#users_id_checkbox_2').val();
                        // console.log($('.users_id_checkbox_' + i).val());
                        // console.log(this.value);
                        if ($(this).is(':checked')) {
                            console.log('checked');
                            // console.log($(this).val);
                            // var coba = $('#users_id_checkbox_' + i).val();
                            // console.log(this.value);
                            array_users_id_checkbox.push(this.value);
                            // chexboxFunction(this.value, 'checked');
                            // console.log(i);
                        } else {
                            console.log('unchecked');
                            // console.log($(this).val);
                            // var coba = $('#users_id_checkbox_' + i).val();
                            // console.log(this.value);
                            // array_users_id_checkbox.unshift(this.value);
                            array_users_id_checkbox.splice(array_users_id_checkbox.indexOf(this.value), 1);
                            // array_users_id_checkbox.push(this.value);
                            // chexboxFunction(this.value, 'unchecked');

                            // console.log(i);
                        }
                        // var favorite = [];
                        // $.each($("input[name='users_id_checkbox']:checked"), function() {
                        //     favorite.push($(this).val());
                        // });
                        // alert("My favourite sports are: " + favorite.join(", "));

                        console.log(array_users_id_checkbox);
                        // console.log(array_users_id_checkbox[0]);
                        console.log(array_users_id_checkbox.length);

                        if (array_users_id_checkbox.length == 0) {
                            $("#btn_delete_users_array").css("display", "none");
                        } else {
                            $("#btn_delete_users_array").css("display", "block");
                        }
                    });


                }


                // $.each(array_users_id_checkbox, function(i, el) {
                //     if ($.inArray(el, array_users_id_checkbox_uniq) === -1) array_users_id_checkbox_uniq.push(el);
                // });

                // for (var j = 0; j < array_users_id_checkbox_uniq.length; j++) {
                //     $('.' + 'users_id_checkbox_' + array_users_id_checkbox_uniq[j]).prop('checked', true);
                // }

                // console.log(array_users_id_checkbox);
                for (var m = 0; m < array_users_id_checkbox.length; m++) {
                    $('.' + 'users_id_checkbox_' + array_users_id_checkbox[m]).prop('checked', true);
                }

                // $('#tb_user').DataTable().ajax.reload(null, false);
                // $('#tb_user').DataTable().ajax.url("{{ url('user/getDataUser') }}").load();

            },
            initComplete: function(settings, json) {
                // $("#tb_preview").wrap(
                //     "<div style='overflow-x:auto; width:100%; position:relative;'></div>");

                $('.dataTables_scrollHead').css({
                    'overflow-x': 'scroll'
                }).on('scroll', function(e) {
                    var scrollBody = $(this).parent().find('.dataTables_scrollBody').get(0);
                    scrollBody.scrollLeft = this.scrollLeft;
                    $(scrollBody).trigger('scroll');
                });

                $(document).on('scroll', function() {
                    $('.dtfh-floatingparenthead').on('scroll', function() {
                        // console.log('data')
                        $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
                    });
                })
                // $('#tb_user').DataTable().ajax.reload(null, false);
            },
        })

        $(document).on('change', '#users_id_checkbox_all', function() {

            if ($(this).is(':checked')) {
                console.log('checked');
                // console.log($(this).val);
                // var coba = $('#users_id_checkbox_' + i).val();
                // console.log(this.value);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // data: $('#formTambahIssues').serialize(),
                    url: "{{url('user/getDataUserCheckAll')}}" + '?' + kolom_search_gabung,
                    type: "GET",
                    dataType: 'json',
                    async: false,
                    success: function(data) {

                        // array_users_id_checkbox.push(this.value);
                        console.log(data);
                        for (var i = 0; i < data.data.length; i++) {

                            array_users_id_checkbox.push(data.data[i]['users_id']);

                        }

                        $("#btn_delete_users_array").css("display", "block");

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#modalPenghargaan').modal('show');
                    }
                });

            } else {
                console.log('unchecked');
                // console.log($(this).val);
                // var coba = $('#users_id_checkbox_' + i).val();
                // console.log(this.value);
                // array_users_id_checkbox.unshift(this.value);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // data: $('#formTambahIssues').serialize(),
                    url: "{{url('user/getDataUserCheckAll')}}" + '?' + kolom_search_gabung,
                    type: "GET",
                    dataType: 'json',
                    async: false,
                    success: function(data) {

                        for (var j = 0; j < data.data.length; j++) {

                            array_users_id_checkbox.splice(array_users_id_checkbox.indexOf(data.data[j]['users_id']), 1);

                        };
                        // console.log(data);

                        $("#btn_delete_users_array").css("display", "none");

                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#modalPenghargaan').modal('show');
                    }
                });


                // array_users_id_checkbox.push(this.value);
                // chexboxFunction(this.value, 'unchecked');

                // console.log(i);
            }
        });

        $(document).on('click', '#btn_delete_users_array', function() {
            console.log('coba coba coba coba');
            console.log(array_users_id_checkbox_uniq);
            console.log('coba coba coba coba coba coba');
            console.log(array_users_id_checkbox);

            // $.each(array_users_id_checkbox, function(i2, el2) {
            //     if ($.inArray(el2, array_users_id_checkbox_uniq_uniq) === -1) array_users_id_checkbox_uniq_uniq.push(el2);
            // });

            var data_delete_checkbox =
                // $("#formSuratPerjanjianIssues").serialize() +
                "array_users_id_checkbox=" + array_users_id_checkbox;

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data_delete_checkbox,
                url: "{{url('user/delete_checkbox')}}",
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    console.log(data.kode);
                    if (data.kode == 201) {
                        // toastr.clear();
                        // NioApp.Toast(data.success, 'success', {
                        //     position: 'top-right'
                        // });
                        toastr.success(data.message);
                        // document.location = "{{ url('/home/index') }}";
                        // $("#modalFormTambahUser").modal('hide');
                        $('#tb_user').DataTable().ajax.reload(null, false);
                        $("#btn_delete_users_array").css("display", "none");
                        $('.' + 'users_id_checkbox_all').prop('checked', false);
                    } else {
                        // toastr.clear();
                        // NioApp.Toast(data.success, 'error', {
                        //     position: 'top-right'
                        // });
                        toastr.success(data.message);
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#modalPenghargaan').modal('show');
                }
            });
        });
        // };
    });
</script>

<script>
    $(document).on('click', '#tambah', function() {
        console.log('tambah');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#formTambahUser').serialize(),
            url: "{{url('user/tambah')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    // toastr.clear();
                    // NioApp.Toast(data.success, 'success', {
                    //     position: 'top-right'
                    // });
                    toastr.success(data.message);
                    // document.location = "{{ url('/home/index') }}";
                    $("#modalFormTambahUser").modal('hide');
                    $('#tb_user').DataTable().ajax.reload(null, false);
                } else if (data.kode == 401) {
                    toastr.success(data.message);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        // $("#edit").click(function() {
        $(document).on('click', '#edit', function() {
            console.log('coba');
            var users_id = $(this).attr("data-users_id");
            var email = $(this).attr("data-email");
            var nama = $(this).attr("data-nama");
            var username = $(this).attr("data-username");
            var password = $(this).attr("data-password");
            var role = $(this).attr("data-role");

            console.log(role);
            $("#users_id_edit").val(users_id);
            $("#email_edit").val(email);
            $("#nama_edit").val(nama);
            $("#username_edit").val(username);
            $("#role_edit").val(role).trigger('change');

        });
    });
</script>

<script>
    $(document).ready(function() {
        // $("#edit").click(function() {
        $(document).on('click', '#edit_kadaluwarsa', function() {
            console.log('coba');
            var users_id = $(this).attr("data-users_id");
            var tanggal_kadaluwarsa = $(this).attr("data-tanggal_kadaluwarsa");
            var tanggal_start = $(this).attr("data-tanggal_start");

            console.log(role);
            $("#users_id_edit_tanggal_kadaluwarsa").val(users_id);
            $("#tanggal_kadaluwarsa_edit").val(tanggal_kadaluwarsa);
            $("#tanggal_start_edit").val(tanggal_start);

        });
    });
</script>

<script>
    $(document).on('click', '#update', function() {
        console.log('update');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormEditUser').serialize(),
            url: "{{url('user/update')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    // NioApp.Toast(data.success, 'success', {
                    //     position: 'top-right'
                    // });
                    toastr.success(data.message);
                    // document.location = "{{ url('/home/index') }}";
                    $("#modalFormEditUser").modal('hide');
                    $('#tb_user').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    // NioApp.Toast(data.success, 'error', {
                    //     position: 'top-right'
                    // });
                    toastr.success(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    $(document).on('click', '#update_tanggal_kadaluwarsa', function() {
        console.log('update');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormEditKadaluwarsa').serialize(),
            url: "{{url('user/updateKadaluwarsa')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    // NioApp.Toast(data.success, 'success', {
                    //     position: 'top-right'
                    // });
                    toastr.success(data.message);
                    // document.location = "{{ url('/home/index') }}";
                    $("#modalFormEditKadaluwarsa").modal('hide');
                    $('#tb_user').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    // NioApp.Toast(data.success, 'error', {
                    //     position: 'top-right'
                    // });
                    toastr.success(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>


<script>
    $(document).on('click', '#delete', function() {
        console.log('delete');
        var users_id = $(this).attr("data-users_id");
        console.log(users_id);

        // $('.eg-swal-av2').on("click", function(e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
            if (result.value) {
                console.log(users_id);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // data: $('#FormEditUser').serialize(),
                    url: "{{url('user/delete')}}" + '/' + users_id,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        console.log(data.kode);
                        if (data.kode == 201) {
                            toastr.clear();
                            // NioApp.Toast(data.success, 'success', {
                            //     position: 'top-right'
                            // });
                            toastr.success(data.message);
                            // document.location = "{{ url('/home/index') }}";
                            // $("#modalFormEditUser").modal('hide');
                            // Swal.fire('Deleted!', 'Berhasil Di Delete.', 'success');
                            $('#tb_user').DataTable().ajax.reload(null, false);
                        } else {
                            toastr.clear();
                            // NioApp.Toast(data.success, 'error', {
                            //     position: 'top-right'
                            // });
                            toastr.success(data.message);
                            // Swal.fire('Deleted!', 'Gagal Di Delete.', 'error');
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#modalPenghargaan').modal('show');
                    }
                });

            }
        });
        // });


    });
</script>

<script>
    $('#formimportDataUser').submit(function(event) {
        var formData = new FormData(this);
        // console.log(formData);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // contentType: 'multipart/form-data',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            url: "{{ url('user/import_excel_user') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log("data conslo : ".data);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);

                    console.log(data.data_user_yang_behasil_di_input);
                    console.log("database succes!");

                    Swal.fire({
                        title: data.success,
                        icon: 'success',
                        html: 'Data excel berhasil di import!'
                        // html: 'Data baru yang berhasil di masukan : ' + data.jumlah_data_user_yang_behasil_di_input +
                        //     '<br>' +
                        //     'Data lama yang berhasil di masukan : ' + data.jumlah_data_user_yang_tidak_behasil_di_input,
                    })

                    $('#tb_user').DataTable().ajax.reload(null, false);

                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
                // window.location.href = window.location.href;
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    function copyCodeQR() {
        document.querySelector("#inputcopy").select();
        document.execCommand("copy");
        var data = "code berhasil disalin!";
        toastr.success(data);
    }
</script>


@endsection