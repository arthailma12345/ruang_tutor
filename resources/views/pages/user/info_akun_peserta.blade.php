@extends('layouts_frontend.app')
@section('style')

@endsection
@section('content')
<header>
    <div class="inner-page-header_home inner-header-bg-1">
        <div class="container">
            <div class="inner-header-content inner-header-content-white">
                <div class="header-content-text">
                    <h1 style="text-shadow: 0px 0px 16px black;">Informasi Akun</h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- .end product-section -->
        <!-- cart-section -->
        <section class="cart-section pt-100 bg-dark">
            <div class="container">
                <div class="border-bottom pb-70">
                    <div class="cart-section-item cart-section-contents"> <!-- Add class "cart-section-contents-hide" if no content are in the cart. -->
                        <div class="text-center">
                            <div class="cart-table cart-table-white">
                                <table border="2">
                                    <thead>
                                        <tr>
                                            <th style="background-color: blue; font-weight: bolder; text-align: center;" colspan="3">INFORMASI SATU</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tb_user as $info1)
                                        <tr>
                                            <td style="width: 30%;">Nama</td>
                                            <td>:</td>
                                            <td style="width: 70%;"> {{$get_session_nama}} </td>
                                        </tr>
                                        <tr>
                                        <td style="width: 30%;">Username</td>
                                            <td>:</td>
                                            <td style="width: 70%;">{{$get_session_username}}</td>
                                        </tr>
                                        <tr>
                                        <td style="width: 30%;">Email</td>
                                            <td>:</td>
                                            <td style="width: 70%;">{{$info1->email}}</td>
                                        </tr>
                                        <tr>
                                        <td style="width: 30%;">Jenis</td>
                                            <td>:</td>
                                            <td style="width: 70%;">{{$get_session_flag}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        <div class="divider"></div>
                                <table border="2">
                                    <thead>
                                        <tr>
                                            <th style="background-color: blue; font-weight: bolder; text-align: center;" colspan="3">INFORMASI DUA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tb_user as $info2)
                                        <tr>
                                            <td style="width: 30%;">Jumlah Teofl Test</td>
                                            <td>:</td>
                                            <td style="width: 70%;">{{$total_paket_tt}} Paket</td>
                                        </tr>
                                        <tr>
                                        <td style="width: 30%;">Jumlah Teofl Preparation</td>
                                            <td>:</td>
                                            <td style="width: 70%;">{{$total_paket_tp}} Paket</td>
                                        </tr>
                                        <tr>
                                        <td style="width: 30%;">Masa aktif akun</td>
                                            <td>:</td>
                                            <td style="width: 70%;">{{$info2->tanggal_expired}}</td>
                                        </tr>
                                        <tr>
                                        <td style="width: 30%;">Setatus</td>
                                            <td>:</td>
                                            <td style="width: 70%;">Aktif</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
                <div class="row justify-content-center justify-content-lg-end">
                            <div class="col-sm-12 col-md-6 col-lg-5 pb-30">
                                <div class="cart-details cart-details-white">
                                    <h6 class="cart-details-title">Melakukan Perubahan Password dan Email</h6>
                                    <a href="{{url('/profil_peserta/ubahakun')}}" class="btn main-btn">Edit Akun</a>
                                </div>
                            </div>
                        </div>
            </div>
            </div>
        </section>
        <!-- .end cart-section -->

@endsection