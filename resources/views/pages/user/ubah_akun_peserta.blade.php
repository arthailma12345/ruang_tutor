@extends('layouts_frontend.app')
@section('content')
<header>
    <div class="inner-page-header_home inner-header-bg-1">
        <div class="container">
            <div class="inner-header-content inner-header-content-white">
                <div class="header-content-text">
                    <h1 style="text-shadow: 0px 0px 16px black;">Edit Akun</h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- product-section -->
<section class="product-section bg-dark pt-100">
    <div class="account-page-section bg-dark pt-100">
        <div class="container">
            <div class="border-bottom pb-100">
                <div class="row">
                    <div style="width: 100%;" class="col-sm-12 col-md-12 col-lg-12 pb-30">
                        <div class="account-info">
                            <form action="#" name="FormUpdatePassword" id="FormUpdatePassword">
                                {{ csrf_field() }}
                                <div class="account-setting-item">
                                    <div class="row">
                                        <div style="width: 100%; class=" col-sm-12 col-md-12 col-lg-12">
                                            <h5>Password</h5>
                                            <div class="form-group mb-20">
                                                <input type="text" name="inputpasslama" id="inputpasslama" class="form-control form-control" required placeholder="Password Lama" />
                                            </div>
                                        </div>
                                        <div style="width: 100%; class=" col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group mb-20">
                                                <input type="text" name="inputpassbaru1" id="inputpassbaru1" class="form-control form-control" required placeholder="Password Baru" />
                                            </div>
                                        </div>
                                        <div style="width: 100%; class=" col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group mb-20">
                                                <input type="text" name="inputpassbaru2" id="inputpassbaru2" class="form-control form-control" required placeholder="Konformasi Password Baru" />
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="account-setting-item account-setting-button">
                                            <button id="btnupdatepassword" name="btnupdatepassword" type="button" class="btn main-btn btn-small">Save Changes Password</button>
                                            <button type="reset" class="btn main-btn btn-white btn-small">Cancel</button>
                                        </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- ----------------- -->
                <div class="account-info">
                    <form action="#" class="row gx-3 gy-2 align-items-center" name="FormUpdateEmail" id="FormUpdateEmail">
                        {{ csrf_field() }}
                        <div style="width: 100%; class=" col-sm-12 col-md-12 col-lg-12">
                            <h5>Email</h5>
                            <div class="form-group">
                                <input type="text" name="inputeemail" id="inputeemail" class="form-control form-control" required placeholder="Email" />
                            </div>
                        </div>
                        <div class="account-setting-item account-setting-button">
                            <button id="btnupdateemail" name="btnupdateemail" type="button" class="btn main-btn btn-small">Save Changes Email</button>
                            <button type="reset" class="btn main-btn btn-white btn-small">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    </div>
    </div>
</section>
<!-- .end product-section -->

@endsection

@section('script')
<!-- script irhas -->

<script>
    $(document).on('click', '#btnupdateemail', function() {
        console.log('update email aktif');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormUpdateEmail').serialize(),
            url: "{{url('profil_peserta/update_email_profil')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data.kode == 201) {
                    // alert("Berhasil Update Email");
                    Swal.fire(
                        'Oke.. Berhasil Update Email',
                    );
                    location.reload();
                } else {}
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $(document).on('click', '#btnupdatepassword', function() {
        console.log('update ps aktif');
        var pass = $('#inputpassbaru1').val();
        var pass2 = $('#inputpassbaru2').val();
        if (pass != pass2) {
            Swal.fire(
                'Maaf.. Konfirmasi ulang passowrd kamu tidak sama!!',
            );
            return false;
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormUpdatePassword').serialize(),
            url: "{{url('profil_peserta/update_pass_profil')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {

                console.log(data);
                if (data.kode == 201) {
                    Swal.fire(
                        'Oke.. Berhasil Update Password, Kamu akan logout otomatis!!',
                    );
                    window.location.href = "{{ url('/logout_lwt')}}";
                } else if (data.kode == 202) {
                    Swal.fire(
                        'Maaf.. Password lama kamu salah!!',
                    );
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

@endsection