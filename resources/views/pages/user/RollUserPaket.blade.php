@extends('layouts.app')

@section('content')

<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">PAKET PESERTA</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Akses Paket</a></li>
                            <li class="breadcrumb-item active">Paket Peserta</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="row">
                    <div class="col-lg-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-xl-12">
                                        <!-- <div class="card"> -->
                                        <!-- <div class="card-body"> -->
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="mt-4 mt-xl-0">
                                                    <table class="table stripe" id="tb_roll_paket_user" style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10%;">No</th>
                                                                <th style="width: 30%;">Username</th>
                                                                <th style="width: 10%;">Jml MT C</th>
                                                                <th style="width: 10%;">Jml MT E</th>
                                                                <th style="width: 50%;">Aksi</th>
                                                            </tr>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Username</th>
                                                                <th>Jml TT</th>
                                                                <th>Jml TP</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div><!-- .card-preview -->
                                        </div> <!-- nk-block -->
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .card-preview -->

<div class="modal fade" id="modalFormLihatPaket">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #92a8d1;">
                <h5 class="modal-title">Paket MT Class</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <table class="table stripe" id="tb_lihat_paket" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Paket</th>
                            <!-- <th>Aksi</th> -->
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div class="modal-header" style="background-color: #fcca03;">
                <h5 class="modal-title">Paket MT Exclusive</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <table class="table stripe" id="tb_lihat_paket_teofl_p" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Paket</th>
                            <!-- <th>Aksi</th> -->
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalFormTambahPaket">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Paket</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                    <label class="form-label" for="formrow-email-input">[Search] Paket MT Class</label>
                        <div class="input-group mb-12">
                            <form action="#" class="form-validate is-alter" name="formTambahPaket" id="formTambahPaket" style="width:70%;">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_username_roll" id="id_username_roll">
                                <select name="paket_toefl_test" id="paket_toefl_test" class="paket_toefl_test_select2" style="width:100%;">
                                    <option value=""></option>
                                    @foreach($tb_paket_teofl_test as $data)
                                    <option value="{{$data->id_paket}}">{{$data->nama_paket}}</option>
                                    @endforeach
                                </select>
                            </form>
                            <button type="button" name="tambahceklis" id="tambahceklis" class="btn btn-warning">Ceklis</button>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-hover" id="tb_lihat_paket1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Paket</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <hr>
                <div class="form-group">
                    <div class="row">
                    <label class="form-label" for="formrow-email-input">[Search] Paket MT Exclusive</label>
                        <div class="input-group mb-12">
                            <form action="#" class="form-validate is-alter" name="formTambahPaket2" id="formTambahPaket2" style="width:70%;">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_username_roll_2" id="id_username_roll_2">
                                <select name="nama_toefl_preparation" id="nama_toefl_preparation" class="paket_toefl_preparation_select2" style="width:100%;">
                                    <option value=""></option>
                                    @foreach($m_toefl_preparation as $data)
                                    <option value="{{$data->id}}">{{$data->nama_toefl_preparation}}</option>
                                    @endforeach
                                </select>
                            </form>
                            <button type="button" name="tambahceklis2" id="tambahceklis2" class="btn btn-warning">Ceklis</button>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-hover" id="tb_lihat_paket2">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Paket</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('script')

<script>
    $(document).ready(function() {
        $("#paket_toefl_test").select2({
            theme: "bootstrap-5",
            placeholder: "Pilih Paket",
            dropdownParent: $('#modalFormTambahPaket'),
        });
    });
</script>
<script>
    $(document).ready(function() {
        $("#paket_toefl_preparation_select2").select2({
            theme: "bootstrap-5",
            placeholder: "Pilih Paket",
            dropdownParent: $('#modalFormTambahPaket'),
        });
    });
</script>

<script>
    $(document).ready(function() {
        $("#nama_toefl_preparation").select2({
            theme: "bootstrap-5",
            placeholder: "Pilih Paket",
            dropdownParent: $('#modalFormTambahPaket'),
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        var no_kolom = 0;
        var kolom_search_gabung = '';
        var kolom_title = '';
        var value_kolom = '';
        $('#tb_roll_paket_user thead tr:eq(1) th').each(function(i) {


            // console.log(no_kolom);

            if (no_kolom == 0 || no_kolom == 2 || no_kolom == 3 || no_kolom == 4) {
                $(this).html('');
            } else {
                var title = $(this).text();
                // console.log(title.toLowerCase());
                $(this).html('<input type="text" style="width:100%;" placeholder="Search ' + title.toLowerCase() + '" class="form-control ' + title.toLowerCase().toLowerCase().replace(" ", "_") + "_search" + '" />');
            }

            $('input', this).on('keyup', function() {

                kolom_search_gabung += '&' + title + '=' + this.value;
                const myArray = kolom_search_gabung.split("&");
                console.log(kolom_search_gabung);
                console.log(myArray);
                console.log('coba');
                $('#tb_roll_paket_user').DataTable().ajax.url("{{ url('RollUserPaket/getDataUser') }}" + '?' + kolom_search_gabung).load();
            });

            // kolom_gabung = '';

            // console.log('coba = ' + kolom_gabung);
            no_kolom++;

        });

        // menampilkan data di tabel awal
        var tb_user = $('#tb_roll_paket_user').DataTable({
            processing: true,
            serverSide: true,
            searching: true,
            scrollX: true,
            sDom: 'lrtip', // untuk hidden search box di datatable
            // autoWidth: 'false',
            fixedHeader: {
                header: true,
                headerOffset: $('#page-topbar').height()
            },
            orderCellsTop: true, // untuk hidden search box di datatable
            // language: {
            // 	processing: "Sedang diproses...<img src='{{ asset('public/assets/images/load.gif') }}'>"
            // },
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('RollUserPaket/getDataUser') }}",
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center'
                },
                {
                    data: 'username',
                    name: 'username',
                    className: 'text-center'
                },
                {
                    data: 'jumlah',
                    name: 'jumlah',
                    className: 'text-center'
                },
                {
                    data: 'jumlah2',
                    name: 'jumlah2',
                    className: 'text-center'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    className: 'text-center'
                },

            ]
        })
        // };
    });
</script>

<!-- menampilkan data ketika di klik lihat -->
<script type="text/javascript">
    $(document).on('click', '#lihat', function() {

        var id = $(this).attr("data-id");
        $("#id").val(id);

        console.log(id);

        $('#tb_lihat_paket').DataTable().destroy();
        $('#tb_lihat_paket_teofl_p').DataTable().destroy();

        var tb_user = $('#tb_lihat_paket').DataTable({
            responsive: {
                details: true
            },
            processing: true,
            serverSide: true,
            searching: true,
            responsive: {
                details: true
            },
            sDom: 'lrtip',
            paging: false,
            ordering: false,
            info: false,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('RollUserPaket/getDataLihatPaketTT') }}" + "/" + id,
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center'
                },
                {
                    data: 'namapakett',
                    name: 'namapakett',
                    className: 'text-center'
                },
                // {
                //     data: 'aksi',
                //     name: 'aksi',
                //     className: 'text-center'
                // },

            ]
        })

        var tb_user = $('#tb_lihat_paket_teofl_p').DataTable({
            responsive: {
                details: true
            },
            processing: true,
            serverSide: true,
            searching: true,
            responsive: {
                details: true
            },
            sDom: 'lrtip',
            paging: false,
            ordering: false,
            info: false,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('RollUserPaket/getDataLihatPaketTP') }}" + "/" + id,
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center'
                },
                {
                    data: 'namapaketp',
                    name: 'namapaketp',
                    className: 'text-center'
                },
                // {
                //     data: 'aksi',
                //     name: 'aksi',
                //     className: 'text-center'
                // },

            ]
        })
    });
</script>

<script>
    $(document).on('click', '#deletepakettt', function() {
        var id = $(this).attr("data-id");
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('RollUserPaket/deletepakettt')}}" + '/' + id,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        if (data.kode == 201) {
                            toastr.clear();
                            toastr.success(data.message);
                            $('#tb_roll_paket_user').DataTable().ajax.reload(null, false);
                            $('#tb_lihat_paket1').DataTable().ajax.reload(null, false);
                        } else {
                            toastr.clear();
                            toastr.success(data.message);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });
</script>

<script>
    $(document).on('click', '#deletepakettp', function() {
        var id = $(this).attr("data-id");
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('RollUserPaket/deletepakettp')}}" + '/' + id,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        if (data.kode == 201) {
                            toastr.clear();
                            toastr.success(data.message);
                            $('#tb_roll_paket_user').DataTable().ajax.reload(null, false);
                            $('#tb_lihat_paket2').DataTable().ajax.reload(null, false);
                        } else {
                            toastr.clear();
                            toastr.success(data.message);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });
</script>

<!-- <script type="text/javascript">
    $(document).ready(function() {
        $('select').selectpicker();
    });
</script> -->

<script type="text/javascript">
    $('#search').on('keyup', function() {
        $value = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{URL::to('search')}}',
            data: {
                'search': $value
            },
            success: function(data) {
                $('tbody').html(data);
            }
        });
    })
</script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'csrftoken': '{{ csrf_token() }}'
        }
    });
</script>

<script>
    $(document).on('click', '#getIDuser', function() {
        var id = $(this).attr("data-id");
        $("#id_username_roll").val(id);
        $("#id_username_roll_2").val(id);

        console.log('id Username', id);

        $('#tb_lihat_paket1').DataTable().destroy();
        $('#tb_lihat_paket2').DataTable().destroy();

        var tb_user = $('#tb_lihat_paket1').DataTable({
            responsive: {
                details: true
            },
            processing: true,
            serverSide: true,
            searching: true,
            responsive: {
                details: true
            },
            sDom: 'lrtip',
            paging: false,
            ordering: false,
            info: false,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('RollUserPaket/getDataLihatPaketTT') }}" + "/" + id,
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center'
                },
                {
                    data: 'namapakett',
                    name: 'namapakett',
                    className: 'text-center'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    className: 'text-center'
                },

            ]
        })

        var tb_user = $('#tb_lihat_paket2').DataTable({
            responsive: {
                details: true
            },
            processing: true,
            serverSide: true,
            searching: true,
            responsive: {
                details: true
            },
            sDom: 'lrtip',
            paging: false,
            ordering: false,
            info: false,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('RollUserPaket/getDataLihatPaketTP') }}" + "/" + id,
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center'
                },
                {
                    data: 'namapaketp',
                    name: 'namapaketp',
                    className: 'text-center'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    className: 'text-center'
                },

            ]
        })
    });
</script>

<script>
    $(document).on('click', '#tambahceklis', function() {
        console.log('button ceklis aktif');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#formTambahPaket').serialize(),
            url: "{{url('RollUserPaket/tambahrollpaket1')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.success(data.message);
                    $('#tb_lihat_paket1').DataTable().ajax.reload(null, false);
                    $('#tb_roll_paket_user').DataTable().ajax.reload(null, false);
                } else {
                    toastr.success(data.message);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $(document).on('click', '#tambahceklis2', function() {
        console.log('button ceklis2 aktif');
        console.log($('#formTambahPaket2').serialize());
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#formTambahPaket2').serialize(),
            url: "{{url('RollUserPaket/tambahrollpaket2')}}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data.kode == 201) {
                    toastr.success(data.message);
                    $('#tb_lihat_paket2').DataTable().ajax.reload(null, false);
                    $('#tb_roll_paket_user').DataTable().ajax.reload(null, false);
                } else {
                    toastr.success(data.message);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

@endsection