@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="mb-0">Sub Paket {{ $m_toefl_preparation->nama_toefl_preparation }}</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Study Room</a></li>
                            <li class="breadcrumb-item">MT Exclusive</a></li>
                            <li class="breadcrumb-item active">Sub</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="row">
                    <div class="col-lg-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-xl-12">
                                        <!-- <div class="card"> -->
                                        <!-- <div class="card-body"> -->
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="mt-4 mt-xl-0">


                                                    <div class="card-title-group">
                                                        <div class="card-title">
                                                            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalFormTambahSubToeflPreparation">Tambah Sub Toefl Preparation</button> -->
                                                            <button type="button"
                                                                class="btn btn-primary waves-effect waves-light"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#modalFormTambahSubToeflPreparation">Tambah
                                                                Sub MT Exclusive</button>
                                                            </h6>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <table class="table" id="tb_sub_toefl_preparation"
                                                        style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <!-- <th>No Urut</th> -->
                                                                <th>Nama</th>
                                                                <th>Deskripsi</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>




                                                </div>
                                            </div><!-- .card-preview -->
                                        </div> <!-- nk-block -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalFormTambahSubToeflPreparation">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah MT Exclusive</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="formTambahSubToeflPreparation"
                    id="formTambahSubToeflPreparation">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="m_toefl_preparation_id" name="m_toefl_preparation_id"
                        value="{{ $m_toefl_preparation->id }}">
                    <div class="form-group">
                        <label class="form-label" for="full-name">Nama MT Exclusive</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="nama_sub_toefl_preparation"
                                name="nama_sub_toefl_preparation" required>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
                <button type="button" id="tambah" namme="tambah" class="btn btn-md btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalFormEditSubToeflPreparation">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit MT Exclusive</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="FormEditSubToeflPreparation"
                    id="FormEditSubToeflPreparation">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="m_sub_toefl_preparation_id_edit"
                        name="m_sub_toefl_preparation_id_edit" required>
                    <div class="form-group">
                        <label class="form-label" for="full-name">Nama</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="nama_sub_toefl_preparation_edit"
                                name="nama_sub_toefl_preparation_edit" required>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
                <button type="button" id="update" namme="update" class="btn btn-md btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalFormLatihan">
    <div class="modal-dialog modal-dialog-top modal-xl" role="document"
        style="max-width: 95%; margin-bottom: -10px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Latihan MT Exclusive</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                {{ csrf_field() }}
                <input type="hidden" class="form-control" id="m_sub_toefl_preparation_id_latihan"
                    name="m_sub_toefl_preparation_id_latihan" required>
                <div class="row">
                    <div class="col-xl-6">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <form action="#" class="form-validate is-alter"
                                        name="FormTambahEditLatihanMateri" id="FormTambahEditLatihanMateri">
                                        {{ csrf_field() }}
                                        <label class="form-label" for="full-name">Latihan Materi (Url Video)</label>
                                        <div class="form-control-wrap">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control"
                                                    placeholder="Latihan Materi (Url Video)"
                                                    aria-label="Latihan Materi (Url Video)"
                                                    aria-describedby="button-addon2" id="latihan_materi_url_video"
                                                    name="latihan_materi_url_video">
                                                <button type="button" id="simpan_latihan_materi"
                                                    name="simpan_latihan_materi"
                                                    class="btn btn-outline btn-success">Simpan Video</button>
                                                <button type="button" onclick="Materi_latihan()"
                                                    class="btn btn-outline btn-info">Tinjau Video</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <form action="#" class="form-validate is-alter"
                                        name="FormTambahEditPembahasanMateri" id="FormTambahEditPembahasanMateri">
                                        <label class="form-label" for="full-name">Pembahasan Materi (Url
                                            Video)</label>
                                        <div class="form-control-wrap">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control"
                                                    placeholder="Pembahasan Materi (Url Video)"
                                                    aria-label="Pembahasan Materi (Url Video)"
                                                    aria-describedby="button-addon2" id="pembahasan_materi_url_video"
                                                    name="pembahasan_materi_url_video">
                                                <button type="button" id="simpan_pembahasan_materi"
                                                    name="simpan_pembahasan_materi"
                                                    class="btn btn-outline btn-success">Simpan Video</button>
                                                <button type="button" onclick="Pembahasan_latihan()"
                                                    class="btn btn-outline btn-info">Tinjau Video</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="full-name">Tinjau Video</label>
                                    <div class="form-control-wrap">
                                        <div id="append_html_tinjau">
                                            <!-- conten -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ================================================================html soal reading====================================== -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <form action="#" class="form-validate is-alter" name="FormTambahLatihanSoalReading"
                                id="FormTambahLatihanSoalReading">
                                {{ csrf_field() }}
                                <label class="form-label" for="full-name">Bacaan Soal Text</label>
                                <div class="form-control-wrap">
                                    <div name="soal_reading" id="soal_reading" class="soal_reading mb-2"
                                        style="height: 900px; width: 100%;">
                                    </div>
                                    <div class="d-grid gap-2 d-md-flex justify-content-md-end"
                                        style="margin-bottom: -14px">
                                        <button type="button" id="simpan_latihan_soal_reading"
                                            namme="simpan_latihan_soal_reading"
                                            class="btn btn-md btn-primary mb-4">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <form action="#" class="form-validate is-alter" name="FormTambahEditLatihanSoal"
                        id="FormTambahEditLatihanSoal">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="form-label mt-2">Kategori Soal</label>
                                <div class="form-control-wrap">
                                    <select class="form-control" name="kategori_soal" id="kategori_soal"
                                        style="width:100%;">
                                        <option value="PILIHAN_GANDA">PILIHAN GANDA</option>
                                        <option value="ESAI">ESAI</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="form-label" for="full-name">Latihan Soal</label>

                                <div class="form-control-wrap">
                                    <!-- <input type="text" class="form-control mb-1" id="soal" name="soal" required> -->
                                    <div name="soal" id="soal" class="soal mb-2"
                                        style="height: 90px; width: 100%;">
                                    </div>
                                    <div class="d-grid gap-2 d-md-flex justify-content-md-end"
                                        style="margin-bottom: -14px">
                                        <button type="button" id="simpan_latihan_soal" namme="simpan_latihan_soal"
                                            class="btn btn-md btn-primary mb-4">Simpan</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label" for="full-name">Tabel Soal</label>
                                    <div class="form-control-wrap">
                                        <table class="table" id="tb_latihan_soal" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <!-- <th>No</th> -->
                                                    <th>Soal</th>
                                                    <th>Deskripsi</th>
                                                    <th>Kategori Soal</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-light">
                <!-- <button type="button" id="update" namme="update" class="btn btn-md btn-primary">Simpan</button> -->
            </div>
        </div>
    </div>
</div>

<!-- modal edit paket -->
<div class="modal fade" id="modalFormEditPaket">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Paket</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="FormEditPaket" id="FormEditPaket">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="id_paket_edit" name="id_paket_edit" required>
                    <div class="form-group">
                        <label class="form-label" for="full-name">Nama Paket</label>
                        <div class="form-control-wrap">
                            <input type="text" class="form-control" id="nama_paket_edit" name="nama_paket_edit"
                                required>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->
                <button type="button" id="update" name="update" class="btn btn-md btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalFormUploadDokumen">
    <div class="modal-dialog modal-dialog-top modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Upload Dokumen MT Exclusive</h5>
                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                    <em class="icon ni ni-cross"></em>
                </a>
            </div>
            <div class="modal-body">
                <form action="#" class="form-validate is-alter" name="FormUploadDokumen" id="FormUploadDokumen"
                    enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" id="id_sub_toefl_preparation_dokumen"
                        name="id_sub_toefl_preparation_dokumen">
                    <div class="form-group">
                        <label class="form-label" for="full-name">Upload Dokumen</label>
                        <div class="form-control-wrap">
                            <input type="file" class="form-control" id="dokumen_toefl_preparation"
                                name="dokumen_toefl_preparation" required>
                        </div>
                        <!-- <label class="form-label mt-1" for="full-name">Deskripsi</label>
                                                                                                                                                                                                <div class="form-control-wrap">
                                                                                                                                                                                                    <textarea type="text" class="form-control" id="deskripsi_toefl_preparation" name="deskripsi_toefl_preparation"> coba </textarea>
                                                                                                                                                                                                </div> -->
                        <button type="submit" id="btn_upload_dokumen" name="btn_upload_dokumen"
                            class="btn btn-md btn-primary mt-2">Simpan</button>
                    </div>
                </form>

                <table class="table" id="tb_sub_toefl_preparation_dokumen" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <!-- <th>No Urut</th> -->
                            <th>Nama</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer bg-light">
                <!-- <span class="sub-text">Modal Footer Text</span> -->

            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<!-- ============================================javascript soal reading========================== -->
<script>
    var quill_soal_reading
    $(document).ready(function() {
        quill_soal_reading = new Quill("#soal_reading", {
            placeholder: 'Isikan soal text Kamu disini',
            theme: 'snow',
            imageResize: {
                displaySize: true
            },
            modules: {
                toolbar: [
                    [{
                        font: []
                    }, {
                        size: []
                    }],
                    ["bold", "italic", "underline", "strike"],
                    [{
                        color: []
                    }, {
                        background: []
                    }],
                    [{
                        script: "super"
                    }, {
                        script: "sub"
                    }],
                    [{
                        header: [!1, 1, 2, 3, 4, 5, 6]
                    }, "blockquote", "code-block"],
                    [{
                        list: "ordered"
                    }, {
                        list: "bullet"
                    }, {
                        indent: "-1"
                    }, {
                        indent: "+1"
                    }],
                    ["direction", {
                        align: []
                    }],
                    // ['link', 'image', 'video', 'formula'],
                ]
            }
        })

    });
</script>

<script type="text/javascript">
    // var table_riwayat_kenaikan_pangkat = ;
    $(document).ready(function() {
        $.fn.modal.Constructor.prototype._initializeFocusTrap = function() {
            return {
                activate: function() {},
                deactivate: function() {}
            }
        };
        // NioApp.DataTable.init = function() {
        // NioApp.DataTable('#tb_user', {

        var tb_user = $('#tb_sub_toefl_preparation').DataTable({
            responsive: {
                details: true
            },
            processing: true,
            serverSide: true,
            searching: true,
            responsive: {
                details: true
            },
            sDom: 'lrtip', // untuk hidden search box di datatable
            // language: {
            // 	processing: "Sedang diproses...<img src='{{ asset('public/assets/images/load.gif') }}'>"
            // },
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('sub_toefl_preparation/getDataSubToeflPreparation') }}" + '/' +
                    "{{ $toefl_preparation_id }}",
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center'
                },
                {
                    data: 'nama_sub_toefl_preparation',
                    name: 'nama_sub_toefl_preparation',
                    className: 'text-center'
                },
                {
                    data: 'deskripsi',
                    name: 'deskripsi',
                    className: 'text-center'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    className: 'text-center'
                },

            ]
        })
        // };
    });
</script>

<script>
    $(document).on('click', '#tambah', function() {
        console.log('tambah');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#formTambahSubToeflPreparation').serialize(),
            url: "{{ url('sub_toefl_preparation/tambah') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    // document.location = "{{ url('/home/index') }}";
                    $("#modalFormTambahSubToeflPreparation").modal('hide');
                    $('#tb_sub_toefl_preparation').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>
<!-- ====================================tampil soal reading======================== -->
<script>
    $(document).on('click', '#latihan', function() {
        var sub_latihan_id = $(this).attr("data-m_sub_toefl_preparation_id_latihan");
        console.log("id_sub : " + sub_latihan_id);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('latihan/editLatihanSoalReading') }}" + "/" + sub_latihan_id,
            type: "GET",
            dataType: 'json',
            success: function(data) {
                // var data_statis = '<p>PARAGRAF <span style="color: rgb(181, 206, 168);">1</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p><p>PARAGRAF <span style="color: rgb(181, 206, 168);">2</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p><p>PARAGRAF <span style="color: rgb(181, 206, 168);">3</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p><p>PARAGRAF <span style="color: rgb(181, 206, 168);">4</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p><p>PARAGRAF <span style="color: rgb(181, 206, 168);">5</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p><p>PARAGRAF <span style="color: rgb(181, 206, 168);">6</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p><p>PARAGRAF <span style="color: rgb(181, 206, 168);">7</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p><p>PARAGRAF <span style="color: rgb(181, 206, 168);">8</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p><p>PARAGRAF <span style="color: rgb(181, 206, 168);">9</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p><p>PARAGRAF <span style="color: rgb(181, 206, 168);">10</span></p><p>Suatu hari, sebuah keluarga kecil tinggal di dalam hutan. Keluarga kecil itu terdiri dari seorang ayah dan <span style="color: rgb(181, 206, 168);">2</span> anak laki-lakinya yang masih kecil.</p><p>Sang ayah sangat suka bercerita tentang batu peta yang dulunya pernah dituliskan oleh istrinya dan disembunyikan di dalam hutan.</p><p>Batu peta tersebut memiliki cerita menarik, apalagi petanya mengandung informasi tempat sang ibu disemayamkan sekaligus warisan emas.</p><p>Sang ayah berharap suatu hari nanti, kedua anaknya dapat menemukan sendiri informasi itu tanpa diberitahu.</p><p>Cerita tentang batu peta sudah sangat sering diberikan oleh sang ayah sampai membuat kedua anak laki-laki itu penasaran.</p><p>Akhirnya kedua anak laki-laki itu memutuskan melakukan perjalanan untuk menemukan batu peta yang disembunyikan oleh ibunya di hutan tersebut.</p><p>Perjalanan mencari batu peta tidak mudah, kedua anak tersebut harus berjalan menuju berbagai arah karena tidak ada petunjuk awal tentang lokasi batu peta.</p><p>Perjalanan terus dilakukan menuju ke barat, sampai bekal makan dan minum mereka habis.</p><p>Perjalanan ke arah barat akhirnya membuahkan hasil. Mereka menemukan batu peta yang menunjukkan informasi keberadaan makam ibunya dan warisan emas.</p><p>Mereka terkejut karena ternyata lokasi makam dan warisan tersebut adalah di belakang rumah mereka sendiri.</p>';
                var datas = data.m_sub_toefl_preparation_soal_prepro;
                quill_soal_reading.root.innerHTML = datas;
                console.log(datas);
            },
            error: function(data) {
                quill_soal_reading.root.innerHTML = "";
            }

        });
    });
</script>

<script>
    $(document).on('click', '#editsoallatihan', function() {
        console.log('edit');
        var soal_id = $(this).attr("data-soal_id");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('latihan/editLatihanSoal') }}" + "/" + soal_id,
            type: "GET",
            dataType: 'json',
            success: function(data) {

                var html_jawaban =
                    '<form action="#" class="form-validate is-alter" name="FormUpdateLatihanSoal" id="FormUpdateLatihanSoal">' +
                    '{{ csrf_field() }}' +
                    // '<select name="edit_kategori_soal" id="edit_kategori_soal" style="width:100%;">' +
                    // '<option value="PILIHAN_GANDA"> PILIHAN GANDA </option>' +
                    // '<option value="ESAI"> ESAI </option> ' +
                    // '</select>' +
                    '<div name="edit_soal_quill" id="edit_soal_quill" class="edit_soal_quill" style="height: 412px; max-height: 412px;"></div>' +
                    '</form>';

                console.log(data);

                Swal.fire({
                    title: 'Edit Soal',
                    // input: input,
                    html: true,
                    html: html_jawaban,
                    customClass: 'swal-wide-850',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCancelButton: true,
                    showConfirmButton: true,
                    confirmButtonText: 'Update',
                }).then(function(result) {
                    if (result.value) {

                        console.log('Update Soal');
                        var edit_soal_quill = $('#edit_soal_quill').html().replaceAll(
                            'type="text"', 'type="hidden"');
                        var edit_kategori_soal = $('#edit_kategori_soal').val();
                        var formData = $('#FormUpdateLatihanSoal').serialize() +
                            "&edit_soal_quill=" + edit_soal_quill + "&soal_id=" + soal_id +
                            "&edit_kategori_soal=" + edit_kategori_soal;

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                    'content')
                            },
                            data: formData,
                            url: "{{ url('latihan/updateLatihanSoal') }}",
                            type: "POST",
                            dataType: 'json',
                            success: function(data) {
                                console.log(data);
                                console.log(data.kode);
                                if (data.kode == 201) {
                                    toastr.clear();
                                    toastr.success(data.success);
                                    $('#tb_latihan_soal').DataTable().ajax
                                        .reload(null, false);
                                } else {
                                    toastr.clear();
                                    toastr.error(data.success);
                                }
                            },
                            error: function(data) {
                                console.log('Error:', data);
                            }
                        });
                    }
                });

                $("#edit_soal_quill").html(data.data.soal);
                var edit_soal_quill = new Quill("#edit_soal_quill", {
                    theme: "snow",
                    modules: {
                        toolbar: [
                            [{
                                font: ['', 'times-new-roman', 'arial']
                            }, {
                                size: []
                            }],
                            ["bold", "italic", "underline", "strike"],
                            [{
                                color: []
                            }, {
                                background: []
                            }],
                            [{
                                script: "super"
                            }, {
                                script: "sub"
                            }],
                            [{
                                header: [!1, 1, 2, 3, 4, 5, 6]
                            }, "blockquote", "code-block"],
                            [{
                                list: "ordered"
                            }, {
                                list: "bullet"
                            }, {
                                indent: "-1"
                            }, {
                                indent: "+1"
                            }],
                            ["direction", {
                                align: []
                            }],
                            // ["link", "image", "video"],
                            ["clean"]
                        ]
                    }
                });
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $(document).on('click', '#edit_jawaban', function() {
        var update_latihan_jawaban_id = $(this).attr("data-jawaban_id");
        var update_latihan_jawaban = $(this).attr("data-jawaban");
        var html_jawaban =
            '<form action="#" class="form-validate is-alter" name="FormUpdateLatihanJawaban" id="FormUpdateLatihanJawaban">' +
            '{{ csrf_field() }}' +
            '<input type="hidden" name="update_latihan_jawaban_id" id="update_latihan_jawaban_id" value="' +
            update_latihan_jawaban_id + '" class="form-control">' +
            '<input type="text" name="update_latihan_jawaban" id="update_latihan_jawaban" value="' +
            update_latihan_jawaban + '" class="form-control">' +
            '</form>';

        Swal.fire({
            title: 'Edit Jawaban',
            // input: input,
            html: true,
            html: html_jawaban,
            customClass: 'swal-wide-850',
            allowOutsideClick: false,
            allowEscapeKey: false,
            showCancelButton: true,
            showConfirmButton: true,
            confirmButtonText: 'Update',
        }).then(function(result) {
            if (result.value) {

                console.log('Update Jawaban');
                var formData = $('#FormUpdateLatihanJawaban').serialize();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: formData,
                    url: "{{ url('latihan/updateLatihanJawaban') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        console.log(data.kode);
                        if (data.kode == 201) {
                            toastr.clear();
                            toastr.success(data.success);
                            $('#tb_jawaban').DataTable().ajax.reload(null, false);
                        } else {
                            toastr.clear();
                            toastr.error(data.success);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    });
</script>
<!-- ================================simpan soal reading========================= -->
<script>
    $(document).on('click', '#simpan_latihan_soal_reading', function() {
        var m_sub_toefl_preparation_id_latihan = $('#m_sub_toefl_preparation_id_latihan').val();

        $("#m_sub_toefl_preparation_id_latihan").val(m_sub_toefl_preparation_id_latihan);
        var soal_reading = $('#soal_reading').html().replaceAll('type="text"', 'type="hidden"');
        var formData = $('#FormTambahLatihanSoalReading').serialize() + "&soal_reading=" + soal_reading +
            "&m_sub_toefl_preparation_id_latihan=" + m_sub_toefl_preparation_id_latihan;
        console.log(formData);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            url: "{{ url('latihan/tambahLatihanMateriReading') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    $('#tb_latihan_soal').DataTable().ajax.reload(null, false);
                    $("#modalFormLatihan").modal('hide');
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $(document).on('click', '#simpan_latihan_materi', function() {
        // console.log('simpan_edit_latihan_materi');
        var m_sub_toefl_preparation_id_latihan = $('#m_sub_toefl_preparation_id_latihan').val();

        console.log(nama_sub_toefl_preparation);
        $("#m_sub_toefl_preparation_id_latihan").val(m_sub_toefl_preparation_id_latihan);
        var formData = $('#FormTambahEditLatihanMateri').serialize() + "&m_sub_toefl_preparation_id_latihan=" +
            m_sub_toefl_preparation_id_latihan;
        // formData.append('komentar_issues_detail', komentar_issues_detail);
        console.log(formData);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            url: "{{ url('latihan/tambahLatihanMateri') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    // document.location = "{{ url('/home/index') }}";
                    // $("#modalFormTambahSubToeflPreparation").modal('hide');
                    $('#tb_latihan_soal').DataTable().ajax.reload(null, false);
                    $('#tb_sub_toefl_preparation').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    $(document).on('click', '#simpan_pembahasan_materi', function() {
        // console.log('simpan_edit_latihan_materi');
        var m_sub_toefl_preparation_id_latihan = $('#m_sub_toefl_preparation_id_latihan').val();

        console.log(nama_sub_toefl_preparation);

        $("#m_sub_toefl_preparation_id_latihan").val(m_sub_toefl_preparation_id_latihan);
        var formData = $('#FormTambahEditPembahasanMateri').serialize() +
            "&m_sub_toefl_preparation_id_latihan=" + m_sub_toefl_preparation_id_latihan;
        // formData.append('komentar_issues_detail', komentar_issues_detail);
        console.log(formData);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            url: "{{ url('latihan/tambahPembahasanMateri') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    // document.location = "{{ url('/home/index') }}";
                    // $("#modalFormTambahSubToeflPreparation").modal('hide');
                    $('#tb_latihan_soal').DataTable().ajax.reload(null, false);
                    $('#tb_sub_toefl_preparation').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    $(document).on('click', '#simpan_latihan_soal', function() {
        // console.log('simpan_edit_latihan_materi');
        var m_sub_toefl_preparation_id_latihan = $('#m_sub_toefl_preparation_id_latihan').val();
        var kategori_soal = $('#kategori_soal').val();

        console.log(nama_sub_toefl_preparation);
        $("#m_sub_toefl_preparation_id_latihan").val(m_sub_toefl_preparation_id_latihan);
        var soal = $('#soal').html().replaceAll('type="text"', 'type="hidden"');
        var formData = $('#FormTambahEditLatihanSoal').serialize() + "&soal=" + soal +
            "&m_sub_toefl_preparation_id_latihan=" + m_sub_toefl_preparation_id_latihan +
            "&kategori_soal=" + kategori_soal;
        // formData.append('komentar_issues_detail', komentar_issues_detail);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            url: "{{ url('latihan/tambahLatihanSoal') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    // document.location = "{{ url('/home/index') }}";
                    // $("#modalFormTambahSubToeflPreparation").modal('hide');
                    $('#tb_latihan_soal').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    $(document).on('click', '#simpan_jawaban', function() {
        // console.log('simpan_edit_latihan_materi');
        var latihan_soal_id_input_text = $('#latihan_soal_id_input_text').val();

        console.log(latihan_soal_id_input_text);
        // $("#m_sub_toefl_preparation_id_latihan").val(m_sub_toefl_preparation_id_latihan);
        // var soal = $('#soal').html().replaceAll('type="text"', 'type="hidden"');
        var formData = $('#formTambahJawaban').serialize() + '&latihan_soal_id_input_text=' +
            latihan_soal_id_input_text;
        console.log(formData);
        // formData.append('komentar_issues_detail', komentar_issues_detail);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            url: "{{ url('latihan/tambahJawaban') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    // document.location = "{{ url('/home/index') }}";
                    // $("#modalFormTambahSubToeflPreparation").modal('hide');
                    $('#tb_jawaban').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    $(document).on('click', '#simpan_kunci_jawaban', function() {
        // console.log('simpan_edit_latihan_materi');
        var latihan_soal_id_input_text = $('#latihan_soal_id_input_text').val();

        console.log(latihan_soal_id_input_text);
        // $("#m_sub_toefl_preparation_id_latihan").val(m_sub_toefl_preparation_id_latihan);
        // var soal = $('#soal').html().replaceAll('type="text"', 'type="hidden"');
        var jawaban_id = $(this).attr("data-jawaban_id");
        var kategori_soal = $(this).attr("data-kategori_soal");
        var formData = 'jawaban_id=' + jawaban_id + '&latihan_soal_id=' + latihan_soal_id_input_text +
            '&kategori_soal=' + kategori_soal;
        console.log(formData);
        // formData.append('komentar_issues_detail', komentar_issues_detail);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            url: "{{ url('latihan/updateKunciJawaban') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    // document.location = "{{ url('/home/index') }}";
                    // $("#modalFormTambahSubToeflPreparation").modal('hide');
                    $('#tb_jawaban').DataTable().ajax.reload(null, false);

                    $(".kunci_jawaban_append").html('');
                    $(".kunci_jawaban_append").html(data.m_latihan_jawaban_jawaban);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        // $("#edit").click(function() {
        $(document).on('click', '#edit', function() {
            console.log('coba');
            var sub_toefl_preparation_id = $(this).attr("data-sub_toefl_preparation_id");
            var nama_sub_toefl_preparation = $(this).attr("data-nama_sub_toefl_preparation");

            console.log(nama_sub_toefl_preparation);
            $("#m_sub_toefl_preparation_id_edit").val(sub_toefl_preparation_id);
            $("#nama_sub_toefl_preparation_edit").val(nama_sub_toefl_preparation);
        });
    });
</script>

<script>
    $(document).ready(function() {
        $(document).on('click', '#editlatihan', function() {
            console.log('coba');
            var sub_toefl_preparation_id = $(this).attr("data-sub_toefl_preparation_id");
            var nama_sub_toefl_preparation = $(this).attr("data-nama_sub_toefl_preparation");

            console.log(nama_sub_toefl_preparation);
            $("#m_sub_toefl_preparation_id_edit").val(sub_toefl_preparation_id);
            $("#nama_sub_toefl_preparation_edit").val(nama_sub_toefl_preparation);
        });
    });
</script>

<script>
    $(document).on('click', '#update', function() {
        console.log('update');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#FormEditSubToeflPreparation').serialize(),
            url: "{{ url('sub_toefl_preparation/update') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    // document.location = "{{ url('/home/index') }}";
                    $("#modalFormEditSubToeflPreparation").modal('hide');
                    $('#tb_sub_toefl_preparation').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    $(document).on('click', '#jawaban', function() {

        // console.log('delete');
        var latihan_soal_id = $(this).attr("data-latihan_soal_id");
        var m_latihan_jawaban_huruf = $(this).attr("data-m_latihan_jawaban_huruf");
        var m_latihan_jawaban_jawaban = $(this).attr("data-m_latihan_jawaban_jawaban");
        var m_latihan_jawaban_view = $(this).attr("data-m_latihan_kategori_soal_view");
        console.log('kategori : ' + m_latihan_jawaban_view);
        var m_latihan_kunci_jawaban = m_latihan_jawaban_jawaban;
        // $('.eg-swal-av2').on("click", function(e) {
        var html_jawaban =
            // '<div class="row">' +

            // '<div class="col-xl-4">' +
            // '<div class="card">' +
            // '<div class="card-body">' +
            // '<div class="row">' +
            // '<div class="col-lg-12">' +
            '<form action="#" class="form-validate is-alter" name="formTambahJawaban" id="formTambahJawaban">' +
            '<input type="hidden" name="latihan_soal_id_input_text" id="latihan_soal_id_input_text" value="' +
            latihan_soal_id + '" class="form-control" placeholder="Enter First Name">' +
            // '<div class="row mb-4">' +
            // '<label for="horizontal-firstname-input" ' +
            // 'class="col-sm-3" style="text-align:left">Huruf</label>' +
            // '<div class="col-sm-1" style="text-align:left">' +
            // ':' +
            // '</div>' +
            // '<div class="col-sm-8">' +
            // '<input type="huruf" name="huruf_input_text" id="huruf_input_text" class="form-control" placeholder="Enter First Name">' +
            // '</div>' +
            // '</div>' +


            '<div class="row mb-4">' +
            '<label for="horizontal-firstname-input" ' +
            'class="col-sm-3" style="text-align:left">Jawaban</label>' +
            '<div class="col-sm-1" style="text-align:left">' +
            ':' +
            '</div>' +
            '<div class="col-sm-8">' +
            '<input type="huruf" name="jawaban_input_text" id="jawaban_input_text" class="form-control" placeholder="Enter First Name">' +
            '</div>' +
            '</div>' +

            '</form>' +


            '<div class="row mb-4">' +
            // '<label for="horizontal-firstname-input" ' +
            // 'class="col-sm-3 col-form-label" style="text-align:left">First name</label>' +
            // '<div class="col-sm-1" style="text-align:left">' +
            // ':' +
            // '</div>' +
            '<div class="col-sm-12">' +
            '<button type="button" name="simpan_jawaban" id="simpan_jawaban" class="btn btn-primary btn-md float-end">Simpan</button>' +
            '</div>' +
            '</div>' +


            '<div class="row mb-4">' +
            '<div class="col-sm-12">' +
            '<table class="table" id="tb_jawaban" name="tb_jawaban" style="width: 100%;">' +
            '<thead>' +
            '<tr>' +
            '<th style="width: 10% !important;">No</th>' +
            // '<th style="width: 15% !important;">Huruf</th>' +
            '<th style="width: 35% !important;">Jawaban</th>' +
            '<th style="width: 40% !important;">Aksi</th>' +
            // '<th style="width: 20% !important;">Created By</th>' +
            // '<th style="width: 20% !important;">Created At</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody>' +
            '</tbody>' +
            '</table>' +
            '</div>' +
            '</div>' +

            '<div class="row mb-4">' +
            '<label for="horizontal-firstname-input" ' +
            'class="col-sm-3" style="text-align:left;">Kunci Jawaban</label>' +
            '<div class="col-sm-1" style="text-align:left">' +
            ':' +
            '</div>' +
            '<div class="col-sm-8" style="text-align:left">';
        if (m_latihan_jawaban_view == "PILIHAN_GANDA") {
            // console.log(m_latihan_jawaban_view);
            html_jawaban +=
                '<p class="kunci_jawaban_append"> ' + m_latihan_kunci_jawaban + ' </p>';
        } else if (m_latihan_jawaban_view == "ESAI") {
            html_jawaban +=
                '<p class="kunci_jawaban_append"> Semua</p>';
        }
        '</div>' +
        '</div>';

        console.log(html_jawaban);

        Swal.fire({
            title: 'Masukan Jawaban',
            // input: input,
            html: true,
            html: html_jawaban,
            customClass: 'swal-wide-850',
            allowOutsideClick: false,
            allowEscapeKey: false,
            showCancelButton: true,
            cancelButtonText: "Exit",
            showConfirmButton: false,
        }).then(function(result) {
            if (result.value) {


            }
        });
        // });

        $('#tb_jawaban').DataTable().destroy();

        var tb_jawaban = $('#tb_jawaban').DataTable({
            processing: true,
            serverSide: true,
            searching: true,
            scrollX: true,
            scrollY: true,
            sDom: 'lrtip', // untuk hidden search box di datatable
            autoWidth: 'false',
            bPaginate: false,
            bInfo: false,
            oLanguage: {
                sEmptyTable: "No Comment"
            },
            // language: {
            // 	processing: "Sedang diproses...<img src='{{ asset('public/assets/images/load.gif') }}'>"
            // },
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('latihan/getDataJawabanSoal') }}" + "/" + latihan_soal_id,
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center',
                    // width: "10%",
                },
                // {
                //     data: 'huruf',
                //     name: 'huruf',
                //     className: 'text-center',
                //     // width: "10%",
                // },
                {
                    data: 'jawaban',
                    name: 'jawaban',
                    className: 'text-center',
                    // width: "20px",
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    className: 'text-center',
                    // width: "20px",
                }
            ],
            drawCallback: function(settings) {

            },
            error: function(xhr, error, code) {
                // console.log(xhr, code);
                $('#tb_detail_status').DataTable().ajax.reload(null, false);
            }
        })


    });
</script>

<script>
    $(document).on('click', '#delete', function() {
        console.log('delete');
        var sub_toefl_preparation_id = $(this).attr("data-sub_toefl_preparation_id");
        console.log(sub_toefl_preparation_id);

        // $('.eg-swal-av2').on("click", function(e) {
        Swal.fire({
            title: 'Apakah Anda Yakin..?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
            if (result.value) {
                console.log(sub_toefl_preparation_id);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // data: $('#FormEditUser').serialize(),
                    url: "{{ url('sub_toefl_preparation/delete') }}" + '/' +
                        sub_toefl_preparation_id,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        console.log(data.kode);
                        if (data.kode == 201) {
                            toastr.clear();
                            toastr.success(data.success);
                            // document.location = "{{ url('/home/index') }}";
                            // $("#modalFormEditUser").modal('hide');
                            // Swal.fire('Deleted!', 'Berhasil Di Delete.', 'success');
                            $('#tb_sub_toefl_preparation').DataTable().ajax.reload(null,
                                false);
                        } else {
                            toastr.clear();
                            toastr.error(data.success);
                            // Swal.fire('Deleted!', 'Gagal Di Delete.', 'error');
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                        //$('#modalPenghargaan').modal('show');
                    }
                });

            }
        });
        // });


    });
</script>

<script>
    $(document).on('click', '#hapus_soal_jawaban', function() {
        console.log('btn hapus_soal_jawaban ');
        var hapus_soal_jawaban_id = $(this).attr("data-hapus_soal_jawaban_id");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('latihan/deleteSoalJawaban') }}" + '/' + hapus_soal_jawaban_id,
            type: "GET",
            dataType: 'json',
            success: function(data) {
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    $('#tb_latihan_soal').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });


    });
</script>

<script>
    $(document).on('click', '#hapus_jawaban', function() {
        console.log('delete');
        var jawaban_id = $(this).attr("data-jawaban_id");
        var kategori_soal = $(this).attr("data-kategori_soal");
        var latihan_soal_id_input_text = $('#latihan_soal_id_input_text').val();
        console.log(jawaban_id);

        var formData = 'kategori_soal=' + kategori_soal + '&latihan_soal_id=' + latihan_soal_id_input_text;
        console.log('formData' + formData);


        // $('.eg-swal-av2').on("click", function(e) {
        // Swal.fire({
        //     title: 'Apakah Anda Yakin..?',
        //     text: "You won't be able to revert this!",
        //     icon: 'warning',
        //     showCancelButton: true,
        //     confirmButtonText: 'Yes, delete it!'
        // }).then(function(result) {
        //     if (result.value) {
        //         console.log(jawaban_id);


        //     }
        // });
        // });

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            url: "{{ url('latihan/deleteJawaban') }}" + '/' + jawaban_id,
            type: "GET",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    // document.location = "{{ url('/home/index') }}";
                    // $("#modalFormEditUser").modal('hide');
                    // Swal.fire('Deleted!', 'Berhasil Di Delete.', 'success');
                    $('#tb_jawaban').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                    // Swal.fire('Deleted!', 'Gagal Di Delete.', 'error');
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });


    });
</script>

<script>
    $(document).ready(function() {

        var quill_soal = new Quill("#soal", {
            placeholder: 'Isikan pertanyaan soal pilihan ganda kamu disini',
            theme: "snow",
            imageResize: {
                displaySize: true
            },
            modules: {
                toolbar: [
                    [{
                        font: []
                    }, {
                        size: []
                    }],
                    ["bold", "italic", "underline", "strike"],
                    [{
                        color: []
                    }, {
                        background: []
                    }],
                    [{
                        script: "super"
                    }, {
                        script: "sub"
                    }],
                    [{
                        header: [!1, 1, 2, 3, 4, 5, 6]
                    }, "blockquote", "code-block"],
                    [{
                        list: "ordered"
                    }, {
                        list: "bullet"
                    }, {
                        indent: "-1"
                    }, {
                        indent: "+1"
                    }],
                    ["direction", {
                        align: []
                    }],
                    // ["link", "image", "video"],
                    // ["clean"],
                ]
            }
        })
    });
</script>

<script>
    $(document).ready(function() {
        $(document).on('click', '#latihan', function() {
            var m_sub_toefl_preparation_id_latihan = $(this).attr(
                "data-m_sub_toefl_preparation_id_latihan");
            var m_pembahasan_materi_url_vdeo_pembahasan_materi = $(this).attr(
                "data-m_pembahasan_materi_url_vdeo_pembahasan_materi");
            var m_latihan_materi_url_vdeo_latihan_materi = $(this).attr(
                "data-m_latihan_materi_url_vdeo_latihan_materi");
            var tb_reading_materi_text_reading = $(this).attr("data-tb_reading_materi_text_reading");

            $("#m_sub_toefl_preparation_id_latihan").val(m_sub_toefl_preparation_id_latihan);
            $("#pembahasan_materi_url_video").val(m_pembahasan_materi_url_vdeo_pembahasan_materi);
            $("#latihan_materi_url_video").val(m_latihan_materi_url_vdeo_latihan_materi);
            $(document).ready(function() {
                $('#tb_latihan_soal').DataTable().destroy();
                var tb_user = $('#tb_latihan_soal').DataTable({
                    responsive: {
                        details: true
                    },
                    processing: true,
                    serverSide: true,
                    searching: true,
                    scrollY: true,
                    responsive: {
                        details: true
                    },
                    sDom: 'lrtip', // untuk hidden search box di datatable
                    ajax: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{ url('latihan/getDataLatihanSoal') }}" + '/' +
                            m_sub_toefl_preparation_id_latihan,
                        type: 'GET',
                    },
                    columns: [
                        // {
                        //     data: 'no',
                        //     name: 'no',
                        //     className: 'text-center'
                        // },
                        {
                            data: 'soal',
                            name: 'soal',
                            className: 'text-center'
                        },
                        {
                            data: 'deskripsi',
                            name: 'deskripsi',
                            className: 'text-center'
                        },
                        {
                            data: 'kategori_soal',
                            name: 'kategori_soal',
                            className: 'text-center'
                        },
                        {
                            data: 'aksi',
                            name: 'aksi',
                            className: 'text-center'
                        },

                    ]
                })
                // };
            });

        });
    });
</script>

<script>
    function Materi_latihan() {
        // $('iframe').hide();
        $(document).ready(function() {
            var idurl = document.getElementById("latihan_materi_url_video").value;
            let getURL = idurl;
            // let newURL = getURL.replace("watch?v=", "embed/");
            // $('iframe').attr("src", getURL).show();
            // document.getElementById("video_tinjau").innerHTML = getURL;
            $("#append_html_tinjau").html(getURL);

        });
    }
</script>

<script>
    function Pembahasan_latihan() {
        // $('iframe').hide();
        $(document).ready(function() {
            var idurl = document.getElementById("pembahasan_materi_url_video").value;
            let getURL = idurl;
            // let newURL = getURL.replace("watch?v=", "embed/");
            // $('iframe').attr("src", getURL).show();
            // document.getElementById("video_tinjau").innerHTML = getURL;
            $("#append_html_tinjau").html(getURL);

        });
    }
</script>

<!-- 31-03-23 -->
<script>
    $(document).on('click', '#urut_atas', function() {
        console.log('urut atas');
        var id_toeflpreparation_urut_atas = $(this).attr("data-id_toeflpreparation_urut_atas");
        var datas = "id_toeflpreparation_urut_atas=" + id_toeflpreparation_urut_atas;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: datas,
            url: "{{ url('sub_toefl_preparation/urut_atas') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    $('#tb_sub_toefl_preparation').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
                //$('#modalPenghargaan').modal('show');
            }
        });
    });
</script>

<script>
    $(document).on('click', '#urut_bawah', function() {
        console.log('urut bawah');
        var id_toeflpreparation_urut_bawah = $(this).attr("data-id_toeflpreparation_urut_bawah");
        var datas = "id_toeflpreparation_urut_bawah=" + id_toeflpreparation_urut_bawah;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: datas,
            url: "{{ url('sub_toefl_preparation/urut_bawah') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    $('#tb_sub_toefl_preparation').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $(document).on('click', '#upload_dokumen', function() {

        var m_sub_toefl_preparation_id_latihan = $(this).attr("data-m_sub_toefl_preparation_id_latihan");
        console.log(m_sub_toefl_preparation_id_latihan);

        $("#id_sub_toefl_preparation_dokumen").val(m_sub_toefl_preparation_id_latihan);

        $('#tb_sub_toefl_preparation_dokumen').DataTable().destroy();

        var tb_sub_toefl_preparation_dokumen = $('#tb_sub_toefl_preparation_dokumen').DataTable({
            responsive: {
                details: true
            },
            processing: true,
            serverSide: true,
            searching: true,
            scrollY: true,
            responsive: {
                details: true
            },
            sDom: 'lrtip', // untuk hidden search box di datatable
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('sub_toefl_preparation/getDokumenToeflPreparation') }}" + '/' +
                    m_sub_toefl_preparation_id_latihan,
                type: 'GET',
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    className: 'text-center'
                },
                {
                    data: 'dokumen_file',
                    name: 'dokumen_file',
                    className: 'text-center'
                },
                {
                    data: 'aksi',
                    name: 'aksi',
                    className: 'text-center'
                },

            ]
        })

    });
</script>

<script>
    $('#FormUploadDokumen').submit(function(event) {
        var m_sub_toefl_preparation_id_latihan = $(this).attr("data-m_sub_toefl_preparation_id_latihan");
        console.log(m_sub_toefl_preparation_id_latihan);

        var formData = new FormData(this);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            processData: false,
            contentType: false,
            url: "{{ url('sub_toefl_preparation/tambahDokumenToeflPreparation') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data);
                console.log(data.kode);
                if (data.kode == 201) {
                    toastr.clear();
                    toastr.success(data.success);
                    $('#tb_sub_toefl_preparation_dokumen').DataTable().ajax.reload(null, false);
                } else {
                    toastr.clear();
                    toastr.error(data.success);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });

    });
</script>

<script>
    $(document).on('click', '#delete_dokumen', function() {
        console.log('delete_dokumen');
        var sub_toefl_preparation_id_dokumen = $(this).attr("data-sub_toefl_preparation_id_dokuemn");
        console.log(sub_toefl_preparation_id_dokumen);

        Swal.fire({
            title: 'Apakah Anda Yakin..?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
            if (result.value) {
                console.log(sub_toefl_preparation_id_dokumen);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ url('sub_toefl_preparation/delete_dokumen') }}" + '/' +
                        sub_toefl_preparation_id_dokumen,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        console.log(data.kode);
                        if (data.kode == 201) {
                            toastr.clear();
                            toastr.success(data.success);
                            $('#tb_sub_toefl_preparation_dokumen').DataTable().ajax.reload(
                                null,
                                false);
                        } else {
                            toastr.clear();
                            toastr.error(data.success);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });

            }
        });
        // });


    });
</script>

<script>
    $(document).on('click', '#tambahdeskripsi', function() {
        console.log('tambahdeskripsi');
        var soal_id = $(this).attr("data-soal_id");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('latihan/getDataDeskripsi') }}" + "/" + soal_id,
            type: "GET",
            dataType: 'json',
            success: function(data) {

                var html_jawaban =
                    '<form action="#" class="form-validate is-alter" name="FormTambahDeskripsi" id="FormTambahDeskripsi">' +
                    '{{ csrf_field() }}' +
                    '<div name="tambah_deskripsi_quill" id="tambah_deskripsi_quill" class="tambah_deskripsi_quill" style="height: 412px; max-height: 412px;"></div>' +
                    '</form>';

                console.log(data);

                Swal.fire({
                    title: 'Tambah Deskripsi',
                    // input: input,
                    html: true,
                    html: html_jawaban,
                    customClass: 'swal-wide-850',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCancelButton: true,
                    showConfirmButton: true,
                    confirmButtonText: 'Update',
                }).then(function(result) {
                    if (result.value) {

                        console.log('Tambah Deskripsi');
                        var tambah_deskripsi_quill = $('#tambah_deskripsi_quill').html()
                            .replaceAll(
                                'type="text"', 'type="hidden"');
                        // var formData = $('#FormTambahDeskripsi').serialize() +
                        //     "&tambah_deskripsi_quill=" + tambah_deskripsi_quill + "&soal_id=" + soal_id;
                        var formData = new FormData();
                        formData.append('tambah_deskripsi_quill', tambah_deskripsi_quill);
                        formData.append('soal_id', soal_id);

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                    'content')
                            },
                            processData: false,
                            contentType: false,
                            cache: false,
                            data: formData,
                            enctype: 'multipart/form-data',
                            url: "{{ url('latihan/tambahDeskripsi') }}",
                            type: "POST",
                            dataType: 'json',
                            success: function(data) {
                                console.log(data);
                                console.log(data.kode);
                                if (data.kode == 201) {
                                    toastr.clear();
                                    toastr.success(data.success);
                                    $('#tb_latihan_soal').DataTable().ajax
                                        .reload(null, false);
                                } else {
                                    toastr.clear();
                                    toastr.error(data.success);
                                }
                            },
                            error: function(data) {
                                console.log('Error:', data);
                            }
                        });
                    }
                });

                $("#tambah_deskripsi_quill").html(data.data.deskripsi);
                var tambah_deskripsi_quill = new Quill("#tambah_deskripsi_quill", {
                    theme: "snow",
                    modules: {
                        toolbar: [
                            [{
                                font: ['', 'times-new-roman', 'arial']
                            }, {
                                size: []
                            }],
                            ["bold", "italic", "underline", "strike"],
                            [{
                                color: []
                            }, {
                                background: []
                            }],
                            [{
                                script: "super"
                            }, {
                                script: "sub"
                            }],
                            [{
                                header: [!1, 1, 2, 3, 4, 5, 6]
                            }, "blockquote", "code-block"],
                            [{
                                list: "ordered"
                            }, {
                                list: "bullet"
                            }, {
                                indent: "-1"
                            }, {
                                indent: "+1"
                            }],
                            ["direction", {
                                align: []
                            }],
                            ["link", "image", "video"],
                            ["clean"]
                        ]
                    }
                });
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>

<script>
    $(document).on('click', '#tambah_deskripsi_sub_toefl_preparation', function() {
        console.log('tambah_deskripsi_sub_toefl_preparation');
        var sub_toefl_preparation_id = $(this).attr("data-sub_toefl_preparation_id");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('sub_toefl_preparation/getDataDeskripsi') }}" + "/" +
                sub_toefl_preparation_id,
            type: "GET",
            dataType: 'json',
            success: function(data) {

                var html_jawaban =
                    '<form action="#" class="form-validate is-alter" name="FormTambahDeskripsiSubToeflPreperation" id="FormTambahDeskripsiSubToeflPreperation">' +
                    '{{ csrf_field() }}' +
                    '<div name="tambah_deskripsi_sub_toefl_preparation_quill" id="tambah_deskripsi_sub_toefl_preparation_quill" class="tambah_deskripsi_sub_toefl_preparation_quill" style="height: 412px; max-height: 412px;"></div>' +
                    '</form>';

                console.log(data);

                Swal.fire({
                    title: 'Tambah Deskripsi',
                    // input: input,
                    html: true,
                    html: html_jawaban,
                    customClass: 'swal-wide-850',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCancelButton: true,
                    showConfirmButton: true,
                    confirmButtonText: 'Update',
                }).then(function(result) {
                    if (result.value) {

                        console.log('Tambah Deskripsi');
                        var tambah_deskripsi_sub_toefl_preparation_quill = $(
                                '#tambah_deskripsi_sub_toefl_preparation_quill').html()
                            .replaceAll(
                                'type="text"', 'type="hidden"');
                        // var formData = $('#FormTambahDeskripsi').serialize() +
                        //     "&tambah_deskripsi_quill=" + tambah_deskripsi_quill + "&soal_id=" + soal_id;
                        var formData = new FormData();
                        formData.append('tambah_deskripsi_sub_toefl_preparation_quill',
                            tambah_deskripsi_sub_toefl_preparation_quill);
                        formData.append('sub_toefl_preparation_id',
                            sub_toefl_preparation_id);

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                    'content')
                            },
                            processData: false,
                            contentType: false,
                            cache: false,
                            data: formData,
                            enctype: 'multipart/form-data',
                            url: "{{ url('sub_toefl_preparation/tambahDeskripsi') }}",
                            type: "POST",
                            dataType: 'json',
                            success: function(data) {
                                console.log(data);
                                console.log(data.kode);
                                if (data.kode == 201) {
                                    toastr.clear();
                                    toastr.success(data.success);
                                    $('#tb_sub_toefl_preparation').DataTable()
                                        .ajax
                                        .reload(null, false);
                                } else {
                                    toastr.clear();
                                    toastr.error(data.success);
                                }
                            },
                            error: function(data) {
                                console.log('Error:', data);
                            }
                        });
                    }
                });

                $("#tambah_deskripsi_sub_toefl_preparation_quill").html(data.data.deskripsi);
                var tambah_deskripsi_sub_toefl_preparation_quill = new Quill(
                    "#tambah_deskripsi_sub_toefl_preparation_quill", {
                        theme: "snow",
                        modules: {
                            toolbar: [
                                [{
                                    font: ['', 'times-new-roman', 'arial']
                                }, {
                                    size: []
                                }],
                                ["bold", "italic", "underline", "strike"],
                                [{
                                    color: []
                                }, {
                                    background: []
                                }],
                                [{
                                    script: "super"
                                }, {
                                    script: "sub"
                                }],
                                [{
                                    header: [!1, 1, 2, 3, 4, 5, 6]
                                }, "blockquote", "code-block"],
                                [{
                                    list: "ordered"
                                }, {
                                    list: "bullet"
                                }, {
                                    indent: "-1"
                                }, {
                                    indent: "+1"
                                }],
                                ["direction", {
                                    align: []
                                }],
                                ["link", "image", "video"],
                                ["clean"]
                            ]
                        }
                    });
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    });
</script>
@endsection