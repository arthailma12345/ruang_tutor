<footer class="bg-dark">
            <div class="footer-upper pt-100">
                <div class="container">
                    <div class="border-bottom pb-80">
                        <div class="footer-content">
                            <div class="footer-content-item">
                                <ul class="footer-list footer-list-white">
                                    <li><a href="https://goo.gl/maps/PnrDKqpcNeRwH2fW8" target="_blank">Jl. Puring, Kampung Inggris, Tulungrejo,
Pare, Kediri, Jawa Timur, kode pos 64212</a></li>
                                </ul>
                            </div>
                            <div class="footer-content-item">
                                <div class="footer-logo">
                                    <a href="#"><img style="width:70%;" src="{{asset('public/image/meet_tutor_logo1.png')}}" alt="logo"></a>
                                </div>
                            </div>
                            @php
                            $get_session = Session::get('user_app');
                            $get_session_username = $get_session['username'];
                            $get_session_name = $get_session['nama'];
                            @endphp
                            <div class="footer-content-item">
                                <ul class="footer-social social-list social-list-dark">
                                    <li><a href="https://linktr.ee/meettutor?utm_source=linktree_profile_share&ltsid=b3265653-a473-4777-9431-be0cc1ed26fc" target="_blank"><i><img style="width: 50%;" src="{{asset('public/image/icon/link.png')}}"></i></a></li>
                                    <li><a href="https://wa.me/6281252772530?text=Hallo..%20nama%20saya%20{{$get_session_name}}%20, Username%20{{$get_session_username}}" target="_blank"><i><img style="width: 50%;" src="{{asset('public/image/icon/whatsapp.png')}}"></i></a></li>
                                    <!-- <li><a href="#" target="_blank"><i><img style="width: 50%;" src="{{asset('public/image/icon/youtube.png')}}"></i></a></li> -->
                                    <li><a href="https://www.tiktok.com/@meettutor.official"><i><img style="width: 50%;" src="{{asset('public/image/icon/tiktok.png')}}" target="_blank"></i></a></li>
                                    <!-- <li><a href="#" target="_blank"><i><img style="width: 50%;" src="{{asset('public/image/icon/facebook.png')}}"></i></a></li> -->
                                    <li><a href="https://instagram.com/meettutorofficial"><i><img style="width: 50%;" src="{{asset('public/image/icon/instagram.png')}}" target="_blank"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-lower">
                <div class="container">
                    <div class="footer-copyright-text footer-copyright-text-white">
                        <p>Copyright @<script>document.write(new Date().getFullYear())</script> RUANGAN TUTOR &amp; Developed by <a href="https://www.instagram.com/arirprogramming_id/" target="_blank">ARIR Programming</a></p>
                    </div>
                </div>
            </div>
        </footer>