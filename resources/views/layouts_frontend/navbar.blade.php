<div class="fixed-top">
            <div class="navbar-area navbar-area-default sticky-black">
                <div class="mobile-nav">
                    <a href="#" class="mobile-brand">
                        <img style="width:20%;" src="{{asset('public/image/meet_tutor_logo1.png')}}" alt="logo" class="logo">
                    </a>
                    <div class="navbar-option">
                            @php
                            $get_session = Session::get('user_app');
                            $get_session_username = $get_session['username'];
                            @endphp
                        <h5 style="color:white ;text-shadow: 2px 2px 5px red; margin-top: 10px;">{{$get_session_username}}</h5>
                    </div>
                </div>
                <div class="main-nav">
                    <div class="container-fluid">
                        <nav class="navbar navbar-expand-md navbar-light">
                            <a class="navbar-brand" href="#">
                                <img style="width:20%;" src="{{asset('public/image/meet_tutor_logo1.png')}}" alt="logo" class="logo">
                            </a>
                            <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                                <ul class="navbar-nav mx-auto">
                                @foreach(Session::get('user_app')['menu'] as $datas => $value)
                                    <li class="nav-item">
                                        <a style="background-color:yellow; border-radius:5%" href="#" class="nav-link dropdown-toggle">{{$datas}}</a>
                                        <ul class="dropdown-menu">
                                        @php
                                            for($i = 0; $i < count($value); $i++)
                                            {
                                            @endphp
                                            <li class="nav-item" style="text-shadow:none;">
                                                <a href="{{url($value[$i]->m_sub_menu_url_sub_menu)}}" class="nav-link">{{$value[$i]->m_sub_menu_nama_sub_menu}}</a>
                                            </li>
                                            @php
                                                }
                                            @endphp
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="navbar-option">
                            <h5 style="color:white ;text-shadow: 2px 2px 5px red;">{{$get_session_username}}</h5>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>