<!DOCTYPE html>
<html lang="zxx">
<!-- ------------------------------------------------- -->
@include('layouts_frontend.head')
<!-- ------------------------------------------------- -->
<body>
        <!-- preloader -->
        <div class="preloader bg-dark">
            <div class="preloader-wrapper">
                <div class="preloader-content">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
<!-- ------------------------------------------------- -->
        <!-- navbar -->
        @include('layouts_frontend.navbar')
        <!-- .end navbar -->
<!-- ------------------------------------------------- -->
        <!-- conten -->
        @yield('content')
        <!-- .end conten -->
<!-- ------------------------------------------------- -->
        <!-- footer -->
        @include('layouts_frontend.footer')
        <!-- .end footer -->

        <!-- scroll-top -->
        <div class="scroll-top" id="scrolltop">
            <div class="scroll-top-inner">
                <span><i class="bx bx-up-arrow-alt"></i></span>
            </div>
        </div>
        <!-- .end scroll-top -->
<!-- ------------------------------------------------- -->
        @include('layouts_frontend.script')
        @yield('script')
<!-- ------------------------------------------------- -->

    </body>
</html>