<head>
    <meta charset="utf-8">
    <meta name="description" content="Abaz">
    <meta name="keywords" content="HTML,CSS,JavaScript">
    <meta name="author" content="HiBootstrap">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>RUANGAN TUTOR</title>
    <link rel="shortcut icon" href="{{asset('public/assets/images/logo1.jpg')}}">

    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/bootstrap.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/bootstrap-reboot.css')}}" type="text/css" media="all" />
    <!-- animate css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/animate.min.css')}}" type="text/css" media="all" />
    <!-- swiper css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/slick.css')}}" type="text/css" media="all" />
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/meanmenu.min.css')}}" type="text/css" media="all" />
    <!-- magnific popup css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/magnific-popup.min.css')}}" type="text/css" media="all" />
    <!-- boxicons css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/boxicons.min.css')}}" type="text/css" media="all" />
    <!-- flaticon css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/flaticon.css')}}" type="text/css" media="all" />
    <!-- flaticon css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/bookblock.css')}}" type="text/css" media="all" />
    <!-- revolution slider css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/settings.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/layers.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/navigation.css')}}" type="text/css" media="all" />
    <!-- nice select css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/nice-select.css')}}" type="text/css" media="all" />
    <!-- style css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/style.css')}}" type="text/css" media="all" />
    <!-- responsive css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/responsive.css')}}" type="text/css" media="all" />
    <!-- theme dark css -->
    <link rel="stylesheet" href="{{asset('public/assets_frontend/css/theme-dark.css')}}" type="text/css" media="all" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet"/>
    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <style>
        .inner-page-header_home {
            padding-top: 450px;
            /* padding-bottom: 100px; */
            background-position: top;
            background-size: cover;
            background-repeat: no-repeat;
            position: relative;
        }
    </style>

<style type="text/css">
        button {
            padding: 1em;
            background-color: #1560bd;
            color: #ffffff;
            border-radius: 0.2em;
            border-style: none;
            cursor: pointer;
        }
    </style>
    @yield('style')
</head>