       <!-- essential js -->
       <script src="{{asset('public/assets_frontend/js/jquery.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/bootstrap.bundle.min.js')}}"></script>

       <!-- magnific popup js -->
       <script src="{{asset('public/assets_frontend/js/jquery.magnific-popup.min.js')}}"></script>
       <!-- counter js -->

       <script src="{{asset('public/assets_frontend/js/jquery.waypoints.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/jquery.counterup.min.js')}}"></script>
       <!-- slick js -->

       <script src="{{asset('public/assets_frontend/js/slick.min.js')}}"></script>
       <!-- nice select js -->

       <script src="{{asset('public/assets_frontend/js/jquery.nice-select.min.js')}}"></script>
       <!-- bookblock js -->

       <script src="{{asset('public/assets_frontend/js/modernizr.custom.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/jquerypp.custom.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/jquery.bookblock.js')}}"></script>
       <!-- revolution slider js -->

       <script src="{{asset('public/assets_frontend/js/jquery.themepunch.revolution.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/jquery.themepunch.tools.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/revolution.addon.slicey.min.js')}}"></script>
       <!-- slider revolution extension scripts. only needed for local testing -->

       <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.actions.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.carousel.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.migration.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.navigation.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.parallax.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
       <script src="{{asset('public/assets_frontend/js/extensions/revolution.extension.video.min.js')}}"></script>
       <!-- form ajazchimp js -->

       <script src="{{asset('public/assets_frontend/js/jquery.ajaxchimp.min.js')}}"></script>
       <!-- form validator js  -->

       <script src="{{asset('public/assets_frontend/js/form-validator.min.js')}}"></script>
       <!-- contact form js -->

       <script src="{{asset('public/assets_frontend/js/contact-form-script.js')}}"></script>
       <!-- meanmenu js -->

       <script src="{{asset('public/assets_frontend/js/jquery.meanmenu.min.js')}}"></script>
       <!-- main js -->

       <script src="{{asset('public/assets_frontend/js/script.js')}}"></script>

       <!-- alert -->
       <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
       <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.8/dist/sweetalert2.all.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

       <script>
              $("#btnLogout").click(function() {
                     console.log('logout');
                     $.ajax({
                            headers: {
                                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{url('logout')}}",
                            type: "GET",
                            dataType: 'json',
                            success: function(data) {
                                   document.location = "{{ url('/') }}";
                            },
                            error: function(data) {
                                   console.log('Error:', data);
                                   //$('#modalPenghargaan').modal('show');
                                   document.location = "{{ url('/') }}";
                            }
                     });
              });
       </script>