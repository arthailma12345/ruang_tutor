<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::get('/', 'loginController@index');
Route::get('/login', 'loginController@login');
Route::get('/loginQRCode', 'loginController@loginQRCode');
Route::post('/resetPasword', 'loginController@resetPasword');

// Route::get('/getapilogin', 'pegawaiController@getapilogin');

// Route::get('/getpegawai', 'pegawaiController@getpegawai');

// Route::get('/getPegawaiTambahDatabase', 'pegawaiController@getPegawaiTambahDatabase');

// Route::get('/getPerbaruiPegawai', 'pegawaiController@getPerbaruiPegawai');

Route::group(['prefix' => 'home_surat_perjanjian'], function () {
    Route::get('/index', 'homeSuratPerjanjianController@index')->name('home_surat_perjanjian.index');
    Route::get('/surat_perjanjian', 'homeSuratPerjanjianController@surat_perjanjian');
    Route::get('/surat_perjanjian_download_pdf', 'homeSuratPerjanjianController@surat_perjanjian_download_pdf');
    Route::post('/update_tanda_tangan', 'homeSuratPerjanjianController@update_tanda_tangan');
    Route::post('/simpan_surat_perjanjian', 'homeSuratPerjanjianController@simpan_surat_perjanjian');
});

Route::group(['middleware' => 'usersessionlogin'], function () { //middleware usersessionlogin di file CheckUserSessionLogin.php
    // Route::get('/', function () {
    //     // Uses User Session Middleware
    // });
    Route::get('/logout', 'loginController@logout');
    Route::get('/logout_lwt', 'loginController@logout_lwt_menu');
    Route::get('/index_profil', 'ProfilController@index');
    Route::post('/update_email_profil', 'ProfilController@udpateemail');
    Route::post('/update_pass_profil', 'ProfilController@udpatepassowrd');

    Route::group(['prefix' => 'home'], function () {
        Route::get('/index', 'homeController@index');
        Route::get('/index_peserta', 'homePesertaController@index');
        Route::get('/getMateriIndexPesertaTT', 'homePesertaController@getMateriIndexTT');
        Route::get('/getMateriIndexPesertaTP', 'homePesertaController@getMateriIndexTP');
    });


    Route::group(['prefix' => 'user'], function () {
        Route::get('/index', 'userController@index');
        Route::get('/getDataUser', 'userController@getDataUser');
        Route::post('/tambah', 'userController@tambah');
        Route::post('/update', 'userController@update');
        Route::post('/updateKadaluwarsa', 'userController@updateKadaluwarsa');
        Route::get('/delete/{id}', 'userController@delete');
        Route::post('/delete_checkbox', 'userController@delete_checkbox');

        Route::get('/download_template_excel', 'userController@download_template_excel');
        Route::post('/import_excel_user', 'userController@import_excel_user');

        Route::get('/getDataUserCheckAll', 'userController@getDataUserCheckAll');
    });

    Route::group(['prefix' => 'profil_peserta'], function () {
        Route::get('/akun', 'profilPesertaController@index');
        Route::get('/ubahakun', 'profilPesertaController@ubahakun');
        Route::post('/update_email_profil', 'profilPesertaController@udpateemail');
        Route::post('/update_pass_profil', 'profilPesertaController@udpatepassowrd');
    });

    Route::group(['prefix' => 'role'], function () {
        Route::get('/index', 'roleController@index');
        Route::get('/getDataRole', 'roleController@getDataRole');
        Route::post('/tambah', 'roleController@tambah');
        Route::post('/update', 'roleController@update');
        Route::get('/delete/{id}', 'roleController@delete');
    });

    Route::group(['prefix' => 'menu'], function () {
        Route::get('/index', 'menuController@index');
        Route::get('/getDataMenu', 'menuController@getDataMenu');
        Route::post('/tambah', 'menuController@tambah');
        Route::post('/update', 'menuController@update');
        Route::get('/delete/{id}', 'menuController@delete');
    });

    Route::group(['prefix' => 'sub_menu'], function () {
        Route::get('/index/{menu_id}', 'subMenuController@index');
        Route::get('/getDataSubMenu/{menu_id}', 'subMenuController@getDataSubMenu');
        Route::post('/tambah', 'subMenuController@tambah');
        Route::post('/update', 'subMenuController@update');
        Route::get('/delete/{sub_menu_id}', 'subMenuController@delete');
    });

    Route::group(['prefix' => 'mapping_menu'], function () {
        Route::get('/index', 'mappingMenuController@index');
        Route::get('/getDataRoleMappingMenu', 'mappingMenuController@getDataRoleMappingMenu');
        Route::get('/getDataMappingMenu/{role_id}', 'mappingMenuController@getDataMappingMenu');
        Route::post('/update', 'mappingMenuController@update');
    });

    Route::group(['prefix' => 'study_room'], function () {
        Route::get('/index', 'teofltestController@index');
        Route::post('/tambah', 'teofltestController@tambah');
        Route::post('/tambahmateri', 'teofltestController@tambahmateri');
        Route::get('/getEditMateri/{id_materi}', 'teofltestController@getEditMateri');
        Route::get('/getDataPaket', 'teofltestController@getDataPaket');
        Route::get('/getDataMateri/{id_paket}', 'teofltestController@getDataMateri');
        Route::post('/update', 'teofltestController@update');
        Route::post('/updateMateri', 'teofltestController@updateMateri');
        Route::get('/delete/{id}', 'teofltestController@delete');
        Route::get('/deletemateri/{id}', 'teofltestController@deletemateri');
    });

    Route::group(['prefix' => 'toefl_preparation'], function () {
        Route::get('/index', 'toeflPreparationController@index');
        Route::get('/getDataToeflPreparation', 'toeflPreparationController@getDataToeflPreparation');
        Route::post('/tambah', 'toeflPreparationController@tambah');
        Route::post('/update', 'toeflPreparationController@update');
        Route::get('/delete/{id}', 'toeflPreparationController@delete');
        // 31-03-23
        Route::post('/urut_atas', 'toeflPreparationController@urut_atas');
        Route::post('/urut_bawah', 'toeflPreparationController@urut_atas');
        Route::post('/duplikatDataToeflPreparation', 'toeflPreparationController@duplikatDataToeflPreparation');
    });

    Route::group(['prefix' => 'sub_toefl_preparation'], function () {
        Route::get('/index/{toefl_preparation_id}', 'subToeflPreparationController@index');
        Route::get('/getDataSubToeflPreparation/{toefl_preparation_id}', 'subToeflPreparationController@getDataSubToeflPreparation');
        Route::post('/tambah', 'subToeflPreparationController@tambah');
        Route::post('/update', 'subToeflPreparationController@update');
        Route::get('/delete/{sub_toefl_preparation_id}', 'subToeflPreparationController@delete');
        // 31-03-23
        Route::post('/urut_atas', 'subToeflPreparationController@urut_atas');
        Route::post('/urut_bawah', 'subToeflPreparationController@urut_bawah');

        Route::post('/tambahDokumenToeflPreparation', 'subToeflPreparationController@tambahDokumenToeflPreparation');
        Route::get('/getDokumenToeflPreparation/{toefl_preparation_id}', 'subToeflPreparationController@getDokumenToeflPreparation');
        Route::get('/delete_dokumen/{sub_toefl_preparation_id_dokumen}', 'subToeflPreparationController@delete_dokumen');

        Route::get('/getDataDeskripsi/{toefl_preparation_id}', 'subToeflPreparationController@getDataDeskripsi');
        Route::post('/tambahDeskripsi', 'subToeflPreparationController@tambahDeskripsi');
    });

    Route::group(['prefix' => 'toefl_preparation_peserta'], function () {
        Route::get('/index', 'toeflPreparationPesertaController@index');

        Route::get('/getListDataToeflpreparation', 'toeflPreparationPesertaController@getListDataToeflpreparation');
    });

    Route::group(['prefix' => 'sub_toefl_preparation_peserta'], function () {
        Route::get('/index/{toefl_preparation_id}', 'subToeflPreparationPesertaController@index');

        Route::get('/getListDataSubToeflpreparation/{toefl_preparation_id}', 'subToeflPreparationPesertaController@getListDataSubToeflpreparation');
        Route::get('/getListDataLatihanMateri/{sub_toefl_preparation_id}', 'subToeflPreparationPesertaController@getListDataLatihanMateri');
        Route::get('/getListDataLatihanMateriReading/{sub_toefl_preparation_id}', 'subToeflPreparationPesertaController@getListDataLatihanMateriReading');
        Route::get('/getListDataLatihanMateriPembahasan/{sub_toefl_preparation_id}', 'subToeflPreparationPesertaController@getListDataLatihanMateriPembahasan');
        Route::get('/getListDataLatihanSoal/{sub_toefl_preparation_id}', 'subToeflPreparationPesertaController@getListDataLatihanSoal');
        Route::get('/getListDataLatihanJawaban/{latihan_soal_id}', 'subToeflPreparationPesertaController@getListDataLatihanJawaban');

        Route::post('/sub_toefl_preparation_peserta_id_sebelumnya_prev_forw', 'subToeflPreparationPesertaController@sub_toefl_preparation_peserta_id_sebelumnya_prev_forw');

        Route::get('/getListSimpanJawaban', 'subToeflPreparationPesertaController@getListSimpanJawaban');
        Route::get('/download_dokumen/{id_dokumnen}', 'subToeflPreparationPesertaController@download_dokumen');
    });

    Route::group(['prefix' => 'latihan'], function () {
        Route::get('/index/{sub_toefl_preparation_id}', 'latihanController@index');

        Route::get('/getDataLatihanMateri/{sub_toefl_preparation_id}', 'latihanController@getDataLatihanMateri');
        Route::post('/tambahLatihanMateri', 'latihanController@tambahLatihanMateri');

        Route::post('/tambahLatihanMateriReading', 'latihanController@tambahLatihanMateriReading');

        Route::get('/getDataPembahasanMateri/{sub_toefl_preparation_id}', 'latihanController@getDataPembahasanMateri');
        Route::post('/tambahPembahasanMateri', 'latihanController@tambahPembahasanMateri');

        Route::get('/getDataLatihanSoal/{sub_toefl_preparation_id}', 'latihanController@getDataLatihanSoal');
        Route::post('/tambahLatihanSoal', 'latihanController@tambahLatihanSoal');
        Route::get('/editLatihanSoal/{m_latihan_soal_id}', 'latihanController@editLatihanSoal');
        Route::get('/editLatihanSoalReading/{sub_latihan_id}', 'latihanController@editLatihanSoalReading');
        Route::post('/updateLatihanJawaban', 'latihanController@updateLatihanJawaban');
        Route::post('/updateLatihanSoal', 'latihanController@updateLatihanSoal');

        Route::get('/getDataJawabanSoal/{m_latihan_jawaban}', 'latihanController@getDataJawabanSoal');
        Route::post('/tambahJawaban', 'latihanController@tambahJawaban');
        Route::get('/deleteSoalJawaban/{hapus_soal_jawaban_id}', 'latihanController@deleteSoalJawaban');
        Route::get('/deleteJawaban/{jawaban_id}', 'latihanController@deleteJawaban');

        Route::post('/updateKunciJawaban', 'latihanController@updateKunciJawaban');

        Route::get('/getDataDeskripsi/{m_latihan_jawaban}', 'latihanController@getDataDeskripsi');
        Route::post('/tambahDeskripsi', 'latihanController@tambahDeskripsi');
    });

    Route::group(['prefix' => 'study_room_peserta'], function () {
        Route::get('/index', 'teofltestControllerPeserta@index');
        Route::get('/getDataPaket', 'teofltestControllerPeserta@getDataPaket');
        Route::get('/materiIndex/{id_paket}', 'teofltestControllerPeserta@materiIndex');
        Route::get('/getDataMateri/{id_paket}', 'teofltestControllerPeserta@getDataMateri');
    });

    Route::group(['prefix' => 'RollUserPaket'], function () {
        Route::get('/index', 'RollUserPaketController@index');
        Route::get('/getDataUser', 'RollUserPaketController@getDataUser');
        Route::get('/getDataLihatPaketTT/{id}', 'RollUserPaketController@getDataLihatPaketTT');
        Route::get('/getDataLihatPaketTP/{id}', 'RollUserPaketController@getDataLihatPaketTP');
        Route::get('/deletepakettt/{id}', 'RollUserPaketController@deletepakettt');
        Route::get('/deletepakettp/{id}', 'RollUserPaketController@deletepakettp');
        Route::post('/tambahrollpaket1', 'RollUserPaketController@tambahceklis1');
        Route::post('/tambahrollpaket2', 'RollUserPaketController@tambahceklis2');
    });
});
